﻿<#
.SYNOPSIS
   <A brief description of the script>
.DESCRIPTION
   <A detailed description of the script>
.PARAMETER <paramName>
   <Description of script parameter>
.EXAMPLE
   <An example of using the script>
#>

#TO-DO
#Le ellenorizini, hogy az ipv6 summary ki iratodik a reportban ahogy azt elvarjuk.c

#Meg irni a html reportot

#Szetszedni fuggvenyekre a tobb mint egy soros dolgokat a tyr catch finally blockbol




#region Helper Functions



#-----------------------------------------------------------------------------Helper Functions------------------------------------------------------------

function DateEcho($Var)
{
    #begin{}
    process
    {
         $TimeStamp=Get-Date -Format "dd.MM.yyyy-HH:mm:ss> "
        "$TimeStamp$Var$_"
        
    }
    #end{Write-host "bela"}

}
Function FullLineHeader($Text,$PadCharacer,$ThreeLine)
{
    
        process
        {
            if(-not $_ -eq "")
            {
                $CurrentText=" $_"
                $TextOutput= "$text$CurrentText" 
            }
            else
            {
                $TextOutput=$Text
            }
            
            $ConsoleWidth=(Get-Host).ui.rawui.BufferSize.Width
            $ConsoleWidth--
            $ConsoleWidth=([math]::Truncate($ConsoleWidth/4))*4
            write-verbose "Console: $ConsoleWidth"
            $padWidth=[int](($ConsoleWidth-$TextOutput.Length-2)/2)
            if($ThreeLine -eq $true)
            {
                
                $Pad="".PadLeft($padWidth," ")
                Write-host "".PadLeft($ConsoleWidth-1,$PadCharacer)
                Write-host "$Pad $TextOutput $Pad"
                Write-host "".PadLeft($ConsoleWidth-1,$PadCharacer)
            }
            else
            {
                $Pad="".PadLeft($padWidth,$PadCharacer)
            
                if("$Pad $TextOutput $Pad".Length -lt $ConsoleWidth)
                {
                    Write-host "$Pad $TextOutput $Pad$PadCharacer"
                }
                else
                {
                    Write-host "$Pad $TextOutput $Pad"
                }
                
                write-verbose "Text:$("$Pad $TextOutput $Pad".Length)"
            
            }
        }
    

}

function Help-ATS_General()
{
    Write-host $FullHelp
}

function Get-Verion()
{
    Write-host $CurrentVersion
}
function Get-VersionHistory()
{
    Write-host $AllVersions
}

Function Filter-InterfaceLog($logFileName, $ListOfTimes)
{
    #[CmdletBinding()]
    #param([string]$logFileName, $ListOfTimes)
    $ListOfLogs=@()
    foreach($nextItem in $ListOfTimes)
    {
        Write-Debug $nextItem.StartTime
        Write-Debug $nextItem.LastTime
        $StartTime=[datetime]::ParseExact($nextItem.StartTime,'dd.MM.yyyy-HH:mm:ss',$null)
        $LastTime=[datetime]::ParseExact($nextItem.LastTime,'dd.MM.yyyy-HH:mm:ss',$null)
        $currentLog=""
        $FullInterfaceLog=Get-Content $logFileName
        foreach($nextLine in $FullInterfaceLog)
        {
            $lineTime=[datetime]::ParseExact(($nextLine.split(">"))[0],'dd.MM.yyyy-HH:mm:ss',$null)
            if($lineTime -ge $StartTime)
            {
                if($LastTime -ge $lineTime)
                {
                    $currentLog+="`n$nextLine"   
                }
                else
                {
                    break
                }
            }
            
        }
        #$ListOfLogs[$ListOfLogs.Count]=@{"StartTime"=$nextItem["StartTime"];"LastTime"=$nextItem["LastTime"];"InterfaceLog"=$currentLog}
        $ListOfLogs+=(@{StartTime=$nextItem.StartTime;LastTime=$nextItem.LastTime;InterfaceLog=$currentLog})
    }
    return $ListOfLogs
}
Function Filter-EventLog($ListOfTimes, $ExtraMinutes)
{
    $ListOfEventLogs=@()
    foreach($nextItem in $ListOfTimes)
    {
        $StartTime=[datetime]::ParseExact($nextItem.StartTime,'dd.MM.yyyy-HH:mm:ss',$null)
        $LastTime=[datetime]::ParseExact($nextItem.LastTime,'dd.MM.yyyy-HH:mm:ss',$null)
        $StartTime=$StartTime.AddMinutes(-$ExtraMinutes)
        $LastTime=$LastTime.AddMinutes($ExtraMinutes)
        Write-Verbose "Event log Filter - Start Time in the loop: $StartTime"
        Write-Verbose "Event log Filter - Stop Time in the loop: $LastTime"
        #milyen logot kell ossze szedni???
        #wlan
        Write-Verbose "Get-EventLog -LogName System -source Microsoft-Windows-WLAN-AutoConfig -After $StartTime -Before $LastTime"
        try{($WlanAutoConfig=Get-EventLog -LogName System -source Microsoft-Windows-WLAN-AutoConfig -After $StartTime -Before $LastTime) >$null 2>&1 }catch{$WlanAutoConfig=$null}
        
        Write-Verbose "Get-WinEvent Microsoft-Windows-WLAN-AutoConfig/Operational | Where-Object {$_.TimeCreated -ge $StartTime -and $_.TimeCreated -le $LastTime}"
        try{($WlanOperational=Get-WinEvent Microsoft-Windows-WLAN-AutoConfig/Operational | Where-Object {$_.TimeCreated -ge $StartTime -and $_.TimeCreated -le $LastTime}) >$null 2>&1}catch{$WlanOperational=$null}
        #lan
        Write-Verbose "Get-WinEvent Microsoft-Windows-Wired-AutoConfig/Operational | Where-Object {$_.TimeCreated -ge $StartTime -and $_.TimeCreated -le $LastTime}"
        try{ ($wiredOperational=Get-WinEvent Microsoft-Windows-Wired-AutoConfig/Operational | Where-Object {$_.TimeCreated -ge $StartTime -and $_.TimeCreated -le $LastTime}) >$null 2>&1}catch{$wiredOperational=$null}
        #dhcp
        Write-Verbose "Get-WinEvent Microsoft-Windows-Dhcpv6-Client/Admin | Where-Object {$_.TimeCreated -ge $StartTime -and $_.TimeCreated -le $LastTime}"
        try{($DHCPv6Client=Get-WinEvent Microsoft-Windows-Dhcpv6-Client/Admin | Where-Object {$_.TimeCreated -ge $StartTime -and $_.TimeCreated -le $LastTime}) >$null 2>&1}catch{$DHCPv6Client=$null}
        Write-Verbose "Get-WinEvent Microsoft-Windows-Dhcp-Client/Admin | Where-Object {$_.TimeCreated -ge $StartTime -and $_.TimeCreated -le $LastTime}"
        try{($DHCPClient=Get-WinEvent Microsoft-Windows-Dhcp-Client/Admin | Where-Object {$_.TimeCreated -ge $StartTime -and $_.TimeCreated -le $LastTime}) >$null 2>&1}catch{$DHCPClient=$null}

        #dns
        Write-Verbose "Get-EventLog -LogName System -Source Microsoft-Windows-DNS-Client -After $StartTime -Before $LastTime"
        try{($DNSClient=Get-EventLog -LogName System -Source Microsoft-Windows-DNS-Client -After $StartTime -Before $LastTime) >$null 2>&1}catch{$DNSClient=$null}

            
        
        #$ListOfLogs[$ListOfLogs.Count]=@{"StartTime"=$nextItem["StartTime"];"LastTime"=$nextItem["LastTime"];"InterfaceLog"=$currentLog}
        $ListOfEventLogs+=(@{StartTime=$nextItem.StartTime;LastTime=$nextItem.LastTime;WlanAutoConfig=$WlanAutoConfig;WlanOperational=$WlanOperational;WiredOperational=$wiredOperational;DHCPv6Client=$DHCPv6Client;DHCPClient=$DHCPClient;DNSClient=$DNSClient})
    }
    return $ListOfEventLogs
}
Function Save-EventLogs($startTime,$StopTime,$OutPutPath)
{
    
    $startTimeString=Get-date $startTime -Format "yyyy-MM-ddTHH:mm:ss"
    Write-Verbose "Save Eventlogs function - startTimeString:$startTimeString"
    
    $stopTimeString=Get-date $StopTime -Format "yyyy-MM-ddTHH:mm:ss"
    Write-Verbose "Save Eventlogs function - stopTimeString:$stopTimeString"
    wevtutil epl Microsoft-Windows-WLAN-AutoConfig/Operational "$OutPutPath.WlanOperational.evtx" /q:"*[System[TimeCreated[@SystemTime>='$($startTimeString)' and @SystemTime<'$($stopTimeString)']]]" 
    wevtutil epl System  "$OutPutPath.WlanAutoConfig.evtx" /q:"*[System[Provider[@Name='Microsoft-Windows-WLAN-AutoConfig'] and TimeCreated[@SystemTime>='$($startTimeString)' and @SystemTime<='$($stopTimeString)']]]"     
    wevtutil epl Microsoft-Windows-Wired-AutoConfig/Operational "$OutPutPath.WiredOperational.evtx" /q:"*[System[TimeCreated[@SystemTime>='$($startTimeString)' and @SystemTime<'$($stopTimeString)']]]" 
    wevtutil epl Microsoft-Windows-Dhcpv6-Client/Admin "$OutPutPath.DHCPv6Client.evtx" /q:"*[System[TimeCreated[@SystemTime>='$($startTimeString)' and @SystemTime<'$($stopTimeString)']]]" 
    wevtutil epl Microsoft-Windows-Dhcp-Client/Admin "$OutPutPath.DHCPClient.evtx" /q:"*[System[TimeCreated[@SystemTime>='$($startTimeString)' and @SystemTime<'$($stopTimeString)']]]" 
    wevtutil epl System  "$OutPutPath.DNSClient.evtx" /q:"*[System[Provider[@Name='Microsoft-Windows-DNS-Client'] and TimeCreated[@SystemTime>='$($startTimeString)' and @SystemTime<='$($stopTimeString)']]]" 
    



    #wevtutil epl System  "$OutPutPath.WiredAutoConfig.evtx" /q:"*[System[Provider[@Name='Microsoft-Windows-Wired-AutoConfig'] and TimeCreated[@SystemTime>='$($startTimeString)' and @SystemTime<='$($stopTimeString)']]]" 
    #ez nem a jot adja visza: wevtutil epl System  "$OutPutPath.DHCPv6Client.evtx" /q:"*[System[Provider[@Name='Microsoft-Windows-DHCPv6-Client'] and TimeCreated[@SystemTime>='$($startTimeString)' and @SystemTime<='$($stopTimeString)']]]" 
    #wevtutil epl System  "$OutPutPath.DHCPClient.evtx" /q:"*[System[Provider[@Name='Microsoft-Windows-DHCP-Client'] and TimeCreated[@SystemTime>='$($startTimeString)' and @SystemTime<='$($stopTimeString)']]]" 
}

Function Show-PingSummary($CurrentPing, $Current6Ping, $IPv6)
{
    FullLineHeader "Summary" "-" $true
    Write-host "`n$($CurrentPing.Summary)`n"
    if($IPv6)
    {
        Write-host "`nPing to V6 target:"
        Write-host $Current6Ping.Summary
    }
}
Function Show-Disconnections($CurrentPing)
{
    FullLineHeader "Disconnections - Count:$($CurrentPing.ListOfDisconnections.Count)" "#" 
    if ($CurrentPing.ListOfDisconnections.Count -gt 0)
    {
        Write-host ""
        $DiscoCounter=0
        foreach($nextDisco in $CurrentPing.ListOfDisconnections)
        {
            Write-host "Nr: $DiscoCounter > Started:$($nextDisco.StartTime) Connection Resumed:$($nextDisco.LastTime) Missed ping:$($nextDisco.Count)"
            $DiscoCounter++
        }
        Write-host ""
    }    
}
Function Show-PingDisconnections($CurrentPing)
{
    FullLineHeader "Ping logs of Disconnections - Count:$($CurrentPing.ListOfDisconnections.Count)" "-" 
    if ($CurrentPing.ListOfDisconnections.Count -gt 0)
    {
        Write-host ""
        $DiscoCounter=0
        foreach($nextDisco in $CurrentPing.ListOfDisconnections)
        {
            Write-host "Nr: $DiscoCounter > Started:$($nextDisco.StartTime) ConnectionResumed:$($nextDisco.LastTime) Missed ping:$($nextDisco.Count)"
            Write-host "`n$($nextDisco.LogMessage)`n"
            $DiscoCounter++
        }
        Write-host ""
    }
}
Function Show-InterfaceDisconnections($filteredInterfaceLogs)
{
    FullLineHeader "Interface logs of Disconnections - Count: $($filteredInterfaceLogs.Count)" "-" 
    if ($filteredInterfaceLogs.Count -gt 0)
    {
        $DiscoCounter=0
        Write-host ""
        foreach($nextInterfaceLog in $filteredInterfaceLogs)
        {
            Write-host "Nr: $DiscoCounter > Started: $($nextInterfaceLog.StartTime) Connection resumed: $($nextInterfaceLog.LastTime)`n$($nextInterfaceLog.InterfaceLog)`n"
            $DiscoCounter++
        }
        Write-host ""
    }
}

Function Show-DisconnectionsEvents($filteredEvetLogs)
{
    FullLineHeader "Eventlogs of Disconnections - Count $($filteredEvetLogs.Count)" "-" 
    if($filteredEvetLogs.Count -gt 0)
    {
        foreach ($nextEventLog in $filteredEvetLogs)
        {
            Write-host "`n`n"
            FullLineHeader "$($nextEventLog.StartTime) - $($nextEventLog.LastTime)" "~"
            Write-host ""
            if($nextEventLog.WlanAutoConfig.Count -gt 0)
            {
                FullLineHeader "Wlan AutoConfig" " "
                #Write-host $($nextEventLog.WlanAutoConfig | format-table -Property Id, RecordID, TimeCreated, LevelDisplayName,Message | Out-String)
                Write-host $($nextEventLog.WlanAutoConfig | format-table -Property InstanceId, Index, TimeGenerated, EntryType,Message | Out-String)

            }
            if($nextEventLog.WlanOperational.Count -gt 0)
            {
                FullLineHeader "Wlan AutoOperational" " "
                Write-host $($nextEventLog.WlanOperational | format-table -Property Id, RecordID, TimeCreated, LevelDisplayName,Message | Out-String)
                
            }
            if($nextEventLog.WiredOperational -and -not $nextEventLog.WiredOperational -like "Get-WinEvent*")
            {
                FullLineHeader "Wired AutoOperational" " "
                Write-host $($nextEventLog.WiredOperational | format-table -Property Id, RecordID, TimeCreated, LevelDisplayName,Message | Out-String)
            }
            if($nextEventLog.DHCPv6Client)
            {
                FullLineHeader "DHCP v6 Client" " "
                Write-host $($nextEventLog.DHCPv6Client | format-table -Property Id, RecordID, TimeCreated, LevelDisplayName,Message | Out-String)
            }
            if($nextEventLog.DHCPClient)
            {
                FullLineHeader "DHCP Client" " "
                Write-host $($nextEventLog.DHCPClient | format-table -Property Id, RecordID, TimeCreated, LevelDisplayName,Message | Out-String)
            }
            if($nextEventLog.DNSClient)
            {
                FullLineHeader "DNS Client" " "
                #Write-host $($nextEventLog.DNSClient | format-table -Property Id, RecordID, TimeCreated, LevelDisplayName,Message | Out-String) 
                Write-host $($nextEventLog.DNSClient | format-table -Property InstanceId, Index, TimeGenerated, EntryType,Message | Out-String) 
            }
            
                
                
        }
    }

}

Function Get-DisconnectionsEvents($filteredEvetLogs)
{
    
    $var=@()
    $nextOutPut=""
    if($filteredEvetLogs.Count -gt 0)
    {
        foreach ($nextEventLog in $filteredEvetLogs)
        {
            
            
            if($nextEventLog.WlanAutoConfig.Count -gt 0)
            {
                $nextOutPut="<center>Wlan AutoConfig</center>"
                $nextOutPut+=$($nextEventLog.WlanAutoConfig | format-table -Property InstanceId, Index, TimeGenerated, EntryType,Message | Out-String)

            }
            if($nextEventLog.WlanOperational.Count -gt 0)
            {
                $nextOutPut+="<center>Wlan AutoOperational</center>"
                $nextOutPut+=$($nextEventLog.WlanOperational | format-table -Property Id, RecordID, TimeCreated, LevelDisplayName,Message | Out-String)
                
            }
            if($nextEventLog.WiredOperational -and -not $nextEventLog.WiredOperational -like "Get-WinEvent*")
            {
                $nextOutPut+="<center>Wired AutoOperational</center>"
                $nextOutPut+=$($nextEventLog.WiredOperational | format-table -Property Id, RecordID, TimeCreated, LevelDisplayName,Message | Out-String)
            }
            if($nextEventLog.DHCPv6Client)
            {
                $nextOutPut+="<center>DHCP v6 Client</center>"
                $nextOutPut+=$($nextEventLog.DHCPv6Client | format-table -Property Id, RecordID, TimeCreated, LevelDisplayName,Message | Out-String)
            }
            if($nextEventLog.DHCPClient)
            {
                $nextOutPut+="<center>DHCP Client</center>"
                $nextOutPut+=$($nextEventLog.DHCPClient | format-table -Property Id, RecordID, TimeCreated, LevelDisplayName,Message | Out-String)
            }
            if($nextEventLog.DNSClient)
            {
                $nextOutPut+="<center>DNS Client</center>"
                $nextOutPut+=$($nextEventLog.DNSClient | format-table -Property InstanceId, Index, TimeGenerated, EntryType,Message | Out-String) 
            }
            $var+=$nextOutPut
                
                
        }
    }
    return $var
}



Function Show-ErrorEvents($fullFilteredEventLogs)
{
    FullLineHeader "EventLogs - Level: Error" "#" 

    $FullWlanAutoConfigError= $fullFilteredEventLogs.WlanAutoConfig | Where-Object {$_.EntryType -eq "Information"}
    if($FullWlanAutoConfigError.Count -gt 0)
    {
        FullLineHeader "WLan Autoconfig - EventLogs" "-" 
        Write-host $($FullWlanAutoConfigError | Format-Table | Out-String)
    }
        
        
    $FullWlanOperationalError=$fullFilteredEventLogs.WlanOperational | Where-Object {$_.LevelDisplayName -eq "Error"}
    if($FullWlanOperationalError.Count -gt 0)
    {
        FullLineHeader "WLan Operational - EventLogs" "-" 
        Write-host $($FullWlanOperationalError | Format-Table | Out-String)
    }

    $FullWiredOperationalError=$fullFilteredEventLogs.WiredOperational | Where-Object {$_.LevelDisplayName -eq "Error"}
    if($FullWiredOperationalError.Count -gt 0 )
    {
        FullLineHeader "Wired Operational - EventLogs" "-" 
        Write-host $($FullWiredOperationalError | Format-Table | Out-String)
    }
        
    $FullDHCPv6ClientError=$fullFilteredEventLogs.DHCPv6Client | Where-Object {$_.LevelDisplayName -eq "Error"}
    if($FullDHCPv6ClientError.Count -gt 0)
    {
        FullLineHeader "DHCPv6 Client - EventLogs" "-" 
        Write-host $($FullDHCPv6ClientError | Format-Table | Out-String)
    }
        
        
    $FullDHCPClientError=$fullFilteredEventLogs.DHCPClient | Where-Object {$_.LevelDisplayName -eq "Error"}
    if($FullDHCPClientError.Count -gt 0)
    {
        FullLineHeader "DHCP Client - EventLogs" "-" 
        Write-host $($FullDHCPClientError | Format-Table | Out-String)
    }

        
    $FullDNSClientError=$fullFilteredEventLogs.DNSClient | Where-Object {$_.EntryType -eq "Error"}
    if($FullDNSClientError.Count -gt 0)
    {
        FullLineHeader "DNS Client - EventLogs" "-" 
        Write-host $($FullDNSClientError | Format-Table | Out-String)
    }
}


function Connect-WiFi()
{
<#
   
.DESCRIPTION
   This function can help you to connet specified wireless network.
   
       
.PARAMETER ProfileName
   You can specify the exact profile name, where you want to connect.

.EXAMPLE
   Connect-WiFi -ProfileName TP007
   
   Connection will be proceed only if profile exists.

.EXAMPLE
   Connect-WiFi -ProfileName TP007 -InterfaceName Wi-Fi

   Connection will be procced, if profile exists on the specified interface.
#>
    [CmdletBinding()]
    param()
    
    DynamicParam {
        $ParameterName="ProfileName"
        $RuntimeParameterDictionary = New-Object System.Management.Automation.RuntimeDefinedParameterDictionary
        $AttributeCollection = New-Object System.Collections.ObjectModel.Collection[System.Attribute]
        $ParameterAttribute = New-Object System.Management.Automation.ParameterAttribute
        $ParameterAttribute.Mandatory = $true
        $ParameterAttribute.Position = 1
        $AttributeCollection.Add($ParameterAttribute)
        $arrSet = Get-WifiProfiles
        $ValidateSetAttribute = New-Object System.Management.Automation.ValidateSetAttribute($arrSet)
        $AttributeCollection.Add($ValidateSetAttribute)
        $RuntimeParameter = New-Object System.Management.Automation.RuntimeDefinedParameter($ParameterName, [string], $AttributeCollection)
        $RuntimeParameterDictionary.Add($ParameterName, $RuntimeParameter)


        $ParameterNameInterface="InterfaceName"
        $AttributeCollectionInterface = New-Object System.Collections.ObjectModel.Collection[System.Attribute]
        $ParameterAttributeInterface = New-Object System.Management.Automation.ParameterAttribute
        $ParameterAttributeInterface.Mandatory = $false
        $ParameterAttributeInterface.Position = 2
        $AttributeCollectionInterface.Add($ParameterAttributeInterface)
        $arrSetInterface=get-netadapter | where-Object {$_.PhysicalMediaType -eq "Native 802.11"} | & {process{return $_.Name}}
        $ValidateSetAttributeInterface=New-Object System.Management.Automation.ValidateSetAttribute($arrSetInterface)
        $AttributeCollectionInterface.Add($ValidateSetAttributeInterface)
        $RuntimeParameterInterface = New-Object System.Management.Automation.RuntimeDefinedParameter($ParameterNameInterface, [string], $AttributeCollectionInterface)
        $RuntimeParameterDictionary.Add($ParameterNameInterface, $RuntimeParameterInterface)




        return $RuntimeParameterDictionary
    }
    begin{
        $ProfileName = $PsBoundParameters[$ParameterName]
        $SelectedInterface=$PsBoundParameters[$ParameterNameInterface]
    }
    process{
    $allProfiles=$(netsh wlan show profiles)
    $IsProfileExist=$false
    foreach($nextline in $allProfiles)
    {
        if($nextline -match "^.*Profile\s*:\s$ProfileName$")
        {
            $IsProfileExist=$true      
        }
    }
    if($IsProfileExist)
    {
        if($SelectedInterface -eq $null)
        {
            $SelectedInterface=get-netadapter  | where-Object {$_.PhysicalMediaType -eq "Native 802.11"}|Select-Object -First 1 | & {process{return $_.Name}}
        } 
        netsh wlan connect name=$ProfileName interface=$SelectedInterface   
    }
    else
    {
        Write-host "Network profile does not exists."
    }
    }
}
Function Get-WifiProfiles()
{
    $AllProfiles=$(netsh wlan show profiles)
    $ArrayProfiles=@()
    foreach($nextline in $AllProfiles)
    {
        if($nextline -match "    All User Profile     : (.*)")
        {
            $ArrayProfiles+=$Matches[1]
        }

    }
    return $ArrayProfiles
}
Function show-IperfTestrailSummary()
{
    param($IperfSummary)
    Write-Output "|||:Ch|:TCP Down|:TCP Up|:UDP Down|: UDP Down(lost %) |: UDP Up|:UDP Up(lost %)"
    foreach($nextResult in $IperfSummary)
    {
        Write-Output "|| $($nextResult.LoopCounter) | $($nextResult.TCPDown)| $($nextResult.TCPUp)| $($nextResult.UDPDown)| $($nextResult.UDPDownLostPercentage)| $($nextResult.UDPUp)| $($nextResult.UDPUpLostPercentage)"
    }

}

function Get-IperfSummary()
{
    param($Files, $NrOfTestLoops, $DumpDir)
    $AllResult=@()
    $currentLocation = Get-Location
    Set-Location $DumpDir
for($i=1;$i -le $NrOfTestLoops; $i++)
{
    $LongI="{0:d3}" -f $i
   $CurrentFiles= $Files | Where-Object {$_.Name -like "*.$LongI.*"}
   $TCPDownResult=0
   $TCPUpResult=0
   $UDPDownResult=0
   $UDPDownLostPercentage=0
   $UDPUpResult=0
   $UDPUpLostPercentage=0
   #procced TCP Down log
   $tcpDownFile=$CurrentFiles | Where-Object{$_.Name -like "*.TCP.Down.*"}
   #Verify file path
   if($tcpDownFile -ne $null)
   {
       #verify Log is completed
       $lastLine=Get-Content $tcpDownFile | select -Last 1
       if($lastLine -eq "iperf Done.")
       {
            #Extract result
            $resultLine = Get-Content $tcpDownFile | select -Last 3 | select -First 1
            if($resultLine  -match ".*[MG]Bytes\s{1,10}([0-9]{1,5}\.{0,1}[0-9]{0,3})\s")
            {
                $TCPDownResult=$Matches[1]
            }
            else
            {
                $TCPDownResult="N/A"
            }
        
       }
       else
       {
            
            $TCPDownResult="N/A"
       }    
   }
   else
   {
        $TCPDownResult="N/A"
   }










   $tcpUpFile=$CurrentFiles | Where-Object{$_.Name -like "*.TCP.UP.*"}
   #Verify file path
   if($tcpUpFile -ne $null)
   {
       #verify Log is completed
       $lastLine=Get-Content $tcpUpFile | select -Last 1
       if($lastLine -eq "iperf Done.")
       {
            #Extract result
            $resultLine = Get-Content $tcpUpFile | select -Last 3 | select -First 1
            if($resultLine  -match ".*[MG]Bytes\s{1,10}([0-9]{1,5}\.{0,1}[0-9]{0,3})\s")
            {
                $TCPUpResult=$Matches[1]
            }
            else
            {
                $TCPUpResult="N/A"
            }
        
       }
       else
       {
            
            $TCPUpResult="N/A"
       }    
   }
   else
   {
        $TCPUpResult="N/A"
   }





      #procced TCP Down log
   $udpDownFile=$CurrentFiles | Where-Object{$_.Name -like "*.UDP.Down.*"}
   #Verify file path
   if($udpDownFile -ne $null)
   {
       #verify Log is completed
       $lastLine=Get-Content $udpDownFile | select -Last 1
       if($lastLine -eq "iperf Done.")
       {

            #find summary line
            $TailofLogFile=Get-Content $udpDownFile | select -Last 10
            $resultLine=""
            for($j=0;$j -lt 10; $j++)
            {
                if($TailofLogFile[$j] -like "- - - - - - - - - - - -*")
                {
                    $resultLine=$TailofLogFile[$j+2]
                }
            }
            #Extract result
            #$resultLine = Get-Content $udpDownFile | select -Last 4 | select -First 1
            #[  5]   0.00-300.00 sec  3.49 GBytes   100 Mbits/sec  5.986 ms  20499/457771 (4.5%)
            if($resultLine  -match ".*[MG]Bytes\s{1,10}([0-9]{1,5}\.{0,1}[0-9]{0,3})\s.*\(([0-9]{1,3}\.{0,1}[0-9]{0,3}).*%\)")
            {
                $UDPDownResult=$Matches[1]
                $UDPDownLostPercentage=$Matches[2]
            }
            else
            {
                $UDPDownResult="N/A"
                $UDPDownLostPercentage="N/A"
            }
        
       }
       else
       {
            
            $UDPDownResult="N/A"
            $UDPDownLostPercentage="N/A"
       }    
   }
   else
   {
        $UDPDownResult="N/A"
        $UDPDownLostPercentage="N/A"
   }




      #procced TCP Down log
   $udpUpFile=$CurrentFiles | Where-Object{$_.Name -like "*.UDP.UP.*"}
   #Verify file path
   if($udpUpFile -ne $null)
   {
       #verify Log is completed
       $lastLine=Get-Content $udpUpFile | select -Last 1
       if($lastLine -eq "iperf Done.")
       {
            #find summary line
            $TailofLogFile=Get-Content $udpUpFile | select -Last 10
            $resultLine=""
            for($j=0;$j -lt 10; $j++)
            {
                if($TailofLogFile[$j] -like "- - - - - - - - - - - -*")
                {
                    $resultLine=$TailofLogFile[$j+2]
                }
            }
            #Extract result
            #$resultLine = Get-Content $udpUpFile | select -Last 4 | select -First 1
            if($resultLine  -match ".*[MG]Bytes\s{1,10}([0-9]{1,5}\.{0,1}[0-9]{0,3})\s.*\(([0-9]{1,3}\.{0,1}[0-9]{0,3}).*%\)")
            {
                $UDPUpResult=$Matches[1]
                $UDPUpLostPercentage=$Matches[2]
            }
            else
            {
                $UDPUpResult="N/A"
                $UDPUpLostPercentage="N/A"
            }
        
       }
       else
       {
            
            $UDPUpResult="N/A"
            $UDPUpLostPercentage="N/A"
       }    
   }
   else
   {
        $UDPUpResult="N/A"
        $UDPUpLostPercentage="N/A"
   }






   

    $AllResult+=[pscustomobject]@{LoopCounter=$i;TCPDown=$TCPDownResult;TCPUp=$TCPUpResult;UDPDown=$UDPDownResult;UDPDownLostPercentage=$UDPDownLostPercentage;UDPUp=$UDPUpResult;UDPUpLostPercentage=$UDPUpLostPercentage}
    
    
}

Set-Location $currentLocation
return $AllResult 

}

#-----------------------------------------------------------------------------Helper Functions------------------------------------------------------------
#-----------------------------------------------------------------------------HTML Report helper functions------------------------------------------------
function Generate-HtmlReport()
{
    param($OutputFile, $IPv4Statistics, $IPv6Statistics, $DisconnectionTable, $DisconnectionsDetails, $ErrorEvents, $ChartDivs, $FileList, $DataRows, $GrapInitializer, $header )
    $var=@"
<!DOCTYPE html>
<html lang="en">
<head>
  <title>$header</title>
  <meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <style type="text/css">
  /*!* Bootstrap v2.3.0** Copyright 2012 Twitter, Inc* Licensed under the Apache License v2.0* http://www.apache.org/licenses/LICENSE-2.0** Designed and built with all the love in the world @twitter by @mdo and @fat.*/.clearfix {*zoom:1}.clearfix:before, .clearfix:after {display:table;line-height:0;content:""}.clearfix:after {clear:both}.hide-text {font:0/0 a;color:transparent;text-shadow:none;background-color:transparent;border:0}.input-block-level {display:block;width:100%;min-height:30px;-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box}article, aside, details, figcaption, figure, footer, header, hgroup, nav, section {display:block}audio, canvas, video {display:inline-block;*display:inline;*zoom:1}audio:not([controls]) {display:none}html {font-size:100%;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%}a:focus {outline:thin dotted #333;outline:5px auto -webkit-focus-ring-color;outline-offset:-2px}a:hover, a:active {outline:0}sub, sup {position:relative;font-size:75%;line-height:0;vertical-align:baseline}sup {top:-0.5em}sub {bottom:-0.25em}img {width:auto\9;height:auto;max-width:100%;vertical-align:middle;border:0;-ms-interpolation-mode:bicubic}#map_canvas img, .google-maps img {max-width:none}button, input, select, textarea {margin:0;font-size:100%;vertical-align:middle}button, input {*overflow:visible;line-height:normal}button::-moz-focus-inner, input::-moz-focus-inner {padding:0;border:0}button, html input[type="button"], input[type="reset"], input[type="submit"] {cursor:pointer;-webkit-appearance:button}label, select, button, input[type="button"], input[type="reset"], input[type="submit"], input[type="radio"], input[type="checkbox"] {cursor:pointer}input[type="search"] {-webkit-box-sizing:content-box;-moz-box-sizing:content-box;box-sizing:content-box;-webkit-appearance:textfield}input[type="search"]::-webkit-search-decoration, input[type="search"]::-webkit-search-cancel-button {-webkit-appearance:none}textarea {overflow:auto;vertical-align:top}@media print {* {color:#000!important;text-shadow:none!important;background:transparent!important;box-shadow:none!important}a, a:visited {text-decoration:underline}a[href]:after {content:" (" attr(href) ")"}abbr[title]:after {content:" (" attr(title) ")"}.ir a:after, a[href^="javascript:"]:after, a[href^="#"]:after {content:""}pre, blockquote {border:1px solid #999;page-break-inside:avoid}thead {display:table-header-group}tr, img {page-break-inside:avoid}img {max-width:100%!important}@page {margin:.5cm}p, h2, h3 {orphans:3;widows:3}h2, h3 {page-break-after:avoid}}body {margin:0;font-family:"Helvetica Neue", Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;color:#333;background-color:#fff}a {color:#08c;text-decoration:none}a:hover, a:focus {color:#005580;text-decoration:underline}.img-rounded {-webkit-border-radius:6px;-moz-border-radius:6px;border-radius:6px}.img-polaroid {padding:4px;background-color:#fff;border:1px solid #ccc;border:1px solid rgba(0,0,0,0.2);-webkit-box-shadow:0 1px 3px rgba(0,0,0,0.1);-moz-box-shadow:0 1px 3px rgba(0,0,0,0.1);box-shadow:0 1px 3px rgba(0,0,0,0.1)}.img-circle {-webkit-border-radius:500px;-moz-border-radius:500px;border-radius:500px}.row {margin-left:-20px;*zoom:1}.row:before, .row:after {display:table;line-height:0;content:""}.row:after {clear:both}[class*="span"] {float:left;min-height:1px;margin-left:20px}.container, .navbar-static-top .container, .navbar-fixed-top .container, .navbar-fixed-bottom .container {width:940px}.span12 {width:940px}.span11 {width:860px}.span10 {width:780px}.span9 {width:700px}.span8 {width:620px}.span7 {width:540px}.span6 {width:460px}.span5 {width:380px}.span4 {width:300px}.span3 {width:220px}.span2 {width:140px}.span1 {width:60px}.offset12 {margin-left:980px}.offset11 {margin-left:900px}.offset10 {margin-left:820px}.offset9 {margin-left:740px}.offset8 {margin-left:660px}.offset7 {margin-left:580px}.offset6 {margin-left:500px}.offset5 {margin-left:420px}.offset4 {margin-left:340px}.offset3 {margin-left:260px}.offset2 {margin-left:180px}.offset1 {margin-left:100px}.row-fluid {width:100%;*zoom:1}.row-fluid:before, .row-fluid:after {display:table;line-height:0;content:""}.row-fluid:after {clear:both}.row-fluid [class*="span"] {display:block;float:left;width:100%;min-height:30px;margin-left:2.127659574468085%;*margin-left:2.074468085106383%;-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box}.row-fluid [class*="span"]:first-child {margin-left:0}.row-fluid .controls-row [class*="span"]+[class*="span"] {margin-left:2.127659574468085%}.row-fluid .span12 {width:100%;*width:99.94680851063829%}.row-fluid .span11 {width:91.48936170212765%;*width:91.43617021276594%}.row-fluid .span10 {width:82.97872340425532%;*width:82.92553191489361%}.row-fluid .span9 {width:74.46808510638297%;*width:74.41489361702126%}.row-fluid .span8 {width:65.95744680851064%;*width:65.90425531914893%}.row-fluid .span7 {width:57.44680851063829%;*width:57.39361702127659%}.row-fluid .span6 {width:48.93617021276595%;*width:48.88297872340425%}.row-fluid .span5 {width:40.42553191489362%;*width:40.37234042553192%}.row-fluid .span4 {width:31.914893617021278%;*width:31.861702127659576%}.row-fluid .span3 {width:23.404255319148934%;*width:23.351063829787233%}.row-fluid .span2 {width:14.893617021276595%;*width:14.840425531914894%}.row-fluid .span1 {width:6.382978723404255%;*width:6.329787234042553%}.row-fluid .offset12 {margin-left:104.25531914893617%;*margin-left:104.14893617021275%}.row-fluid .offset12:first-child {margin-left:102.12765957446808%;*margin-left:102.02127659574467%}.row-fluid .offset11 {margin-left:95.74468085106382%;*margin-left:95.6382978723404%}.row-fluid .offset11:first-child {margin-left:93.61702127659574%;*margin-left:93.51063829787232%}.row-fluid .offset10 {margin-left:87.23404255319149%;*margin-left:87.12765957446807%}.row-fluid .offset10:first-child {margin-left:85.1063829787234%;*margin-left:84.99999999999999%}.row-fluid .offset9 {margin-left:78.72340425531914%;*margin-left:78.61702127659572%}.row-fluid .offset9:first-child {margin-left:76.59574468085106%;*margin-left:76.48936170212764%}.row-fluid .offset8 {margin-left:70.2127659574468%;*margin-left:70.10638297872339%}.row-fluid .offset8:first-child {margin-left:68.08510638297872%;*margin-left:67.9787234042553%}.row-fluid .offset7 {margin-left:61.70212765957446%;*margin-left:61.59574468085106%}.row-fluid .offset7:first-child {margin-left:59.574468085106375%;*margin-left:59.46808510638297%}.row-fluid .offset6 {margin-left:53.191489361702125%;*margin-left:53.085106382978715%}.row-fluid .offset6:first-child {margin-left:51.063829787234035%;*margin-left:50.95744680851063%}.row-fluid .offset5 {margin-left:44.68085106382979%;*margin-left:44.57446808510638%}.row-fluid .offset5:first-child {margin-left:42.5531914893617%;*margin-left:42.4468085106383%}.row-fluid .offset4 {margin-left:36.170212765957444%;*margin-left:36.06382978723405%}.row-fluid .offset4:first-child {margin-left:34.04255319148936%;*margin-left:33.93617021276596%}.row-fluid .offset3 {margin-left:27.659574468085104%;*margin-left:27.5531914893617%}.row-fluid .offset3:first-child {margin-left:25.53191489361702%;*margin-left:25.425531914893618%}.row-fluid .offset2 {margin-left:19.148936170212764%;*margin-left:19.04255319148936%}.row-fluid .offset2:first-child {margin-left:17.02127659574468%;*margin-left:16.914893617021278%}.row-fluid .offset1 {margin-left:10.638297872340425%;*margin-left:10.53191489361702%}.row-fluid .offset1:first-child {margin-left:8.51063829787234%;*margin-left:8.404255319148938%}[class*="span"].hide, .row-fluid [class*="span"].hide {display:none}[class*="span"].pull-right, .row-fluid [class*="span"].pull-right {float:right}.container {margin-right:auto;margin-left:auto;*zoom:1}.container:before, .container:after {display:table;line-height:0;content:""}.container:after {clear:both}.container-fluid {padding-right:20px;padding-left:20px;*zoom:1}.container-fluid:before, .container-fluid:after {display:table;line-height:0;content:""}.container-fluid:after {clear:both}p {margin:0 0 10px}.lead {margin-bottom:20px;font-size:21px;font-weight:200;line-height:30px}small {font-size:85%}strong {font-weight:bold}em {font-style:italic}cite {font-style:normal}.muted {color:#999}a.muted:hover, a.muted:focus {color:#808080}.text-warning {color:#c09853}a.text-warning:hover, a.text-warning:focus {color:#a47e3c}.text-error {color:#b94a48}a.text-error:hover, a.text-error:focus {color:#953b39}.text-info {color:#3a87ad}a.text-info:hover, a.text-info:focus {color:#2d6987}.text-success {color:#468847}a.text-success:hover, a.text-success:focus {color:#356635}.text-left {text-align:left}.text-right {text-align:right}.text-center {text-align:center}h1, h2, h3, h4, h5, h6 {margin:10px 0;font-family:inherit;font-weight:bold;line-height:20px;color:inherit;text-rendering:optimizelegibility}h1 small, h2 small, h3 small, h4 small, h5 small, h6 small {font-weight:normal;line-height:1;color:#999}h1, h2, h3 {line-height:40px}h1 {font-size:38.5px}h2 {font-size:31.5px}h3 {font-size:24.5px}h4 {font-size:17.5px}h5 {font-size:14px}h6 {font-size:11.9px}h1 small {font-size:24.5px}h2 small {font-size:17.5px}h3 small {font-size:14px}h4 small {font-size:14px}.page-header {padding-bottom:9px;margin:20px 0 30px;border-bottom:1px solid #eee}ul, ol {padding:0;margin:0 0 10px 25px}ul ul, ul ol, ol ol, ol ul {margin-bottom:0}li {line-height:20px}ul.unstyled, ol.unstyled {margin-left:0;list-style:none}ul.inline, ol.inline {margin-left:0;list-style:none}ul.inline>li, ol.inline>li {display:inline-block;*display:inline;padding-right:5px;padding-left:5px;*zoom:1}dl {margin-bottom:20px}dt, dd {line-height:20px}dt {font-weight:bold}dd {margin-left:10px}.dl-horizontal {*zoom:1}.dl-horizontal:before, .dl-horizontal:after {display:table;line-height:0;content:""}.dl-horizontal:after {clear:both}.dl-horizontal dt {float:left;width:160px;overflow:hidden;clear:left;text-align:right;text-overflow:ellipsis;white-space:nowrap}.dl-horizontal dd {margin-left:180px}hr {margin:20px 0;border:0;border-top:1px solid #eee;border-bottom:1px solid #fff}abbr[title], abbr[data-original-title] {cursor:help;border-bottom:1px dotted #999}abbr.initialism {font-size:90%;text-transform:uppercase}blockquote {padding:0 0 0 15px;margin:0 0 20px;border-left:5px solid #eee}blockquote p {margin-bottom:0;font-size:17.5px;font-weight:300;line-height:1.25}blockquote small {display:block;line-height:20px;color:#999}blockquote small:before {content:'\2014 \00A0'}blockquote.pull-right {float:right;padding-right:15px;padding-left:0;border-right:5px solid #eee;border-left:0}blockquote.pull-right p, blockquote.pull-right small {text-align:right}blockquote.pull-right small:before {content:''}blockquote.pull-right small:after {content:'\00A0 \2014'}q:before, q:after, blockquote:before, blockquote:after {content:""}address {display:block;margin-bottom:20px;font-style:normal;line-height:20px}code, pre {padding:0 3px 2px;font-family:Monaco, Menlo, Consolas, "Courier New", monospace;font-size:12px;color:#333;-webkit-border-radius:3px;-moz-border-radius:3px;border-radius:3px}code {padding:2px 4px;color:#d14;white-space:nowrap;background-color:#f7f7f9;border:1px solid #e1e1e8}pre {display:block;padding:9.5px;margin:0 0 10px;font-size:13px;line-height:20px;word-break:break-all;word-wrap:break-word;white-space:pre;white-space:pre-wrap;background-color:#f5f5f5;border:1px solid #ccc;border:1px solid rgba(0,0,0,0.15);-webkit-border-radius:4px;-moz-border-radius:4px;border-radius:4px}pre.prettyprint {margin-bottom:20px}pre code {padding:0;color:inherit;white-space:pre;white-space:pre-wrap;background-color:transparent;border:0}.pre-scrollable {max-height:340px;overflow-y:scroll}form {margin:0 0 20px}fieldset {padding:0;margin:0;border:0}legend {display:block;width:100%;padding:0;margin-bottom:20px;font-size:21px;line-height:40px;color:#333;border:0;border-bottom:1px solid #e5e5e5}legend small {font-size:15px;color:#999}label, input, button, select, textarea {font-size:14px;font-weight:normal;line-height:20px}input, button, select, textarea {font-family:"Helvetica Neue", Helvetica, Arial, sans-serif}label {display:block;margin-bottom:5px}select, textarea, input[type="text"], input[type="password"], input[type="datetime"], input[type="datetime-local"], input[type="date"], input[type="month"], input[type="time"], input[type="week"], input[type="number"], input[type="email"], input[type="url"], input[type="search"], input[type="tel"], input[type="color"], .uneditable-input {display:inline-block;height:20px;padding:4px 6px;margin-bottom:10px;font-size:14px;line-height:20px;color:#555;vertical-align:middle;-webkit-border-radius:4px;-moz-border-radius:4px;border-radius:4px}input, textarea, .uneditable-input {width:206px}textarea {height:auto}textarea, input[type="text"], input[type="password"], input[type="datetime"], input[type="datetime-local"], input[type="date"], input[type="month"], input[type="time"], input[type="week"], input[type="number"], input[type="email"], input[type="url"], input[type="search"], input[type="tel"], input[type="color"], .uneditable-input {background-color:#fff;border:1px solid #ccc;-webkit-box-shadow:inset 0 1px 1px rgba(0,0,0,0.075);-moz-box-shadow:inset 0 1px 1px rgba(0,0,0,0.075);box-shadow:inset 0 1px 1px rgba(0,0,0,0.075);-webkit-transition:border linear .2s, box-shadow linear .2s;-moz-transition:border linear .2s, box-shadow linear .2s;-o-transition:border linear .2s, box-shadow linear .2s;transition:border linear .2s, box-shadow linear .2s}textarea:focus, input[type="text"]:focus, input[type="password"]:focus, input[type="datetime"]:focus, input[type="datetime-local"]:focus, input[type="date"]:focus, input[type="month"]:focus, input[type="time"]:focus, input[type="week"]:focus, input[type="number"]:focus, input[type="email"]:focus, input[type="url"]:focus, input[type="search"]:focus, input[type="tel"]:focus, input[type="color"]:focus, .uneditable-input:focus {border-color:rgba(82,168,236,0.8);outline:0;outline:thin dotted \9;-webkit-box-shadow:inset 0 1px 1px rgba(0,0,0,0.075), 0 0 8px rgba(82,168,236,0.6);-moz-box-shadow:inset 0 1px 1px rgba(0,0,0,0.075), 0 0 8px rgba(82,168,236,0.6);box-shadow:inset 0 1px 1px rgba(0,0,0,0.075), 0 0 8px rgba(82,168,236,0.6)}input[type="radio"], input[type="checkbox"] {margin:4px 0 0;margin-top:1px \9;*margin-top:0;line-height:normal}input[type="file"], input[type="image"], input[type="submit"], input[type="reset"], input[type="button"], input[type="radio"], input[type="checkbox"] {width:auto}select, input[type="file"] {height:30px;*margin-top:4px;line-height:30px}select {width:220px;background-color:#fff;border:1px solid #ccc}select[multiple], select[size] {height:auto}select:focus, input[type="file"]:focus, input[type="radio"]:focus, input[type="checkbox"]:focus {outline:thin dotted #333;outline:5px auto -webkit-focus-ring-color;outline-offset:-2px}.uneditable-input, .uneditable-textarea {color:#999;cursor:not-allowed;background-color:#fcfcfc;border-color:#ccc;-webkit-box-shadow:inset 0 1px 2px rgba(0,0,0,0.025);-moz-box-shadow:inset 0 1px 2px rgba(0,0,0,0.025);box-shadow:inset 0 1px 2px rgba(0,0,0,0.025)}.uneditable-input {overflow:hidden;white-space:nowrap}.uneditable-textarea {width:auto;height:auto}input:-moz-placeholder, textarea:-moz-placeholder {color:#999}input:-ms-input-placeholder, textarea:-ms-input-placeholder {color:#999}input::-webkit-input-placeholder, textarea::-webkit-input-placeholder {color:#999}.radio, .checkbox {min-height:20px;padding-left:20px}.radio input[type="radio"], .checkbox input[type="checkbox"] {float:left;margin-left:-20px}.controls>.radio:first-child, .controls>.checkbox:first-child {padding-top:5px}.radio.inline, .checkbox.inline {display:inline-block;padding-top:5px;margin-bottom:0;vertical-align:middle}.radio.inline+.radio.inline, .checkbox.inline+.checkbox.inline {margin-left:10px}.input-mini {width:60px}.input-small {width:90px}.input-medium {width:150px}.input-large {width:210px}.input-xlarge {width:270px}.input-xxlarge {width:530px}input[class*="span"], select[class*="span"], textarea[class*="span"], .uneditable-input[class*="span"], .row-fluid input[class*="span"], .row-fluid select[class*="span"], .row-fluid textarea[class*="span"], .row-fluid .uneditable-input[class*="span"] {float:none;margin-left:0}.input-append input[class*="span"], .input-append .uneditable-input[class*="span"], .input-prepend input[class*="span"], .input-prepend .uneditable-input[class*="span"], .row-fluid input[class*="span"], .row-fluid select[class*="span"], .row-fluid textarea[class*="span"], .row-fluid .uneditable-input[class*="span"], .row-fluid .input-prepend [class*="span"], .row-fluid .input-append [class*="span"] {display:inline-block}input, textarea, .uneditable-input {margin-left:0}.controls-row [class*="span"]+[class*="span"] {margin-left:20px}input.span12, textarea.span12, .uneditable-input.span12 {width:926px}input.span11, textarea.span11, .uneditable-input.span11 {width:846px}input.span10, textarea.span10, .uneditable-input.span10 {width:766px}input.span9, textarea.span9, .uneditable-input.span9 {width:686px}input.span8, textarea.span8, .uneditable-input.span8 {width:606px}input.span7, textarea.span7, .uneditable-input.span7 {width:526px}input.span6, textarea.span6, .uneditable-input.span6 {width:446px}input.span5, textarea.span5, .uneditable-input.span5 {width:366px}input.span4, textarea.span4, .uneditable-input.span4 {width:286px}input.span3, textarea.span3, .uneditable-input.span3 {width:206px}input.span2, textarea.span2, .uneditable-input.span2 {width:126px}input.span1, textarea.span1, .uneditable-input.span1 {width:46px}.controls-row {*zoom:1}.controls-row:before, .controls-row:after {display:table;line-height:0;content:""}.controls-row:after {clear:both}.controls-row [class*="span"], .row-fluid .controls-row [class*="span"] {float:left}.controls-row .checkbox[class*="span"], .controls-row .radio[class*="span"] {padding-top:5px}input[disabled], select[disabled], textarea[disabled], input[readonly], select[readonly], textarea[readonly] {cursor:not-allowed;background-color:#eee}input[type="radio"][disabled], input[type="checkbox"][disabled], input[type="radio"][readonly], input[type="checkbox"][readonly] {background-color:transparent}.control-group.warning .control-label, .control-group.warning .help-block, .control-group.warning .help-inline {color:#c09853}.control-group.warning .checkbox, .control-group.warning .radio, .control-group.warning input, .control-group.warning select, .control-group.warning textarea {color:#c09853}.control-group.warning input, .control-group.warning select, .control-group.warning textarea {border-color:#c09853;-webkit-box-shadow:inset 0 1px 1px rgba(0,0,0,0.075);-moz-box-shadow:inset 0 1px 1px rgba(0,0,0,0.075);box-shadow:inset 0 1px 1px rgba(0,0,0,0.075)}.control-group.warning input:focus, .control-group.warning select:focus, .control-group.warning textarea:focus {border-color:#a47e3c;-webkit-box-shadow:inset 0 1px 1px rgba(0,0,0,0.075), 0 0 6px #dbc59e;-moz-box-shadow:inset 0 1px 1px rgba(0,0,0,0.075), 0 0 6px #dbc59e;box-shadow:inset 0 1px 1px rgba(0,0,0,0.075), 0 0 6px #dbc59e}.control-group.warning .input-prepend .add-on, .control-group.warning .input-append .add-on {color:#c09853;background-color:#fcf8e3;border-color:#c09853}.control-group.error .control-label, .control-group.error .help-block, .control-group.error .help-inline {color:#b94a48}.control-group.error .checkbox, .control-group.error .radio, .control-group.error input, .control-group.error select, .control-group.error textarea {color:#b94a48}.control-group.error input, .control-group.error select, .control-group.error textarea {border-color:#b94a48;-webkit-box-shadow:inset 0 1px 1px rgba(0,0,0,0.075);-moz-box-shadow:inset 0 1px 1px rgba(0,0,0,0.075);box-shadow:inset 0 1px 1px rgba(0,0,0,0.075)}.control-group.error input:focus, .control-group.error select:focus, .control-group.error textarea:focus {border-color:#953b39;-webkit-box-shadow:inset 0 1px 1px rgba(0,0,0,0.075), 0 0 6px #d59392;-moz-box-shadow:inset 0 1px 1px rgba(0,0,0,0.075), 0 0 6px #d59392;box-shadow:inset 0 1px 1px rgba(0,0,0,0.075), 0 0 6px #d59392}.control-group.error .input-prepend .add-on, .control-group.error .input-append .add-on {color:#b94a48;background-color:#f2dede;border-color:#b94a48}.control-group.success .control-label, .control-group.success .help-block, .control-group.success .help-inline {color:#468847}.control-group.success .checkbox, .control-group.success .radio, .control-group.success input, .control-group.success select, .control-group.success textarea {color:#468847}.control-group.success input, .control-group.success select, .control-group.success textarea {border-color:#468847;-webkit-box-shadow:inset 0 1px 1px rgba(0,0,0,0.075);-moz-box-shadow:inset 0 1px 1px rgba(0,0,0,0.075);box-shadow:inset 0 1px 1px rgba(0,0,0,0.075)}.control-group.success input:focus, .control-group.success select:focus, .control-group.success textarea:focus {border-color:#356635;-webkit-box-shadow:inset 0 1px 1px rgba(0,0,0,0.075), 0 0 6px #7aba7b;-moz-box-shadow:inset 0 1px 1px rgba(0,0,0,0.075), 0 0 6px #7aba7b;box-shadow:inset 0 1px 1px rgba(0,0,0,0.075), 0 0 6px #7aba7b}.control-group.success .input-prepend .add-on, .control-group.success .input-append .add-on {color:#468847;background-color:#dff0d8;border-color:#468847}.control-group.info .control-label, .control-group.info .help-block, .control-group.info .help-inline {color:#3a87ad}.control-group.info .checkbox, .control-group.info .radio, .control-group.info input, .control-group.info select, .control-group.info textarea {color:#3a87ad}.control-group.info input, .control-group.info select, .control-group.info textarea {border-color:#3a87ad;-webkit-box-shadow:inset 0 1px 1px rgba(0,0,0,0.075);-moz-box-shadow:inset 0 1px 1px rgba(0,0,0,0.075);box-shadow:inset 0 1px 1px rgba(0,0,0,0.075)}.control-group.info input:focus, .control-group.info select:focus, .control-group.info textarea:focus {border-color:#2d6987;-webkit-box-shadow:inset 0 1px 1px rgba(0,0,0,0.075), 0 0 6px #7ab5d3;-moz-box-shadow:inset 0 1px 1px rgba(0,0,0,0.075), 0 0 6px #7ab5d3;box-shadow:inset 0 1px 1px rgba(0,0,0,0.075), 0 0 6px #7ab5d3}.control-group.info .input-prepend .add-on, .control-group.info .input-append .add-on {color:#3a87ad;background-color:#d9edf7;border-color:#3a87ad}input:focus:invalid, textarea:focus:invalid, select:focus:invalid {color:#b94a48;border-color:#ee5f5b}input:focus:invalid:focus, textarea:focus:invalid:focus, select:focus:invalid:focus {border-color:#e9322d;-webkit-box-shadow:0 0 6px #f8b9b7;-moz-box-shadow:0 0 6px #f8b9b7;box-shadow:0 0 6px #f8b9b7}.form-actions {padding:19px 20px 20px;margin-top:20px;margin-bottom:20px;background-color:#f5f5f5;border-top:1px solid #e5e5e5;*zoom:1}.form-actions:before, .form-actions:after {display:table;line-height:0;content:""}.form-actions:after {clear:both}.help-block, .help-inline {color:#595959}.help-block {display:block;margin-bottom:10px}.help-inline {display:inline-block;*display:inline;padding-left:5px;vertical-align:middle;*zoom:1}.input-append, .input-prepend {display:inline-block;margin-bottom:10px;font-size:0;white-space:nowrap;vertical-align:middle}.input-append input, .input-prepend input, .input-append select, .input-prepend select, .input-append .uneditable-input, .input-prepend .uneditable-input, .input-append .dropdown-menu, .input-prepend .dropdown-menu, .input-append .popover, .input-prepend .popover {font-size:14px}.input-append input, .input-prepend input, .input-append select, .input-prepend select, .input-append .uneditable-input, .input-prepend .uneditable-input {position:relative;margin-bottom:0;*margin-left:0;vertical-align:top;-webkit-border-radius:0 4px 4px 0;-moz-border-radius:0 4px 4px 0;border-radius:0 4px 4px 0}.input-append input:focus, .input-prepend input:focus, .input-append select:focus, .input-prepend select:focus, .input-append .uneditable-input:focus, .input-prepend .uneditable-input:focus {z-index:2}.input-append .add-on, .input-prepend .add-on {display:inline-block;width:auto;height:20px;min-width:16px;padding:4px 5px;font-size:14px;font-weight:normal;line-height:20px;text-align:center;text-shadow:0 1px 0 #fff;background-color:#eee;border:1px solid #ccc}.input-append .add-on, .input-prepend .add-on, .input-append .btn, .input-prepend .btn, .input-append .btn-group>.dropdown-toggle, .input-prepend .btn-group>.dropdown-toggle {vertical-align:top;-webkit-border-radius:0;-moz-border-radius:0;border-radius:0}.input-append .active, .input-prepend .active {background-color:#a9dba9;border-color:#46a546}.input-prepend .add-on, .input-prepend .btn {margin-right:-1px}.input-prepend .add-on:first-child, .input-prepend .btn:first-child {-webkit-border-radius:4px 0 0 4px;-moz-border-radius:4px 0 0 4px;border-radius:4px 0 0 4px}.input-append input, .input-append select, .input-append .uneditable-input {-webkit-border-radius:4px 0 0 4px;-moz-border-radius:4px 0 0 4px;border-radius:4px 0 0 4px}.input-append input+.btn-group .btn:last-child, .input-append select+.btn-group .btn:last-child, .input-append .uneditable-input+.btn-group .btn:last-child {-webkit-border-radius:0 4px 4px 0;-moz-border-radius:0 4px 4px 0;border-radius:0 4px 4px 0}.input-append .add-on, .input-append .btn, .input-append .btn-group {margin-left:-1px}.input-append .add-on:last-child, .input-append .btn:last-child, .input-append .btn-group:last-child>.dropdown-toggle {-webkit-border-radius:0 4px 4px 0;-moz-border-radius:0 4px 4px 0;border-radius:0 4px 4px 0}.input-prepend.input-append input, .input-prepend.input-append select, .input-prepend.input-append .uneditable-input {-webkit-border-radius:0;-moz-border-radius:0;border-radius:0}.input-prepend.input-append input+.btn-group .btn, .input-prepend.input-append select+.btn-group .btn, .input-prepend.input-append .uneditable-input+.btn-group .btn {-webkit-border-radius:0 4px 4px 0;-moz-border-radius:0 4px 4px 0;border-radius:0 4px 4px 0}.input-prepend.input-append .add-on:first-child, .input-prepend.input-append .btn:first-child {margin-right:-1px;-webkit-border-radius:4px 0 0 4px;-moz-border-radius:4px 0 0 4px;border-radius:4px 0 0 4px}.input-prepend.input-append .add-on:last-child, .input-prepend.input-append .btn:last-child {margin-left:-1px;-webkit-border-radius:0 4px 4px 0;-moz-border-radius:0 4px 4px 0;border-radius:0 4px 4px 0}.input-prepend.input-append .btn-group:first-child {margin-left:0}input.search-query {padding-right:14px;padding-right:4px \9;padding-left:14px;padding-left:4px \9;margin-bottom:0;-webkit-border-radius:15px;-moz-border-radius:15px;border-radius:15px}.form-search .input-append .search-query, .form-search .input-prepend .search-query {-webkit-border-radius:0;-moz-border-radius:0;border-radius:0}.form-search .input-append .search-query {-webkit-border-radius:14px 0 0 14px;-moz-border-radius:14px 0 0 14px;border-radius:14px 0 0 14px}.form-search .input-append .btn {-webkit-border-radius:0 14px 14px 0;-moz-border-radius:0 14px 14px 0;border-radius:0 14px 14px 0}.form-search .input-prepend .search-query {-webkit-border-radius:0 14px 14px 0;-moz-border-radius:0 14px 14px 0;border-radius:0 14px 14px 0}.form-search .input-prepend .btn {-webkit-border-radius:14px 0 0 14px;-moz-border-radius:14px 0 0 14px;border-radius:14px 0 0 14px}.form-search input, .form-inline input, .form-horizontal input, .form-search textarea, .form-inline textarea, .form-horizontal textarea, .form-search select, .form-inline select, .form-horizontal select, .form-search .help-inline, .form-inline .help-inline, .form-horizontal .help-inline, .form-search .uneditable-input, .form-inline .uneditable-input, .form-horizontal .uneditable-input, .form-search .input-prepend, .form-inline .input-prepend, .form-horizontal .input-prepend, .form-search .input-append, .form-inline .input-append, .form-horizontal .input-append {display:inline-block;*display:inline;margin-bottom:0;vertical-align:middle;*zoom:1}.form-search .hide, .form-inline .hide, .form-horizontal .hide {display:none}.form-search label, .form-inline label, .form-search .btn-group, .form-inline .btn-group {display:inline-block}.form-search .input-append, .form-inline .input-append, .form-search .input-prepend, .form-inline .input-prepend {margin-bottom:0}.form-search .radio, .form-search .checkbox, .form-inline .radio, .form-inline .checkbox {padding-left:0;margin-bottom:0;vertical-align:middle}.form-search .radio input[type="radio"], .form-search .checkbox input[type="checkbox"], .form-inline .radio input[type="radio"], .form-inline .checkbox input[type="checkbox"] {float:left;margin-right:3px;margin-left:0}.control-group {margin-bottom:10px}legend+.control-group {margin-top:20px;-webkit-margin-top-collapse:separate}.form-horizontal .control-group {margin-bottom:20px;*zoom:1}.form-horizontal .control-group:before, .form-horizontal .control-group:after {display:table;line-height:0;content:""}.form-horizontal .control-group:after {clear:both}.form-horizontal .control-label {float:left;width:160px;padding-top:5px;text-align:right}.form-horizontal .controls {*display:inline-block;*padding-left:20px;margin-left:180px;*margin-left:0}.form-horizontal .controls:first-child {*padding-left:180px}.form-horizontal .help-block {margin-bottom:0}.form-horizontal input+.help-block, .form-horizontal select+.help-block, .form-horizontal textarea+.help-block, .form-horizontal .uneditable-input+.help-block, .form-horizontal .input-prepend+.help-block, .form-horizontal .input-append+.help-block {margin-top:10px}.form-horizontal .form-actions {padding-left:180px}table {max-width:100%;background-color:transparent;border-collapse:collapse;border-spacing:0}.table {width:100%;margin-bottom:20px}.table th, .table td {padding:8px;line-height:20px;text-align:left;vertical-align:top;border-top:1px solid #ddd}.table th {font-weight:bold}.table thead th {vertical-align:bottom}.table caption+thead tr:first-child th, .table caption+thead tr:first-child td, .table colgroup+thead tr:first-child th, .table colgroup+thead tr:first-child td, .table thead:first-child tr:first-child th, .table thead:first-child tr:first-child td {border-top:0}.table tbody+tbody {border-top:2px solid #ddd}.table .table {background-color:#fff}.table-condensed th, .table-condensed td {padding:4px 5px}.table-bordered {border:1px solid #ddd;border-collapse:separate;*border-collapse:collapse;border-left:0;-webkit-border-radius:4px;-moz-border-radius:4px;border-radius:4px}.table-bordered th, .table-bordered td {border-left:1px solid #ddd}.table-bordered caption+thead tr:first-child th, .table-bordered caption+tbody tr:first-child th, .table-bordered caption+tbody tr:first-child td, .table-bordered colgroup+thead tr:first-child th, .table-bordered colgroup+tbody tr:first-child th, .table-bordered colgroup+tbody tr:first-child td, .table-bordered thead:first-child tr:first-child th, .table-bordered tbody:first-child tr:first-child th, .table-bordered tbody:first-child tr:first-child td {border-top:0}.table-bordered thead:first-child tr:first-child>th:first-child, .table-bordered tbody:first-child tr:first-child>td:first-child, .table-bordered tbody:first-child tr:first-child>th:first-child {-webkit-border-top-left-radius:4px;border-top-left-radius:4px;-moz-border-radius-topleft:4px}.table-bordered thead:first-child tr:first-child>th:last-child, .table-bordered tbody:first-child tr:first-child>td:last-child, .table-bordered tbody:first-child tr:first-child>th:last-child {-webkit-border-top-right-radius:4px;border-top-right-radius:4px;-moz-border-radius-topright:4px}.table-bordered thead:last-child tr:last-child>th:first-child, .table-bordered tbody:last-child tr:last-child>td:first-child, .table-bordered tbody:last-child tr:last-child>th:first-child, .table-bordered tfoot:last-child tr:last-child>td:first-child, .table-bordered tfoot:last-child tr:last-child>th:first-child {-webkit-border-bottom-left-radius:4px;border-bottom-left-radius:4px;-moz-border-radius-bottomleft:4px}.table-bordered thead:last-child tr:last-child>th:last-child, .table-bordered tbody:last-child tr:last-child>td:last-child, .table-bordered tbody:last-child tr:last-child>th:last-child, .table-bordered tfoot:last-child tr:last-child>td:last-child, .table-bordered tfoot:last-child tr:last-child>th:last-child {-webkit-border-bottom-right-radius:4px;border-bottom-right-radius:4px;-moz-border-radius-bottomright:4px}.table-bordered tfoot+tbody:last-child tr:last-child td:first-child {-webkit-border-bottom-left-radius:0;border-bottom-left-radius:0;-moz-border-radius-bottomleft:0}.table-bordered tfoot+tbody:last-child tr:last-child td:last-child {-webkit-border-bottom-right-radius:0;border-bottom-right-radius:0;-moz-border-radius-bottomright:0}.table-bordered caption+thead tr:first-child th:first-child, .table-bordered caption+tbody tr:first-child td:first-child, .table-bordered colgroup+thead tr:first-child th:first-child, .table-bordered colgroup+tbody tr:first-child td:first-child {-webkit-border-top-left-radius:4px;border-top-left-radius:4px;-moz-border-radius-topleft:4px}.table-bordered caption+thead tr:first-child th:last-child, .table-bordered caption+tbody tr:first-child td:last-child, .table-bordered colgroup+thead tr:first-child th:last-child, .table-bordered colgroup+tbody tr:first-child td:last-child {-webkit-border-top-right-radius:4px;border-top-right-radius:4px;-moz-border-radius-topright:4px}.table-striped tbody>tr:nth-child(odd)>td, .table-striped tbody>tr:nth-child(odd)>th {background-color:#f9f9f9}.table-hover tbody tr:hover>td, .table-hover tbody tr:hover>th {background-color:#f5f5f5}table td[class*="span"], table th[class*="span"], .row-fluid table td[class*="span"], .row-fluid table th[class*="span"] {display:table-cell;float:none;margin-left:0}.table td.span1, .table th.span1 {float:none;width:44px;margin-left:0}.table td.span2, .table th.span2 {float:none;width:124px;margin-left:0}.table td.span3, .table th.span3 {float:none;width:204px;margin-left:0}.table td.span4, .table th.span4 {float:none;width:284px;margin-left:0}.table td.span5, .table th.span5 {float:none;width:364px;margin-left:0}.table td.span6, .table th.span6 {float:none;width:444px;margin-left:0}.table td.span7, .table th.span7 {float:none;width:524px;margin-left:0}.table td.span8, .table th.span8 {float:none;width:604px;margin-left:0}.table td.span9, .table th.span9 {float:none;width:684px;margin-left:0}.table td.span10, .table th.span10 {float:none;width:764px;margin-left:0}.table td.span11, .table th.span11 {float:none;width:844px;margin-left:0}.table td.span12, .table th.span12 {float:none;width:924px;margin-left:0}.table tbody tr.success>td {background-color:#dff0d8}.table tbody tr.error>td {background-color:#f2dede}.table tbody tr.warning>td {background-color:#fcf8e3}.table tbody tr.info>td {background-color:#d9edf7}.table-hover tbody tr.success:hover>td {background-color:#d0e9c6}.table-hover tbody tr.error:hover>td {background-color:#ebcccc}.table-hover tbody tr.warning:hover>td {background-color:#faf2cc}.table-hover tbody tr.info:hover>td {background-color:#c4e3f3}[class^="icon-"], [class*=" icon-"] {display:inline-block;width:14px;height:14px;margin-top:1px;*margin-right:.3em;line-height:14px;vertical-align:text-top; background-image:url("../img/glyphicons-halflings.png");background-position:14px 14px;background-repeat:no-repeat}.icon-white, .nav-pills>.active>a>[class^="icon-"], .nav-pills>.active>a>[class*=" icon-"], .nav-list>.active>a>[class^="icon-"], .nav-list>.active>a>[class*=" icon-"], .navbar-inverse .nav>.active>a>[class^="icon-"], .navbar-inverse .nav>.active>a>[class*=" icon-"], .dropdown-menu>li>a:hover>[class^="icon-"], .dropdown-menu>li>a:focus>[class^="icon-"], .dropdown-menu>li>a:hover>[class*=" icon-"], .dropdown-menu>li>a:focus>[class*=" icon-"], .dropdown-menu>.active>a>[class^="icon-"], .dropdown-menu>.active>a>[class*=" icon-"], .dropdown-submenu:hover>a>[class^="icon-"], .dropdown-submenu:focus>a>[class^="icon-"], .dropdown-submenu:hover>a>[class*=" icon-"], .dropdown-submenu:focus>a>[class*=" icon-"] {background-image:url("../img/glyphicons-halflings-white.png")}.icon-glass {background-position:0 0}.icon-music {background-position:-24px 0}.icon-search {background-position:-48px 0}.icon-envelope {background-position:-72px 0}.icon-heart {background-position:-96px 0}.icon-star {background-position:-120px 0}.icon-star-empty {background-position:-144px 0}.icon-user {background-position:-168px 0}.icon-film {background-position:-192px 0}.icon-th-large {background-position:-216px 0}.icon-th {background-position:-240px 0}.icon-th-list {background-position:-264px 0}.icon-ok {background-position:-288px 0}.icon-remove {background-position:-312px 0}.icon-zoom-in {background-position:-336px 0}.icon-zoom-out {background-position:-360px 0}.icon-off {background-position:-384px 0}.icon-signal {background-position:-408px 0}.icon-cog {background-position:-432px 0}.icon-trash {background-position:-456px 0}.icon-home {background-position:0 -24px}.icon-file {background-position:-24px -24px}.icon-time {background-position:-48px -24px}.icon-road {background-position:-72px -24px}.icon-download-alt {background-position:-96px -24px}.icon-download {background-position:-120px -24px}.icon-upload {background-position:-144px -24px}.icon-inbox {background-position:-168px -24px}.icon-play-circle {background-position:-192px -24px}.icon-repeat {background-position:-216px -24px}.icon-refresh {background-position:-240px -24px}.icon-list-alt {background-position:-264px -24px}.icon-lock {background-position:-287px -24px}.icon-flag {background-position:-312px -24px}.icon-headphones {background-position:-336px -24px}.icon-volume-off {background-position:-360px -24px}.icon-volume-down {background-position:-384px -24px}.icon-volume-up {background-position:-408px -24px}.icon-qrcode {background-position:-432px -24px}.icon-barcode {background-position:-456px -24px}.icon-tag {background-position:0 -48px}.icon-tags {background-position:-25px -48px}.icon-book {background-position:-48px -48px}.icon-bookmark {background-position:-72px -48px}.icon-print {background-position:-96px -48px}.icon-camera {background-position:-120px -48px}.icon-font {background-position:-144px -48px}.icon-bold {background-position:-167px -48px}.icon-italic {background-position:-192px -48px}.icon-text-height {background-position:-216px -48px}.icon-text-width {background-position:-240px -48px}.icon-align-left {background-position:-264px -48px}.icon-align-center {background-position:-288px -48px}.icon-align-right {background-position:-312px -48px}.icon-align-justify {background-position:-336px -48px}.icon-list {background-position:-360px -48px}.icon-indent-left {background-position:-384px -48px}.icon-indent-right {background-position:-408px -48px}.icon-facetime-video {background-position:-432px -48px}.icon-picture {background-position:-456px -48px}.icon-pencil {background-position:0 -72px}.icon-map-marker {background-position:-24px -72px}.icon-adjust {background-position:-48px -72px}.icon-tint {background-position:-72px -72px}.icon-edit {background-position:-96px -72px}.icon-share {background-position:-120px -72px}.icon-check {background-position:-144px -72px}.icon-move {background-position:-168px -72px}.icon-step-backward {background-position:-192px -72px}.icon-fast-backward {background-position:-216px -72px}.icon-backward {background-position:-240px -72px}.icon-play {background-position:-264px -72px}.icon-pause {background-position:-288px -72px}.icon-stop {background-position:-312px -72px}.icon-forward {background-position:-336px -72px}.icon-fast-forward {background-position:-360px -72px}.icon-step-forward {background-position:-384px -72px}.icon-eject {background-position:-408px -72px}.icon-chevron-left {background-position:-432px -72px}.icon-chevron-right {background-position:-456px -72px}.icon-plus-sign {background-position:0 -96px}.icon-minus-sign {background-position:-24px -96px}.icon-remove-sign {background-position:-48px -96px}.icon-ok-sign {background-position:-72px -96px}.icon-question-sign {background-position:-96px -96px}.icon-info-sign {background-position:-120px -96px}.icon-screenshot {background-position:-144px -96px}.icon-remove-circle {background-position:-168px -96px}.icon-ok-circle {background-position:-192px -96px}.icon-ban-circle {background-position:-216px -96px}.icon-arrow-left {background-position:-240px -96px}.icon-arrow-right {background-position:-264px -96px}.icon-arrow-up {background-position:-289px -96px}.icon-arrow-down {background-position:-312px -96px}.icon-share-alt {background-position:-336px -96px}.icon-resize-full {background-position:-360px -96px}.icon-resize-small {background-position:-384px -96px}.icon-plus {background-position:-408px -96px}.icon-minus {background-position:-433px -96px}.icon-asterisk {background-position:-456px -96px}.icon-exclamation-sign {background-position:0 -120px}.icon-gift {background-position:-24px -120px}.icon-leaf {background-position:-48px -120px}.icon-fire {background-position:-72px -120px}.icon-eye-open {background-position:-96px -120px}.icon-eye-close {background-position:-120px -120px}.icon-warning-sign {background-position:-144px -120px}.icon-plane {background-position:-168px -120px}.icon-calendar {background-position:-192px -120px}.icon-random {width:16px;background-position:-216px -120px}.icon-comment {background-position:-240px -120px}.icon-magnet {background-position:-264px -120px}.icon-chevron-up {background-position:-288px -120px}.icon-chevron-down {background-position:-313px -119px}.icon-retweet {background-position:-336px -120px}.icon-shopping-cart {background-position:-360px -120px}.icon-folder-close {width:16px;background-position:-384px -120px}.icon-folder-open {width:16px;background-position:-408px -120px}.icon-resize-vertical {background-position:-432px -119px}.icon-resize-horizontal {background-position:-456px -118px}.icon-hdd {background-position:0 -144px}.icon-bullhorn {background-position:-24px -144px}.icon-bell {background-position:-48px -144px}.icon-certificate {background-position:-72px -144px}.icon-thumbs-up {background-position:-96px -144px}.icon-thumbs-down {background-position:-120px -144px}.icon-hand-right {background-position:-144px -144px}.icon-hand-left {background-position:-168px -144px}.icon-hand-up {background-position:-192px -144px}.icon-hand-down {background-position:-216px -144px}.icon-circle-arrow-right {background-position:-240px -144px}.icon-circle-arrow-left {background-position:-264px -144px}.icon-circle-arrow-up {background-position:-288px -144px}.icon-circle-arrow-down {background-position:-312px -144px}.icon-globe {background-position:-336px -144px}.icon-wrench {background-position:-360px -144px}.icon-tasks {background-position:-384px -144px}.icon-filter {background-position:-408px -144px}.icon-briefcase {background-position:-432px -144px}.icon-fullscreen {background-position:-456px -144px}.dropup, .dropdown {position:relative}.dropdown-toggle {*margin-bottom:-3px}.dropdown-toggle:active, .open .dropdown-toggle {outline:0}.caret {display:inline-block;width:0;height:0;vertical-align:top;border-top:4px solid #000;border-right:4px solid transparent;border-left:4px solid transparent;content:""}.dropdown .caret {margin-top:8px;margin-left:2px}.dropdown-menu {position:absolute;top:100%;left:0;z-index:1000;display:none;float:left;min-width:160px;padding:5px 0;margin:2px 0 0;list-style:none;background-color:#fff;border:1px solid #ccc;border:1px solid rgba(0,0,0,0.2);*border-right-width:2px;*border-bottom-width:2px;-webkit-border-radius:6px;-moz-border-radius:6px;border-radius:6px;-webkit-box-shadow:0 5px 10px rgba(0,0,0,0.2);-moz-box-shadow:0 5px 10px rgba(0,0,0,0.2);box-shadow:0 5px 10px rgba(0,0,0,0.2);-webkit-background-clip:padding-box;-moz-background-clip:padding;background-clip:padding-box}.dropdown-menu.pull-right {right:0;left:auto}.dropdown-menu .divider {*width:100%;height:1px;margin:9px 1px;*margin:-5px 0 5px;overflow:hidden;background-color:#e5e5e5;border-bottom:1px solid #fff}.dropdown-menu>li>a {display:block;padding:3px 20px;clear:both;font-weight:normal;line-height:20px;color:#333;white-space:nowrap}.dropdown-menu>li>a:hover, .dropdown-menu>li>a:focus, .dropdown-submenu:hover>a, .dropdown-submenu:focus>a {color:#fff;text-decoration:none;background-color:#0081c2;background-image:-moz-linear-gradient(top, #08c, #0077b3);background-image:-webkit-gradient(linear, 0 0, 0 100%, from(#08c), to(#0077b3));background-image:-webkit-linear-gradient(top, #08c, #0077b3);background-image:-o-linear-gradient(top, #08c, #0077b3);background-image:linear-gradient(to bottom, #08c, #0077b3);background-repeat:repeat-x;filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#ff0088cc', endColorstr='#ff0077b3', GradientType=0)}.dropdown-menu>.active>a, .dropdown-menu>.active>a:hover, .dropdown-menu>.active>a:focus {color:#fff;text-decoration:none;background-color:#0081c2;background-image:-moz-linear-gradient(top, #08c, #0077b3);background-image:-webkit-gradient(linear, 0 0, 0 100%, from(#08c), to(#0077b3));background-image:-webkit-linear-gradient(top, #08c, #0077b3);background-image:-o-linear-gradient(top, #08c, #0077b3);background-image:linear-gradient(to bottom, #08c, #0077b3);background-repeat:repeat-x;outline:0;filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#ff0088cc', endColorstr='#ff0077b3', GradientType=0)}.dropdown-menu>.disabled>a, .dropdown-menu>.disabled>a:hover, .dropdown-menu>.disabled>a:focus {color:#999}.dropdown-menu>.disabled>a:hover, .dropdown-menu>.disabled>a:focus {text-decoration:none;cursor:default;background-color:transparent;background-image:none;filter:progid:DXImageTransform.Microsoft.gradient(enabled=false)}.open {*z-index:1000}.open>.dropdown-menu {display:block}.pull-right>.dropdown-menu {right:0;left:auto}.dropup .caret, .navbar-fixed-bottom .dropdown .caret {border-top:0;border-bottom:4px solid #000;content:""}.dropup .dropdown-menu, .navbar-fixed-bottom .dropdown .dropdown-menu {top:auto;bottom:100%;margin-bottom:1px}.dropdown-submenu {position:relative}.dropdown-submenu>.dropdown-menu {top:0;left:100%;margin-top:-6px;margin-left:-1px;-webkit-border-radius:0 6px 6px 6px;-moz-border-radius:0 6px 6px 6px;border-radius:0 6px 6px 6px}.dropdown-submenu:hover>.dropdown-menu {display:block}.dropup .dropdown-submenu>.dropdown-menu {top:auto;bottom:0;margin-top:0;margin-bottom:-2px;-webkit-border-radius:5px 5px 5px 0;-moz-border-radius:5px 5px 5px 0;border-radius:5px 5px 5px 0}.dropdown-submenu>a:after {display:block;float:right;width:0;height:0;margin-top:5px;margin-right:-10px;border-color:transparent;border-left-color:#ccc;border-style:solid;border-width:5px 0 5px 5px;content:" "}.dropdown-submenu:hover>a:after {border-left-color:#fff}.dropdown-submenu.pull-left {float:none}.dropdown-submenu.pull-left>.dropdown-menu {left:-100%;margin-left:10px;-webkit-border-radius:6px 0 6px 6px;-moz-border-radius:6px 0 6px 6px;border-radius:6px 0 6px 6px}.dropdown .dropdown-menu .nav-header {padding-right:20px;padding-left:20px}.typeahead {z-index:1051;margin-top:2px;-webkit-border-radius:4px;-moz-border-radius:4px;border-radius:4px}.well {min-height:20px;padding:19px;margin-bottom:20px;background-color:#f5f5f5;border:1px solid #e3e3e3;-webkit-border-radius:4px;-moz-border-radius:4px;border-radius:4px;-webkit-box-shadow:inset 0 1px 1px rgba(0,0,0,0.05);-moz-box-shadow:inset 0 1px 1px rgba(0,0,0,0.05);box-shadow:inset 0 1px 1px rgba(0,0,0,0.05)}.well blockquote {border-color:#ddd;border-color:rgba(0,0,0,0.15)}.well-large {padding:24px;-webkit-border-radius:6px;-moz-border-radius:6px;border-radius:6px}.well-small {padding:9px;-webkit-border-radius:3px;-moz-border-radius:3px;border-radius:3px}.fade {opacity:0;-webkit-transition:opacity .15s linear;-moz-transition:opacity .15s linear;-o-transition:opacity .15s linear;transition:opacity .15s linear}.fade.in {opacity:1}.collapse {position:relative;height:0;overflow:hidden;-webkit-transition:height .35s ease;-moz-transition:height .35s ease;-o-transition:height .35s ease;transition:height .35s ease}.collapse.in {height:auto}.close {float:right;font-size:20px;font-weight:bold;line-height:20px;color:#000;text-shadow:0 1px 0 #fff;opacity:.2;filter:alpha(opacity=20)}.close:hover, .close:focus {color:#000;text-decoration:none;cursor:pointer;opacity:.4;filter:alpha(opacity=40)}button.close {padding:0;cursor:pointer;background:transparent;border:0;-webkit-appearance:none}.btn {display:inline-block;*display:inline;padding:4px 12px;margin-bottom:0;*margin-left:.3em;font-size:14px;line-height:20px;color:#333;text-align:center;text-shadow:0 1px 1px rgba(255,255,255,0.75);vertical-align:middle;cursor:pointer;background-color:#f5f5f5;*background-color:#e6e6e6;background-image:-moz-linear-gradient(top, #fff, #e6e6e6);background-image:-webkit-gradient(linear, 0 0, 0 100%, from(#fff), to(#e6e6e6));background-image:-webkit-linear-gradient(top, #fff, #e6e6e6);background-image:-o-linear-gradient(top, #fff, #e6e6e6);background-image:linear-gradient(to bottom, #fff, #e6e6e6);background-repeat:repeat-x;border:1px solid #ccc;*border:0;border-color:#e6e6e6 #e6e6e6 #bfbfbf;border-color:rgba(0,0,0,0.1) rgba(0,0,0,0.1) rgba(0,0,0,0.25);border-bottom-color:#b3b3b3;-webkit-border-radius:4px;-moz-border-radius:4px;border-radius:4px;filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#ffffffff', endColorstr='#ffe6e6e6', GradientType=0);filter:progid:DXImageTransform.Microsoft.gradient(enabled=false);*zoom:1;-webkit-box-shadow:inset 0 1px 0 rgba(255,255,255,0.2), 0 1px 2px rgba(0,0,0,0.05);-moz-box-shadow:inset 0 1px 0 rgba(255,255,255,0.2), 0 1px 2px rgba(0,0,0,0.05);box-shadow:inset 0 1px 0 rgba(255,255,255,0.2), 0 1px 2px rgba(0,0,0,0.05)}.btn:hover, .btn:focus, .btn:active, .btn.active, .btn.disabled, .btn[disabled] {color:#333;background-color:#e6e6e6;*background-color:#d9d9d9}.btn:active, .btn.active {background-color:#ccc \9}.btn:first-child {*margin-left:0}.btn:hover, .btn:focus {color:#333;text-decoration:none;background-position:0 -15px;-webkit-transition:background-position .1s linear;-moz-transition:background-position .1s linear;-o-transition:background-position .1s linear;transition:background-position .1s linear}.btn:focus {outline:thin dotted #333;outline:5px auto -webkit-focus-ring-color;outline-offset:-2px}.btn.active, .btn:active {background-image:none;outline:0;-webkit-box-shadow:inset 0 2px 4px rgba(0,0,0,0.15), 0 1px 2px rgba(0,0,0,0.05);-moz-box-shadow:inset 0 2px 4px rgba(0,0,0,0.15), 0 1px 2px rgba(0,0,0,0.05);box-shadow:inset 0 2px 4px rgba(0,0,0,0.15), 0 1px 2px rgba(0,0,0,0.05)}.btn.disabled, .btn[disabled] {cursor:default;background-image:none;opacity:.65;filter:alpha(opacity=65);-webkit-box-shadow:none;-moz-box-shadow:none;box-shadow:none}.btn-large {padding:11px 19px;font-size:17.5px;-webkit-border-radius:6px;-moz-border-radius:6px;border-radius:6px}.btn-large [class^="icon-"], .btn-large [class*=" icon-"] {margin-top:4px}.btn-small {padding:2px 10px;font-size:11.9px;-webkit-border-radius:3px;-moz-border-radius:3px;border-radius:3px}.btn-small [class^="icon-"], .btn-small [class*=" icon-"] {margin-top:0}.btn-mini [class^="icon-"], .btn-mini [class*=" icon-"] {margin-top:-1px}.btn-mini {padding:0 6px;font-size:10.5px;-webkit-border-radius:3px;-moz-border-radius:3px;border-radius:3px}.btn-block {display:block;width:100%;padding-right:0;padding-left:0;-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box}.btn-block+.btn-block {margin-top:5px}input[type="submit"].btn-block, input[type="reset"].btn-block, input[type="button"].btn-block {width:100%}.btn-primary.active, .btn-warning.active, .btn-danger.active, .btn-success.active, .btn-info.active, .btn-inverse.active {color:rgba(255,255,255,0.75)}.btn-primary {color:#fff;text-shadow:0 -1px 0 rgba(0,0,0,0.25);background-color:#006dcc;*background-color:#04c;background-image:-moz-linear-gradient(top, #08c, #04c);background-image:-webkit-gradient(linear, 0 0, 0 100%, from(#08c), to(#04c));background-image:-webkit-linear-gradient(top, #08c, #04c);background-image:-o-linear-gradient(top, #08c, #04c);background-image:linear-gradient(to bottom, #08c, #04c);background-repeat:repeat-x;border-color:#04c #04c #002a80;border-color:rgba(0,0,0,0.1) rgba(0,0,0,0.1) rgba(0,0,0,0.25);filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#ff0088cc', endColorstr='#ff0044cc', GradientType=0);filter:progid:DXImageTransform.Microsoft.gradient(enabled=false)}.btn-primary:hover, .btn-primary:focus, .btn-primary:active, .btn-primary.active, .btn-primary.disabled, .btn-primary[disabled] {color:#fff;background-color:#04c;*background-color:#003bb3}.btn-primary:active, .btn-primary.active {background-color:#039 \9}.btn-warning {color:#fff;text-shadow:0 -1px 0 rgba(0,0,0,0.25);background-color:#faa732;*background-color:#f89406;background-image:-moz-linear-gradient(top, #fbb450, #f89406);background-image:-webkit-gradient(linear, 0 0, 0 100%, from(#fbb450), to(#f89406));background-image:-webkit-linear-gradient(top, #fbb450, #f89406);background-image:-o-linear-gradient(top, #fbb450, #f89406);background-image:linear-gradient(to bottom, #fbb450, #f89406);background-repeat:repeat-x;border-color:#f89406 #f89406 #ad6704;border-color:rgba(0,0,0,0.1) rgba(0,0,0,0.1) rgba(0,0,0,0.25);filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#fffbb450', endColorstr='#fff89406', GradientType=0);filter:progid:DXImageTransform.Microsoft.gradient(enabled=false)}.btn-warning:hover, .btn-warning:focus, .btn-warning:active, .btn-warning.active, .btn-warning.disabled, .btn-warning[disabled] {color:#fff;background-color:#f89406;*background-color:#df8505}.btn-warning:active, .btn-warning.active {background-color:#c67605 \9}.btn-danger {color:#fff;text-shadow:0 -1px 0 rgba(0,0,0,0.25);background-color:#da4f49;*background-color:#bd362f;background-image:-moz-linear-gradient(top, #ee5f5b, #bd362f);background-image:-webkit-gradient(linear, 0 0, 0 100%, from(#ee5f5b), to(#bd362f));background-image:-webkit-linear-gradient(top, #ee5f5b, #bd362f);background-image:-o-linear-gradient(top, #ee5f5b, #bd362f);background-image:linear-gradient(to bottom, #ee5f5b, #bd362f);background-repeat:repeat-x;border-color:#bd362f #bd362f #802420;border-color:rgba(0,0,0,0.1) rgba(0,0,0,0.1) rgba(0,0,0,0.25);filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#ffee5f5b', endColorstr='#ffbd362f', GradientType=0);filter:progid:DXImageTransform.Microsoft.gradient(enabled=false)}.btn-danger:hover, .btn-danger:focus, .btn-danger:active, .btn-danger.active, .btn-danger.disabled, .btn-danger[disabled] {color:#fff;background-color:#bd362f;*background-color:#a9302a}.btn-danger:active, .btn-danger.active {background-color:#942a25 \9}.btn-success {color:#fff;text-shadow:0 -1px 0 rgba(0,0,0,0.25);background-color:#5bb75b;*background-color:#51a351;background-image:-moz-linear-gradient(top, #62c462, #51a351);background-image:-webkit-gradient(linear, 0 0, 0 100%, from(#62c462), to(#51a351));background-image:-webkit-linear-gradient(top, #62c462, #51a351);background-image:-o-linear-gradient(top, #62c462, #51a351);background-image:linear-gradient(to bottom, #62c462, #51a351);background-repeat:repeat-x;border-color:#51a351 #51a351 #387038;border-color:rgba(0,0,0,0.1) rgba(0,0,0,0.1) rgba(0,0,0,0.25);filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#ff62c462', endColorstr='#ff51a351', GradientType=0);filter:progid:DXImageTransform.Microsoft.gradient(enabled=false)}.btn-success:hover, .btn-success:focus, .btn-success:active, .btn-success.active, .btn-success.disabled, .btn-success[disabled] {color:#fff;background-color:#51a351;*background-color:#499249}.btn-success:active, .btn-success.active {background-color:#408140 \9}.btn-info {color:#fff;text-shadow:0 -1px 0 rgba(0,0,0,0.25);background-color:#49afcd;*background-color:#2f96b4;background-image:-moz-linear-gradient(top, #5bc0de, #2f96b4);background-image:-webkit-gradient(linear, 0 0, 0 100%, from(#5bc0de), to(#2f96b4));background-image:-webkit-linear-gradient(top, #5bc0de, #2f96b4);background-image:-o-linear-gradient(top, #5bc0de, #2f96b4);background-image:linear-gradient(to bottom, #5bc0de, #2f96b4);background-repeat:repeat-x;border-color:#2f96b4 #2f96b4 #1f6377;border-color:rgba(0,0,0,0.1) rgba(0,0,0,0.1) rgba(0,0,0,0.25);filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#ff5bc0de', endColorstr='#ff2f96b4', GradientType=0);filter:progid:DXImageTransform.Microsoft.gradient(enabled=false)}.btn-info:hover, .btn-info:focus, .btn-info:active, .btn-info.active, .btn-info.disabled, .btn-info[disabled] {color:#fff;background-color:#2f96b4;*background-color:#2a85a0}.btn-info:active, .btn-info.active {background-color:#24748c \9}.btn-inverse {color:#fff;text-shadow:0 -1px 0 rgba(0,0,0,0.25);background-color:#363636;*background-color:#222;background-image:-moz-linear-gradient(top, #444, #222);background-image:-webkit-gradient(linear, 0 0, 0 100%, from(#444), to(#222));background-image:-webkit-linear-gradient(top, #444, #222);background-image:-o-linear-gradient(top, #444, #222);background-image:linear-gradient(to bottom, #444, #222);background-repeat:repeat-x;border-color:#222 #222 #000;border-color:rgba(0,0,0,0.1) rgba(0,0,0,0.1) rgba(0,0,0,0.25);filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#ff444444', endColorstr='#ff222222', GradientType=0);filter:progid:DXImageTransform.Microsoft.gradient(enabled=false)}.btn-inverse:hover, .btn-inverse:focus, .btn-inverse:active, .btn-inverse.active, .btn-inverse.disabled, .btn-inverse[disabled] {color:#fff;background-color:#222;*background-color:#151515}.btn-inverse:active, .btn-inverse.active {background-color:#080808 \9}button.btn, input[type="submit"].btn {*padding-top:3px;*padding-bottom:3px}button.btn::-moz-focus-inner, input[type="submit"].btn::-moz-focus-inner {padding:0;border:0}button.btn.btn-large, input[type="submit"].btn.btn-large {*padding-top:7px;*padding-bottom:7px}button.btn.btn-small, input[type="submit"].btn.btn-small {*padding-top:3px;*padding-bottom:3px}button.btn.btn-mini, input[type="submit"].btn.btn-mini {*padding-top:1px;*padding-bottom:1px}.btn-link, .btn-link:active, .btn-link[disabled] {background-color:transparent;background-image:none;-webkit-box-shadow:none;-moz-box-shadow:none;box-shadow:none}.btn-link {color:#08c;cursor:pointer;border-color:transparent;-webkit-border-radius:0;-moz-border-radius:0;border-radius:0}.btn-link:hover, .btn-link:focus {color:#005580;text-decoration:underline;background-color:transparent}.btn-link[disabled]:hover, .btn-link[disabled]:focus {color:#333;text-decoration:none}.btn-group {position:relative;display:inline-block;*display:inline;*margin-left:.3em;font-size:0;white-space:nowrap;vertical-align:middle;*zoom:1}.btn-group:first-child {*margin-left:0}.btn-group+.btn-group {margin-left:5px}.btn-toolbar {margin-top:10px;margin-bottom:10px;font-size:0}.btn-toolbar>.btn+.btn, .btn-toolbar>.btn-group+.btn, .btn-toolbar>.btn+.btn-group {margin-left:5px}.btn-group>.btn {position:relative;-webkit-border-radius:0;-moz-border-radius:0;border-radius:0}.btn-group>.btn+.btn {margin-left:-1px}.btn-group>.btn, .btn-group>.dropdown-menu, .btn-group>.popover {font-size:14px}.btn-group>.btn-mini {font-size:10.5px}.btn-group>.btn-small {font-size:11.9px}.btn-group>.btn-large {font-size:17.5px}.btn-group>.btn:first-child {margin-left:0;-webkit-border-bottom-left-radius:4px;border-bottom-left-radius:4px;-webkit-border-top-left-radius:4px;border-top-left-radius:4px;-moz-border-radius-bottomleft:4px;-moz-border-radius-topleft:4px}.btn-group>.btn:last-child, .btn-group>.dropdown-toggle {-webkit-border-top-right-radius:4px;border-top-right-radius:4px;-webkit-border-bottom-right-radius:4px;border-bottom-right-radius:4px;-moz-border-radius-topright:4px;-moz-border-radius-bottomright:4px}.btn-group>.btn.large:first-child {margin-left:0;-webkit-border-bottom-left-radius:6px;border-bottom-left-radius:6px;-webkit-border-top-left-radius:6px;border-top-left-radius:6px;-moz-border-radius-bottomleft:6px;-moz-border-radius-topleft:6px}.btn-group>.btn.large:last-child, .btn-group>.large.dropdown-toggle {-webkit-border-top-right-radius:6px;border-top-right-radius:6px;-webkit-border-bottom-right-radius:6px;border-bottom-right-radius:6px;-moz-border-radius-topright:6px;-moz-border-radius-bottomright:6px}.btn-group>.btn:hover, .btn-group>.btn:focus, .btn-group>.btn:active, .btn-group>.btn.active {z-index:2}.btn-group .dropdown-toggle:active, .btn-group.open .dropdown-toggle {outline:0}.btn-group>.btn+.dropdown-toggle {*padding-top:5px;padding-right:8px;*padding-bottom:5px;padding-left:8px;-webkit-box-shadow:inset 1px 0 0 rgba(255,255,255,0.125), inset 0 1px 0 rgba(255,255,255,0.2), 0 1px 2px rgba(0,0,0,0.05);-moz-box-shadow:inset 1px 0 0 rgba(255,255,255,0.125), inset 0 1px 0 rgba(255,255,255,0.2), 0 1px 2px rgba(0,0,0,0.05);box-shadow:inset 1px 0 0 rgba(255,255,255,0.125), inset 0 1px 0 rgba(255,255,255,0.2), 0 1px 2px rgba(0,0,0,0.05)}.btn-group>.btn-mini+.dropdown-toggle {*padding-top:2px;padding-right:5px;*padding-bottom:2px;padding-left:5px}.btn-group>.btn-small+.dropdown-toggle {*padding-top:5px;*padding-bottom:4px}.btn-group>.btn-large+.dropdown-toggle {*padding-top:7px;padding-right:12px;*padding-bottom:7px;padding-left:12px}.btn-group.open .dropdown-toggle {background-image:none;-webkit-box-shadow:inset 0 2px 4px rgba(0,0,0,0.15), 0 1px 2px rgba(0,0,0,0.05);-moz-box-shadow:inset 0 2px 4px rgba(0,0,0,0.15), 0 1px 2px rgba(0,0,0,0.05);box-shadow:inset 0 2px 4px rgba(0,0,0,0.15), 0 1px 2px rgba(0,0,0,0.05)}.btn-group.open .btn.dropdown-toggle {background-color:#e6e6e6}.btn-group.open .btn-primary.dropdown-toggle {background-color:#04c}.btn-group.open .btn-warning.dropdown-toggle {background-color:#f89406}.btn-group.open .btn-danger.dropdown-toggle {background-color:#bd362f}.btn-group.open .btn-success.dropdown-toggle {background-color:#51a351}.btn-group.open .btn-info.dropdown-toggle {background-color:#2f96b4}.btn-group.open .btn-inverse.dropdown-toggle {background-color:#222}.btn .caret {margin-top:8px;margin-left:0}.btn-large .caret {margin-top:6px}.btn-large .caret {border-top-width:5px;border-right-width:5px;border-left-width:5px}.btn-mini .caret, .btn-small .caret {margin-top:8px}.dropup .btn-large .caret {border-bottom-width:5px}.btn-primary .caret, .btn-warning .caret, .btn-danger .caret, .btn-info .caret, .btn-success .caret, .btn-inverse .caret {border-top-color:#fff;border-bottom-color:#fff}.btn-group-vertical {display:inline-block;*display:inline;*zoom:1}.btn-group-vertical>.btn {display:block;float:none;max-width:100%;-webkit-border-radius:0;-moz-border-radius:0;border-radius:0}.btn-group-vertical>.btn+.btn {margin-top:-1px;margin-left:0}.btn-group-vertical>.btn:first-child {-webkit-border-radius:4px 4px 0 0;-moz-border-radius:4px 4px 0 0;border-radius:4px 4px 0 0}.btn-group-vertical>.btn:last-child {-webkit-border-radius:0 0 4px 4px;-moz-border-radius:0 0 4px 4px;border-radius:0 0 4px 4px}.btn-group-vertical>.btn-large:first-child {-webkit-border-radius:6px 6px 0 0;-moz-border-radius:6px 6px 0 0;border-radius:6px 6px 0 0}.btn-group-vertical>.btn-large:last-child {-webkit-border-radius:0 0 6px 6px;-moz-border-radius:0 0 6px 6px;border-radius:0 0 6px 6px}.alert {padding:8px 35px 8px 14px;margin-bottom:20px;text-shadow:0 1px 0 rgba(255,255,255,0.5);background-color:#fcf8e3;border:1px solid #fbeed5;-webkit-border-radius:4px;-moz-border-radius:4px;border-radius:4px}.alert, .alert h4 {color:#c09853}.alert h4 {margin:0}.alert .close {position:relative;top:-2px;right:-21px;line-height:20px}.alert-success {color:#468847;background-color:#dff0d8;border-color:#d6e9c6}.alert-success h4 {color:#468847}.alert-danger, .alert-error {color:#b94a48;background-color:#f2dede;border-color:#eed3d7}.alert-danger h4, .alert-error h4 {color:#b94a48}.alert-info {color:#3a87ad;background-color:#d9edf7;border-color:#bce8f1}.alert-info h4 {color:#3a87ad}.alert-block {padding-top:14px;padding-bottom:14px}.alert-block>p, .alert-block>ul {margin-bottom:0}.alert-block p+p {margin-top:5px}.nav {margin-bottom:20px;margin-left:0;list-style:none}.nav>li>a {display:block}.nav>li>a:hover, .nav>li>a:focus {text-decoration:none;background-color:#eee}.nav>li>a>img {max-width:none}.nav>.pull-right {float:right}.nav-header {display:block;padding:3px 15px;font-size:11px;font-weight:bold;line-height:20px;color:#999;text-shadow:0 1px 0 rgba(255,255,255,0.5);text-transform:uppercase}.nav li+.nav-header {margin-top:9px}.nav-list {padding-right:15px;padding-left:15px;margin-bottom:0}.nav-list>li>a, .nav-list .nav-header {margin-right:-15px;margin-left:-15px;text-shadow:0 1px 0 rgba(255,255,255,0.5)}.nav-list>li>a {padding:3px 15px}.nav-list>.active>a, .nav-list>.active>a:hover, .nav-list>.active>a:focus {color:#fff;text-shadow:0 -1px 0 rgba(0,0,0,0.2);background-color:#08c}.nav-list [class^="icon-"], .nav-list [class*=" icon-"] {margin-right:2px}.nav-list .divider {*width:100%;height:1px;margin:9px 1px;*margin:-5px 0 5px;overflow:hidden;background-color:#e5e5e5;border-bottom:1px solid #fff}.nav-tabs, .nav-pills {*zoom:1}.nav-tabs:before, .nav-pills:before, .nav-tabs:after, .nav-pills:after {display:table;line-height:0;content:""}.nav-tabs:after, .nav-pills:after {clear:both}.nav-tabs>li, .nav-pills>li {float:left}.nav-tabs>li>a, .nav-pills>li>a {padding-right:12px;padding-left:12px;margin-right:2px;line-height:14px}.nav-tabs {border-bottom:1px solid #ddd}.nav-tabs>li {margin-bottom:-1px}.nav-tabs>li>a {padding-top:8px;padding-bottom:8px;line-height:20px;border:1px solid transparent;-webkit-border-radius:4px 4px 0 0;-moz-border-radius:4px 4px 0 0;border-radius:4px 4px 0 0}.nav-tabs>li>a:hover, .nav-tabs>li>a:focus {border-color:#eee #eee #ddd}.nav-tabs>.active>a, .nav-tabs>.active>a:hover, .nav-tabs>.active>a:focus {color:#555;cursor:default;background-color:#fff;border:1px solid #ddd;border-bottom-color:transparent}.nav-pills>li>a {padding-top:8px;padding-bottom:8px;margin-top:2px;margin-bottom:2px;-webkit-border-radius:5px;-moz-border-radius:5px;border-radius:5px}.nav-pills>.active>a, .nav-pills>.active>a:hover, .nav-pills>.active>a:focus {color:#fff;background-color:#08c}.nav-stacked>li {float:none}.nav-stacked>li>a {margin-right:0}.nav-tabs.nav-stacked {border-bottom:0}.nav-tabs.nav-stacked>li>a {border:1px solid #ddd;-webkit-border-radius:0;-moz-border-radius:0;border-radius:0}.nav-tabs.nav-stacked>li:first-child>a {-webkit-border-top-right-radius:4px;border-top-right-radius:4px;-webkit-border-top-left-radius:4px;border-top-left-radius:4px;-moz-border-radius-topright:4px;-moz-border-radius-topleft:4px}.nav-tabs.nav-stacked>li:last-child>a {-webkit-border-bottom-right-radius:4px;border-bottom-right-radius:4px;-webkit-border-bottom-left-radius:4px;border-bottom-left-radius:4px;-moz-border-radius-bottomright:4px;-moz-border-radius-bottomleft:4px}.nav-tabs.nav-stacked>li>a:hover, .nav-tabs.nav-stacked>li>a:focus {z-index:2;border-color:#ddd}.nav-pills.nav-stacked>li>a {margin-bottom:3px}.nav-pills.nav-stacked>li:last-child>a {margin-bottom:1px}.nav-tabs .dropdown-menu {-webkit-border-radius:0 0 6px 6px;-moz-border-radius:0 0 6px 6px;border-radius:0 0 6px 6px}.nav-pills .dropdown-menu {-webkit-border-radius:6px;-moz-border-radius:6px;border-radius:6px}.nav .dropdown-toggle .caret {margin-top:6px;border-top-color:#08c;border-bottom-color:#08c}.nav .dropdown-toggle:hover .caret, .nav .dropdown-toggle:focus .caret {border-top-color:#005580;border-bottom-color:#005580}.nav-tabs .dropdown-toggle .caret {margin-top:8px}.nav .active .dropdown-toggle .caret {border-top-color:#fff;border-bottom-color:#fff}.nav-tabs .active .dropdown-toggle .caret {border-top-color:#555;border-bottom-color:#555}.nav>.dropdown.active>a:hover, .nav>.dropdown.active>a:focus {cursor:pointer}.nav-tabs .open .dropdown-toggle, .nav-pills .open .dropdown-toggle, .nav>li.dropdown.open.active>a:hover, .nav>li.dropdown.open.active>a:focus {color:#fff;background-color:#999;border-color:#999}.nav li.dropdown.open .caret, .nav li.dropdown.open.active .caret, .nav li.dropdown.open a:hover .caret, .nav li.dropdown.open a:focus .caret {border-top-color:#fff;border-bottom-color:#fff;opacity:1;filter:alpha(opacity=100)}.tabs-stacked .open>a:hover, .tabs-stacked .open>a:focus {border-color:#999}.tabbable {*zoom:1}.tabbable:before, .tabbable:after {display:table;line-height:0;content:""}.tabbable:after {clear:both}.tab-content {overflow:auto}.tabs-below>.nav-tabs, .tabs-right>.nav-tabs, .tabs-left>.nav-tabs {border-bottom:0}.tab-content>.tab-pane, .pill-content>.pill-pane {display:none}.tab-content>.active, .pill-content>.active {display:block}.tabs-below>.nav-tabs {border-top:1px solid #ddd}.tabs-below>.nav-tabs>li {margin-top:-1px;margin-bottom:0}.tabs-below>.nav-tabs>li>a {-webkit-border-radius:0 0 4px 4px;-moz-border-radius:0 0 4px 4px;border-radius:0 0 4px 4px}.tabs-below>.nav-tabs>li>a:hover, .tabs-below>.nav-tabs>li>a:focus {border-top-color:#ddd;border-bottom-color:transparent}.tabs-below>.nav-tabs>.active>a, .tabs-below>.nav-tabs>.active>a:hover, .tabs-below>.nav-tabs>.active>a:focus {border-color:transparent #ddd #ddd #ddd}.tabs-left>.nav-tabs>li, .tabs-right>.nav-tabs>li {float:none}.tabs-left>.nav-tabs>li>a, .tabs-right>.nav-tabs>li>a {min-width:74px;margin-right:0;margin-bottom:3px}.tabs-left>.nav-tabs {float:left;margin-right:19px;border-right:1px solid #ddd}.tabs-left>.nav-tabs>li>a {margin-right:-1px;-webkit-border-radius:4px 0 0 4px;-moz-border-radius:4px 0 0 4px;border-radius:4px 0 0 4px}.tabs-left>.nav-tabs>li>a:hover, .tabs-left>.nav-tabs>li>a:focus {border-color:#eee #ddd #eee #eee}.tabs-left>.nav-tabs .active>a, .tabs-left>.nav-tabs .active>a:hover, .tabs-left>.nav-tabs .active>a:focus {border-color:#ddd transparent #ddd #ddd;*border-right-color:#fff}.tabs-right>.nav-tabs {float:right;margin-left:19px;border-left:1px solid #ddd}.tabs-right>.nav-tabs>li>a {margin-left:-1px;-webkit-border-radius:0 4px 4px 0;-moz-border-radius:0 4px 4px 0;border-radius:0 4px 4px 0}.tabs-right>.nav-tabs>li>a:hover, .tabs-right>.nav-tabs>li>a:focus {border-color:#eee #eee #eee #ddd}.tabs-right>.nav-tabs .active>a, .tabs-right>.nav-tabs .active>a:hover, .tabs-right>.nav-tabs .active>a:focus {border-color:#ddd #ddd #ddd transparent;*border-left-color:#fff}.nav>.disabled>a {color:#999}.nav>.disabled>a:hover, .nav>.disabled>a:focus {text-decoration:none;cursor:default;background-color:transparent}.navbar {*position:relative;*z-index:2;margin-bottom:20px;overflow:visible}.navbar-inner {min-height:40px;padding-right:20px;padding-left:20px;background-color:#fafafa;background-image:-moz-linear-gradient(top, #fff, #f2f2f2);background-image:-webkit-gradient(linear, 0 0, 0 100%, from(#fff), to(#f2f2f2));background-image:-webkit-linear-gradient(top, #fff, #f2f2f2);background-image:-o-linear-gradient(top, #fff, #f2f2f2);background-image:linear-gradient(to bottom, #fff, #f2f2f2);background-repeat:repeat-x;border:1px solid #d4d4d4;-webkit-border-radius:4px;-moz-border-radius:4px;border-radius:4px;filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#ffffffff', endColorstr='#fff2f2f2', GradientType=0);*zoom:1;-webkit-box-shadow:0 1px 4px rgba(0,0,0,0.065);-moz-box-shadow:0 1px 4px rgba(0,0,0,0.065);box-shadow:0 1px 4px rgba(0,0,0,0.065)}.navbar-inner:before, .navbar-inner:after {display:table;line-height:0;content:""}.navbar-inner:after {clear:both}.navbar .container {width:auto}.nav-collapse.collapse {height:auto;overflow:visible}.navbar .brand {display:block;float:left;padding:10px 20px 10px;margin-left:-20px;font-size:20px;font-weight:200;color:#777;text-shadow:0 1px 0 #fff}.navbar .brand:hover, .navbar .brand:focus {text-decoration:none}.navbar-text {margin-bottom:0;line-height:40px;color:#777}.navbar-link {color:#777}.navbar-link:hover, .navbar-link:focus {color:#333}.navbar .divider-vertical {height:40px;margin:0 9px;border-right:1px solid #fff;border-left:1px solid #f2f2f2}.navbar .btn, .navbar .btn-group {margin-top:5px}.navbar .btn-group .btn, .navbar .input-prepend .btn, .navbar .input-append .btn, .navbar .input-prepend .btn-group, .navbar .input-append .btn-group {margin-top:0}.navbar-form {margin-bottom:0;*zoom:1}.navbar-form:before, .navbar-form:after {display:table;line-height:0;content:""}.navbar-form:after {clear:both}.navbar-form input, .navbar-form select, .navbar-form .radio, .navbar-form .checkbox {margin-top:5px}.navbar-form input, .navbar-form select, .navbar-form .btn {display:inline-block;margin-bottom:0}.navbar-form input[type="image"], .navbar-form input[type="checkbox"], .navbar-form input[type="radio"] {margin-top:3px}.navbar-form .input-append, .navbar-form .input-prepend {margin-top:5px;white-space:nowrap}.navbar-form .input-append input, .navbar-form .input-prepend input {margin-top:0}.navbar-search {position:relative;float:left;margin-top:5px;margin-bottom:0}.navbar-search .search-query {padding:4px 14px;margin-bottom:0;font-family:"Helvetica Neue", Helvetica, Arial, sans-serif;font-size:13px;font-weight:normal;line-height:1;-webkit-border-radius:15px;-moz-border-radius:15px;border-radius:15px}.navbar-static-top {position:static;margin-bottom:0}.navbar-static-top .navbar-inner {-webkit-border-radius:0;-moz-border-radius:0;border-radius:0}.navbar-fixed-top, .navbar-fixed-bottom {position:fixed;right:0;left:0;z-index:1030;margin-bottom:0}.navbar-fixed-top .navbar-inner, .navbar-static-top .navbar-inner {border-width:0 0 1px}.navbar-fixed-bottom .navbar-inner {border-width:1px 0 0}.navbar-fixed-top .navbar-inner, .navbar-fixed-bottom .navbar-inner {padding-right:0;padding-left:0;-webkit-border-radius:0;-moz-border-radius:0;border-radius:0}.navbar-static-top .container, .navbar-fixed-top .container, .navbar-fixed-bottom .container {width:940px}.navbar-fixed-top {top:0}.navbar-fixed-top .navbar-inner, .navbar-static-top .navbar-inner {-webkit-box-shadow:0 1px 10px rgba(0,0,0,0.1);-moz-box-shadow:0 1px 10px rgba(0,0,0,0.1);box-shadow:0 1px 10px rgba(0,0,0,0.1)}.navbar-fixed-bottom {bottom:0}.navbar-fixed-bottom .navbar-inner {-webkit-box-shadow:0 -1px 10px rgba(0,0,0,0.1);-moz-box-shadow:0 -1px 10px rgba(0,0,0,0.1);box-shadow:0 -1px 10px rgba(0,0,0,0.1)}.navbar .nav {position:relative;left:0;display:block;float:left;margin:0 10px 0 0}.navbar .nav.pull-right {float:right;margin-right:0}.navbar .nav>li {float:left}.navbar .nav>li>a {float:none;padding:10px 15px 10px;color:#777;text-decoration:none;text-shadow:0 1px 0 #fff}.navbar .nav .dropdown-toggle .caret {margin-top:8px}.navbar .nav>li>a:focus, .navbar .nav>li>a:hover {color:#333;text-decoration:none;background-color:transparent}.navbar .nav>.active>a, .navbar .nav>.active>a:hover, .navbar .nav>.active>a:focus {color:#555;text-decoration:none;background-color:#e5e5e5;-webkit-box-shadow:inset 0 3px 8px rgba(0,0,0,0.125);-moz-box-shadow:inset 0 3px 8px rgba(0,0,0,0.125);box-shadow:inset 0 3px 8px rgba(0,0,0,0.125)}.navbar .btn-navbar {display:none;float:right;padding:7px 10px;margin-right:5px;margin-left:5px;color:#fff;text-shadow:0 -1px 0 rgba(0,0,0,0.25);background-color:#ededed;*background-color:#e5e5e5;background-image:-moz-linear-gradient(top, #f2f2f2, #e5e5e5);background-image:-webkit-gradient(linear, 0 0, 0 100%, from(#f2f2f2), to(#e5e5e5));background-image:-webkit-linear-gradient(top, #f2f2f2, #e5e5e5);background-image:-o-linear-gradient(top, #f2f2f2, #e5e5e5);background-image:linear-gradient(to bottom, #f2f2f2, #e5e5e5);background-repeat:repeat-x;border-color:#e5e5e5 #e5e5e5 #bfbfbf;border-color:rgba(0,0,0,0.1) rgba(0,0,0,0.1) rgba(0,0,0,0.25);filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#fff2f2f2', endColorstr='#ffe5e5e5', GradientType=0);filter:progid:DXImageTransform.Microsoft.gradient(enabled=false);-webkit-box-shadow:inset 0 1px 0 rgba(255,255,255,0.1), 0 1px 0 rgba(255,255,255,0.075);-moz-box-shadow:inset 0 1px 0 rgba(255,255,255,0.1), 0 1px 0 rgba(255,255,255,0.075);box-shadow:inset 0 1px 0 rgba(255,255,255,0.1), 0 1px 0 rgba(255,255,255,0.075)}.navbar .btn-navbar:hover, .navbar .btn-navbar:focus, .navbar .btn-navbar:active, .navbar .btn-navbar.active, .navbar .btn-navbar.disabled, .navbar .btn-navbar[disabled] {color:#fff;background-color:#e5e5e5;*background-color:#d9d9d9}.navbar .btn-navbar:active, .navbar .btn-navbar.active {background-color:#ccc \9}.navbar .btn-navbar .icon-bar {display:block;width:18px;height:2px;background-color:#f5f5f5;-webkit-border-radius:1px;-moz-border-radius:1px;border-radius:1px;-webkit-box-shadow:0 1px 0 rgba(0,0,0,0.25);-moz-box-shadow:0 1px 0 rgba(0,0,0,0.25);box-shadow:0 1px 0 rgba(0,0,0,0.25)}.btn-navbar .icon-bar+.icon-bar {margin-top:3px}.navbar .nav>li>.dropdown-menu:before {position:absolute;top:-7px;left:9px;display:inline-block;border-right:7px solid transparent;border-bottom:7px solid #ccc;border-left:7px solid transparent;border-bottom-color:rgba(0,0,0,0.2);content:''}.navbar .nav>li>.dropdown-menu:after {position:absolute;top:-6px;left:10px;display:inline-block;border-right:6px solid transparent;border-bottom:6px solid #fff;border-left:6px solid transparent;content:''}.navbar-fixed-bottom .nav>li>.dropdown-menu:before {top:auto;bottom:-7px;border-top:7px solid #ccc;border-bottom:0;border-top-color:rgba(0,0,0,0.2)}.navbar-fixed-bottom .nav>li>.dropdown-menu:after {top:auto;bottom:-6px;border-top:6px solid #fff;border-bottom:0}.navbar .nav li.dropdown>a:hover .caret, .navbar .nav li.dropdown>a:focus .caret {border-top-color:#333;border-bottom-color:#333}.navbar .nav li.dropdown.open>.dropdown-toggle, .navbar .nav li.dropdown.active>.dropdown-toggle, .navbar .nav li.dropdown.open.active>.dropdown-toggle {color:#555;background-color:#e5e5e5}.navbar .nav li.dropdown>.dropdown-toggle .caret {border-top-color:#777;border-bottom-color:#777}.navbar .nav li.dropdown.open>.dropdown-toggle .caret, .navbar .nav li.dropdown.active>.dropdown-toggle .caret, .navbar .nav li.dropdown.open.active>.dropdown-toggle .caret {border-top-color:#555;border-bottom-color:#555}.navbar .pull-right>li>.dropdown-menu, .navbar .nav>li>.dropdown-menu.pull-right {right:0;left:auto}.navbar .pull-right>li>.dropdown-menu:before, .navbar .nav>li>.dropdown-menu.pull-right:before {right:12px;left:auto}.navbar .pull-right>li>.dropdown-menu:after, .navbar .nav>li>.dropdown-menu.pull-right:after {right:13px;left:auto}.navbar .pull-right>li>.dropdown-menu .dropdown-menu, .navbar .nav>li>.dropdown-menu.pull-right .dropdown-menu {right:100%;left:auto;margin-right:-1px;margin-left:0;-webkit-border-radius:6px 0 6px 6px;-moz-border-radius:6px 0 6px 6px;border-radius:6px 0 6px 6px}.navbar-inverse .navbar-inner {background-color:#1b1b1b;background-image:-moz-linear-gradient(top, #222, #111);background-image:-webkit-gradient(linear, 0 0, 0 100%, from(#222), to(#111));background-image:-webkit-linear-gradient(top, #222, #111);background-image:-o-linear-gradient(top, #222, #111);background-image:linear-gradient(to bottom, #222, #111);background-repeat:repeat-x;border-color:#252525;filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#ff222222', endColorstr='#ff111111', GradientType=0)}.navbar-inverse .brand, .navbar-inverse .nav>li>a {color:#999;text-shadow:0 -1px 0 rgba(0,0,0,0.25)}.navbar-inverse .brand:hover, .navbar-inverse .nav>li>a:hover, .navbar-inverse .brand:focus, .navbar-inverse .nav>li>a:focus {color:#fff}.navbar-inverse .brand {color:#999}.navbar-inverse .navbar-text {color:#999}.navbar-inverse .nav>li>a:focus, .navbar-inverse .nav>li>a:hover {color:#fff;background-color:transparent}.navbar-inverse .nav .active>a, .navbar-inverse .nav .active>a:hover, .navbar-inverse .nav .active>a:focus {color:#fff;background-color:#111}.navbar-inverse .navbar-link {color:#999}.navbar-inverse .navbar-link:hover, .navbar-inverse .navbar-link:focus {color:#fff}.navbar-inverse .divider-vertical {border-right-color:#222;border-left-color:#111}.navbar-inverse .nav li.dropdown.open>.dropdown-toggle, .navbar-inverse .nav li.dropdown.active>.dropdown-toggle, .navbar-inverse .nav li.dropdown.open.active>.dropdown-toggle {color:#fff;background-color:#111}.navbar-inverse .nav li.dropdown>a:hover .caret, .navbar-inverse .nav li.dropdown>a:focus .caret {border-top-color:#fff;border-bottom-color:#fff}.navbar-inverse .nav li.dropdown>.dropdown-toggle .caret {border-top-color:#999;border-bottom-color:#999}.navbar-inverse .nav li.dropdown.open>.dropdown-toggle .caret, .navbar-inverse .nav li.dropdown.active>.dropdown-toggle .caret, .navbar-inverse .nav li.dropdown.open.active>.dropdown-toggle .caret {border-top-color:#fff;border-bottom-color:#fff}.navbar-inverse .navbar-search .search-query {color:#fff;background-color:#515151;border-color:#111;-webkit-box-shadow:inset 0 1px 2px rgba(0,0,0,0.1), 0 1px 0 rgba(255,255,255,0.15);-moz-box-shadow:inset 0 1px 2px rgba(0,0,0,0.1), 0 1px 0 rgba(255,255,255,0.15);box-shadow:inset 0 1px 2px rgba(0,0,0,0.1), 0 1px 0 rgba(255,255,255,0.15);-webkit-transition:none;-moz-transition:none;-o-transition:none;transition:none}.navbar-inverse .navbar-search .search-query:-moz-placeholder {color:#ccc}.navbar-inverse .navbar-search .search-query:-ms-input-placeholder {color:#ccc}.navbar-inverse .navbar-search .search-query::-webkit-input-placeholder {color:#ccc}.navbar-inverse .navbar-search .search-query:focus, .navbar-inverse .navbar-search .search-query.focused {padding:5px 15px;color:#333;text-shadow:0 1px 0 #fff;background-color:#fff;border:0;outline:0;-webkit-box-shadow:0 0 3px rgba(0,0,0,0.15);-moz-box-shadow:0 0 3px rgba(0,0,0,0.15);box-shadow:0 0 3px rgba(0,0,0,0.15)}.navbar-inverse .btn-navbar {color:#fff;text-shadow:0 -1px 0 rgba(0,0,0,0.25);background-color:#0e0e0e;*background-color:#040404;background-image:-moz-linear-gradient(top, #151515, #040404);background-image:-webkit-gradient(linear, 0 0, 0 100%, from(#151515), to(#040404));background-image:-webkit-linear-gradient(top, #151515, #040404);background-image:-o-linear-gradient(top, #151515, #040404);background-image:linear-gradient(to bottom, #151515, #040404);background-repeat:repeat-x;border-color:#040404 #040404 #000;border-color:rgba(0,0,0,0.1) rgba(0,0,0,0.1) rgba(0,0,0,0.25);filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#ff151515', endColorstr='#ff040404', GradientType=0);filter:progid:DXImageTransform.Microsoft.gradient(enabled=false)}.navbar-inverse .btn-navbar:hover, .navbar-inverse .btn-navbar:focus, .navbar-inverse .btn-navbar:active, .navbar-inverse .btn-navbar.active, .navbar-inverse .btn-navbar.disabled, .navbar-inverse .btn-navbar[disabled] {color:#fff;background-color:#040404;*background-color:#000}.navbar-inverse .btn-navbar:active, .navbar-inverse .btn-navbar.active {background-color:#000 \9}.breadcrumb {padding:8px 15px;margin:0 0 20px;list-style:none;background-color:#f5f5f5;-webkit-border-radius:4px;-moz-border-radius:4px;border-radius:4px}.breadcrumb>li {display:inline-block;*display:inline;text-shadow:0 1px 0 #fff;*zoom:1}.breadcrumb>li>.divider {padding:0 5px;color:#ccc}.breadcrumb>.active {color:#999}.pagination {margin:20px 0}.pagination ul {display:inline-block;*display:inline;margin-bottom:0;margin-left:0;-webkit-border-radius:4px;-moz-border-radius:4px;border-radius:4px;*zoom:1;-webkit-box-shadow:0 1px 2px rgba(0,0,0,0.05);-moz-box-shadow:0 1px 2px rgba(0,0,0,0.05);box-shadow:0 1px 2px rgba(0,0,0,0.05)}.pagination ul>li {display:inline}.pagination ul>li>a, .pagination ul>li>span {float:left;padding:4px 12px;line-height:20px;text-decoration:none;background-color:#fff;border:1px solid #ddd;border-left-width:0}.pagination ul>li>a:hover, .pagination ul>li>a:focus, .pagination ul>.active>a, .pagination ul>.active>span {background-color:#f5f5f5}.pagination ul>.active>a, .pagination ul>.active>span {color:#999;cursor:default}.pagination ul>.disabled>span, .pagination ul>.disabled>a, .pagination ul>.disabled>a:hover, .pagination ul>.disabled>a:focus {color:#999;cursor:default;background-color:transparent}.pagination ul>li:first-child>a, .pagination ul>li:first-child>span {border-left-width:1px;-webkit-border-bottom-left-radius:4px;border-bottom-left-radius:4px;-webkit-border-top-left-radius:4px;border-top-left-radius:4px;-moz-border-radius-bottomleft:4px;-moz-border-radius-topleft:4px}.pagination ul>li:last-child>a, .pagination ul>li:last-child>span {-webkit-border-top-right-radius:4px;border-top-right-radius:4px;-webkit-border-bottom-right-radius:4px;border-bottom-right-radius:4px;-moz-border-radius-topright:4px;-moz-border-radius-bottomright:4px}.pagination-centered {text-align:center}.pagination-right {text-align:right}.pagination-large ul>li>a, .pagination-large ul>li>span {padding:11px 19px;font-size:17.5px}.pagination-large ul>li:first-child>a, .pagination-large ul>li:first-child>span {-webkit-border-bottom-left-radius:6px;border-bottom-left-radius:6px;-webkit-border-top-left-radius:6px;border-top-left-radius:6px;-moz-border-radius-bottomleft:6px;-moz-border-radius-topleft:6px}.pagination-large ul>li:last-child>a, .pagination-large ul>li:last-child>span {-webkit-border-top-right-radius:6px;border-top-right-radius:6px;-webkit-border-bottom-right-radius:6px;border-bottom-right-radius:6px;-moz-border-radius-topright:6px;-moz-border-radius-bottomright:6px}.pagination-mini ul>li:first-child>a, .pagination-small ul>li:first-child>a, .pagination-mini ul>li:first-child>span, .pagination-small ul>li:first-child>span {-webkit-border-bottom-left-radius:3px;border-bottom-left-radius:3px;-webkit-border-top-left-radius:3px;border-top-left-radius:3px;-moz-border-radius-bottomleft:3px;-moz-border-radius-topleft:3px}.pagination-mini ul>li:last-child>a, .pagination-small ul>li:last-child>a, .pagination-mini ul>li:last-child>span, .pagination-small ul>li:last-child>span {-webkit-border-top-right-radius:3px;border-top-right-radius:3px;-webkit-border-bottom-right-radius:3px;border-bottom-right-radius:3px;-moz-border-radius-topright:3px;-moz-border-radius-bottomright:3px}.pagination-small ul>li>a, .pagination-small ul>li>span {padding:2px 10px;font-size:11.9px}.pagination-mini ul>li>a, .pagination-mini ul>li>span {padding:0 6px;font-size:10.5px}.pager {margin:20px 0;text-align:center;list-style:none;*zoom:1}.pager:before, .pager:after {display:table;line-height:0;content:""}.pager:after {clear:both}.pager li {display:inline}.pager li>a, .pager li>span {display:inline-block;padding:5px 14px;background-color:#fff;border:1px solid #ddd;-webkit-border-radius:15px;-moz-border-radius:15px;border-radius:15px}.pager li>a:hover, .pager li>a:focus {text-decoration:none;background-color:#f5f5f5}.pager .next>a, .pager .next>span {float:right}.pager .previous>a, .pager .previous>span {float:left}.pager .disabled>a, .pager .disabled>a:hover, .pager .disabled>a:focus, .pager .disabled>span {color:#999;cursor:default;background-color:#fff}.modal-backdrop {position:fixed;top:0;right:0;bottom:0;left:0;z-index:1040;background-color:#000}.modal-backdrop.fade {opacity:0}.modal-backdrop, .modal-backdrop.fade.in {opacity:.8;filter:alpha(opacity=80)}.modal {position:fixed;top:10%;left:50%;z-index:1050;width:560px;margin-left:-280px;background-color:#fff;border:1px solid #999;border:1px solid rgba(0,0,0,0.3);*border:1px solid #999;-webkit-border-radius:6px;-moz-border-radius:6px;border-radius:6px;outline:0;-webkit-box-shadow:0 3px 7px rgba(0,0,0,0.3);-moz-box-shadow:0 3px 7px rgba(0,0,0,0.3);box-shadow:0 3px 7px rgba(0,0,0,0.3);-webkit-background-clip:padding-box;-moz-background-clip:padding-box;background-clip:padding-box}.modal.fade {top:-25%;-webkit-transition:opacity .3s linear, top .3s ease-out;-moz-transition:opacity .3s linear, top .3s ease-out;-o-transition:opacity .3s linear, top .3s ease-out;transition:opacity .3s linear, top .3s ease-out}.modal.fade.in {top:10%}.modal-header {padding:9px 15px;border-bottom:1px solid #eee}.modal-header .close {margin-top:2px}.modal-header h3 {margin:0;line-height:30px}.modal-body {position:relative;max-height:400px;padding:15px;overflow-y:auto}.modal-form {margin-bottom:0}.modal-footer {padding:14px 15px 15px;margin-bottom:0;text-align:right;background-color:#f5f5f5;border-top:1px solid #ddd;-webkit-border-radius:0 0 6px 6px;-moz-border-radius:0 0 6px 6px;border-radius:0 0 6px 6px;*zoom:1;-webkit-box-shadow:inset 0 1px 0 #fff;-moz-box-shadow:inset 0 1px 0 #fff;box-shadow:inset 0 1px 0 #fff}.modal-footer:before, .modal-footer:after {display:table;line-height:0;content:""}.modal-footer:after {clear:both}.modal-footer .btn+.btn {margin-bottom:0;margin-left:5px}.modal-footer .btn-group .btn+.btn {margin-left:-1px}.modal-footer .btn-block+.btn-block {margin-left:0}.tooltip {position:absolute;z-index:1030;display:block;font-size:11px;line-height:1.4;opacity:0;filter:alpha(opacity=0);visibility:visible}.tooltip.in {opacity:.8;filter:alpha(opacity=80)}.tooltip.top {padding:5px 0;margin-top:-3px}.tooltip.right {padding:0 5px;margin-left:3px}.tooltip.bottom {padding:5px 0;margin-top:3px}.tooltip.left {padding:0 5px;margin-left:-3px}.tooltip-inner {max-width:200px;padding:8px;color:#fff;text-align:center;text-decoration:none;background-color:#000;-webkit-border-radius:4px;-moz-border-radius:4px;border-radius:4px}.tooltip-arrow {position:absolute;width:0;height:0;border-color:transparent;border-style:solid}.tooltip.top .tooltip-arrow {bottom:0;left:50%;margin-left:-5px;border-top-color:#000;border-width:5px 5px 0}.tooltip.right .tooltip-arrow {top:50%;left:0;margin-top:-5px;border-right-color:#000;border-width:5px 5px 5px 0}.tooltip.left .tooltip-arrow {top:50%;right:0;margin-top:-5px;border-left-color:#000;border-width:5px 0 5px 5px}.tooltip.bottom .tooltip-arrow {top:0;left:50%;margin-left:-5px;border-bottom-color:#000;border-width:0 5px 5px}.popover {position:absolute;top:0;left:0;z-index:1010;display:none;min-width:200px;max-width:200px;padding:1px;text-align:left;white-space:normal;background-color:#fff;border:1px solid #ccc;border:1px solid rgba(0,0,0,0.2);-webkit-border-radius:6px;-moz-border-radius:6px;border-radius:6px;-webkit-box-shadow:0 5px 10px rgba(0,0,0,0.2);-moz-box-shadow:0 5px 10px rgba(0,0,0,0.2);box-shadow:0 5px 10px rgba(0,0,0,0.2);-webkit-background-clip:padding-box;-moz-background-clip:padding;background-clip:padding-box}.popover.top {margin-top:-10px}.popover.right {margin-left:10px}.popover.bottom {margin-top:10px}.popover.left {margin-left:-10px}.popover-title {padding:8px 14px;margin:0;font-size:14px;font-weight:normal;line-height:18px;background-color:#f7f7f7;border-bottom:1px solid #ebebeb;-webkit-border-radius:5px 5px 0 0;-moz-border-radius:5px 5px 0 0;border-radius:5px 5px 0 0}.popover-title:empty {display:none}.popover-content {padding:9px 14px}.popover .arrow, .popover .arrow:after {position:absolute;display:block;width:0;height:0;border-color:transparent;border-style:solid}.popover .arrow {border-width:11px}.popover .arrow:after {border-width:10px;content:""}.popover.top .arrow {bottom:-11px;left:50%;margin-left:-11px;border-top-color:#999;border-top-color:rgba(0,0,0,0.25);border-bottom-width:0}.popover.top .arrow:after {bottom:1px;margin-left:-10px;border-top-color:#fff;border-bottom-width:0}.popover.right .arrow {top:50%;left:-11px;margin-top:-11px;border-right-color:#999;border-right-color:rgba(0,0,0,0.25);border-left-width:0}.popover.right .arrow:after {bottom:-10px;left:1px;border-right-color:#fff;border-left-width:0}.popover.bottom .arrow {top:-11px;left:50%;margin-left:-11px;border-bottom-color:#999;border-bottom-color:rgba(0,0,0,0.25);border-top-width:0}.popover.bottom .arrow:after {top:1px;margin-left:-10px;border-bottom-color:#fff;border-top-width:0}.popover.left .arrow {top:50%;right:-11px;margin-top:-11px;border-left-color:#999;border-left-color:rgba(0,0,0,0.25);border-right-width:0}.popover.left .arrow:after {right:1px;bottom:-10px;border-left-color:#fff;border-right-width:0}.thumbnails {margin-left:-20px;list-style:none;*zoom:1}.thumbnails:before, .thumbnails:after {display:table;line-height:0;content:""}.thumbnails:after {clear:both}.row-fluid .thumbnails {margin-left:0}.thumbnails>li {float:left;margin-bottom:20px;margin-left:20px}.thumbnail {display:block;padding:4px;line-height:20px;border:1px solid #ddd;-webkit-border-radius:4px;-moz-border-radius:4px;border-radius:4px;-webkit-box-shadow:0 1px 3px rgba(0,0,0,0.055);-moz-box-shadow:0 1px 3px rgba(0,0,0,0.055);box-shadow:0 1px 3px rgba(0,0,0,0.055);-webkit-transition:all .2s ease-in-out;-moz-transition:all .2s ease-in-out;-o-transition:all .2s ease-in-out;transition:all .2s ease-in-out}a.thumbnail:hover, a.thumbnail:focus {border-color:#08c;-webkit-box-shadow:0 1px 4px rgba(0,105,214,0.25);-moz-box-shadow:0 1px 4px rgba(0,105,214,0.25);box-shadow:0 1px 4px rgba(0,105,214,0.25)}.thumbnail>img {display:block;max-width:100%;margin-right:auto;margin-left:auto}.thumbnail .caption {padding:9px;color:#555}.media, .media-body {overflow:hidden;*overflow:visible;zoom:1}.media, .media .media {margin-top:15px}.media:first-child {margin-top:0}.media-object {display:block}.media-heading {margin:0 0 5px}.media>.pull-left {margin-right:10px}.media>.pull-right {margin-left:10px}.media-list {margin-left:0;list-style:none}.label, .badge {display:inline-block;padding:2px 4px;font-size:11.844px;font-weight:bold;line-height:14px;color:#fff;text-shadow:0 -1px 0 rgba(0,0,0,0.25);white-space:nowrap;vertical-align:baseline;background-color:#999}.label {-webkit-border-radius:3px;-moz-border-radius:3px;border-radius:3px}.badge {padding-right:9px;padding-left:9px;-webkit-border-radius:9px;-moz-border-radius:9px;border-radius:9px}.label:empty, .badge:empty {display:none}a.label:hover, a.label:focus, a.badge:hover, a.badge:focus {color:#fff;text-decoration:none;cursor:pointer}.label-important, .badge-important {background-color:#b94a48}.label-important[href], .badge-important[href] {background-color:#953b39}.label-warning, .badge-warning {background-color:#f89406}.label-warning[href], .badge-warning[href] {background-color:#c67605}.label-success, .badge-success {background-color:#468847}.label-success[href], .badge-success[href] {background-color:#356635}.label-info, .badge-info {background-color:#3a87ad}.label-info[href], .badge-info[href] {background-color:#2d6987}.label-inverse, .badge-inverse {background-color:#333}.label-inverse[href], .badge-inverse[href] {background-color:#1a1a1a}.btn .label, .btn .badge {position:relative;top:-1px}.btn-mini .label, .btn-mini .badge {top:0}@-webkit-keyframes progress-bar-stripes {from {background-position:40px 0}to {background-position:0 0}}@-moz-keyframes progress-bar-stripes {from {background-position:40px 0}to {background-position:0 0}}@-ms-keyframes progress-bar-stripes {from {background-position:40px 0}to {background-position:0 0}}@-o-keyframes progress-bar-stripes {from {background-position:0 0}to {background-position:40px 0}}@keyframes progress-bar-stripes {from {background-position:40px 0}to {background-position:0 0}}.progress {height:20px;margin-bottom:20px;overflow:hidden;background-color:#f7f7f7;background-image:-moz-linear-gradient(top, #f5f5f5, #f9f9f9);background-image:-webkit-gradient(linear, 0 0, 0 100%, from(#f5f5f5), to(#f9f9f9));background-image:-webkit-linear-gradient(top, #f5f5f5, #f9f9f9);background-image:-o-linear-gradient(top, #f5f5f5, #f9f9f9);background-image:linear-gradient(to bottom, #f5f5f5, #f9f9f9);background-repeat:repeat-x;-webkit-border-radius:4px;-moz-border-radius:4px;border-radius:4px;filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#fff5f5f5', endColorstr='#fff9f9f9', GradientType=0);-webkit-box-shadow:inset 0 1px 2px rgba(0,0,0,0.1);-moz-box-shadow:inset 0 1px 2px rgba(0,0,0,0.1);box-shadow:inset 0 1px 2px rgba(0,0,0,0.1)}.progress .bar {float:left;width:0;height:100%;font-size:12px;color:#fff;text-align:center;text-shadow:0 -1px 0 rgba(0,0,0,0.25);background-color:#0e90d2;background-image:-moz-linear-gradient(top, #149bdf, #0480be);background-image:-webkit-gradient(linear, 0 0, 0 100%, from(#149bdf), to(#0480be));background-image:-webkit-linear-gradient(top, #149bdf, #0480be);background-image:-o-linear-gradient(top, #149bdf, #0480be);background-image:linear-gradient(to bottom, #149bdf, #0480be);background-repeat:repeat-x;filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#ff149bdf', endColorstr='#ff0480be', GradientType=0);-webkit-box-shadow:inset 0 -1px 0 rgba(0,0,0,0.15);-moz-box-shadow:inset 0 -1px 0 rgba(0,0,0,0.15);box-shadow:inset 0 -1px 0 rgba(0,0,0,0.15);-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;-webkit-transition:width .6s ease;-moz-transition:width .6s ease;-o-transition:width .6s ease;transition:width .6s ease}.progress .bar+.bar {-webkit-box-shadow:inset 1px 0 0 rgba(0,0,0,0.15), inset 0 -1px 0 rgba(0,0,0,0.15);-moz-box-shadow:inset 1px 0 0 rgba(0,0,0,0.15), inset 0 -1px 0 rgba(0,0,0,0.15);box-shadow:inset 1px 0 0 rgba(0,0,0,0.15), inset 0 -1px 0 rgba(0,0,0,0.15)}.progress-striped .bar {background-color:#149bdf;background-image:-webkit-gradient(linear, 0 100%, 100% 0, color-stop(0.25, rgba(255,255,255,0.15)), color-stop(0.25, transparent), color-stop(0.5, transparent), color-stop(0.5, rgba(255,255,255,0.15)), color-stop(0.75, rgba(255,255,255,0.15)), color-stop(0.75, transparent), to(transparent));background-image:-webkit-linear-gradient(45deg, rgba(255,255,255,0.15) 25%, transparent 25%, transparent 50%, rgba(255,255,255,0.15) 50%, rgba(255,255,255,0.15) 75%, transparent 75%, transparent);background-image:-moz-linear-gradient(45deg, rgba(255,255,255,0.15) 25%, transparent 25%, transparent 50%, rgba(255,255,255,0.15) 50%, rgba(255,255,255,0.15) 75%, transparent 75%, transparent);background-image:-o-linear-gradient(45deg, rgba(255,255,255,0.15) 25%, transparent 25%, transparent 50%, rgba(255,255,255,0.15) 50%, rgba(255,255,255,0.15) 75%, transparent 75%, transparent);background-image:linear-gradient(45deg, rgba(255,255,255,0.15) 25%, transparent 25%, transparent 50%, rgba(255,255,255,0.15) 50%, rgba(255,255,255,0.15) 75%, transparent 75%, transparent);-webkit-background-size:40px 40px;-moz-background-size:40px 40px;-o-background-size:40px 40px;background-size:40px 40px}.progress.active .bar {-webkit-animation:progress-bar-stripes 2s linear infinite;-moz-animation:progress-bar-stripes 2s linear infinite;-ms-animation:progress-bar-stripes 2s linear infinite;-o-animation:progress-bar-stripes 2s linear infinite;animation:progress-bar-stripes 2s linear infinite}.progress-danger .bar, .progress .bar-danger {background-color:#dd514c;background-image:-moz-linear-gradient(top, #ee5f5b, #c43c35);background-image:-webkit-gradient(linear, 0 0, 0 100%, from(#ee5f5b), to(#c43c35));background-image:-webkit-linear-gradient(top, #ee5f5b, #c43c35);background-image:-o-linear-gradient(top, #ee5f5b, #c43c35);background-image:linear-gradient(to bottom, #ee5f5b, #c43c35);background-repeat:repeat-x;filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#ffee5f5b', endColorstr='#ffc43c35', GradientType=0)}.progress-danger.progress-striped .bar, .progress-striped .bar-danger {background-color:#ee5f5b;background-image:-webkit-gradient(linear, 0 100%, 100% 0, color-stop(0.25, rgba(255,255,255,0.15)), color-stop(0.25, transparent), color-stop(0.5, transparent), color-stop(0.5, rgba(255,255,255,0.15)), color-stop(0.75, rgba(255,255,255,0.15)), color-stop(0.75, transparent), to(transparent));background-image:-webkit-linear-gradient(45deg, rgba(255,255,255,0.15) 25%, transparent 25%, transparent 50%, rgba(255,255,255,0.15) 50%, rgba(255,255,255,0.15) 75%, transparent 75%, transparent);background-image:-moz-linear-gradient(45deg, rgba(255,255,255,0.15) 25%, transparent 25%, transparent 50%, rgba(255,255,255,0.15) 50%, rgba(255,255,255,0.15) 75%, transparent 75%, transparent);background-image:-o-linear-gradient(45deg, rgba(255,255,255,0.15) 25%, transparent 25%, transparent 50%, rgba(255,255,255,0.15) 50%, rgba(255,255,255,0.15) 75%, transparent 75%, transparent);background-image:linear-gradient(45deg, rgba(255,255,255,0.15) 25%, transparent 25%, transparent 50%, rgba(255,255,255,0.15) 50%, rgba(255,255,255,0.15) 75%, transparent 75%, transparent)}.progress-success .bar, .progress .bar-success {background-color:#5eb95e;background-image:-moz-linear-gradient(top, #62c462, #57a957);background-image:-webkit-gradient(linear, 0 0, 0 100%, from(#62c462), to(#57a957));background-image:-webkit-linear-gradient(top, #62c462, #57a957);background-image:-o-linear-gradient(top, #62c462, #57a957);background-image:linear-gradient(to bottom, #62c462, #57a957);background-repeat:repeat-x;filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#ff62c462', endColorstr='#ff57a957', GradientType=0)}.progress-success.progress-striped .bar, .progress-striped .bar-success {background-color:#62c462;background-image:-webkit-gradient(linear, 0 100%, 100% 0, color-stop(0.25, rgba(255,255,255,0.15)), color-stop(0.25, transparent), color-stop(0.5, transparent), color-stop(0.5, rgba(255,255,255,0.15)), color-stop(0.75, rgba(255,255,255,0.15)), color-stop(0.75, transparent), to(transparent));background-image:-webkit-linear-gradient(45deg, rgba(255,255,255,0.15) 25%, transparent 25%, transparent 50%, rgba(255,255,255,0.15) 50%, rgba(255,255,255,0.15) 75%, transparent 75%, transparent);background-image:-moz-linear-gradient(45deg, rgba(255,255,255,0.15) 25%, transparent 25%, transparent 50%, rgba(255,255,255,0.15) 50%, rgba(255,255,255,0.15) 75%, transparent 75%, transparent);background-image:-o-linear-gradient(45deg, rgba(255,255,255,0.15) 25%, transparent 25%, transparent 50%, rgba(255,255,255,0.15) 50%, rgba(255,255,255,0.15) 75%, transparent 75%, transparent);background-image:linear-gradient(45deg, rgba(255,255,255,0.15) 25%, transparent 25%, transparent 50%, rgba(255,255,255,0.15) 50%, rgba(255,255,255,0.15) 75%, transparent 75%, transparent)}.progress-info .bar, .progress .bar-info {background-color:#4bb1cf;background-image:-moz-linear-gradient(top, #5bc0de, #339bb9);background-image:-webkit-gradient(linear, 0 0, 0 100%, from(#5bc0de), to(#339bb9));background-image:-webkit-linear-gradient(top, #5bc0de, #339bb9);background-image:-o-linear-gradient(top, #5bc0de, #339bb9);background-image:linear-gradient(to bottom, #5bc0de, #339bb9);background-repeat:repeat-x;filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#ff5bc0de', endColorstr='#ff339bb9', GradientType=0)}.progress-info.progress-striped .bar, .progress-striped .bar-info {background-color:#5bc0de;background-image:-webkit-gradient(linear, 0 100%, 100% 0, color-stop(0.25, rgba(255,255,255,0.15)), color-stop(0.25, transparent), color-stop(0.5, transparent), color-stop(0.5, rgba(255,255,255,0.15)), color-stop(0.75, rgba(255,255,255,0.15)), color-stop(0.75, transparent), to(transparent));background-image:-webkit-linear-gradient(45deg, rgba(255,255,255,0.15) 25%, transparent 25%, transparent 50%, rgba(255,255,255,0.15) 50%, rgba(255,255,255,0.15) 75%, transparent 75%, transparent);background-image:-moz-linear-gradient(45deg, rgba(255,255,255,0.15) 25%, transparent 25%, transparent 50%, rgba(255,255,255,0.15) 50%, rgba(255,255,255,0.15) 75%, transparent 75%, transparent);background-image:-o-linear-gradient(45deg, rgba(255,255,255,0.15) 25%, transparent 25%, transparent 50%, rgba(255,255,255,0.15) 50%, rgba(255,255,255,0.15) 75%, transparent 75%, transparent);background-image:linear-gradient(45deg, rgba(255,255,255,0.15) 25%, transparent 25%, transparent 50%, rgba(255,255,255,0.15) 50%, rgba(255,255,255,0.15) 75%, transparent 75%, transparent)}.progress-warning .bar, .progress .bar-warning {background-color:#faa732;background-image:-moz-linear-gradient(top, #fbb450, #f89406);background-image:-webkit-gradient(linear, 0 0, 0 100%, from(#fbb450), to(#f89406));background-image:-webkit-linear-gradient(top, #fbb450, #f89406);background-image:-o-linear-gradient(top, #fbb450, #f89406);background-image:linear-gradient(to bottom, #fbb450, #f89406);background-repeat:repeat-x;filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#fffbb450', endColorstr='#fff89406', GradientType=0)}.progress-warning.progress-striped .bar, .progress-striped .bar-warning {background-color:#fbb450;background-image:-webkit-gradient(linear, 0 100%, 100% 0, color-stop(0.25, rgba(255,255,255,0.15)), color-stop(0.25, transparent), color-stop(0.5, transparent), color-stop(0.5, rgba(255,255,255,0.15)), color-stop(0.75, rgba(255,255,255,0.15)), color-stop(0.75, transparent), to(transparent));background-image:-webkit-linear-gradient(45deg, rgba(255,255,255,0.15) 25%, transparent 25%, transparent 50%, rgba(255,255,255,0.15) 50%, rgba(255,255,255,0.15) 75%, transparent 75%, transparent);background-image:-moz-linear-gradient(45deg, rgba(255,255,255,0.15) 25%, transparent 25%, transparent 50%, rgba(255,255,255,0.15) 50%, rgba(255,255,255,0.15) 75%, transparent 75%, transparent);background-image:-o-linear-gradient(45deg, rgba(255,255,255,0.15) 25%, transparent 25%, transparent 50%, rgba(255,255,255,0.15) 50%, rgba(255,255,255,0.15) 75%, transparent 75%, transparent);background-image:linear-gradient(45deg, rgba(255,255,255,0.15) 25%, transparent 25%, transparent 50%, rgba(255,255,255,0.15) 50%, rgba(255,255,255,0.15) 75%, transparent 75%, transparent)}.accordion {margin-bottom:20px}.accordion-group {margin-bottom:2px;border:1px solid #e5e5e5;-webkit-border-radius:4px;-moz-border-radius:4px;border-radius:4px}.accordion-heading {border-bottom:0}.accordion-heading .accordion-toggle {display:block;padding:8px 15px}.accordion-toggle {cursor:pointer}.accordion-inner {padding:9px 15px;border-top:1px solid #e5e5e5}.carousel {position:relative;margin-bottom:20px;line-height:1}.carousel-inner {position:relative;width:100%;overflow:hidden}.carousel-inner>.item {position:relative;display:none;-webkit-transition:.6s ease-in-out left;-moz-transition:.6s ease-in-out left;-o-transition:.6s ease-in-out left;transition:.6s ease-in-out left}.carousel-inner>.item>img, .carousel-inner>.item>a>img {display:block;line-height:1}.carousel-inner>.active, .carousel-inner>.next, .carousel-inner>.prev {display:block}.carousel-inner>.active {left:0}.carousel-inner>.next, .carousel-inner>.prev {position:absolute;top:0;width:100%}.carousel-inner>.next {left:100%}.carousel-inner>.prev {left:-100%}.carousel-inner>.next.left, .carousel-inner>.prev.right {left:0}.carousel-inner>.active.left {left:-100%}.carousel-inner>.active.right {left:100%}.carousel-control {position:absolute;top:40%;left:15px;width:40px;height:40px;margin-top:-20px;font-size:60px;font-weight:100;line-height:30px;color:#fff;text-align:center;background:#222;border:3px solid #fff;-webkit-border-radius:23px;-moz-border-radius:23px;border-radius:23px;opacity:.5;filter:alpha(opacity=50)}.carousel-control.right {right:15px;left:auto}.carousel-control:hover, .carousel-control:focus {color:#fff;text-decoration:none;opacity:.9;filter:alpha(opacity=90)}.carousel-indicators {position:absolute;top:15px;right:15px;z-index:5;margin:0;list-style:none}.carousel-indicators li {display:block;float:left;width:10px;height:10px;margin-left:5px;text-indent:-999px;background-color:#ccc;background-color:rgba(255,255,255,0.25);border-radius:5px}.carousel-indicators .active {background-color:#fff}.carousel-caption {position:absolute;right:0;bottom:0;left:0;padding:15px;background:#333;background:rgba(0,0,0,0.75)}.carousel-caption h4, .carousel-caption p {line-height:20px;color:#fff}.carousel-caption h4 {margin:0 0 5px}.carousel-caption p {margin-bottom:0}.hero-unit {padding:60px;margin-bottom:30px;font-size:18px;font-weight:200;line-height:30px;color:inherit;background-color:#eee;-webkit-border-radius:6px;-moz-border-radius:6px;border-radius:6px}.hero-unit h1 {margin-bottom:0;font-size:60px;line-height:1;letter-spacing:-1px;color:inherit}.hero-unit li {line-height:30px}.pull-right {float:right}.pull-left {float:left}.hide {display:none}.show {display:block}.invisible {visibility:hidden}.affix {position:fixed}
    .dropdown-menu>li>a,body{font-size:12px;color:#666}#content,#header,#header h1{position:relative}#breadcrumb a,a,body{color:#666}*{outline:0!important;-moz-outline:none!important}body,html{height:100%}body{overflow-x:hidden;margin-top:-10px;font-family:'Open Sans',sans-serif}a:focus,a:hover{text-decoration:none;color:#28b779}.dropdown-menu .divider{margin:4px 0}.dropdown-menu{min-width:180px}.dropdown-menu>li>a{padding:3px 10px}.dropdown-menu>li>a i{padding-right:3px}.userphoto img{width:19px;height:19px}.alert,.btn,.btn-group>.btn:first-child,.btn-group>.btn:last-child,.btn-group>.dropdown-toggle,.dropdown-menu,.label,.progress,.table-bordered,.uneditable-input,.well,input[type=email],input[type=url],input[type=search],input[type=tel],input[type=color],input[type=text],input[type=password],input[type=datetime],input[type=datetime-local],input[type=date],input[type=month],input[type=time],input[type=week],input[type=number],select,textarea{border-radius:0}.btn,.uneditable-input,input[type=email],input[type=url],input[type=search],input[type=tel],input[type=color],input[type=text],input[type=password],input[type=datetime],input[type=datetime-local],input[type=date],input[type=month],input[type=time],input[type=week],input[type=number],textarea{box-shadow:none}.btn,.btn-primary,.progress,.progress .bar-danger,.progress .bar-info,.progress .bar-success,.progress .bar-warning,.progress-danger .bar,.progress-info .bar,.progress-success .bar,.progress-warning .bar{background-image:none}.accordion-heading h5{width:70%}.form-horizontal .form-actions{padding-left:20px}#footer{padding:10px;text-align:center}hr{border-top-color:#dadada}.carousel{margin-bottom:0}.fl{float:left}.fr{float:right}.badge-important,.label-important{background:#f74d4d}.bg_lb{background:#27a9e3}.bg_db{background:#2295c9}.bg_dg,.bg_lg{background:#28b779}.bg_ly{background:#ffb848}.bg_dy{background:#da9628}.bg_ls{background:#2255a4}.bg_lo{background:#da542e}.bg_lr{background:#f74d4d}.bg_lv{background:#603bbc}.bg_lh{background:#b6b3b3}#header{height:77px;width:100%;z-index:-9}#header h1{background:url(../img/logo.png) no-repeat;height:31px;left:20px;line-height:600px;overflow:hidden;top:26px;width:191px}#header h1 a{display:block}#content{background:#eee;padding-bottom:25px;min-height:100%;margin:0 auto;max-width:70%}#content-header{position:abslute;width:100%;margin-top:-38px;z-index:20}#content-header .btn-group,#content-header h1,.container-fluid .row-fluid:first-child{margin-top:20px}#content-header h1{color:#555;font-size:28px;font-weight:400;float:none;text-shadow:0 1px 0 #fff;margin-left:20px;position:relative}#content-header .btn-group{float:right;right:20px;position:absolute}#content-header .btn-group .btn{padding:11px 14px 9px}#content-header .btn-group .btn .label{position:absolute;top:-7px}#breadcrumb{background-color:#fff;border-bottom:1px solid #e3ebed}#breadcrumb a{padding:8px 20px 8px 10px;display:inline-block;background-image:url(../img/breadcrumb.png);background-position:center right;background-repeat:no-repeat;font-size:11px}#breadcrumb a:hover{color:#333}#breadcrumb a:last-child{background-image:none}#breadcrumb a.current{font-weight:700;color:#444}#breadcrumb a i{margin-right:5px;opacity:.6}#breadcrumb a:hover i{margin-right:5px;opacity:.8}.todo ul{list-style:none;margin:0}.todo li .by,.todo li .date{margin-right:10px}.todo li{border-bottom:1px solid #EBEBEB;margin-bottom:0;padding:10px 0}.todo li:hover{background:#FCFCFC;border-color:#D9D9D9}.todo li:last-child{border-bottom:0}.todo li .txt{float:left}.todo li .by{margin-left:10px}.quick-actions_homepage{width:100%;text-align:center;position:relative;float:left;margin-top:10px}.quick-actions,.quick-actions-horizontal,.stat-boxes,.stats-plain{display:block;list-style:none;margin:15px 0;text-align:center}.stat-boxes2{display:inline-block;list-style:none;margin:0;text-align:center}.stat-boxes2 li{display:inline-block;line-height:18px;margin:0 10px 10px;padding:0 10px;background:#fff;border:1px solid #DCDCDC}.stat-boxes2 li:hover{background:#f6f6f6}.stat-boxes .right,.stat-boxes2 .left{text-shadow:0 1px 0 #fff;float:left}.stat-boxes2 .left{border-right:1px solid #DCDCDC;box-shadow:1px 0 0 0 #FFF;font-size:10px;font-weight:700;margin-right:10px;padding:10px 14px 6px 4px}.stat-boxes2 .right{color:#666;font-size:12px;padding:9px 10px 7px 0;text-align:center;min-width:70px;float:left}.stat-boxes2 .left span,.stat-boxes2 .right strong{display:block}.stat-boxes2 .right strong{font-size:26px;margin-bottom:3px;margin-top:6px}.quick-actions_homepage .quick-actions li{position:relative}.quick-actions_homepage .quick-actions li .label{position:absolute;padding:5px;top:-10px;right:-5px}.stats-plain{width:100%}.quick-actions li,.quick-actions-horizontal li,.stat-boxes li{float:left;line-height:18px;margin:0 10px 10px 0;padding:0 10px}.quick-actions li a:hover,.quick-actions li:hover,.quick-actions-horizontal li a:hover,.quick-actions-horizontal li:hover,.stat-boxes li a:hover,.stat-boxes li:hover{background:#2E363F}.quick-actions li{min-width:14%;min-height:70px}.quick-actions_homepage .quick-actions .span3{width:30%}.quick-actions li,.quick-actions-horizontal li{padding:0}.stats-plain li{padding:0 30px;display:inline-block;margin:0 10px 20px}.quick-actions li a{padding:10px 30px}.stats-plain li h4{font-size:40px;margin-bottom:15px}.stats-plain li span{font-size:14px;color:#fff}.quick-actions-horizontal li a span{padding:10px 12px 10px 10px;display:inline-block}.quick-actions li a,.quick-actions-horizontal li a{display:block;color:#fff;font-size:14px;font-weight:lighter}.quick-actions li a i[class*=" icon-"],.quick-actions li a i[class^=icon-]{font-size:30px;display:block;margin:0 auto 5px}.quick-actions-horizontal li a i[class*=" icon-"],.quick-actions-horizontal li a i[class^=icon-]{background-repeat:no-repeat;background-attachment:scroll;background-position:center;background-color:transparent;width:16px;height:16px;display:inline-block;margin:-2px 10px 0 0!important;border-right:1px solid #ddd;padding:10px;vertical-align:middle}.quick-actions li:active,.quick-actions-horizontal li:active{background-image:-webkit-gradient(linear,0 0,0 100%,from(#EEE),to(#F4F4F4));background-image:-webkit-linear-gradient(top,#EEE 0,#F4F4F4 100%);background-image:-moz-linear-gradient(top,#EEE 0,#F4F4F4 100%);background-image:-ms-linear-gradient(top,#EEE 0,#F4F4F4 100%);background-image:-o-linear-gradient(top,#EEE 0,#F4F4F4 100%);background-image:linear-gradient(top,#EEE 0,#F4F4F4 100%);box-shadow:0 1px 4px 0 rgba(0,0,0,.2) inset,0 1px 0 rgba(255,255,255,.4)}.stat-boxes .left,.stat-boxes .right{text-shadow:0 1px 0 #fff;float:left}.stat-boxes .left{border-right:1px solid #DCDCDC;box-shadow:1px 0 0 0 #FFF;font-size:10px;font-weight:700;margin-right:10px;padding:10px 14px 6px 4px}.stat-boxes .right{color:#666;font-size:12px;padding:9px 10px 7px 0;text-align:center;min-width:70px}.stat-boxes .left span,.stat-boxes .right strong{display:block}.stat-boxes .right strong{font-size:26px;margin-bottom:3px;margin-top:6px},.stat-boxes .peity_bar_good,.stat-boxes .peity_line_good{color:#459D1C}.stat-boxes .peity_bar_neutral,.stat-boxes .peity_line_neutral{color:#757575}.stat-boxes .peity_bar_bad,.stat-boxes .peity_line_bad{color:#BA1E20}.site-stats{margin:0;padding:0;text-align:center;list-style:none}.site-stats li{cursor:pointer;display:inline-block;margin:0 5px 10px;text-align:center;width:42%;padding:10px 0;color:#fff;position:relative}#choices br,#tooltip{display:none}.site-stats li i{font-size:24px;clear:both;vertical-align:baseline}.site-stats li:hover{background:#2E363F}.site-stats li strong{font-weight:700;font-size:20px;width:100%;float:left;margin-left:0}.site-stats li small{margin-left:0;font-size:11px;width:100%;float:left}#donut,#interactive,#placeholder,#placeholder2,.bars,.chart,.pie{height:300px;max-width:100%}#choices{border-top:1px solid #dcdcdc;margin:10px 0;padding:10px}#choices input{margin:-5px 5px 0 0}#choices label{display:inline-block;padding-right:20px}#tooltip{position:absolute;border:none;padding:3px 8px;border-radius:3px;font-size:10px;background-color:#222;color:#fff;z-index:25}.widget-box{background:#F9F9F9;border-left:1px solid #CDCDCD;border-top:1px solid #CDCDCD;border-right:1px solid #CDCDCD;clear:both;margin-top:16px;margin-bottom:16px;position:relative}.widget-box.widget-calendar,.widget-box.widget-chat{overflow:hidden!important}.accordion .widget-box{margin-top:-2px;margin-bottom:0;border-radius:0}.widget-box.widget-plain{background:0 0;border:none;margin-top:0;margin-bottom:0}.modal-header,.table th,.widget-title,div.dataTables_wrapper .ui-widget-header{background:#efefef;border-bottom:1px solid #CDCDCD;height:36px}.widget-title .nav-tabs{border-bottom:0 none}.widget-title .nav-tabs li a{border-bottom:medium none!important;border-left:1px solid #DDD;border-radius:0;border-right:1px solid #DDD;border-top:medium none;color:#999;margin:0;outline:0;padding:9px 10px 8px;font-weight:700;text-shadow:0 1px 0 #FFF}.widget-title .nav-tabs li:first-child a{border-left:medium none!important}.widget-title .nav-tabs li a:hover{background-color:transparent!important;border-color:#D6D6D6;border-width:0 1px;color:#2b2b2b}.widget-title .nav-tabs li.active a{background-color:#F9F9F9!important;color:#444}.widget-title span.icon{padding:9px 10px 7px 11px;float:left;border-right:1px solid #dadada}.widget-title h5{color:#666;float:left;font-size:12px;font-weight:700;padding:12px;line-height:12px;margin:0}.widget-title .buttons{float:right;margin:8px 10px 0 0}.widget-title .label{padding:3px 5px 2px;float:right;margin:9px 11px 0 0;box-shadow:0 1px 2px rgba(0,0,0,.3) inset,0 1px 0 #fff}.widget-calendar .widget-title .label{margin-right:190px}.widget-content{padding:15px;border-bottom:1px solid #cdcdcd}.widget-box.widget-plain .widget-content{padding:12px 0 0}.widget-box.collapsible .collapse.in .widget-content{border-bottom:1px solid #CDCDCD}.recent-comments,.recent-posts,.recent-users{margin:0;padding:0}.article-post li,.recent-comments li,.recent-posts li,.recent-users li{border-bottom:1px dotted #AEBDC8;list-style:none;padding:10px}.recent-comments li.viewall,.recent-posts li.viewall,.recent-users li.viewall{padding:0}.recent-comments li.viewall a,.recent-posts li.viewall a,.recent-users li.viewall a{padding:5px;text-align:center;display:block;color:#888}.recent-comments li.viewall a:hover,.recent-posts li.viewall a:hover,.recent-users li.viewall a:hover{background-color:#eee}.recent-comments li:last-child,.recent-posts li:last-child,.recent-users li:last-child{border-bottom:none!important}.user-thumb{background:#FFF;float:left;height:40px;margin-right:10px;margin-top:5px;padding:2px;width:40px}.panel-right,.panel-right2{background-color:#fff;position:absolute}.user-info{color:#666;font-size:11px}.invoice-content{padding:20px}.invoice-action{margin-bottom:30px}.invoice-head{clear:both;margin-bottom:40px;overflow:hidden;width:auto}.invoice-meta{font-size:18px;margin-bottom:40px}.invoice-date{float:right;font-size:80%}.invoice-content h5{color:#333;font-size:16px;font-weight:400;margin-bottom:10px}.invoice-content ul{list-style:none;margin:0;padding:0}.invoice-to{float:left;width:370px}.invoice-from{float:right;width:300px}.invoice-from li,.invoice-to li{clear:left}.invoice-from li span,.invoice-to li span{display:block}.invoice-content th.total-label{text-align:right}.invoice-content th.total-amount{text-align:left}.amount-word{color:#666;margin-bottom:40px;margin-top:40px}.amount-word span{color:#5476A6;font-weight:700;padding-left:20px}.panel-left{margin-top:103px}.panel-left2{margin-left:176px}.panel-right{width:100%;border-bottom:1px solid #ddd;right:0;overflow:auto;top:38px;height:76px}.panel-right2{border-right:1px solid #ddd;left:0;overflow:auto;top:0;height:94%;width:175px}.panel-right .panel-title,.panel-right2 .panel-title{width:100%;background-color:#ececec;border-bottom:1px solid #ddd}.panel-right .panel-title h5,.panel-right2 .panel-title h5{font-size:12px;color:#777;text-shadow:0 1px 0 #fff;padding:6px 10px 5px;margin:0}.panel-right .panel-content{padding:10px}.chat-content{height:470px;padding:15px}.chat-messages{height:420px;overflow:auto;position:relative}.chat-message{padding:7px 15px;margin:7px 0 0}.chat-message input[type=text]{margin-bottom:0!important;width:100%}.chat-message .input-box{display:block;margin-right:90px}.chat-message button{float:right}#chat-messages-inner p{padding:0;margin:10px 0 0}#chat-messages-inner p img{display:inline-block;float:left;vertical-align:middle;width:28px;height:28px;margin-top:-1px;margin-right:10px}#chat-messages-inner .msg-block,#chat-messages-inner p.offline span{background:#FFF;border:1px solid #ccc;box-shadow:1px 1px 0 1px rgba(0,0,0,.05);display:block;margin-left:0;padding:10px;position:relative}#chat-messages-inner p.offline span{background:#FFF5F5}#chat-messages-inner .time{color:#999;font-size:11px;float:right}#chat-messages-inner .msg{display:block;margin-top:13px;border-top:1px solid #dadada}#chat-messages-inner .msg-block:before{border-right:7px solid rgba(0,0,0,.1);border-top:7px solid transparent;border-bottom:7px solid transparent;content:"";display:none;left:-7px;position:absolute;top:11px}#chat-messages-inner .msg-block:after{border-right:6px solid #fff;border-top:6px solid transparent;border-bottom:6px solid transparent;content:"";display:none;left:-6px;position:absolute;top:12px}.chat-users{padding:0 0 30px}.chat-users .contact-list{line-height:21px;list-style:none;margin:0;padding:0;font-size:10px}.chat-users .contact-list li{border:1px solid #DADADA;margin:5px;padding:1px;position:relative}.chat-users .contact-list li:hover{background-color:#efefef}.chat-users .contact-list li a{color:#666;display:block;padding:8px 5px}.chat-users .contact-list li.online a{font-weight:700}.chat-users .contact-list li.new{background-color:#eaeaea}.chat-users .contact-list li.offline{background-color:#EDE0E0}.chat-users .contact-list li a img{display:inline-block;margin-right:10px;vertical-align:middle;width:28px;height:28px}.chat-users .contact-list li .msg-count{padding:3px 5px;position:absolute;right:10px;top:12px}.taskDesc i{margin:1px 5px 0}.taskOptions,.taskStatus{text-align:center!important}.taskStatus .in-progress{color:#64909E}.taskStatus .pending{color:#AC6363}.taskStatus .done{color:#75B468}.activity-list{list-style:none;margin:0}.activity-list li{border-bottom:1px solid #EEE;display:block}.activity-list li:last-child{border-bottom:medium none}.activity-list li a{display:block;padding:7px 10px}.activity-list li a:hover{background-color:#FBFBFB}.activity-list li a span{color:#AAA;font-size:11px;font-style:italic}.activity-list li a i{margin-right:10px;opacity:.6;vertical-align:middle}.new-update{border-top:1px solid #DDD;padding:10px 12px}.new-update:first-child{border-top:medium none}.new-update span{display:block}.new-update i{float:left;margin-top:3px;margin-right:13px}.new-update .update-date{color:#BBB;float:right;margin:4px -2px 0 0;text-align:center;width:30px}.new-update .update-date .update-day{display:block;font-size:20px;font-weight:700;margin-bottom:-4px}.update-alert,.update-done,.update-notice{display:block;float:left;max-width:76%}tr:hover{background:#f6f6f6}span.icon .checker{margin-top:-5px;margin-right:0}.dataTables_length{color:#878787;margin:7px 5px 0;position:relative;left:5px;width:50%;top:-2px}.dataTables_length div{vertical-align:middle}.dataTables_paginate{line-height:16px;text-align:right;margin-top:5px;margin-right:10px}.dataTables_paginate .ui-button,.pagination.alternate li a{font-size:12px;padding:4px 10px!important;border-style:solid;border-width:1px;border-color:#ddd #ddd #ccc;border-color:rgba(0,0,0,.1) rgba(0,0,0,.1) rgba(0,0,0,.25);display:inline-block;line-height:16px;background:#f5f5f5;color:#333;text-shadow:0 1px 0 #fff}.dataTables_paginate .ui-button:hover,.pagination.alternate li a:hover{background:#e8e8e8;color:#222;text-shadow:0 1px 0 #fff;cursor:pointer}.dataTables_paginate .first{border-radius:4px 0 0 4px}.dataTables_paginate .last{border-radius:0 4px 4px 0}.dataTables_paginate .ui-state-disabled,.fc-state-disabled,.pagination.alternate li.disabled a{color:#AAA!important}.dataTables_paginate .ui-state-disabled:hover,.fc-state-disabled:hover,.pagination.alternate li.disabled a:hover{background:#f5f5f5;cursor:default!important}.dataTables_paginate span .ui-state-disabled,.pagination.alternate li.active a{background:#41BEDD!important;color:#fff!important;cursor:default!important}div.dataTables_wrapper .ui-widget-header{border-right:medium none;border-top:1px solid #D5D5D5;font-weight:400;margin-top:-1px}.dataTables_wrapper .ui-toolbar{padding:5px}.dataTables_filter{color:#878787;font-size:11px;right:0;top:37px;margin:4px 8px 2px 10px;position:absolute;text-align:left}.dataTables_filter input,.nopadding .table{margin-bottom:0}.table th{height:auto;font-size:10px;padding:5px 10px 2px;border-bottom:0;text-align:center;color:#666}.table.with-check tr td:first-child,.table.with-check tr th:first-child{width:10px}.table.with-check tr th:first-child i{margin-top:-2px;opacity:.6}.table.with-check tr td:first-child .checker{margin-right:0}.table tr.checked td{background-color:#FFFFE3!important}.nopadding{padding:0!important}.nopadding .table-bordered{border:0}.thumbnails{margin-left:-2.12766%!important}.thumbnails [class*=span]{margin-left:2.12766%!important;position:relative}.thumbnails .actions{width:auto;height:16px;background-color:#000;padding:4px 8px 8px;position:absolute;bottom:0;left:50%;margin-top:-13px;margin-left:-24px;opacity:0;top:10%;transition:1s ease-out;-moz-transition:opacity .3s ease-in-out}.thumbnails li:hover .actions,.thumbnails li:hover .actions a:hover{opacity:1;color:#fff;top:50%;transition:1s ease-out}.line{background:url(../img/line.png) repeat-x;display:block;height:8px}.modal{z-index:99999!important}.modal-backdrop{z-index:999!important}.modal-header{height:auto;padding:8px 15px 5px}.modal-header h3{font-size:12px;text-shadow:0 1px 0 #fff}.notify-ui ul{list-style:none;margin:0;padding:0}.notify-ui li{background:#eee;margin-bottom:5px;padding:5px 10px;text-align:center;border:1px solid #ddd}.notify-ui li:hover{cursor:pointer;color:#777}form{margin-bottom:0}.form-horizontal .control-group{border-top:1px solid #fff;border-bottom:1px solid #eee;margin-bottom:0}.form-horizontal .control-group:last-child{border-bottom:0}.form-horizontal .control-label{padding-top:15px;width:180px}.form-horizontal .controls{margin-left:200px;padding:10px 0}.row-fluid .span20{width:97.8%}#imgbox,#lightbox{left:0;top:0;width:100%;height:100%;background:url(overlay.png) #000;text-align:center}.form-horizontal .form-actions{margin-top:0;margin-bottom:0}.help-block,.help-inline{color:#999}#lightbox{position:fixed;z-index:9999}#lightbox p{position:absolute;top:10px;right:10px;width:22px;height:22px;cursor:pointer;z-index:22;border:1px solid #fff;border-radius:100%;padding:2px;text-align:center;transition:.5s}#lightbox p:hover{transform:rotate(180deg)}#imgbox{position:absolute;z-index:21}#imgbox img{margin-top:100px;border:10px solid #fff}.error_ex{text-align:center}.error_ex h1{font-size:140px;font-weight:700;padding:50px 0;color:#28B779}#sidebar .content{padding:10px;position:relative;color:#939DA8}#sidebar .percent{font-weight:700;position:absolute;right:10px;top:25px}#sidebar .progress{margin-bottom:2px;margin-top:2px;width:70%}#sidebar .progress-mini{height:6px}#sidebar .stat{font-size:11px}.btn-icon-pg ul{margin:0;padding:0}.btn-icon-pg ul li{margin:5px;padding:5px;list-style:none;display:inline-block;border:1px solid #dadada;min-width:187px;cursor:pointer}.btn-icon-pg ul li:hover i{transition:.3s;-moz-transition:.3s;-webkit-transition:.3s;-o-transition:.3s;margin-left:8px}.accordion{margin-top:16px}.fix_hgt{height:115px;overflow-x:auto}.input-append .add-on:last-child,.input-append .btn:last-child{border-radius:0;padding:6px 5px 2px}.input-append input,.input-append input[class*=span],.input-prepend input,.input-prepend input[class*=span]{width:none}.input-append input,.input-append select,.input-prepend input,.input-prepend span{border-radius:0!important}.bs-docs-tooltip-examples{list-style:none;margin:0 0 10px;position:relative;text-align:center}.bs-docs-tooltip-examples li{display:inline;padding:0 10px;list-style:none;position:relative}@media (max-width:480px){#content,#content-header,#user-nav>ul{margin-top:0}#user-nav,#user-nav>ul{width:100%;position:relative}#content-header,.form-actions{text-align:center}#header h1{top:10px;left:5px;margin:3px auto}#user-nav{left:auto;right:auto;margin-top:-31px;border-top:1px solid #363E48;margin-bottom:0;background:#2E363F;float:right}#content-header .btn-group,#content-header h1,.navbar>.nav{float:none}#my_menu,#sidebar .content{display:none}#my_menu_input{display:block}#user-nav>ul{right:0;margin-left:20%!important;background:#000}#user-nav>ul>li{padding:0}#user-nav>ul>li>a{padding:5px 10px}#content{margin-left:0!important;border-top-left-radius:0}#content-header h1{display:block;text-align:center;margin-left:auto;margin-top:0;padding-top:15px;width:100%}#content-header .btn-group{margin-top:70px;margin-bottom:0;margin-right:0;left:30%}.widget-title .buttons>.btn{width:11px;white-space:nowrap;overflow:hidden}.form-horizontal .control-label{padding-left:30px}.form-horizontal .controls{margin-left:0;padding:10px 30px}.panel-right2{background-color:#fff;border-right:1px solid #ddd;position:relative;left:0;overflow:auto;top:0;height:87%;width:100%}.panel-left2{margin-left:0}.dataTables_paginate .ui-button,.pagination.alternate li a{padding:4px!important}.table th{padding:5px 4px 2px}}@media (min-width:481px) and (max-width:1024px){#content{max-width:100%} .quick-actions li{width: calc( ( 100%  / 5 )- 10px);}}@media (min-width:481px) and (max-width:970px){body{background:#49CCED}#header h1{top:10px;left:35px}#search{top:5px}#my_menu{display:none}#my_menu_input{display:block}#content{margin-top:0}@media (max-width:600px){.widget-title .buttons{float:left}.panel-left{margin-right:0}.panel-right{border-top:1px solid #DDD;border-left:none;position:relative;top:auto;right:auto;height:auto;width:auto}#sidebar .content{display:none}}@media (max-width:767px){body{padding:0!important}.container-fluid{padding-left:20px;padding-right:20px}#sidebar .content{display:none}}@media (min-width:768px) and (max-width:979px){.row-fluid [class*=span],[class*=span]{display:block;float:none;margin-left:0;width:auto}}@media (max-width:979px){div.dataTables_wrapper .ui-widget-header{height:68px}.dataTables_filter{position:relative;top:0}.dataTables_length{width:100%;text-align:center}.dataTables_filter,.dataTables_paginate{text-align:center}#sidebar .content{display:none}}}
    body{background:#EEE}#header{background-color:#1f262d;margin-top:10px}.dropdown-menu .active a,.dropdown-menu .active a:hover,.dropdown-menu li a:hover{color:#eee;background:#666}.top_message{float:right;padding:20px 12px;position:relative;top:-13px;border-left:1px solid #3D3A37;font-size:14px;line-height:20px;color:#333;text-align:center;vertical-align:middle;cursor:pointer}.top_message:hover{background:#000}.rightzero{right:0;display:none}@media (max-width:480px){#sidebar>a{background:#27a9e3}.quick-actions_homepage .quick-actions li{min-width:100%}#user-nav{margin:0 auto;top:-31px;border-top:none;width:250px;left:-3%;text-align:center;float:none}#user-nav>ul>li:last-child{border-right:1px solid #363E48}#user-nav>ul>li{border-top:1px solid #363E48}#sidebar{top:-30px;clear:both}#content{margin-top:-30px}}div.tagsinput div,div.tagsinput span.tag{display:block;float:left}@media (min-width:768px) and (max-width:970px){.quick-actions_homepage .quick-actions li{min-width:20.5%}}@media (min-width:481px) and (max-width:767px){#sidebar>ul ul:after{border-right:6px solid #222}.quick-actions_homepage .quick-actions li{min-width:47%}}div.tagsinput{border:1px solid #CCC;background:#FFF;padding:5px;width:300px;height:100px;overflow-y:auto}div.tagsinput span.tag{border:1px solid #a5d24a;-moz-border-radius:2px;-webkit-border-radius:2px;padding:5px;text-decoration:none;background:#cde69c;color:#638421;margin-right:5px;margin-bottom:5px;font-family:helvetica;font-size:13px}div.tagsinput span.tag a{font-weight:700;color:#82ad2b;text-decoration:none;font-size:11px}div.tagsinput input{width:80px;margin:0 5px 5px 0;font-family:helvetica;font-size:13px;border:1px solid transparent;padding:5px;background:0 0;color:#000;outline:0}.tags_clear{clear:both;width:100%;height:0}.not_valid{background:#FBD8DB!important;color:#90111A!important}
 	 @media (min-width:700px) {
     .quick-actions li{width: calc( ( 100%  / 5 ) - 10px); }
    }
  </style>

  <style>
img.WireSharkIcon {
	height:40px;
    width:40px;
    content: url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAQAAAAEACAYAAABccqhmAABWdElEQVR42u1dB2DU1Bv/krvrLi1Q9ipQNpQyZKoMmQ4ciKKgDP8yZIiKC/cAEUREhigKyhAERYayh6Ds3bIpu0ChjEJ3b+Sfl0tyL7kkl7tc2xv5aUiay13W+37vW+97BOgIJBDFdB6qpG9Uh3dQXA1Gh2sE27vQScQHEGyNrqTgK89Z7XX4inD6ynUELHylYQYCvPUsCejTh2jU8c1yoSHh5SGUjCMpQ7QNIJow2EqBzRBjA1s0EGQ0ZbNFkySUooAwEAREA0UYCSCigKCM9M9E0d8x0SIUpeliCCKLAMpCy2I2/fsWCqgs+vetFAVov9VGwV2CIOnPbFkkkFlAWu9SFvIuSUC2jbBmma3WDFtufsaxEa1usD/pLaHWycEL0AnAfXj6zIj4j+eFlKmaWNVmCKlKkqaq9C9VpgiiGt2WK9KCW4GWtrK0MMeRQNFrMJT0jXr5oVltQNykSeMWUNRN+l6vUxR1nbBRl2hRvkqRhWmW7ILLWTeOXr3w8aBC8FzAdWJwAzoBuIY7z4hoMXFjtLVCXF2CCEsgSbKOlbLWIQ1kbQrIWnTjr1BUF0mxzZ6S+8DjuxfePiG928u3QtwgwHaW1nDOkhR5xkZRqWDNOxOSk356z+iHs8E9IdcJQQE6AThD3TPp04dM7PF+gpEwJVFGoiktEIl0p92Ebm/VPTkpklOK3UBrSrSf+wwkPivph8WRAf/g6B2ExGcE/pnnLe8ibW6k0DeeTK+PWAvNh49sHH8Wli2zqfx+ST8yn4JOAOqeAdFqxqbSlsiK7QiSbE+Rhna09CXRrTha7UlsSFhZ4bZhwm7zASEuKaAHT7JswG3b/yTs+9WCorLofw8TlHWHxUrusGTe2Xns9XaZoO7RBuvjZxCsBODqvhmBN0dW7gok+RBJEG0pAhrQ+0mlL1GskKOuiBP4YBZwreBJgSUE9PDRtgrtwUZQcIKibDspG7XFlHN1496RXe6A61cRdK8qmAhA6V4ZB11MfJsH6UbWDUjDwwRBNVD6MSTsNlbAGYG3UcHXekoIDDHQjIAIgSEGFaRAUcQJsFnX0O9qw90Lu7ercDQGxesMdAIgXH2WNDelDWEKeY7+42n6z4pyByMBR4JupXRh90VwpGBgNQVS2Ya4Rmtny6wFBYuTX266l90XlGQQqARAKO1v+sPhBobQ0Ofpnv5Z+t3WljrQxvbwSOBRsFuH/8HAEYKiT4E4Q1CW36iCvF8PvtziFLtT7oUHXEMIJAJQ7O0TPl4TFR1fqx8QtiEESSZJHYR6dgum2usIHHCmgpGQ1w4om+0wUOQPuddPLTz19uM5EARaQSAQgGJvn/jTvjrGkMjh9GvvT/9ZVnyQje3hLbpaHzRA8o9SJ5GGIMkFFJFBUZYFVF7h7MPDmp3l9sr8nF83G38mAGXBn3OklSHM9CH9inuIj0UOPCTwVr2nD3ogAjAypoKkIxF5ftZbzeZPkgc33cfuCygi8EcCUBb8uUeSjMaQ9+i3+QSIwnZIxTfrNr0OGSCNwCRtItgoG7WcKsgff3hIs2R2X0AQgT8RgKLgN5m1L94QETmDJMme4gOQwJttem+vQx2Q/JtIu4kghs1mW2vLyhqRPKr1RXaXXxOBvxAAIbev8serwyrG1xoHpOE1+s8I/ACk5putum2vwzNw5oHRmQhywWadmn7h3ISrHz+Wz+6TamY+3/R8nQAIpX20ut/RYAiZTav7dfAD9B5fhzchpxEQFJykqMKhhwYm/oft9isi8GUCIOT+Tvr4z1JUjbpTSAM5CN+PbPxCXfB1FBGQ/IegyIGQCCib1TaPuHj6jcMfP3kP3y/6uk+2Sl8kAKVen2g292AnMIT9RPf6/Kg75NVHgq8793QUB5AmgIhAGDWgLlot1ODkwY22ATZgU+LrPtVIfY0ACIW/iWbzkt8Eo+kz+hEauZ1I6At1O19HMQM1zBCDyCwgwEKZLe8eHtxkKggF3We1AV8iADnhJ+pP2BQbVqXyAoIgeA8/0+tbbUyqrg4dJQVJbYCilpPXjg4+8M4zePESnyQBXyAARZW/wXc7GoRFlF6OO/qQrV+g9/o6fASosYYaRL4BijqWl3XrqZMjH0CZhD5rEpQ0Acj2+uifxrP3NDNGlPqL/oMvpWVhVX4dOnwNyCQwCkkg3Zab8/CR4feh5CGfNAlKkgAUhT9pzqHuEBK2lP4jEv2tO/p0+APEJgGtp+ZYLfl9Ul5qvgG4XcI1yPxdLCgpAlASfiLxh4NPG0LDfqa3Q5knQz+aAtre12Vfhz8AKQGhBtJBArTFSuXnDTgyrMUfAMKSj6KvFnsLLwkCUBT+JrP3PWIMj1xKb4cwT0QXfh1+CDEJAPJZZ2c/nTyi1VrwIRIobgJQFP6kObs7kSGlVtFPIAztREJfYLHpzj4dfgnGOWgk+SHHtGGQZ8nLeix5WCsuV6DESaA4CUBZ7Z+1uyUZEb2BYCvtop4/Xxd+HX4O1LjDjAJN4F7h3cyux0a3Owg+QALFRQCKwl9v8vpqEeWq7aD/qsTcua726wggOJsDVFr+5Qv3n3j/kStQwiRQHASgKPzl+nwcVu2RPtsogmjO3LEu/DoCEE4kQNn2X938c8frC74qgBIkgaImAEXhR0vSvOSZBGl8mTugwGKv1KNDR6ABVR0KNTpEwmo2z0r+X9Mx4CCAYieB4iQAJ+FPnHPwRUNI6I/cASjBx6J3/ToCGChRCCUMcbDl5bxwZFir30CZBPySABSFv8GkDfFh5avsp7dLoQ+sbHqvDh2BjlBsEBGt7N7NTT/f/PS7j16GEiCBoiIAReGHhAQyadyfmwiSvB99oIf7dAQTxOFBoKzbD41/qhukpqK5Z4qVBIqCAKTsfsHS9Pv9o8jQ8K+4A1C4T9f8dQQTkPCj8CAHS17+qJThLX4AIQHgRMDBq5JS1ARAgFADIBtOXFcnpGK1PVyOP6rZZ9alX0cQApUZMxk4U4DKKkg73+LEB4+hYqOcJgDgTAI+TQBKqj8zuWvS3OTlBGl4GH1gY5N9dOgIVoRhpgBlsa48/L/EZ8FBAEVuCniTAJTtfpoAEmf+280QWWYVd5Cu+usIdjiZAtl3HkkZef9mKCYSKEoCEC6VWxibfT5vL5CGRsyN6uP6dehgIKgjYLOmHHp/UBu4esACyv4AnyIAl6p/0+/2DCTDI2czV67n+evQwUM8XsCWl/3SkeFtFoFdyHFNAMDLJOANAnCp+pfu0icsvv9HqCoKU8lXT/jRoUMI3CEIlO3C5YnDE2+e2lEIRWwKFAUBiBcy6fvdw4jQKFQpVXf86dAhA4FDsCB75OGhbX4CZwLwqimglQBcqv5x9dqHVHtrdgp9ZzXQhyjbTy/rpUOHM1B2YCimBRya8lwTOHbMDEVoCnibAAQ9P1qazNzV3xgRjRIc9N5fh9dRJswI1UqFQNVo+1Ix0gh5FgquZBXCiVt5cPB6bklfolvAtQBL1t1BKaPbo3ECNpDWBDiUCAHI9f4kv65QwdBswsa9QJIN0Ye67a/DEyCBqF4qFOqUDoWasaGQUDoMatPrmjGhEGEiFb+bnJELk3anw/70nJK+DVUQDBairIcPDW7aHoQEIE4SAolt1fAmATj1/o2+2dI1JKb8Subq6MvL03t/HSpQKcoEieUiILF8ODSJC4dG9HakC0FXAjI5v9x9DeYfvQmi+bx8EuFYRKDwTnrPY693QSXEikQL8PRpuHT8oSXph0OLCJPxCXSQnvKrQwrI7m1YNhyaVYhgluYVIqFCpKlIzvX65ouw5mym/Q8fJgI8ImAzFy49MqQFmgRXrAUAeIEEvE0AvPpfY+TkimVa9DgJbHXfPLMe99dhB1Lh21WJYpb7KkVp6t3dQS7dBrsuOQG38iyOnT5IBOiKwtlnQm/n392+su7Zee/T6ouTGVAiBKCm9zckztw1xhAR9Tk6SB/rH9xAjq22laOgc41S0LF6NJSLKJoeXg1mHkyH6fuvC3f6IAkIagbkZr99eETbmfSmFbwcFvQmAXC9vwFtN/vx0F4wGBugg/QyX8GH6BADdKEFvmvNGKanx/PdSxKpd/Lh0WUn2b/wmX19iwQE5cOsluRD/2uGnIE4AXhFC3D3ruWG+jo8//S11/14ebOIGnX+Za5Gd/4FDZDzqmO1aHgkIRYerFbKEdP2ISBtNGluMjPNnB2+SwK4MzDn3NE2Zz577ihIkwCAdGTAJbxBAMLQH00ATWfu+pQIj3oNHaQ7/wIb6KUj9f7ZhmWgU40YnxR6MZr9eARyLZTjBnyUBATOwNycyckj23wKDgLwSkTAUwKQjvuz6n/Sj8mHgCRqowP0Ib+BiQhaPX2ybhl4tkFZqFc2rKQvRzVQHkqj2QeBItAEniQmAdz0Pb5DAIKhwlbr6cMvJ7UEu+B7TQtw525dxv0Bqf8fLG4WUbPRduYqdPU/4IAScAY0KQePJZR2mYTji9h3NQv6/XmCbrUkI+wOEvBNLQA3A7LPHGqX+sWAFPCiFuAJAUjl/PMEkDhj10dkROQb6ABd/Q8cNC0XDgMTy0P3mjG8d9of8dm287Ag5YZdyJHwkySas09IAj5EALgZYM3Jnpgyqt0EEBKA3BiBIiUAqd6fIYCkOYd2gsFe9ENX//0boXTje6JuaXiuURw0KBte0pejGblmKzzw4z64Z7bRsm9ghN+hCYj6Nx8hAdwMoCyWI0eGNH8Q5AlAaXIRSai9SyXnH7/ED5lQNbbNo8fRZ7r6778g6bbzcO3SMLplRagRE1rSl+M1/JZ8Fd7bnMoLP7P2Ay0AMwNsN3csr5f208fp4PAFaAoJuksASs4/Q+Mpm/sZS5ebhQ7Q1X8/BM3aHaqXgtdaVQqIHl+MnnN3w5k7+XRLNTDCj2sBTr4AHyIAvGSY+U7GkGNvPIRGCFrBC85ATwhA0vmHlsTZe34kQ8KfRgfqyT9+BPo99agVCyNaVIS6ZfzHo+8O9l66Dc8t3m8XfoOJFX5EAqRDC8DNAB8iADwpyFqQtzhleOvhICQAj80ANXepRv03QEyMMenrbafoh1kOHYjyrnX4OGjBr0/39K/TPT7q+QMZb60+Ar8fvUYLv5FfOE3AH8wALuJC29bph15/sCHcvYsGNCj5AjgokoA7BCCl/hvYbUPCW3MaRtVrtQsdoBf+8HHQgl8m3AjvtK0CveqWKZY54ksS9/LN0Prr9VBA0SJuNNmFH/X+iAj8xAzAC4XkHt3Z+vTU4SifmSMAsS8AQKUW4C4ByKr/TaZueslQqtwUdKBu//soaMFHjah/43IwqmUlKBVqKOkrKhb8uv8cvLcmGev9TXSLNfoVAeDhwMI7N0cfH/vQfPCCGeDqDtWp/4gApu+eZYgIfw4dqNv/Pgb2XcTHhMLHD1aDtlWiS/qKVCGn0AyRIdpHDj45ZwscvnrPLvBGpPqbHGTgJwSA+wFsBXkLkl9pMxqcCcBtM0AtASh6/9GS9OOhPfRDrIMO0u1/HwIt/Gi8/ZhWlaFfozifTuK5dDsL/jtzDf49fQX2X7gBjyXFw4ePtdb0myfSM6HnrI12x58fEwAC5wegLOZTR4a2bAsOAvA4GuAOAciq/1X7vFYursfA0+gz3f73EbC9fuNyETClSzzT+/saUA//z8krsCP1Guw+mw6pN+4IPl816jFIql5O0zk+X3sIftyVaicAo/+aAAh4PsC1FdMTrq/+8TZoNAOU7lC1+t/g0+U9QqvUXowO1Kf8KmGwgo86+v8lVYBX76vkmHbKB3A3rxA2Hb8Ea1Muwna6p883W4QHsE2nVrkY+Oft3prOZbHaoNWklXA738oLvl0LYAkAzwVA23gUwMeEHwHPB8i7ntrn1Ljem0CjGeAuAUiq/02+2fa2ITr2bXSgXve/BMEKf6WoEJjUuQa0qhxV0lfE4HZOPqyjBX7t0YtMb2+xWmWu37H5evfmMKZrkqbzbjl1FQYv+JcN+Rn5CIBdA8CiAFJhQB8kAHzeANu9zPHJr3VATnexGeB1ApAq+oFrAMbE2Xt/Jk0hj6GD9Pz/EgIr/N1rxcLnHWqUuIcfdQLbTqXBb/vO0D3+ZTDLCb0Mtr/9NMTHactNGLNsJ6xISbP39Fyvj9n/wGkASNj5RCDfVP8R8HEB1oLcFSmvtHuJ3pTLB8ALh+JrAdQSgKz9Ty/GpDkHdtMPM0HP/y8BsIKPGsaH91eD3vXLlujlnL95D5bSQv/7/lS4fk+pFr9802tcpQysGfO4puvIN1uhxcQ/IcdM8Wo/rwn4of3PgfMDUBYLcgSiMmEcAXjkB5C7S9X2f3jTtpH1Rn93gd426g7AYgYr/BUjTTCzR23G4VcSsNHXseHYJZj773HYc/4aUGqaGiH3BwVv9WgBIzsnarqmNUcvwStLdgqFHicBNhHI3vuL7H9m5ZsEgCUEmU9PGhSfe+oQmvrIYz+AOwQgaf/Xemdui1IJzTeiA3UHYDEBk7DWVaLh2241ITbMWOyXgRx6SOgX7T4FNyR7e8J5kxUsQk7Q6Hv7753eUK2MNv/FkIX/woZT17C0X1z958YBGP1G/eeAOwKzju/pfPbroYdBORyoiQCU7H/U4gwNJq57ITSu4jfoIH3qr2IAJvyP1y0Dn3WsDqGG4q3MczMrD37ZeRIW0MvtnDyJIwjBihlvLxAuwokQONSvEAPrxzym6fpyCy3QfMJyyLcRQu8/rg34ofqPgE8dZr6ZPvLYOz1Q9M1jP4AaAlC0/xt/88+nxqjYV9DBegZgEYN9tigr7PU2lZkwX3ECefNnbUmB+TtPOIfv5IReMMIOMALASAHDa12awKudG2u6zg3H02DIrzvsPT0W+sMdf3b13/cHAYkhyAjMvvtt8pgOqFCox34AdwjAyf4HFAGYtXs+GRLWEx2sz/5TRMBIFc0Y803XmtCxRkyxnT67wAw/bj8GP2w7Ctn5hdgnIhUfV+/xnp6ruiNBAo7fsN/j2hHdoEHFWE3X+9Yfe2Dp4YuC3h+4ECA/DNh/vP840NVxswbZCgpWJ49oPRikCUDKD6CKAFw5ALkRgMgEMDadvfdfml3r6RGAIgIm/GXDDTDnkbrMZJnFgex8M0zffIRW9U/QJCAh+ISj1xcKvUPwmf0kt58rvwWO47D7rBobAf++9rCma0Ym6H0T/4Q7fPKPVPYf6ZfqPwcsEnD0yLD7OoOdANCCVwxW5QhUSwBOw38BEUBUlDFp6r8X6E/C9QhAEQAT/opRJvjp0TqQULp4KvVsPHYJPl65By7fvoftVRB8TJAIVtAd+9mae9x+9jcITNgo+l77tYiHzx/Rlvxz4FIG9P5hs0Tyjzj273/qPweeACjIPvJys1pgF3rcDyBVKox/1PhvKRGAnAOQjwBUefbtauW69kVeSLDaUBagTgBeAyb8qGgHEv64YphT71T6Hfjgz12w++w1bC8mHEq9PUcCBNe7YkJPiIiB+z3uXullbt/W0DGhvKbr/2LtIfh+5xmH0BtNgtg/c11cTUA/7P0RkNOX8/umr57bKH3ldFQjUC4jUNER6IoAxCaAQAOo++HCByOqN1qODtZrAHgRmPCj2P6cR+owBTyKEihxZuaWIzB7azIUWLisPVeC79zbEyTBE4Dgc763FREAK/xhdIve/1oX2r7VlsHY7ds1cOZWDn0+qdg/GwL0s9i/GHhtgOzzh3uljh+0E5w1AFWOQLUEIOkAbDDhr/6h5atMRQfrYwC8BEz4US7/7IcTIFKjULgCGon3xm/baXU/i92jIPisPc8INibseG9PkNIEwPsEhHk/9C3boFOtsvBjnxaa7iP9Xi60mbxaJvXXf2P/YuChwPz0S6NOvv/4EvDQESi+a7ccgI2/2vy2MaYMMwmIHgLUCNGzQ8I/h1b7i3JWXRQv/3TVHli8+6T99KKQnaDH5np4gW1POgs7idYGCXKQ8fwzGoANPuhcFwa0rK7pfpYdOAdvrthv7+nlBv74sfOPg6BI6N3bE1PGPsQNCnK7TqAaApDMAKQXY5Nvt081hEf3Qwfrg4A0gBK+myblI+HnXnUhKqToev79F67DmMXb4dLNe0LBR/8SIsEHrldnVWc5AuBCa5yAcSTAfF/U21L2NknZ7ASwZmArqFtOW/bfq0t3wqqjVzCbX0r991/nHwfBoKDce7+kvNrhTRBqAKozAuUIAF9Lqf/2HICZO38lQ8IeQgfqVYA8hEj4a5UOg4VP1Iey4UXj8EOnm/1PMkxZdwAKBU5bzqbHeni0V1A2W0QCrMATBB5Ww8kBD/9xpxE6/pDwx4YaYO+IBzQVJ0U/1frLFZCRa5Ye+Ye0AMI/Y/9SiOBzAfLXJ49s9yI4QoFiDUCxQpArApCLADAmQNLsvZvpB90I/VqeTgDuQyT8tUuHM8JfVA6/jKw8GLlwK+xKvSpS99mcPJJ0/C3Vs4sFnycAXOUXOwbFGYHcrdvs92+zQc86ZeHbx7Rl/51Mz4QeM9dLqP/2nj9Q1H8OKBkIXbXNXJic/EqbbiAkANWRACUCkIsAGIEbBjx7/zHKQJbVk4A8gEj4K0SGwOIn60PVUkVTuivlyi0Y+stmgaOPwL3xeLxe3LNjgi/s7R1qPt7z2+Wdi/kTzq2Msv9D2awMAXzcOQH6J1XRdH9zd56CT9clC4f78rH/wFH/OfDlwWzW60eG3tcUHMIvTghSjASoIQDJCEB47cbhdd+ZfwHt13MA3IRI+JE6t/jJBtAgrmgy/BbvOcXE9vHwHi/8pJQXH+stRQQgFHyDI+zHRggIgcovIWCs7Y/WHAGsH3gf1C6j7d4HL9gOW85cx2L/gef9x8ENC6b/t556+7H4vNtX8sGDSIAnBMBoAJX7vlOt3EPPHEQH68OA3QTleA/IozuzZwJ0jteW/y4FCy1c7y/fBYt2n2T3yPT6Yjsf7/FZAhCq+pjg4/kA4pJaUsLF2v0cAZQJJWHP8Paa7f+kL/6EuwU26eIfAab+I+DDgq+tmZd048/pV0B9JECSANSEAHkNoOarM5JKNW63AR2sJwG5AUr4DsZ3jIc+DbVVvpXCvbxCePmXTbAzlcvoU+j1sZlyBWTAZ8y5L/hSI/0oLOzHEUCXmqVh9uPa7P/UG/egy/R1Mqm/gZH8IwaeDJR5ZNtDF2e8lgLSkQDFUKArApAeA4CyAMfN7xxeszFTCVivA6ASIuEfmFgBxt2vLfYthbQ72fDij+vhzPVMcA7tCXt9gYOPV/eFBCAO9wkFX9jbE1I2P3//nPPPxqj+FL280a46DG9dQ9P9Lj1wDt5aeUAy/z8Q1X8EPBko58KRZ1LHD/oHhBqAqlCgFAHIhQAFDsAGn/7xdEjF+JnoQD0LUAVEdn+X+NIw4+E64O2K3SlpN2EALfzI4y/wwAsSeaR6eBJT90W2P+/4c0fwRel+7IojAM7+X/h0IrSpXlrTPb+zYi8sOYgN/8XKfvPhPz/O/ZcCXiG44NqFYSc/6v0nODsCXYYClQhAMQTYaOKqocYyVT5hLkDPAlSGSPjjY8Lgj2caQbSXE30OX8qAfj+sZdR/J+F3it2zwsEINtf7S5gCfHafGsFX6PrBPuIPCT2j/lNWMNDbB0a2h6gQbWHPHjPWwcmMHFHhj8AY+isHnADMN6+8d3xcrx9BWgNQDAXKEYCSD8BOAJM2vGuMLfsq+oKeBagAkfBHmQzwR59GULN0mFdPg2z9gT+th7xCC9ZTg8i5J9XrGzD1nyMDTEvAvfrYcF6Xgs9n+9nv2/4/RwB2DaBBmTBYPaCVpvvOyjdD0wkrwMZU+XFM/2Xv/f2r7r87EKQDZ96acvStbpNBORdA0hGolgAMosXUaOqWL42RMSgDSScAOYiEH/0/8aGa8FQD7zr9Dly4Af3nrIXsAou0yi+p6hule308BwDvMQVJPfjZsT8kCnzi9y62/59pVB4mdK+v6d53nrsOz8/bzk/7BVLZf7z5Ehj2P4IgHTjn7tyjr3UeBx7kArgiAMkcALQ0mbZ9NhkeyRRv10uBSUBC+JG3f3znml49zb7z1+HFOesgu1BC+HFHH2fT484+3snH9foGh7ovHvaraOODfMgPu3+KE35a/QerFT7sVBtebF5N0/3P3HYcJm8+xqr+gR/+44CunisNZsnN+ePYmAdHgrwGIBsKdJcAeCdgk293LCTD9HEAshB5/FGSz9KnG3q1gm/y5Zvw7Hd/Owu/OHOP6wkN2N+uen28jh+CnLqvJEjYMxDY/zYLymCDRX2SoLVGB+Dopbtg1bGrAvs/kMN/OLjxANaC/PVHR7UfCEVIAIJhwMAMBNq1nDCFtNHHAUhAJPzI7l/xbCOoHuM9u//sjbvQe+ZquJVTgAm/lL1vEKn8BuFada8vY+e7fA7S9j9YLbBvxANQWuOgpx4zN8DJjGyh/S9V+iuA1H8O3HgAqrBgR/LIdn3AQQBq6gO6TQACEyBx1q4NNOM21glABAnVf3LXWvB4Pe/Z/Sif/8npq+B6Vh7mpBMN2uHsfn4gjFjlx0f4SfT6WgRf8Byk7f/y4QbYOfx+Tc/BbLVBg8+WgxVpMgr2f6Cp/xz4AUGFBUdSRrZHlbnFGoBqAlBKAhLb/4wTMHHW7v9otq2pDwTCICH83RNKw/Qedb12ioysXFr4V8NFmgSchJ8XagOv5vO9v4FT+TGnGNoWhfYUhd8dwZEhAC7+/0D1GJj3dDNNz+I0rQV1m7GB7fWDx/7nwA8IslhSj7zSpgO9ZQY3k4HUEIBTFiBamn635xDdq5TXqwGzkBD+mDADrHk+EcpFhnjlFIUWK/Slbf59F244hJ+vwMOp/ZiwG/CeX0blV9PreyIwYvuf9QFwDsDBzavAuM7aiPHvo5dgxNK90rX/uLCm2P4PEOFH4AiAsNmuHR7eqiUITQBVFYKlsv+4tWwhEGCmBN9/mj55pE4ALER2P2r0U7snwKN147x2irG/bYff9p7GhF/C3ncSesz5J8j1d9j7ssKvRVi454GcfkBh6r+VcQB+0a0e9EnUNgR46uajMG37KXn7Hx/zEGC9PwI3IpDWrrKSh7VC8VQlE0AyG1COAJSyAA0QHh7S9Jv/LtLf14cCI0j0/o/VLQNTutfx2ilmbTkCX/y9T174nWx9jAwIkb2PF/YEEKrHHLwh/LgDkCMAq50AFj3bTHMEYOjiHbDh1HWnyT8DOf6Pw1EenLAcGdoiHuwmQJETgDGsRmJEvXFzz6EDg54AZFT/9f2bQhkvlfX69/QVeHHOevt4C5HaLxR+GfVfUK+PzREoil7f6ZlIOQCtzLJtSDuoGqNtkpOHvl0H527n8jkAwIcAseSmALX/EfD5AVBNgPzMa1xNANXpwFIE4DINuGybR8pUHfTJcfSFoK8FINHbfd6pJjzb2DsTdyKPf8+vV7D5/a6Fn/f+k0bMKajS3veWcLhIACLpdnji9c5MPruWU9T/bDkUUqRwAhAy8Mp/yQEngPPfvF733ontaBont9KB1RCAUymwcl0HVqr89MhD6Auo6ETQEoDI7ker1lWiYf5TjTQVuOCAZuB9bNpKOHXtjoLwG3l13yncpyj8RSD4Es9FygFYNdoE/wx9QNMpbuXkQ4svVwvq//H2vwEjgAB1ACLYi4LYGSB90ZeJ17cvuwFupgOrJQCBBlDxiVHVK/QcsAd9IWgJQEL1NxAUrHquKdQp653SXu8t3wELdpyQFn5uimtfE37Bs+EIwBH/B5sF2laNgQV9W2o6RcrV2/DY91v44b+EweDIABSMAAxM+x8BJ4Aba39qeW3Fd6gqEBcKlCoR7hYB4GFAAQFUG/Be3TLtntyGvmCmX6o5qAnAofq/3LwSvNU+3is/v/LQWRi1cKvDaUeQAiG393JGBeE3yHj6i1j4+Wcj7wDs07gifNGzkaZTbDxxBYb8tls462+QJABxwAkgY+uy9leXfIn8cnKRAI8IQNIHUH3ol41KN39oE/pCUBKARMivfIQJNrzQDCK9MMb/amYO9JiyHDJ5ux8fwScM9TmFvUTC7xACgGIRfv754CnAVl4DQOMAXmtXE0a2r63pFPP3pMJHa5P5JCBBAZAgcAAioJJgJpYAMves73xx7nu0uujeeACPCKDGiCnNYxM7/I2+EHQEIKH6IyfX5G4J8ER9bTPbcj/fd/bfsOtsulD4CQOm5gpDfUKHny8Iv/DZCCIAVguM71Yf+iZpGwX45cZkmL0z1akCkKwDMMCEHwEngLsHt/S88P1baKbuIiMAPg249uhpbaIatmdmBUazywRVPUAJ1R/N4PtH36ZeKe/183/H4MM/d4tSfCV6fT7V14eEX+r5iCIAiABmPpEIPepX1HSaV5fthlXHromGAYsIIIAdgAg4Adw7trPX+emj94NzOrBXCYBZao2Z2Ta6fus/0BeCigAkvP6ogS94qhG0qRaj+ecv3LwH3WnVnxlchc9jj3n8cdtfMOiHq9tXksIvekbOEQAL4wdY9FwLaFOjrKbTPPPTVtiXdlc0/VfwOAAR7IVB7QSQffLfJ89+8xpyzEulA2siACcnYMLrs+6PrHvfUvSFoCUAtvdvW7UUzH9KW1lr7qef/34N7EBlvPGinGjN57cbnG1dwYi+EhZ+0TOSigAgAvh7cFuoX76UptN0/GYtXLxXIJr+mxsERAQdAWSd3NX73Dejd4GbQ4I9IoDar83oEFWvNVsSPEgIQEL4UULLyuebQf24SM0//+vuk/DOsv8An3cPb9TCGLd4gA9Wrrs4vf0unpOQABwmwM6RHaFCtLbaCEkTV8G9Qvr3jdzMv8EVAUDACSDnzN4+qVNG7AA3hwR7RAC13pjdMbpOi1/RF4KCAGQcf4/UKQvfPKytph0CyvLrMHEZU9xDWMiDFXiDUcLj74PCzz8riRAg6wBEZsDxN7tDqNHzykjoFHU+Xc4UAuXHAOCJUQJNKDgIIPvUnufOTh2JQvNuE4DUUGBlE+CtHzpH1mq2EH0huAhA2Puv6d8cammc0w7hXbrn/3XPKWGsn+vpDTIaAOEo5eUzws8/KzwCYA8DcmMAIkgKksd203QKphLwxNVC25+vAWAISgLIOnPg+XNThv0DXiIAqWIgHAGYaAJ4iCaA+egLAU8Aot6fc2x1rVUGZj7aQPPPH027Cb2mrwIrRQiLdeJOP4EPwDG0l7d1fUX4+eclQwB0789UAhr1kKZTpGXmwIPT1gvHAKCFI8UAKwEuB4EJkHrghdSvhm0B5RGBYgIAdwiAjwLUfueHblHxzeahLwQPAQjV2t+fbQpNKkRr/umnZ66GAxczeNXf4eVXcvoZfFj4sWfFzwFosdv/NAlUijTC9hGdNZ3mTMY96DFrk4oQYOD2/ggCE+Ds4QFnJ7+MkvPcqgrkigAkqwEFDQFIZPyhfR3jY+H7XtpSWRH+OnwORi76x66yokbM1rYT9PpSTj+ncJ8PCL/4ecnkAFSLCYEtwzppOs2xa3eg15x/gjoEiCAggAuHBp2dOARN1isOAxYBAbz1fdfIms1+Rl8IGgLAerRlzyZCYiVtYSxU3uuhSX/A5Ts5AtXf0ZjxhB+jk9NPMtxX0g3dFQHQmkDNmDDYMKyDptMcvnwLes/713kUYBCFABFwAsg9f/jF1ElDNoObZcE8IoCEsbM7R9RuvgB9IWAJQLL3t0G7qrEwr7f2uP/c7Ufh09V72Tr9RsBH+OGCL7D7lZx+vtDIJUOAFF8GDGkACWXCYe2QBzWdZu+FDHhu/g6hBiBIAw7cKkA4BBpA6oH+56YM3wrFQQC13viuU1RCi8COAsj0/nOfagTtqmkrZZVTYIaOX6KwX6Gjeg9u87Pqv1/Y/TLPzDkJyE4ADcpFwMrB2moB7Dx/AwYs3GXPAcCfVRDlACAInIBnDzx39qvh20EDAeBrFwQws0NUwn2Bmwcg4/hrWiEafnu2qeaf//6fZJi45oC06i92/vm63S/z3JwIgEkDtkDjilGwfKC2+QB2X7gBLyzQCUBAACf39D07bdS/4EUCkB0LUPvV6Q9E1m+9BB0cFATA9v5Te9aFHnW1jfhDs/c6kn7w+L5K1Z8QvS5fatxKlYBZJ2CzStGwZEB7TadBJkB/WgMI5hwABGEq8M5nzk8bg2cCSo0FcCoMqpYA+HJgaKn56jfto+u3C8yxADK9f4XIENg86D5NdewQ5mxLYXp/QeFOQUN2Q/X3tYbtkgDM0Kg8rQFoNAH2X7wJzy/YqY4AfO0ZeRGiwUBPn5v2hngsQNEQQK1Xp7SNqv/A7+jggCcAtvcffl9VGNU2XtNPI89/x0nL4EZWAZbww1X1MUp7/f1B9Rc8O7wQiGM2IC4NOD4mFNYN05YHcCjtFvT9ZYf9eRlNWB2A4CIAwXDgo/89cWHm6/tAKPzcgCDvEkD8iK/vK9W4/Qp0cEAVBJHx/KMb3zjwPqiocQDLol0n4KOVuyWy/cT2v5+p/oLnJyIAJPyIBFgCKB9hhO2jtKUCn7px154H4FQIJHiyABEEFYEOb+916fs3adWyGAigxsvjm8Y077IGHRywBID1/t0SysLUhxtq+mlU07/z5GVwNTNXWNoLr2cnmMnHj1R/wfOTIAB2QQQQRd/WvrEPazpN2p0c6MJmAgZrGjACTgB39q7rcXneRylQBATgNBioxgvv149p+xhKOggcApDM+bfHsef2bgKtNYb+1qWch1EL/xGp/njv7yrhx4dVf8EzlB8IBBYz3aCscOzdXppOk5lbCG2mrnPWnLgSakFIALf++aPzlaWTTkFxEEC1Z95IKN3xGTT0kJ0YJABmBpLp/WvQNutfA+7TXOe/3/drmUk9+Uq+Mo4/vCH7jeoveIbyA4GYUKDNAoffegRCjZ4XT0XTgid++TefCRiM4wAQkAPQyDql09f/0v7GylkXoDhMgIrdX6xa/vER7LwAgUcAeCmrsQ/Ew8Dm2gpYHrtyE56c/re9dxKF/WQdf/6k+gueoQsCQAVBXusOpSNCNZ2q+eQ1kG8jg3YgEAJOAFdXTGt5c8Ov16A4CKBcuyfKV+r/LjszUAAQgEzoz0ivt7zUhm6s2ub4e+u37bDy8HnZ3t9RzYb071i2SgLYMOIhqBqrrYpSj1lb4HJWoU4ALAFcXvBJ4p1da25BcRBASN0WpeqPmYVqkDPOLb+fHFRmEEuHmrEwo1cTTT99KzsPOnzxO5jp3xUIPtfrCzz/fuj4c3qOrglg8YD20LSqtqKg/RfshENX70kTQBCMA0Cwzw1ov79TE56rW5B2LgeKgwAgJiYs8Yv1Z9HBfk8AClNZT+rRAHrU05b59+O2FPhq3UFM7TcEZu/PP0vXBDCt933QpX5lTad67c99sPH0TZ0A7ARAJb/esRbk5xdCESUCCVKB6cWUOGvXGfpQU8ARANtwI+g73vJyO4gwee6sQj/98Nd/wvnbWaLeX1zsgwtj+XHvzz9L1wTwfrfG8FxLbTMDTdiYAr8eSgtqEyDMSNJNBt0flZ/8SltUnJKrBuQVApAdDASIAGbsOEL3WDE2+iXnW/ydAISJPyj093DdOPiih7bY/56z6TDwp/XB0fvjz9IFAQxplwCjO2orqLJg3zmYtPVkUEcBeAKwWm8nj2rfHOQJwLvDgenF1GTGfzvoRlwFCU2evxKATO+PGu3MxxPh/vgymn7+zSXb4K+Ui45enpvKOhB7f/55KucBoHXPehVh8lOtNJ1qe+oNGLF8v2gsQHARQLjRbu5QVvPllFEPoCGWFigmAjAmTtu+CUwhdQKFAPDQX9kII2x4qS0YNDSebNoce2DCUiiwQXD0/vzzVMgEZAmgcfloWPyStrJgqJLSI3O2BXUiEE8A5sJTKa8+iPKri48AGk/9ZyUZEpaEfinPbC3pZ+E+FJx/zzSpBOM61dX086sOnYV3ft8hLPIpyvxj9gdK788/U+WxAGgdG0LCv288qulUFvq3W329HiyEIWhTgcNNBuY2beaCA0fHdHgKipUAvt7yKxkawQzszg0EAsBU1hmPN4b28drCVEN/3gT/pV53jvsLClgEUO/PP1PXBEDRmsB/Yx+FmPAQTad7au6/cPZOftCOBuQc1Lb83O1H3+j8AkgTgKaioOKy4MzswGi70ZSNcwxhUcywLn8nANz5F07f5T9D7+fHWXuC6/dyoMuk5UBxFX/40tWOHABmum/Sz+P+ks/UWaMSOwHRMrf/A3BffDlNp/tgTTKsOn4taOsBcARgzctae2xst2FgF3qvlgWXnBgErRtMXjfVFBGD1A7GBPCr4UBS6j87dr1DfGmY2itR088v3XPKXvCTH+UnygBU6rGYlZ82WlcFQWz23h8RwLjuTaDvfXU0nW7RgfMweevpoCQAdFfhLAFYcu8uPf5mjzfBQQCaJwbBcwGcBgPRi6nBhL8+NMWUfQl9Kd9iBb+qCSJbvtoGHz5UF55spC1JZQit/u86d92u5vMqPxsFENf5C5TeX/RcJScGtXEmgAV6N60OHz3aQtPpDl+9AwMX7xX5U4KjJiBSHMPYAVWWu7e+Pz7u0QkgDAMW3eSg9GJq+Mkfo41xld9AXyqw2MBK+REDyKiqaFn3UlsoF+V54Q9U8ffBL5aCGT1ag8j5h9cADMTGqqIqMCoLhgigScVoWPhSF02nQ/UVH5i5BWzMswwuAkARKm6C1YKbaV+e+qjPLChOAqj3/q8vhFaq+TlzAVYbkxLsF1Bw/tUvHwmL+t6n6efXJV+AN5f9JxzpZ8Dj/1JJK9gr8OfGqmJeAI4ETHR73PHWE5qGBSMMXLQbkm9kB93EICgFOJT1UxVcOffOqQn9UJVuKQdgkRCAsc5bPz0SXqPhd+hLflUXUEH9H9iiKoxqn6Dp58fRwv/30Yui0B+brSYV+gukhqrwbO21AKysD8DMmATzB3eCxCraoi0z/jsD8/ZddDarAum5SgAvCJp/Lnno6SlD14J8GFATAUiGAeNfmdSuVKMHmNLgflUVSNZTbYMZTzSFtjU8z/5DhSpQye+cQqu9XBXn/GO2RfHqQHL+CZ4tgCQBMPY/Gw1gHYFjuzaB/m3qaTrl/su3YeiyA0GXDoxXA8o6+s8z5797dzcUAwHwYcDqz42rH9v+UTQZof8QgIL6T6Kx/0MfhMhQo8c/v/dcOuMAZITdyJkAJoGXOiCdfy6er9gEANYM6FqvMkzqo22OgEILTbqzNoOZIoMqGxCvBXBr+wpUDiwVhBEArxOAwAcQd/8T5Sv3fWs/+hJyABb4QzqwZAO19/4N4yJg/vOtNf385DX7YdGeU0K7H68AJBiyGmC9PwdXA4J4M8ACsaEG2Dz2CXZEm+cY/ecB2HnxTmCGVmWAHIBcqvqlRZ81z9y19iZIOwGLhACMEBYWkvjV5uP0oaF+MyJQwUvdL6kKjOmgLf330al/wpW7+VjPbwBxzT/JHopZBUgDVZsNyGoBS4Z0g3oVtRVcXXX0Cny26aTDERgEBMCPBKSovORR7dHQSqmRgJoJgEsHdiYANCJw2rbtdE9X1S8GBCmo/2h7yqNN4MHanmempd3Ohse+Wemo8GswgXPJb3uhz4B2UqlKBmLDgTQJvPpQExjYvoGmU2YVmKH7D/8xoVdhReUAfcbgGAgE1sKLya92RCOr5AgAzwR0iwDkMgGx8QCb/yBDwpv7xYAgF/bphiEPQKyG3PS/Dp+FD1fscVb/AznzTwpu5AIgx2CrGmXhuxe0zRSEMGL5QdiXdjdoIgH8OIDCvL1HX3/oWXAIPpcK7FUCEJcFYxyBDSevn2UIj+6Jvujz6cBSDRMRAN0rVYoKgVWDtTmjPlm5C1YcviAK+5kCP/YvhsJz5n0BFjNvChjp9rnlzSchKlTbwKAVtBkwfssp+eccQM8YTwO25matPv5W99HgnAbM1QPUTADSPgCaBOp/seojU3TcQPRFn04HVhj6ixpk51pxMPFRbfn/vaathDTG/nce/BM06j+CgqMVnAYF2TWB8U+2hp6JNTWdFnVAD//0H+Si6qsBrmkJ0oAzM+aceP9xcRqwlA9ANQEw5wDXBGCs/9GSYaZy1d5BX/DpdGAl+59ehrepBYNa1/L45zOycqH7lD9FI/5Mjtl/g0X9R3DxrO2agGNYMFr3bFQNxvfWpoEhjN90HFaeuM77AQL1WeNZgIUZFz4/9Um/H0F5KDA+GhBABQHIRQEEJkCdsXMeD4tvOI25EF/OBnTRK017PAna1Ijz+OfXppyD9//cLVL/naf6DmS1VACFSIC993f4AtDfYfTj2fD6UxARqm3+hZM3smDAb/vY4daBq23hWYB5Z1NGpE4d9je4HgnoVBAUNBAAQwKVX/gwsWyr7qvQF3x6ghAXjqm1Qx6EMhpmqvnirz3w+8FzfK+PRwCCSv3noPi88TAgaw7QZsCE3m2hexNtZgDCi4v3wulbuQHtb8GTgO7s/vvhtEUTjoNzDoDLOQFAggC4bVVVgSLrt4ipNeLbI+g4n00GcmH/x4QaYP3QjppO0XfW33D2VrYgAsCnAQeT+s9BjSOQ7f2ZkCBNCvfXqgDf9NceDdh0Oh0+WH8ioJ+5oxw42E6NH9CkMD2VbnzulwMDlQQgWxUILYnfbNtDGYxxPpsL4MImbVqpFHz/jOcVavPNFujwxTL7kFSjyXn4bzAk/4jh0hHI+gFYHwATDaA/X/PGk1AmKlzTqVFH9NyiPXD5bkHAal18DgBlTU8Z/SBynuAhQLkcAEUCAADJSICiCYDWjadsXkaEhKGa5L5ZGsxFY3y8UUV4p4vnNeqTL2XASz9vtlf6NXJxfzwFOLDtUUm4IF2OBHA/ANoe3qkJDO6gbSo2hLUn0+GzTacC1u/C5wAU5O07NrZLXxAKv+qhwOwT4eGKALhcAEEosMHENV8ZI2OeRF/0yVCgC3V01P0J8HyLeI9//vd9p+HLdYccPT82CtBh/weuR1oWCvUBhbUBHH6ASjFhsOJV7WMDkD/qmQW7IT3HEnBmAB4CNOfc+f3kO4++Dc4hQFUDgbAnAvi2mhGBvAZQ/6PfxpjiqqBEBN8sDKKUmkovkx5Ngvtrez7/3/jVu2HVkYus048zAYIw/CeGaj8AOzjIZieCqc93gvZ1q2o+/YbT1+ETND4gwLQvPARovpn29clP+s4AeQ3A6wTglAyUMGZGj/DaSbOYC/LFYcEuQlIL+reD2nHRHv98/x/WwpmMLIwADA4noNzQXz9ugKrhcmiwVRgRYLMD769TGb7up90ZiM4+7I+DcBRVCwqgaABeByA7df+Q89PGbAIPBwJhTwPwbVfjAQQaQNwjg2tW6jF4C/qiz0UCVCSlbBzxEESGeFYDwIIKgHy5jL5v0q76C9J/jcEX/sMhV3mZI1/ODGCjAPYiIXSbpSzw6/DHoHYFbSMEEY6m34Phyw8DFUAaGD4MOH31rA4ZG369BB6mAWNPA/Btt9KBITTU1GTShkO0qhuJDq4a63lBTa+DcmzwU3+hXog1A8Lou5rVx/MIwNkbmfD8D+v4ar/O9n+Qqv8c1AwMYoTf4tim190b14BPez/olUuY9l8qJF+/J6MFlPQDch9pmfn2Zm2z3k15tUNLcFb/VacBix+BR+nA9GJq9NXGZWRoeBI6eEnfZhATpi2jy1+w5cRleG/5Tmf7P9BLf6mFO+FAbJAQSbfZJSOfgOplS5X0HfgU7uaboe+SQ/aYXkHugWNju6EIACf8ruYDcEoDRv8oEYDYByAeEeiYJGT8qgnGqNLPoC992rUutKqmXX3zByzYeRxmbT2q0v4PMuFHcFGDgU8LZn0B3OShaPvxZrVh3BPaxwcEEvZevgMfbjzNbJuzbi8++f7jH4C8BuAyCxD9I0UA3LZSNiCeDGSqN27+oJAKNd9DX+ybVBkGNK9W0s+qWDB+1R74++gl5+G/wRr/l4JCEVaBGWC18kVCOC1g4Su9oGb54OhM1GD+gTRYfOQKs11wLfWT0xMHLQChBqB6SjBQSQBqkoFMVZ9/t2np1j3/QL/Zsmpp+Kxr/ZJ+VsWCYb9sguSrd7DiHyY9/i+GWjNAUDDUvnRqWB2+eFZ7RCBQ8P6GE3DgSiagtnRrx6peV5d+xY0BcDcJCECCAPC/1eQCmIDTAqKiwpt8/vde+uiI6FAT/Pa8timf/AVPTl8FN3LMwT381xVcmQF8UhA2PsDqKCA6+389oWn1CiV9FyUO9BSfWbQfcgot6Pllp7z1aGsozM4HR+8v1gBchgARPCEAwVgAekGlXIyNvvx7IREaibySMK9Pc6gY7fnoOn8AKoTa8cs/6CdN2KMA4vz/YI3/S8GVGcDUCxSNEmTnEUisFgezX3okaB8dh2v38mHw74eYbVt+zu7j7zwyEOzCXgjS5cC8RgDi0mAmEOYCMEu9934da4qr9D/02292qAudErRN/ezruE2Tb69vV8kX/wyCunSqoXpsAOcDwByDNEmMe6IdPNpcW8Vmf8fm1BswZdsZQG3JcjPth5PjX/gaHD0/HgFA26pyAACUCQBBKhQozANgl5pDJnWJrN9yBnqxjzeqBEPb1S7pZ1akOJ1+GwbP2yx0AArG/wd+ZVrVcDEkmycALieAE36rnSBKh9Nm5ZjeEBUW2FqlEmbtSIW/jqczbSj3xJ7h5+a8uxUcwo+0AE79F5sAsiFABFcEIBUK5DQAE75EN+1YrvoL721Dx9QvFw1fP9GspJ9ZkeK/01fg3eW7dAegWsiNDXDlDGQJ4rl2jWBkd8+TtvwdY/48CKdvorRmsF6a+37HrGN7MkCoAeCOQFUhQAQ5AuC2xXUBpPIAQrh1gwkrl5CmsIbowKUD74cID1Ns/QGLdp2E2duPSY//1x2AznDDGQhWZy3ASH9p9suPQIMqgW1aSiG7wAx9f94BNrr5WPPzUk5+8EQ/sPf6ZmwtVQ1IMQSIoIYApEKBJtGCSCC0zhtzXjWVqzIIqXbv9UiE9rUD13v7yi+b4Oi1u/Y5AElsKnCnAiC68PNwGplp1wTsYwCwmYPE1YNZLaBSTCT8MuopumMJjkxTDv+mpsMX61OYNlV4I+2n1K+HTKd3F4CQBOScgLL2P4JUq1RTGATv/dE2GgAQUunpV9uUbtF1NnqhvRKrwfAOnhfa8GWcoe3//83bCBSpYggw/1R1AnBoAeAYk8GRAD9zkNUpH8DhLLTBs+0awqiebUr6TooVM7Yehb+PpjFt7M7+v4de++O7PWAnALRwkQBxKrBXCQA3Abg1rv4zBGAMj46oO27hJvrVRqDiDj+92Kmkn53XYbXaYPTCLXD06m17r88NBBJUACJ1ApCCmAA4LYARcFFIUEYLIOjjp7zYA+5LqFLSd1NsGPTLVrh+L58WQir7+ITnu0FeXi7YhR7lAYjNAJezAeG/7YoAEORqA+LqfxhwZsC7C741hEe0Rmw++8XOUKOs52PtfQ1I+Ces2gkbj192Mf+fTgCyUNICBMVCLJgvwEEKSAuICDXArJceg9oVy5T03RQ5Lty6B8Pnb2XMS0tu9s7UiQNeA4f6nw8O4ZcjANkIAIISAXDbuCMQ1wB45x+9hHJLtUEf94mMbzIWvcAB7epD3zb1SvoZegUXMjLhq9W7ICXtlkPo2d6f0wKcIwDYo9QJwA5PtQB+enEbQxqoLt6IHq3hsZb1A/rRLt59EubvPMW0t+zUwxPTFnz2JzjUf04DwP0ANnbt0gGIoJYApAqDhICDBDgCCIuo0aBc9cGfrqBZ24gq7Yzu2sxxTspxft+oG0QpXkihxQZpt+/B9uMXYNfpK2BDjj1O0I3iCUCNzuq/TgDScOULwIqGCrQAPHHIZv9OlTJR0LNZXahTqSyUjY5wPhcehfFhEPgW4dj+duMhOHszC7Wtwktz3n4yN+3cTbALPkcCnPBzuQByJgD/xKXPK309ctmAYh8ARwCopnNYwtgfphrCo5ujl1ZgNtOqs525mRfMFeYQkEJxQdjw8H3iWc0IPn2XFWiSZG18o2PIrx4B8AyutADeKWhxhAZtjqHD9spCqC1Z+SIv8g1J6h340PvAhJ1PHGPbkIFuV6Eme36JOTdr77kpQ8aCXejzwEEAYhNALgsQpB6SOwSADwvGxwHgJgDyBYRV6/du74haiWPQC7NYLHRPai/+yLM93/MWk/RLzllIyR7iEH5uYVV6RsiNwp6fjwAYeZJgXqROAMpQqwWIxgngEQFuZKFqEnDa7SvvxTFehGCFHy1I+A1s+8o5c/CrtCWTV4K998c1AK73x5OAuMlAvEIA3BrXAMS5ADgBhEfValSxcv/3f6NfnMGGtIDCQta+Y18a+9KBY35fBJfCy/T+9sE9jrH+IhIQ1ADQHYCqIKcFUOJBQvi4AKvIFLBhxOHj7UkOWDuz/8lqkPQSFhLCFZmxXJ73UZ/cy6dR9h/q/aVMAPFsQIoZgPzp5S5LtC1OCRaPBRAQANquNXrG58bo2HboxZnNFrBYZV5YSUCpkRAY53GqP/9SsIo/XPUfbhJQ9qXpDkA3oKgFSDkEWVOAIwbKptympO06H4RD2+QIwEi3L5PJ3slYsu5sP/ftqI/Bof6LCUAuDVjR/mfPLHdFgm0pR6CUGRDGLRUeGXR/bLOunyMCsNEvLL+g0PFyKcrp5fsMMLWdwE0AptKvQUQC4gKgugPQLVBiM8xBAPwcApTIFODWTEkxq8AM8Ol2JQVRG+Htf3odFhpC84C9rWXuXffu9Q3zd4FD/ecIQKz+u+UAZE6rdHnYWs4PIOUItJOA0Rie8MZP8wkDWRExeYHZancGlqS6JnU+sXAK7HbcBDAIVH1H6W8s/Kfb/+5DwSHIzSOAZwkCHhHARxRiNQdk2xbax8ypR/nWe8HNAHqxO//YjFKr9eqZKf8bCBYL1/NLqf9iB6Aq+x9APQEgyNUHlCIAZl190GeDw6rU7s84A+nFbLFIaAC+xNJCoeV7ckzABVl/kvX/9N7fLYgFVckUENUPwD93OJjBR9uWHMQaJwkhtOpvYNtVflrqz5d+/nA+OARfjf3vMgFIdHZw9ZlSJEAqGsAskfXbVq3SZ/QC+oUY0IvLL7Q4Xi5zSSXoB3DxMhw2GVqLnICYOWA3D2R6f+63dChDwSEomE1IQAY2fu3oVMRti9ko6btzAUJkdiLnn12zpNuV5cqiSf2zzx68Cg6h50gAD/+JJwNRpf6zZ1e6MsG2XEIQVx6MIwFOA2CWmqOnjzPFlu+CXhSjBXDMjU3a4Tvgen6w22K4I1Ak7ILIANfri73//I/pUISUFoCTAFc+jMJJgLX9GVMAE362U/HN9iUFYZsz0R2L0WBvW4V3rq+9MH30ZJAWfnwEoKv4v+yDcNU61eYDcCSAawAMIcS2eTihQs9BP9EvkUQvitECpGy0knLYSAkoa5PxTkCSU+1JzNsvLP2l9/4aIWcKMNs4Cdh425/3A/Dmggvfki85BSV8T+g/pventymSsN5YPXtQ5v4t58Eh8DgRiOcDcNv+Z07r6jIl1lLhQHFIkFszJFB77JwvTKXKtEMEYNcCfNFWEzvtcBOAwGr8kRghYJmCYttf8Fs6VEHJH8DOLcgLP/MZMgGwyJKsE9AX2pcUhG3OZLCH/1CbM9+7te3sV0M+BKHwc9vi0J9U+A/AiwTAbculBYu1AHygUEhc9xcS4x7o/R33gvLNFvaF+SgEXlkAvrinWNhx1V/v/bVDyRRg/sbKiOE+Ai671IZ1er7cviRAosw/o4HNPiep65sXv3x785ITIBR6jgTken+31H8ENa1UjRkgNUSYJwD0Wb33Fk0jI0u1RNditaFZhK2i05TUC5PutZ3KeQsEnxV+9ngn4cd+R4ebUEkCDu2A2cnuB/uxUr8D7HE+A2H7QMJvIO2iZcnN3HPm8xdeB+FIP3wtNxOwW+q/81UoHyM2AzgSkJosxGmJ6/xM7XLdBywgKDCgKyo025jpxO2X6AMvRmqwCAFCARelberCX0SQieE7HINCW18YWeL/Uf49XwDbTtB03yEmku1vCHPG6jn9Mv778yI4F/3EF/E0YPjwX4AiIABuW8oMkJ4xyKEJMGZCwru/vBdSukIv9EM2Rguw8T9MlQA7E2oHicgJvuyxOjTDFQkIjlGXAVgSbUwOeNsLNZK0CWD/u/B2+h+pEwdOAod6jyf6yM0A5JH6b78OtdfrWCuFBMUOQQEhlGrSvny1/h8uA5KIZG7WagOLzXdeiuRjEadrKlX41YXfu5DL5gNQEUnyxXblDCMt+CEG1py02XIuz/+k971ju9CYf7HAu5oD0G31H8ETAuDWUuXCpSYOEfxd9805Q00V4//HXV6e2eq7rwoTaEXBV9qvQxtUhPQof3X8oZx/I8lLVWHa2dlnpg77CZw9/OK/8co/4vLfAEVIANw2rgHgvgA5U4BbDKbwUiF1P1z0C4RGMFMIo9htnpNDsPhAqHkEroRbF/6ihyvhdvG5L6n/HMKNKJfE3nZs+dkpJz56+iUwm3EVHxd+KbtfbPu7pf4juNNylcwAuanDpLQBQ7nuAxIqdH9hAX33yEfAlN7yTVPA1RPRBb/Y4We9vBwY1d/Iqv6UrfD6uvn9MjbMPwfCOf4sMovS5J9uJdd4SgDcWooApGYQFmgBaKn79rxhIRVrvMz9eAEeFfBl6ELvG/CHtiID5PUPNTlSxguu0qr/5JfngKNXlxJ6fNIPOQIAKAYC4LaltAADSDsFxf4AgzG6TFjd9+b/TIZGMNO+UkyCkI/5A3Rh9y/4ASmgFhVuMvDSZMvPOXHmiwGDzfduoyQfKdVfasovuYk/3PaCutvCXTkDxQVD5EwC5phSLTtXrN533AIwGsoyD4M2A/Kx0KAOHYGGMCzkBxbLrUsLJvS7d2TrDVDu/cW2v3jmX496fwRPCYDbdqUFyJEA/1m1fu+0jmnZ7Vv7mFv6Lq0UEx7UoSPQEMLk+vMiZM7ctXpE2tKvD4JQwOUIQEr919T7I3ii47rSAqSSg2QJAC113vjhpdCqCcP4J0OTgFknAR0BBDTQx+QQfsi7fHr62a+HoUIfeM8vRQBKST+aen8ELQTAbYtDgq58AU4EACaTqf57iz83xpTpwv2w30YGdOgQQeDxp2G5e3P9yfH9PmSq5aojgCLp/RG8SQByKcJiAjCItu1LeLipwbiFkw1Rpe/nfhylClt1EtDhx0ADfEIx4bdmZf574ot+b0FentirL+7xpQp9uur9xdsu4amb21MtQIoM+MVYrlp4nddmfmcIj+LnFdc1AR3+CnHPb83NOXrmm1desWRcRgU+xT27nNrvdc8/Dm8SAIJcREBJE+DWzPGlkjqVr/bc27OIkJDq3Al0EtDhbxALP1VYcPHywi9euZeyHU3uISX8cj2/Ws+/eFsVtAS61UQE1EQFxCRBRtZJiq0x6LOvyfDIxtwJdMegDn+B2OFH5eekXJj7wRs5Zw5ngqM3V9P7KyX9aO79EbxJANxarAXI5QZI+wJYEjDGVY2oM2b6l4bImFbcSbghxLouoMMXgRo9PrQXwZqTuffMN6PfttxMywVl4beCcszf670/d81a7xnfltMClBKEhBEBhwMRkUB47VHTPjeViuUdgyjZC5GAzQ+yvnQED9DIPiT8ePKo5e6t7anTRrxvuZOBsvw4IVZS/ZWE3+u9P4K3CYBbK+UGyGkCAl8A/x2jkawzeuaA0Cq1X+ZrblO+XEtAR7CBH9PPSwNlzT+b/E3qzNd/B0evjQu1XI+vxuvvtd4fwRvJ7mrCgu6QgJMmgJZqz7/bPqZ5p4+ANERxJ9NNAh0lCdTrh9C2Pq7yg9V69/b+NR9c/e2b/SDs9ZV6fyXhl+r5vVb5pCgIgFu7MgXkhJ/bxkmDWcp37Vc7rsvzn5OmsBr47RdY9XwBHcULJr4v6PWRpz//3PX1Cz64uXUJquWPq+5WkPf8u3L6SY3191o9fW8Nd/PUFFAiAkkSMIZFG2u+8lW/0KoJg8E+wpAB0gZQpMAvhhTr8FswBTxFtj4Nc0Fa6tzzs8YusuRniWv0i3t/JcFXCvl5VfXn4M3xrmpNAbnxAkpmAE4CzG+V6zGoTvmOT39AhITVxi8CaQKFulmgw8tAjQ4JvoEUioytIDc1Y+uyzzM2zD8DQqGVU/3lSECs9hep6o/flzefkXhbKUtQSRNQRQJhlWtFVOv//uDQCtWfpimZ1wY4JyEiA50IdGgBU+/OyckHKBxVWHjtwtJz8z6aZ7l1Bc3Xp1b45Xp+JadfkfT+3P15+3mJt135A6Qcg3IkwB2LEwoR27RzhYq9Xh5uLF2ui+Aa6EdksdkTiHQi0OEOUCNipuoiCbGUUJY7GZuurP5+Vtbhf9A4fk5AbRKLFVwTgFTPXyzCz91nUTw7fFttkpCrxckfIPpNonK/t9vHNHlwOBkSGi98ZbSRZkPzEupEoEMZyLOPenyTs+CDrTDv/N3D22ddXTJ5FwgFX0wASr2/ks0vl+xTZPXOi5oAuL/dCQ+qIQBuTUisoXLvUc1KJXboZ4iObSO+OMZZaKP0qIEOATihJ0lnkbBmZe6+l7xt0dU/ph9id4kFH4/1u0MAasJ94obq8wQg/l2lasKufAIGic9cagLcUv3ZsS0jG7Z7gYwq1Ux8rxRrHiAi0LMKgxNcb4/UfInyj5Q15+7+7CM7Fqb9wVTtEQulJz2/DdTb/EWq+nMoyqqXaklALk9AjhCkCAAnAqeldLteVePaP9HLVK7yI4TBWEp8oToZBA9cCD2TyFN4/dKKjK2/rso8uBW38aUIQMrmV0MAUt8tduFHKOqyt1pIQEwArrQAMQmQ4nNGVkqILP/UiG7hVRN6ESHhCVIXjEwEKwU6GQQQOKFHA/SkVHwEqjDvTN7FU6uurpq5vuDqeTReXyyIuJCqEX4xEfic8CMUJwHgf8uRADfLkJI2IOULcEUC4vNCmVY9KsW2ebhDaPnqHcnwqIaSzwIRAU0CSDuw6SFFvwHz4tle3kAQcq2csuVlHy+4cemfzJ1//XN7/4Z0bj84O+DcEX5Xgm8V/Y6aST38lgCkzqHGJyClCbhDAlLOQUkiQOtyXZ+vHdOk00MhcRXbEqHCxCIBWEJg8gvYbR0lDyTkSM4NygKPQFEFeWcLb6bvvHtk65aMzYvRTDxS3nY5W1+KAOSEX+ozuQy/EhF+hOKa+UILCcgRgVj4nRKFwFn4SYnzCq4nok7zmLj2vVqGVa3T3hRTti2QhkjFO2OJAAUVkMmgawpFB5IVdLRGmrwLYbfDZss23725Oz/tzI6bO1btzz1z8C5I59TjQigWTrHgSvXqSmufFH6E4pz6Ri0JKJkEaoRfSRuQW0BqbYypEFKh+/OJYZXrJppKl2tsiIhuQOuWyoSAwJABSwiUnRAo9m8drsEJOgGcsNsFXlVrtdlyrLn3Tpjv3Dyam3bqSMbGxSmWu9cLQX4gDaWwyPX6coIvt62k8peY8IPKR1qU53NFAq6chHLCb5D4rpRfQNYskFyHhZHlHnymdlSdpk0NsRXqhUSVqkWYwuPpTx1pyK7AkgOiBY4UKHa/nSgCV4NgHjDBzseMCbhd2N0Qcg4opaMg94I5995Z252MU1mnDx7J+GfJObDw00u5Enp8W629L0cCrpx8Pif83DspbrhDAu4QgSt/gLvagBwZCLaNUbHG0m16xofXaFgnLK5ybTIiuiYRElGFDAmpAOxsRx6BcqwojDCwj4RkIfoMsGM0vSxRrIwQbRDYMfyDYgVaoKFraWnID1tYcN1mzrtizc46b7519WzuheNn7uxcdcGSn20VHOm8rUbolXp9uVi/q32uBL/EhV/ra/HmeeUET8qZRyosSlEBd0jAtTYgfw8MEDHENu9UOaxKQjVTXOWqhojYKoawyPI0McSRptA4MBhjweGTCHbY6F77jtVcmEFZ8m9Z83IzrDl30gpup6cVXjp1OXP/5qsiQUeQExxPVH21wu9K4KUEX2o4r08IP0JJEYDUuaUETE4TcEUESgTgyhzQQgRSz1PyGRujYkyRCS3KRlavX8EYU7aMMbp0GVpziCFCw2INptBYwhhC/x1aijSQsUCawuhfCSvBd+U+KMgHmznfZrVkUoXme5TVfMtamHeXKijIpAryMs05t25bM2/fybl8PD3nxP6bEgLu+CXlfd4UfLHAqzUDXAk+pXB9SvdZ5ChJApA6v1iY1JoFrgiBkPnbExLwSCMAN8hBEmFhhojKCVFh5atFGqJiI0wRpSMhLDzCGBEdQWv5BmNEZARBGAxEqCmCoEgDYQiNAANhpMkjXNMLogpzKQtYKWtBLkXYrFR+YQ6KdVhyc3JpFd9qybubA3kFeebcOznW7Mzc/BuXc3Ivncyme3SrG6dxJeTiv72h6ksJvrsEIKUtKJ1Pzb0VK0qaAOSuQckkcKURSJEBAerMAa3+ATVkIPfM1bwLX3hfaqCmQbsr9Pjfnjj31Aq+KxNATuiVeny5Xl/tsyoy+FKDctckcEcrkBN6T00CdzUCj8wEUP9+Suo9qm28lBv73enpubU76r5cco87ZKCmt/dJlV8MXyIAuetRow1ICbArwZczA0iF35U6t9S1Sa3Vbivtc+fz4oarBu2JsOPb3hB8pd5fiQDkvueJ4Kt5VsUGX2tEctekJFxqiUCt4LujBSg5Cj3VCIpCQyhqeNrDi/92x7bH92kRfiUicEfwAfxM+BF8pQGpvTZXguaKDNSQgyfmgNL1SF233D41f8vtc+dzT6G1l1f6213HHr7trvC7I+RqhN7vBJ+DLxOA3PXJCZFaX4ErNd8TEgBwTQZKa6VtNc/Bnc+0gHLzMy29vXgtJfTiv9UKvzv73BF8tc/BZ+DrBKB0ne4SgdQ+tQLuKn3YU03AlUaghgiU9nt6nLede0rCLrXP054f/0ytMCsdCxCggs/BXwhA6XqVhMgdzUDNMUWpBRSFs9Bb71dLz6+l1+fWWnp/d3pzT1R8vxR8Dv5GAErX7IoIuLU7GoInvb4r4fek99dCAmo+l4NWm1+t8Iv3eYMElD6T+1zp/Er3484z8yn4IwG4unZXgqNWVXeHLFz9ltI+ULFP6n61mgOeQovaj2+74/ST2qeWEFwd4+q8SttqnotPw58JwNU9yAmMmrWWbaV9SmtX+5TutyjNgKJw/HnDFJDa5+62mrWr5+CXgs8hEAhAzf2o0Qqk9qklCW+s1W67u68ooNbu9bYvQG7tyTEAQdbbSyHQCMDVfSn1pmp8CEqfeaOHd6fn9ycTQPy3VvPAnbWrfWqu3dV+v0WgEoCa+3NHwNQKrjeEvaRVflcoCpNAzbYnAu1Oz+7ufQUEAp0A3LlXTwjB0+M86d2LKweAg9pG705v6U0tQctxau4vYIUeRzARgDv37anNrbUnd1fItbw/8Xe1NHh3VWY1wqhW0NV+X809BoXQ4whWAsCh5hm4I4Bae29vqPdF9V61agVyn2nRItz9vqf3FJDQCcAZ3hCyovjMnWvz1vc8FQ4tPW1RfOaNewpI6ATgGu48Iy3ahJbzavmOJ/BEiLwpoO6cXxd4BegE4D6KsxcOlPdTlIThre8FJQKlgfkCvPUsA/2deEtAdUH3AgK9sfkKfOU5q70OXxEuX7mOgIWvNEwdwfcudOH2AQRbowt0+LITUIcP4v+UtoP63D5ivAAAAABJRU5ErkJggg==');
    
}
img.HtmlIcon {
	height:40px;
    width:40px;
    content: url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAYAAADDPmHLAAATG0lEQVR4Xu2dd3hUxfrHv3POtuwmmwaBEEVFIIRQDCDlUizY7hWvSFGkXkpCiSKCggKCokgRUfFSBETlqhcp4vWqP3ks10eainSpgh3SO6Rt9pzfMydmk02ymbM1s+G8z8Mf4bwz8847nzM7Z8o7BB6IPBxiUVSHnpJMboUs9ZBB2guSHCsBoYIg6D3IUkui0gOSJNkE4JIkkDQCcgYy+Z5A/tJacOoA2Qa7ymwcasSdBLkpia0FuSJVloQxgoBYd9Jquv71gCzjgkDwtk3W/zN647E/1JamCoCi8W2byzr9YrskjRcEQac2c00v8B6gPYQokE0g0nzr+rPZLAuYABQkdxwFyf4qBCGSlZn2nCcPSLmAkBq+4dSWhqxyCYA8PNFQEC6tJgSTeKqWZovbHlhnJebpZP1BW30p6wUgfUwXS4ixfDsIucvt4rQE3HlAhvRJMQkd3mr9weLaxtUBgL75hRHyh4B8J3c10Qzy2AMS8HEEMd9XuyeoA0D+pIQNWrfvsZ/5TihjbfjGU9NqGukEgDLgg/w237XQrPPGA7KMEREbT71XlYcDgKIpXWIkW9lpbbTvjXv5TyvJyBGNcrx19ekcaq0DgMLkhPUykMx/FTQLvfaAhDXhr59KdQBAZ/iIveK8NsnjtWuDIgMJUrlRlNuY1529oPQA+cnxywiE2UFhvWakTzxAQJZYN5ycS+jCTr414Xdtbt8nfg2aTAjkP8LiTl9DCid36CNLZF/QWK4Z6jsPSKQXyU9JmEdkPOe7XLWcgsUDMuS5JD85fieBMDhYjNbs9J0HiER2kLzkDicEkI6+y1bLKVg8IBP8QAomxudqkz/B0mS+tZNOCpG8ifHl2jYu3zo2WHKj8wGkIDlBDhaDNTt97wGuAdB3ux1CXHtHrUu/3AZyOVP5W4hpDX2vexzPys8ehXxmD/T9hkGIbMH0lJSfA9vXWyC27wFdh14Ofdtv5yAd3eWUXt9rEISYaxz/V3Z0L/DbEdS1byvI5Sxm2TwpcA1AyMTloM6vkqxZQyHmnITOQKBL7AfzI+sdzy79dzNK3nkeUUu2QryuC9PHtt9+RM7MexA5ehqMg6c79OUKG/Lm3Atd0S/K/4lXxcMyfzsgiA6dgjeWw/b5JoRPX1HLvmHQ5ZyAaGDutGPaFyiF4AJg5lCUn/8BIZECTEn96wBQsGExYlZuhb5tV6b/KACZDw1C+MhpCB3xiJN+2dH9KFw6AUarAMucdyC2ucHpOQXg0s7X0WzOChj7VvdCWYp9J2COJEEDQfABcO4HZQ3TOuAmhM1y7gEoAM1XbIWhvXoAwh6YBusoZwBoa+eumAW91YywlGfrwFQFQOTMFTDfXAsAxT4SNBAEFQDlJw9CKi6qHAOER8HQrrqrpz8BFABDQjcIoeHQGYGQHv1huHWUowGLP9+Bkv2fKX/LxZdQduIAXAFgz8sG0ekghEW4DwBNESQQBBUADfXrVQDU1LEOut/pDa56c2vquAKgobKYPUBV4iCAoEkDYLljOCIeql7mUANAxe/nobv6eqf2r/1/qgEIgp5AA6DWGCB36XSEJ8+DGF35KWn79Swuf/QvRKRWjwXcAoBzCIIKgNwlqbD98qPSMIbEHoic/rzTZyAdA9QUT3qA7KfGgxhNiJ6/FpDsyHr8Aehat0PkI0ucPgPpV0DtQWDFhV8gl5c62VCy91MUbV3L7ZggqABQPrPoKBuAqVt/RD+90S8AlB3dh6jHXoQ9NxMFm5bBPHCIKgDqGzdc/nQL8tcsrHzE4ZhAA6DWTwDtASgAdPQvl5dBLivxHQAcQsA1AKZxS2DoUz0TmPnYcNjOnVBeJmNSPzRbWGMe4MPNKNi01OklNN82FJEP1fjtfvMFXPrgDSedsOFTYB1VPROYvWACyo5945zPrYOdfm4K/swncsYyp3mA+nuA95C/7hnnRxz1BFwDINlkFOdJkKWGPsqC9BknEHANAG1aDQL/As49ABoEGgCKB7SewD8gBEUPUFV1DQLfQ8AFAEJ0K4Q++4nq2slNYA9TRsptsOdkNPrXARcAEGMIwl49qBqApqB4cUgn0M0ndSTAXwdcAECdQAGgIFwJIl0uRNqDN7quagAh4AaA0MW7IDS/+kpof1Rc+BkZUxnhlwIEATcAWJ54t87Wq6ZKA92Ikv3kaHb1AgABNwCYp70K3Q0D2U5pAhp0hTB3Wd1taPVWzc8QcAOAafTTMAy4vwk0L7sKlz9+G/mv1d1r6DKlHyHgBgDjvQ/DePdUtveagEbhO6+g6L017tXETxBwA4DhlpEwPTjfPacEqXb+6gW4vMsRqEt9LfwAATcA6LvfgZDJL6t3RhBr5iyehtJvv/CsBj6GgBsAxHY9YHl8s2dOCbJUdJtZ+ZkjnlvtQwi4AUBoeR1CF33suVOCKGV68kDYM1SH9K+/Zj6CgBsAiDkMYS9/q6oZS77+CLKtXJVuoJSIwYSQ/n9TVdzF4TcoW828Fh9AwA0A1BnWNUcAnYHpF7qQUpH+O1MvkAq6uOvQYu2nzCLl0mJcvD+JqadawUsIuAIgdOmXEKJaMuueNXsEyk8fZuoFUsGYeCOaLWGHWa5I/w0ZKbf71jQvIOAKAMu8bRCvSWQ6J+f5VJR+8zlTL5AKIX3vQtScV5hFUnApwD4XDyHgCgDzw+ug6zyA6Ru6z57ut+dJLHePQsTkBUyTKLgUYL+IBxBwBUDIuMXQ972P6ZvCd1ehaMtqpl4gFegRc3rQlCVOB0VYyp48dxMCrgAwDnkUxrvYAcsvf/Ju3b32njjLh2kiUhfBcucDzBwpuBRgv4obEHAFgOG2sTDd/wTTNyX7doEe4uRJouetgakXezWTHhKhAPtdVELAFQD6noMQMmk50zc0UETWEyNd6lnHPApTT3ZjMAtyQ0HXIg7EZGamoOBSgAMiKiDgCgBdh94wz9zE9A09hZsx1fWdVuETnkDo4PHMfBpDIevJUSg/8X3gimZAwBUAQlw7hC78D9M5NExM2ogeLvVCh0xC+D8eZ+bTGAoUXApwQKUBCLgCgIRFIezFPWzfyDIuDuvicjq49nFudoaB06DgVsU5Clypro+mcwUAiADr2qNOMflcOSl94i2wZ12s97Gp+wBEL9wQUP+qKYyuX1wc2lmNqn906ukJ+AKAbg9fsRvEGs10QNasYSj/8Xi9evrrExHz0vvMPAKtQIGl4Daq1IKAOwAsC3Yq0TlZkvPsFJQe+F+9ajS+T8s3vmZlEfDnFFgKbqNLDQi4A8A8YyN0Hf/C9FHeq/NQ/Nn2evWIqEPLN3crIVkCIcRgVPUJSIGl4HIhf0LAHQAhE5ZB37s6+qYrZxX+6yUUbVvHhS/VxhqkgSrzVs3lwmbFCELAHQCm4bNhuP0fTCfVFxiSmchPChGTn4LlbvZBj6Ltr6Fw80o/WeFZttwBYLhzIkxDZzFrU7L7E+S+8ChTLxAKdBmYLgezhIaxo+DyJNwBoO8zGCHjq+P/uXJW2fHvkD1vDBe+pBtB6IYQluS+MBMlu/na98gdALXvAXDlVBq+NSNV3R48VsN4+5xuBaNbwliSPW8syo6r2/fIystXz7kDQGydAMv8Hcz6SUUFSBvVk6kXCIXYfx+AYLEyi8pIvRsVv59j6gVSgTsASEQLhC2v//veyTGyhAtDOgF2eyD9VacsotOj1Y7jqj4500b3hlSY16j21i6cOwAg6mBdc1SVQ2nQR59sr2Y0SfEXO0E3c9YnYrNYtNz0FbtR7XZcGJIIcBbfhj8A6HTwS/tBLOFspwZII3PmEEeE0tpFGtp2QvOV7J8se14W0sf1C5DF6ovhEoDQZz6CENtGfS38rJk+4SbYs9PrLcXU42ZEL3iNaYHtp1PInMHfDb1cAmCe9SZ08SoGeJIdaChimFh90xezhVwp0KXnoZ3rD+gEQIlHXCNsvatsSg/tQc7TEz02w18JuQQgJGUl9D1UTKzQoM3vV4eMr+2kVlsPq5qjb8i50qVCpI10/Y0fNiwF1rHsiaviLz9A3stz/NWOHufLJQCmEfOcLntyVTs6CKwdIbymbov1n0HXsrXHzqEJK/74CRnT/uoyj/BJcxH693HMMiioNMo4b8IlAMa/TYZxMDuGTvFXHyJvpeutX82Xb4Ghg3fn8FgBnejFEiEDqkPau2rg+u4r4gEGLgGg17+GjF3E9E/Zkb2g8f1dSfTc1TD1vo2ZT0MKrIBOzZ57C8YuvZll5L00G8X/Y+93ZGbkYwUuAdB1vQXmVPbJH9svZ5A5/e8uXRIx7RlY7vLuHB4roFPMPz+CvnU7ZrNkL5yIssMq9jsyc/KtApcA0Lt/LU+yz/5J+dlIG9vXpUesI6cjbIR35/BYAZ1i3/4GgjWS2SqZj9wL28+nmXqBVuASACE6DqFLKm/4bFAYs2u6uGthHjgUpqS+0LdJAN10qkpkCeXnT4L+xBR/tgMVab/Wn0wQEbeTXhXLzjd9XF/Q20h5Ey4BgN4E6+pDqnyVNqYPpIJcpi59S41d+8CU1A/GG/4COoVbU+yZF1F6ZK/S6PTyaKkon5mnGNkcLd9S0a3TdYv7OinX0PEmfAJAp4NXHQAxWZj+ynx4EGy/Vt4l6I7Q5VsKgyzLKDuyx6PDGrRXiXn5A2axdAGILgTxKNwCoDZ4dPb8cXVu+QqUo03d+iH66deZxVVdVc9UbAQFbgGwzHkH4vXsb3h6zTsNGtUYYqbXyc1YxiyaXkNHQeVRuAXAPHUVdEnsb/iCjc/j0odvNYpv1Z5BpIBSUHkUbgEwjVoIw03sgAt0mZVG3aTf2KVH90MuvuRXPwvmMGUwaezWTzmCLkY2Y5ZHAaWg8ijcAmC8JxX0nzsi2ytgO3MUpYf3oOzQ7sp7hr3dgEEIDO06KzeV0vuKDe27Am6uMhZufhFF26tvOXWnTv7W5RYAw80PwjTyKa/qT0ff9B5guhRLgaC9hRoRo2Jg7Nbf8clI7xH2RuhhEHoohEfhFgB9tzsQMsWHwaNlGXQ0Xnbwa6WHoEEaqi5tInoDDIk3go7qjUn9oW/dVtWWNLUNmrNoMkq/V7FtTG2GPtTjFgCxbTdYZrMDL3rqC7m0BGU/fAcIAoyJPfx6YVXNa+89tddf6bgFQIi5BqHP/Z+/6h3QfNMn3Ax7dlpAy1RbGLcAEFMowlZ9p7YeXOspW8o4C25d5TBuAaAGWlcfBvRGrhuXZRzzjkBWBn5+zjUAoUu/gBDlvGjjZ3/4PHtVdwT6vFT1GXINgGXuVojXdlJfGw41WVvKGttkrgHQD5oB0x2jvd7Z21hOpncD0I2rfg8N60UFuQYAElBcJEB3fVLl1GtSP+jbdPTpN7oXvqublM41/HyqcuLp8G6Unzrs8jyBT8v1IjO+AaAVoxDkS7CXV54AEcKjlR0+lTN1fZW/G1OkghyUHt6rzDSWHtkHuk0tmIR/AOqBwOFgQqC/LsExg2fo2A00QJQ/ha43lJ88pLzh9E2nb7zX6w3+NJiRd3AA0BAENSpIQiwwdu6lLNrQnwxvD4VUZU33BJYd2lO5yHTsG9Df9qYiwQOASghqNgwFwNj9z0WdLr1VDyblkstKQ5ce2q0sM/N2QZUv4QsuADyAoMpZNJCDISFJWeyhiz70p8MRR5AO3s6fROnh3cqbTu/1oV39lSDBB0AVBHkS7LaGjgY33HxCRDNlEAlJUnYDq9lZ3BSBCE4AfARBU2xQd+tE8ibGlwuCoHc3IRf69BPRy56Ai3o0ohGkYGJ8LgSBfbapEY1ssGgNAo9bRiaQSV5yhxMCSEePc+EhoQaBR60giKSM5CcnvE8A9mV9HhURwEQaBG47W6cnF0j+pI5zCZEXu52axwQaBG61imiSd5GCSQm9QbDfrZQ8K2sQqG4d0SKMJ/JwiIURCTQKYivVKXlX1CBQ00L2ONNZo3KlRmFywlIZ4C+ElZpquNLRIGjQe4IR+1ptP9tXASBnfKerBcF2PmjnAzQI3HpVaKMbzGL35u+dOuS4VKcwucM6GWSyWzkFg7LWE9RpJdFIjsVuP9OVPqgGIKV9M1kmZwAhKhja1S0bNQgc7pIByRBmbNfi3eM/OQFA/yhITqAhtf7tlnODRVmDQGkpMQQvxm49+1hVs9W5V63J/hTQGl/hEAgG6XirHee61Hxn6wAgp3TXF8iXPiAQ+LiPxcc9DD0tXpLr3VKyj00KSHZEj4xW1lbXkje/Km0QAPrwYkp3s1m+tE2DICBt4/dCaONLor7j1dtO1Amn5vJqTdoTFErFr4Bgqt8tbIQCrpSegHb7sWFX9az95rscA9Rui/xJCQ/IwGqBoHH3X/sBkqYMAR3t60OEV1puPT2zIdepuly3MLVDtFxKFkmCNEmAYPBDWzRalk0NAtqggpEcE4k4NGbbSeYVZaoAqGqd4int4yoqdA+BSKNlkKsardV8XHATgcAuGPGtngjTm287fVCti9wCoCpTZQHJ2rG7LEgDBUnoLolyvCwhFkQKC9YeIpggoDt5BBHlIiHZEHFCELCluXRmM9kGt2PR/j+B6CCxdQIiWAAAAABJRU5ErkJggg==');
    
}

img.CsvIcon {
	height:40px;
    width:40px;
    content: url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAYAAADDPmHLAAAT0klEQVR4Xu1dd1xUxxP/PrBXwJ+9BnuJxkRji1EsWFGjFI0dbFiwodhQVAQRjCVgISIaY5dgLDFiN5bYYq+xd8USu0bu7veZpyBcg3tv79iDN38ptzs7O/N922dGgB5q5tGnslqjaSeo1RUg2BTQV0b5m3VoQC2oXtrA5pytIGyIWx11RltqIfkfmnXydNTYaGYDgot1dE+R0kQNbBHUGLw9ZvHVxHpJAGju7tUgQaXaYGNj42AiU6W4dWngX41G02bnuugDJLYIAPryVVAfUYxvXZaUIe2/Klt1zd2rllwXAdDUrfcGZdiXoU6rrKrZuGNtdDuBFnwatfqcVfZBEVqWBmzV6spCE3cvP0GjmS6Lk1LZKjWgEYRRQlPX3lEQBE+r7IEitDwNqNVLhKZuXusBTXt5nJTa1qkBdYwCAOu0HCOpFQAwUqS1slEAYK2WYyS3AoAkRRYsYI/yjmVQomhh0L9z58qFLLa2ePvuHV69foMH8Y9w6+59XLpyHS9evWZkgPRmk8kBULm8I5p8Uwd1vqyOYoULpckaGo0Gl6/fxMGjJ7Bt70HcexCfpnp8FsqEABAEQTS6e7uWKFu6pGy7HD15FitiN+PUuYuyeVmeQSYDQPXKFeDTpxvKlCzOXNeHj59G+OIVuPvgIXPe5mOYSQCQLWtW9O/hjnbOTqARwFz03/v3iPxlLdZv2WGuJhjzzQQAKFjAAYF+Q1C2TCnGyjPMbu9fRxESHoV3//1nsTalNZTBAVCyeFGETfRFAXs7afqRUevsxcuYMH0O5zuGDAwAWtXPCRwL+/z5ZJhRXtXz/1zFqClh4laST8qgAMibOxfCg/1RvEjatnbmNM6h46cwYfpc0PaRP8qgAAj080Hdr2pwo+8lq9fjl5iN3MjzSZAMCIDWTb/FiP49uVK2Wq2Gz4QgXLh8jSu5ACsAQM4c2cUVfKECDsiaNYt4LEtHsjfv3NMZVvPnzYOlc4ORJ3cuzhQNXLp6A4PGTuVsKuAYAPVrfYF2LZxQs1oV2Nra6Bj032fPsevAYcRs2ob78Y/E3wf17oLvWjXjzviJAoVERGHbHvExLifEIQCKFy2M0QM9UbViuTQpKSEhAat+24INcbuw7MfpyJ4tW5rqpUehW3fuwXOEP0ejAGcA+PLzKgjwHYhcOXOabJ/nL14iX948JtezdIWxQbNw5ISOg46lxfjYHkcAoC9+hv9Irr9gFlbauf8QguZEsmDFgAcnAKAvd9HMKXCwy8+gU3yzoEOhDr2GIEGl4kBQTgAwtE83uDg7caAQy4gwbOJ0nLnwj2UaM9oKBwCgo9qV80ORJUsWDhRiGREWLV8nLlzTnzgAQMfWzTCwV5f014UFJdi6ez9C5y22YIuGmuIAAFP9fFCPo2NbS1iFbgqH+gdboqlU2uAAALR3L1q4IAfKsJwIN27fhdcIf8s1aLAlDgDw25Jw5M5l+r6fA+1JFiH+8RN08R4luT67ihwAIDb6R9D1bWaip8+ew63vcA66nM4AoPd5i2cFomSxIhwow3IikF+BX+BM3Lv/MJ1fDKUDAPLmyY1v69YCXfZUqVgu03392jC7//ARTp2/hANHj+PI8TMWfkdoQQCULlEMHu1boUmDrzPVnt+UcYVGhj92/om1m7biydNnplSVWNYCAKAv3rNLR7Rt1sisT7IlaoDLau/e/Yd1m+Kw/NdNoKfm5iMzA4Bu98YM7gMH+4x/xm8OI92+9wDTZi/EP9dumIM9zPoiyLWtM/p3d1e+epmmoxFg1sKloh8iezLTCNDn+07o3KE1e3kzKUd6Ubzg59WI2byNsQbMAAAyPAFAKj359xn2HjyKV2/eSGXBZT1yN29crxbsZPgp/LBwKX7fsZdh/xgDgM70p4weInnYJ1frweMC8ezFS4ad5IcVGT8iaAIKF5QWfpleF/tOCWPoicwQAOR+teiHqbL29fOWrMSvv2/nx2JmkMTdpQX6dXeXzJlGSLpHePHylWQenyoyBEDAyEH4ps6XsoSauWAJtuz8UxYPUypTBBANNFCp1KZUk1XWpXljDO3bXRYPellML4zlEyMA1KhaETMnjZYtD52IjQyYYZFXs/TUfJxPf6hUKoRELLIICGxsbDB7yhhUqVBWlq5oUUg+BuRrII8YAeCHgNGoXqWiPFk+1j5+5jw2b9+LV6/NF4eHDEGnkhQwgujE2QtYu3EraI41F5GzCn39rPR06O9TGD99jkxxGQDAsXQJRIZOlimIUt1UDdAo0NNnnMyIJAwA4N2zMzq1aW6q/FyUp4sYCphfpOD/uJDHVCHI4ZQcT6UTAwD8Eh6CIoWsT4E0f/pNDQMEAaH+vij3meUiiEg3WMqa12/dRZ+Rcl4WyQQAPeWiJ13WRonGT4z3RxdW1goC177DQX6S0kgmABrVqw3/4QOktW2k1rWbt/H6zVsmfOnRCfkbkucwkbbxExvRBsGz5y9w5/5DZjsSevZmjuhk/iFzcfDYSYm6kgmAHm7t0cOtncTGdauR0ccFz2bmNPFhq9cPBFRjxjcEgp37DjHdItJ2OWjMMGTPzs6BNXLZGqzZuFWiDWQCYHi/HmjTrJHExnWryV/UfOJpqvEtBYI+XV3RuX0rZjpb/8dOhC9eLpGfTAD4D/dGo3q1JDauWy0kfBGTa0+pxrcECFo6fQNf797MdCbPyUQ2AAYkDa8seiSvMx8kkGt8c4OApiQKVcuKaJoKmivV21gmAPwGeaF5o/qs+iIuuJat24iNcbvEUDCmEhl/9EAvNKz7VZrm/NT4ay8Mdx84DLqvkHJ3QCeB7Vs2QdeObVNr1qTfN23bg9k//WxSnU+FZQKA54cfhlb7pmqK9y3iytjfEbUyxtRufSwvEwA8RuRKy2rfVG3xDAJyMqWpUxrJBEDFsp8hIniCtLbNVIvVl68tHq8gGEi3gleuS9SmTADQnEu+fTmyZ5coANtq5jK+oYUhW+lN5/b6zRt06O0j4xZTJgBI5EkjB6JhnQ+LrvQkcxufRxDsO/w3AsIiZKidAQDIzWviCG8ZQqSsSsfANKcZOwq2sRHQusm3qFC2jFjZUsY3BIILl6/ij137oFYbjgecK2cOtHRqiDIlizHTFRmfQCCdGACApoEV80KZhGS/cv0mfCYEG/WPY7XPl660DzW11wRpOTamI+DwaePxWakScpsXXce+HzhKZrApBgCgnnRq4wzvnh6yOzXnp2XYuG23QT68GN/QSJAWEHRo1RSDe38vW1fzl5KfQJxMPowAQDF8o2dPk/2wwhgAeDO+VBBQKFsKaSuHHsQ/hueICSAfQnnECAAkRK0a1TB9vLygB1du3BKjamt3TNv41B5l63r5KvWn0UvXbgCFaJVCNFR37dgm1ap58+TBV9WrJJUzNBLQbom2zeQpLYfkXQEnb5khAIjtEK+uaN+iiZy+iVHA43bvTzoKpgecdIFS3rG0JL5yYvKRcytFL5VCF69cR9zuTwvD3LlzoUXjBrKDYZBnEHkIsSHGAKBYf6H+I/H5x9e2bISUxyW9ACBPav21L165huETQxi6jDMGAIlNlx4zA0YzScrIQokZBQA0jVFf2LrNmQEAZDQK+hQ4ZmiaQ76zMLQhHhkBAHTOQS+lpL/9M6QdMwGAmqOdAW13WL4YkgIUawdA4hX0m7fmyDxmRgAkGqt+7ZqgYNDpkbuPZLBWAFD+A8pCSieM5iMLAICEp7w/Hds0h2vbFrK8h6UowtoAQFvgzTv2ilnGCATmJQsBILETdBTa9Ju64pMo8sujLZ65yVoAQHcg2//8S4wSxnahZ0zDFgZAclFot1C1Qjlxf08JHu3t8oshY21tbWVjgi5eShQtLPJhBQDKVMZsHtZo8PT5c9y591AMAEWp5+l0z/KUjgAwZ2eTH+CwAoAcPubsqzzeCgCM6o8VkOQZyZy1FQAoAGjq5rUe0LQ3J84szZvVl8uKj6X7n/b2lBFAGQGUEcAwBpQRIO1jCVclWRmOFR+ulJNCGGUKUKYAZQpQpgBlF2AAA8oUwO/kZZGhWwFABgDAoydPJYecIQAkpqRXjoKtCAxyHnMa6qYCACsCQNWK5ZiGYaGuB8+NZBCblzclZtBtIG9q5lceBQD82sYikikAsIia+W3EigBAr4wrlXPEZ6WKo4CdnRgN7OWr16In0clzF9McVCpb1qyoVqm86KadP29eMWEEZd+gFz/n/7mqk4nDPn8+vbGQb9+9n2ra1+zZsoGiqWsTZf1InxdA2pJYAQAKFnBAlw6t0bRhXYNZxhNUKuzafxhLVscaVCw9N+vayQVtm32LXDn1ZyunKGWUOyB2yw4cPHpCjFpG4V0XzZyiY8TlMZsQvTrW6MfdrGE9jBnSR6dMwMwI7Dskx6+f1ZjCMQAoxq+Lc2Mx9yB9SWkherNHwSa1gyZQNHPy8StWuFBa2IhlKHvJjPAo3I9/hHnB/knBKBIZUBzhXkPHGY0lHOjng7pf1UjRJr30de8/EgkJCWmWxXwFOQUAGb9fNze4ubQwue/01U4Ki8CBI8fFujR1LAiZJMkj97etO/Fj1HLR4ZUcX7XJ22+Kwaye5B21dtFsUF6i5LR+yw6ER68wuV/mqcApAFycnURnEkP09t07vH+fIEbp0EcUXqav70RxOmjbvBGG9e2hU4yA8ubtW+TMkUNvmjtaEwwcM0V8CUztrI38QSfp9ZoNf4jOG/rIUEjYAX6TcfnaTfPY02SuHAKgYAF7LJkTpHfYJ9doSit34/Zdceil9YFHu5agqBvatCJ2Mxav/FWMWUCxC5LT5u17MG/pKjEOAXk0O5YqDvJgIjc2WvTR8Dx4/LQUhtIXF/nhoyfoOmi03mlAX7tXb9xG/9EBzELQm2xvnQocAmBADw9Q3mFtMhZIulWThhg5oNeHufvcRTHr9t+nz4uKXjwrEKWKF03BzlBwRQrg0MujgzhyxG5Jmb+wTs3qmDZ2qI5cQ/2Dcfbi5RR/z58vrzhiaDu+sAnrIt/snzhwBgCa+1cvnAkHu5TZxiluEIWPMUaUt+DYqXM6xqCIHBTQMjlRrN99h4/hyIkzOHvpCmhLR2AxRrTtXDkvTCcTOgElInpliqr6ph1q02PASDN4+MoBBGcAoMweS+cE6fSo+5AxoLSyUigtUUsodczp85dw+Pgp7P3rmEGfvL5dXcV0c8mJ9vSdB/imCNYYNmkUvqhaKUU5WpRODA2X0gUz1uEMALW/qIbgcSnjDMU/foou3r6SlUDDf2TYZJ3VuCGGlK59Y9xu8UxB2xWMeNGUok2+k0PF8wMiB/v8WL1gps7CclJoOPZ/3JlI7gzzipwBgCKOUuTR5EQrZlo5yyEKaT/Ku7dJzqjkrDkiYIbOyeCP08ajcnnHFOLQonJW5IeQ7frCwFH+IQ/a+6tUcrphhrqcAUDfQosWZLTSlktktL7d3JKyhaaFn75kDPpy/9JRslvf4aKB50wdqxMZJWbzNsxfuiotTVq4DGcAMDTEdvEehfjHT5goh9po8HVN1KhSUbxbIC9lQ0QLQ9c+w1K4a9MBz5rIWeIBU3IaGzQLlMdv5fxQHXb9RwWAQuDxR5wBgHYBMYtmJz3DSlQY5fVduGyNUf3RHp7m4Tv3HuiUI750vkD79uQkppQrUkjM50tbSe2hncomn98T644f2h9ODb5OwStuzwFcvXELtI1lPYWZDzicAYA66uPVDe1aOKXoM32JE0LmghIm66MGtWti8qjB4lZu719HsXL97ykOcWhe7unWHjMiogzm2KM9O3292qFs6Mum7WJy0hcUk0K3006lbJmUGUhpi6h9pmA+g5rKmUMAUDbS6FmBOseulNmbDEs3dYnRsigQBOXh6eX+nXg9nJyiV8WKB0IUKGJhaEDSyeLRk2dAqdaOnz6fFJSaRoK6X1YXQaR9eNN72Hjxqjg5UZkV82bgfw72RjVOawKPfiMsGPEjAwCAutC5Q2tQPiJ9RF/53QfxeP/+PYoXKawzF1MdOuLtN2qSeJM3e8pYvUM7HczQ72/fvoWDvZ14BKxN9EX38Bmr95DIq0sndPmutVGN05UvXf3ySxyOAKQs+iJHD/SUnJFs2pyF4vuAr2t+jqCxwyTrPyQiCtv2HNBbv2SxImKAbGPELqav5C6kUpFTACSCoLtrO3R3ddF7W6evZ3SJEzZ/Cbb/eTDpZ+dG9THY83uDj0AMaYhCsS/4eY3RI+K5geNQpUJZvSxomqITQv72/snF5RgAiWLSk6oeru1Rv/YXRg9yDh0/hchla8WbQm2yy58Pbm2dxaDTdFFjjG7fe4DoVb9iz8GjqX52xrKmpWXnkmoDZi9gBQBI1AF56NSsVgmOpUuKK3WKJkYHMGTwYyfPivN5akQLxQqOZVCxbBnQYpPCvNN08+rVa9x7GI8zFy7j0tXrqV4MJbZD8Q9raJ35J/5GmbzonoBvsiIA8K1Ia5VOAYC1Wo6R3AoAGCnSWtkoALBWyzGSWx0jOLn3+sVGY6P75JVREwobfjUgCIgWmrp7joMGxk80+O2DIpkcDWjgKzh7eFVTqTWn5fBR6lqpBmxsKwokupOr5xYbAS2ttBuK2FI0oEHsjnWLO4oAaNbJ01Fjg2MA7KTwUupYlwbUwOOstpqacauib4kAIGri2ru+IAibFRBYlzFNlZaML0BovXNt1GGqmwQA+k/jzr3K2KqEuYDgYipjpbwVaECDWNssmqH05SdKmwIAiX907tSrUoKtbVtBpaoKGxi/PbGCfmdmEQXB5rlGjbOwtd24Y/VPl7R18X+LrzsnvQfswQAAAABJRU5ErkJggg==');
    
}
img.LogIcon {
	height:40px;
    width:40px;
    content: url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAYAAADDPmHLAAAgAElEQVR4Xu1dB3gURf/+3V1CSwJJCL03UYoIfKICCjaagPSqgFQVsKBU6R0CSFFEsNAhlBCKiOVTv89PkZJAIME/KCSk00lCz93t/3lnb465zd7t3pHEBZ3nuef2Zmf2dud9f79p786Y6J/wty4B09/66f95eLonArRs2dIvOzu7lCRJATabzfxPeRZcCVgsFrvJZLoeFBR04aeffrL6+s8+EwDgZ2VlVbVareX8/PyCicjP15v4J59PJWC1Wq1X/fz80osXL57oKwl8JkDjxo3L5eTk1LRYLFWJqAIR+fv0GP9k8rUEcogo1WazJfr7+/8ZHR2d7suFfCZAo0aNahLRI5Ik1SWiGjExMYN9uYF/8vhWAo0aNfqMiE6bTKZ4Ivo9JibmT1+u5DMBGjRo8JDFYqkrSVI9SZJqHzlypO+ZM2d8uQePeUymu7eYV8f4Q/Faar+9iXOXVu3BlP/rS4GVKlWKGjZsuMFkMp00mUxxNpstPjY29pQv1zI0AXhh5RfweU0Cb4ighxySJKli+sATQA3wgiKBXu+gB+y8sHY1BoSFhT2YHiA/gdeydq3zngDXAlrrvLeuu2TJkg8eATy5+7yyfi2Qtc5rWb1eoPWmc0eM0NDQB4cAeq3+ryKBt41BLZJoWTt/Tnf1P/I/MATw1uqNRgItsO/V0t2RJSQk5P73AErwC9oTKMFTA0tPlaBFAhHEvCJEcHDw/UuAvLD6vPIEelr8eohxLyB7QwpeLdy3BPDG6vV6BE+WrIcovpJAy/K9AVarXaA8X6JEifvPA5h+jyfL8KHsWezLV5Gpbn12jIIy1a4iP+OpJGecWMBS/HGShg1k5yyrVjvzStu3kG36ZDKVKEF+K1eTqQ5GpXOP8s2ZN5vFTxj3gXMEUMu98/Oj3n+HFi1YrHpdDowW2DgP6502YyrLMmXSVJeRyLffHcnil3y4TBcXihcvfv8QgBeOZegAkjp0kgtydxRJn611BeOhymT6I9lZAKK3sL/Wl6hjZ5Ze2hlJfms2sXQ5jz1C/usj2LF14Twq5IgXSZCYmEDf//A9m/d+4YVWVK1qtVz/IQIpAq5GJndxnuJx7syZ0/TNd98QSRK1adOOqler7gL2W++MoKWLP9JFgKCgoPuDACKIliYNyP7DL+wBzc81I+nwcVerchCA57HXrEiW06ksje2xR8jy80H5+Okm5H/sJCPDHRBg3WaYPFn79qBCjngR0F17dlJYyTAWdfnyZerQviPLm5KaQlu3RlBaehrZ7XZm5QBfDB8uXELvvvc2LV60lEXHn4inzREb2XGfXn2pjsPjwIKbN3uaDhz8jV54/kVq26ZdLiAjo7ZTqbBSzBNcunSJOnfq8mATQFnfW+pUJ/uJM8wSTXVrkPR7gosHkGpVIvOfKa6kcLhza40K5A8ySBLl1KpEhc6ksXT21Z+Tdd5MMj/fiuxf76EiCfKMqOiS58ybRcPfGIGs9MmKj2nC+IkszcJF4dSkyRPU9Klm5Ofn58wDwAE8v8Y7o95yEmDm7BnUpXNXln9H1HaaOGEySwfrHTbkdSpeogR99PFSmjcn3Akuv86UaZPo3bdHsftYsvRDmjplugsBRr49nJYt+ViXBwgMDDS2B1Br7Jkfr0/Sj/tlgJ5tShQd5wIUJ4BaL8H22MNk+fkQS29t/rjTA3CwpZjDdGfoa1QkOs6l4LmVj3r3fRa/aPEC6tm9F1WsWIneH/MezZ09j/z978oYcH0RcPyGdfO6GeRYMH8RuxbyL160hB1z9430akAmJSfR5s0baeyY8Sz93PmzqW/vV6hSpcrO+x3x1pv00dLluggQEBBgXAK4a+mbB/cjyVGP085IMn25wcVaRZevvIatf28ydZItT4raTv7rIu6S59o1ujNiKJkrVyH/GXNdSPX1vr307x++dynUF19oRe3avkThC+fTEw4PIJLgvdHv0pTJ06hE8RIsHwjA6+bpM6dRt67dmRvfHrmNNeYQRNBxrARy955d9M23+1zuo03rttT+pQ7OMhg+8g36eNknbgkgjgwalgAJCQkuoLoAGXeMTG8Okc+v+Jyo3qNOsAA+D34O926tXp78E9Llht/xWLIOGcCS+K1aTZYGDdnxrcqlyeTvT+bnXqBCi5eTKSjI5f/nhc9hFl/N0eBCgzBiy2YaN3YCpaam0MZNGyg9I50Byq182/atrC6/c+cOA1607rj447Rh43r2H6/0eZXq1q3H7k+0XvGYP//0mVOZxdeoAQ0N0enTf7L/njRxCvsN8HnwRAKeplixYsb0ACCAOw/gbTwjikMMolYteIoT8+o9VqZT++0uzlM8B02rq+jW9FVOFC1a1JgESExM1CSAJ2D1AC0WZF6QQA/wauC5A1QP0HrSeCJEkSJFjEmAs2fPuq8CVCz6Xsmgll+vxStB8Pa3ltXfK8j3LQG8dfWeLFqPR7gXEngiizdVgBbYWue9cf9IW7hwYWN6gKQk9aFcb0lxL/W/GqG8BdoXb6DlEfKyLVCoUCHjEsBbsL1p6Omt8/WQ4F5JcS8NQtHi9XoHsRtoWAIkJ8tj+XlFAr0NPj2Ae5smr6oAvQB7Uw1g3MKQsnAQIK/BL2gS5HWvwBtg9aY1LAFSUlzH8vOaDFoNPm+t3F16PSTQW+frBdWbdIYmQF6Dnt89AU8NPq3GoDeg5WVVgMkrQ1YBqany9K0aCSDqsA7uz875fbaGLPUbONPerFSKHRdLuaia3x53jG727yOnWbuJ5cV17mzeQLcmjWeCkIA1m8iv/qMu+TlAk6dOYoczps1UFYR4AnrYG0No5Qq8hqc/qIGNRtz4D8axi8yZdXfO4r8//4d2RO0gP4uFunXrTk80eVLzjwxNAHcewDagD5kdEzr2qO1USJjQQZ4bFcMoIPVSLgBx7mafbuTXpTs7Z43cSgGbI9lxVu2qFLg1ih3fmjeLAjdtzwUwxt2//uZrNg3dtu1LVNMxHq904fy+Afinn6zK9e6gO1T0Wvaff/5Be/buYVPa7dt3dN7Hps0bqWPHlwnnv/jyc12qIMMSIC1NnqdXI0HOo7XJ/5fDsuU2bUxF4uWXWXlaTgD++1r5UApKv8LSXAPQh4+ztNmN61GJP+XeRibit+xggpBrXTtSyOncbZBtkVupdKnSbMLn4sWLbDYP18EU7fr1a5kwBIIQgA7wxQDLH/r6YKcHOHb8GK1dt5olGdDvNarv8DhDhg2ili2epV9+/R8Tg0B0ogwRWzdTmdJlnPfRo3tPlyRZWVk0f8E8mjl9lqYHsFgsxqwCQAA18BF3u2pZKpKYwSzgVvXyVPTsOZe01yuUpMC0y6oeIKtcCJUAGSSJMiuUpOCMqyzv7VUr6ObMKeTfqi3d2R1FJc9nOQuP38ekKR/Q+6NGMyHGog8X0MwZsj5w5qzp1KxZc3rm6RYumgARcKTjv3G9CRPHU6+evVn+iC2baNaMOewYBBg5/C0KDglhQhPMLCo9w7gJY2js6HHsPsIXzKM5s+c57xXkXLlqBTVv/gzVdaiMPLHAsARIT3dV5KAQeEHcqV+LCv0aIwP3VCMqeuK0C9icAGoEyqpViYKi41nerEZ1KVhh6dZDB+jagL4UcuK0S8EnpyQzK4cKCHlnzZlB/V7pT5UrV6E3hr9OSxcvIwyqiEEkAPIA3FWffs6SwEPw6drhI9+kTz9Z6SQA0iD94KED6bOVX7hc82zSWVqz5kua7NAPTJsxhV7rP5DdBwKmhitUqEgtnmmhaf1IYGgCiACKx3de6UEWRz1u37GNCm/Y6uIBRJevJMH1np2pUPderHBytkVQ4Jaou3mvX6eswf3IUqUqBc7/0IVUO3dF0b5vvnYpVIhBOr3cmSDuaNa0GbV4piWThPHwxvBhNHfOfAougVVvZOvmBIAH6N2rD3Pj0BXMnil7ABF0NQLsiIqkr1D/CwFiENwHzuFlT9yHpyCOBBqWABkZGS4AiB4Aoo47g/qx84W/WOcUdSANwOeheMZVdphVNphKnMtkQNtij9D1frLrDVi7ifwbNmbHl8MCmSDE/8XWFLj8MzIXL+5CqqnTJ9OrfftRzZq1WHo0tNZvWEfTps6gpKSztHrNl5SalsoARX2P/4I1oi6HIATAiwSIPXaU1qyV2wCw4EcflXsyngiAa06cPIH6vzqAatV6iKX/449TtHbdGpoxfRYNGvKaC+4rlq90qZLUSGE2m43ZBgAB3HkAPIiae/cl3lMetXNinFqBarXktc6L1/QmrUeT93DSsAQ4d+6cWw9QEOCLha/8P2+B1wuk3nS+gn1feQAQ4K/2AGokUBaiJ9D0AKonjTf/6S058P+GHAk8f/58vnsANct25128sXotULXOF2Q1YGgCqIGRV+7/XsB3B6AWsFrnOfB603lr7e5IbEgPcOHChQL3AFou31d3rwWo1vm8ANrdNQzrAUCAgvQAWg09X6xeC1it8/kJvOhtDOkBMNau7Ibll/v3Bfz88ga+gu5pHSBP7RfDegAQwBPg+UUGPQ2w/PAGeoD3FmQ91zTsOABee1YbiBGBz2sS+Aq+r95ACyBfANfKo7xXQxPAHcAQddxwDOcGro9wijpQoFdKye/0lbx0XZVA1mNHKbN3N3YuePN2KvRYI3Z8c8Nayh73PhOEhG6OdApCxLoShTtuwlgWhbeBcX/eeIOBgwfQF5/Jw7/ughaAOK92H7jejz/9QFE7d5C/nz91795DlyDEsHMB8ABiAYtkuNG7K/l37cHKMGf7FgraIgs5eBqM64MAagS62rUDFekhzwXc3rqZQiLliZXzVcpSSNRXsk5g5lQK3b7biRG/Dsb/d3+1m00ld+jwMtVyzAsoweTp9QCOvJ5AVzunvA8uTMGCEx07dCLMXGJtAT1rBBiWAFiBg4OqBDL7oSoU5FgTgE/pimkulQygsMs3nPkvhBSl0ldvsd8Xq5SlksdPMaAv1qtFpZPlAafzVctRaNRXTBByuUMbKuOIFy0cQgwIQhDQS+nZQ55V5FO0KHgIQmDlAF8MPI57gNhjsfTlanlqeOCAQU5BCCZ0nm35HP3vl5+ZIKRjh5dzOYvNWzY5BSG4D6UgBARZu34tTZ86w4OvkU8ZVhEEArir7zPLBjMhByzxavlQCnWIN3h6TgA1D3AuuAiVBRkkiTJCi8nHqAI+XU5ZUyZQkbbt6WbUdiqfeTtX4UGIMW7MeCbEmB8+l031IkydNpmaN3+aASdOBys9AH5/vupLlgeaPkwHI8ByuSAEs4FvjXibCUIWLJzPXitXegFPghDMOELpO2L4SKrziLzQladgWAJcuSJLuNRIAFFH8ZgT7FxmwzoUcuaugJRZdmgxKnXlZq78zHIrl6awOFlCxj2AaOU5B3+jK317UBmHVIwXHqx87drVLkIMSLmqVq1Gg4cOouUffZJLECISACDCujkBANQnH3/KwH1zxBu0csUq9lcgANcMiNPHyvvAegDIO2PWNDY9zAUhiDv1xylaveYLmjPrrlLIHQkMKwsHAdQsGHHXenRyEXUEbd3pkpYTQC3/lc4vUZGesuXdithIoVF7nWUjZWfTldf6kl/ValRiobyYEydh5I7tuYQY0OthgaYp8ADNmufyAAAwfP5C5wohnAAASRSEbI7YxAQhiBdBF4+5F0Ajb+/XX7ng+VK79vRyx05MsPL8cy/QqVMn6bMvVrH1ibSC4QmgBqIo6ghct9kp6uDWzx8a9T7izpUoTGWz7rDonCPRdKWXvERMaESkM296kD8ThBRu3ZaCV65mghDRA4lCDFwTQgwIOqALPHs2kalwIQoFULyeh2AEdTkEIbB8EIBLvCAIgYgEAZ6EC0I46LiOUlOItJOnTmRSNC5MwX3gf7BQFJaO2fPVbsKyL9279aDGjf6lhT/zWoYcCbx6VVbzuOsJ6BkDUKYRS0PZfVPrznkTJ9bTai13ZZy7lr+3PQKtbqMWAwz7ejgI4K4R6IkYotWK5Mkv8JUFrAW0HnLwa/pyLS3AlecNu0JIZmamiwv2xRPosWA9aURSuStgX8DS8gK+XNNbAhh2jSAQwF0j0J2Vu6sWeKHkpdsXC9pboDx5AU9Vidb/eAs+0ht2lTBOAL0kcFct5DX4uB93IGkB5C3w3rYrfCGAYdcJxOtN3jYC3TX69Lh5PWk81ffegs/TK7/xH57iPHkeT+fUng/pDbtULAjgSyNQD5C+VgXuCtgXt32vBNBq/audVysbw64WzgngCwm8afHrIYze+l6PF3AHvBqJvCGWHs+g9qyG3S8gOztbtQrQ0ybQW+/nF/juiID4/CaAlmdQPrNhdwwBAbyxfl/cup48an1yT5audk4NdD11vy8eQIsAyi6tYfcM4gRQI4HteKyLqMO/QUMnWTKKy2/olsvGjuh3A79OTuwRutyjMzsRumWHUxByY/0ayhwziszBwVQyYgcVavCYqrWOHT+G5eWCEK0egWj1eFl0+UcrNL2AViMQ52fMkvcImDhhkvPZeb7//vc/tHN3FC0Ml19wVQaR+IbdNu7atWtuq4DMbh2doo5bWzZR6A7XyRGQQCSA+MCXOrWjYr36smvf2LyBwnZ+zQowrUIYhe2Wl2HPmjGFwoRr8oLFuLsoCKlRvYazbJVEwO/X3xzKZvwQ9Lh/PQ1DXCsh4Qx9+/23bEr7xRdbO7euQf7r16/Rqs9XUUpKsnM/Ak8EMOzGkSCAuyrgEsQbji1dLtV/yCnq4A8qEoCBG+hH5a9Z2emMCmFU5nd5a/pzj1Sncmmy8iitYikK2/U1E4RcfKkVlU+TNYkisBBi8BVCRCEGnyrmK4TgrVyALwasBYCl3LH+P66JLWM2bFzHkvAtYxCPTSaaNW3OlpnHzF7rVm1y3QesG1vXID2UU+IqIpi1rFSpEm3ZGkHz5y5wuQe1No9hN4/mBFAjwYXQYlQG8/2SROdKBjhFHXhapMfMHvcAyodOC/KnCqgeJIlSSxSmig5iXFu+jDInj6ei7TrQjR3bqGJ2Tq4BHy7EsNsl58ocAGH6jKnOFUIgseLEEV0+0mH9f8i0WJ6Z01y2jPlg/CQWj51EBg8cwraMwbY0M6fLq5CI3mH23Jls6xrcx4pPl7M9C3D+3LkMWrdhHY16ZxQTnIjbzajUBKysDLt9/PXrd0WdShJcrFKGSh7/I5esi6fjBFBr5KWXD6Uyv8ubUcADlE+XpWc83Dmwny717kZlHSuH8MLnsi++MQM2buBCjNffHEZLPlzKlDgiUG+OeJ2tAsLdP3YA4TuGwNK5hY4ZN5oWhi9i6bDZFLaSwb1jxxEci9eEl9m+fSu9NVLelGrJsg+pW5fuVL58BaYBeKzBY2waePwHY90KQsRyKVOmjDGng0EAd1XA1S7tmagD5yHqCHHU154IwM9d7NiGivV+hRXeTbQBdsn1Ps7bs7LoUv/ebIWQ4EXyvnu88NVW5uBCDFhz06ea0tPNn2GSMA44dgiZPm0mFQ+StQUgAN9NBI04bBqFtLg29iHEMQcd6d8fM8pJEn4fEH1A/SsGSNGwfc2EifLScWLgUjP+jMrz5cqVMzYB1EhgPRpDVx3S7pDN25moQwSfPySv99EGqHDdxqLvxBymyz3lbdZKohfgEE2kFDM7BSEhq9YwQYhY/3NBCIQYiIfwct36tUyIAUEIVungK4Rwq0c9/NuB/S5bxvBt47BlDJRACL169KZHHqnDrgvQw+ctZMdjxr3P3LjoAbBZVZdOXalKlaosHv8NlRD3CPzZcb+8+sjFCkFqV758eWMS4MaNu6peT+MB/OFEt6bVv1drDPHrKFviohdQO1a27tV+83zitdVa/DyvVjrxPkSSisdqoKuVVcWKFY1LAHfAewuwVnol+HoK2B1YSvDcgamVTk8+tft2F6dGesShx2BISRg8AG6woKzfnSW58whqAGqB6s4TiGTSew2lN/LkqTx5ySpVqhiTADdv5pZ1i2RQeyi1ho6nqkF0k3pdvzvgtYDzlM9bMqkRSYsAauWAuGrVqhmfAGpeIC/rfjXr9+QRtCxWDxmU1/CmSvHUzlDW/UjrrgpEfM2aNY1LAC33r1W354X1K4ngCSg1YLQs3xsiuEsregQ1TyYai7JMa9eubWwC6LV+pfvXIoe3DT9fgNcCH+8RuiON+H9iOrV4tet46vrxthW+69WrZ0wC3Lolv7OnbAiKD6a36+dNt88b168FntIykd4dmGrWLablx/jmH3fXU2sgKg2Jl2vDhg3vTwJ4snBvrV9PG0BPva9FCD3gi0CLANtsNifwWgQQn0dpMCIRsDhEkyZNjEsAve7fGzJ4avm7I4KWK9cCXq/lK4Hlvzn4ShKI10VatbaAp/ofBGjWrJmxCaDW9cMqH1ccw7khEZFOUQceFsO+CHzoV0mOO0dj6FL3TiwNBCEQkyBAEJI19j22QkjIpu3OFUKUde6kKRNZ+qmTpzm55Kkhx88BIEzQYGxejFMeiy4egPMP4kUiYK0C5O3auRv7xvkNm+TpZR569ZBfglWSgLt/EODZZ581PgGUIF7BZJBjlQ8IQkoKb/gibWqAxS0BLr7cloo63g6GIITnzahUmkJ3ym8KY4WQkG27nEBxkDD+jy1jMJXcpk07JsRQAx/XACCjx77nHMtXun9l/S5av9VqdQKvtH78zshIp6PHjrL7qFOnHtMGIP/W7RHUpZO8/I1aNSACj2MQoHXr1sYkwO3b8gINYiOQM/l8pdJUyrFNzIW6NalsqrykHE/PCcCJg4meijdkF5lWDtPBdwUhPG9G5TJshRAJ6wx1bEulk87lAhdbxpQKK8UKG6uYYY1+FDSmaDdv3khp6WnsHKZwMakjBlj+B5PG04xps1iak6dO0s5dO1iS9u06UPXqNVj8vPA59Gj9BhR/Io4aNWzMPkpPcCj6IAUUC2DpMWtat049drxrTxS1b9fRZfLInfVzAnTs2NG4BFBz/4hLh+QLK3hIEqWHFKXyjle/eXolAUQgUuAdBEEI8gLEGys+ZiuEFG7zEt3aGUllLt/IRQC8mv3u26OYEGPJ0g/ZYhHIi9VCmjR5gp56sinbgYO75LHjR7M5eW7tmKGbPnUm+71k2WKm9kHab7/bR0MGDWNAL/wwnBGiSNGitGt3FL3Sp58LAZBmz95d9EzzFmSz2emXX3+mli2eY9fc9+1e9v/QJTxU62HncjZKEsDyOQG6det2/xHgXMUwKhUvbxMjegCRANziXcyQeYAQF0EIPIDYyMMKIVdf6UlhJxNdCABByMaN62kM26tHYpbau2cftj3Le6NHse3boAUQXTsIMHvmXGcc9hyaNmUG+42VPca8P44dhy+cz4gFcCHwGDbkDXb82Rcr6dW+/dkxrxYuXb5ER45EU7OmT7O8v+z/mR6pXYcCA4Mc/2OnCxcvsFVCnnqiqcf6H0To06ePsQmg1sK/3KmdUxACUUfJnfJWLnoIcKFDayYIAYg8LycABCFXB75C5spVKCh8sQsBdu3eyRZgEEOrF1uzhZwAYJPHmzAPgHvg7QK4/NHvj6XAgEAGDjzI1MnTGaAfLV/GRBw4/vcP37NFH3D88SfLaNBrQ9jx6rVfEBpyAJ8T4MTv8XT6jLzEDQ+VK1WhShUrO7uJVzOv0ukzf1DDBvJuKMquH/cAeDG0S5cu9OSTT24wmUwnTSZTnM1mi4+NjT2lNBw9v7Gdnk+hQYMGD1kslrqSJNWTJKn2kSNH+qINoKz/+YOorfIhgs9vgnsBsQ1wO/qQUxCCFUL8HOsE8hVCCrVqQ8VXfEGmoCAXAkACBvFmtWrVWfyZM6eZoGPsmPFMgYvj9Ix0dm7u7PnsG/sMRcccppycHGb5U6ZNoskTpzKg/u/k72ypF6R78flWVKlSZQbyp6s+of6vvsaO0arv2rm7kwCI+9+v/6XatR5mFg+SXL16hc4knKY6j9Sj6COH2KOjCihftgIVL15ClQCoJqAFrFu3LiUlJf0xYcKEg4YjAJZVEdmrPPbmHCeEu34+zivP6enaKdO4G8QR++tiXx7E4NYtfovx4rFYFSh7Cvy6ynvizw4DwVtA2FQK7wLgOysrK2HUqFG/mkwmTL3+aTKZTuTk5MQfP35cbiV7GfLUA2gRwJfBH7GuV5JCPOcL+CLI4rFICnFgRwk8B9rdtxpRRBKI4wf8P1FGsPQKFSqwbwR4VrwNdPPmzZSRI0f+x2Qy3ZYkKcnPzy/hzp07CYULF/7j0KFD8o5dXoY8J4A3IItplfk8gZ3f1i8SQOzTq1k54sQPAFamU3oNsYsoAg6pFzT/SI+uIgQ2AB9eICcnJ33EiBE/2O12uNkks9mcaLfbU0GA0qVLJ/7000/ySxRehr+MAJ6I4o3799byOXncuX534IvAihavJAD/jfTwiPw36ngs7IA3e/GBOy9dujThLR8EkAKA490KJfgmk+ni0KFDv7Hb7TkmkynJZDIB/BRJks5mZmYmJSYmyrNwPoR8JYBeb6DX+kXLv1f37w0BOPjugBeB5oC3atWK1dtovKHljlY8B5oPEYuewB34/v7+VwcNGrTXZrPdMZlM2DQ50Ww2p1it1sRixYol79+/X5Zh+RgKjAB6ySA+h7L+d9cg9NYLeAM+QBKtWrR4DrySAPg9ePBgwnJuYhCrEz3gFylSJHvgwIG7c3JyYPnJsHiz2ZwM8AMCApLuFXzc231JAE+tf+X4vTtyeCIBt1B8i9bvDnyRAKizkW/kyJH3BH7RokVvDB06dOfNmzdR5wN81PvJcP9oA0RHR8sa/HsMeUoAFJAY3DXy/qr6n5NBDXxly59bqFoDD4CLH6Thv0EAuPyhQ+++aOqt5QcEBNweNmxYVHZ2NiZXks1mc5IkSfhOyMnJST527Jj8Dl4eBMMTQGscIC/cv6dun1ojT0kAEXwco/vWv39/n+r8wMDAnJEjR+64ePHiLVi8JElo7CVZLJYzt27dSpr1yNMAAAiQSURBVImPj5ffwc+jkG8E8KbOV2sE5lf/n/e9lf1+TgJlt09JAE/gw/rxefjhh6lz584uWgA9dX5gYKB17Nixu8+ePXsNdb7FYkmx2Wxo9SfcuHEj+eTJk/IaPHkY/nIC6O0BeNMA1GoHeCKB2ELnrl0kAQBWkoADz79btGjBVDvetPaDgoJskydP3nvy5MmrJpMpBeBjXws0+IoUKZJ84MABeQ2+PA4FRgAogtSWeQEBMO6PIM4Gih5AXCIGewaJiqBr40czRVDQuggy162XazpYnsWTl2bhb/Ny61drC3BL5Y2/xUsX0dDBr7OegBJ8/hvfEMRyAvTs2ZNq1aqVa0rYXVcvKCjIPm/evH0xMTFY4SLVYrFgM4VEm82W4OfnlxIdHS2vwZsPoUAIAJCxzAtX9dyM2MiWeUEQBSCVbqqvyIW8opoIewYBxAtVy1GJyN3s+Prs6RS0OTIXATAB9M133zAdwgsvtKIqlas4p3qVngC/MYE0bswEZ+tf2cJX1vccdE4AfL/zzjuEtfx0un1p6dKl3+3fv/+8yWRKxQCPxWJJhOWjv3/06FF5CfZ8CgVGACzzUvqErAc4X6cGW+ZFSQDRA8ArQCMIcJFXVBNhzyDEY8+gYBCAiDI7vUQhp+W1/8XPjp2RzqVZoAhq/1IHRgAogiIjt1HGuQz2G1O+U6dPdinmkcPfpmUfL2FTvQA+8WwiHTz0G0uD+fygoOLM6g8e/o2CS4RQZtZVCg0pSTOmywISTgAPli+tXr36P/v27UsB+JA+wPKtVitWw0g5fvy4vAVLPoYCIwCWeWEqIEmitOAissJH4QE4AZT1PfKKaiLsGYQ0UARdmzaRCrVuR7d37aDQc5m5CACLHjn8LaYI+nj5MjYVjLyLlyyixo3/RY//q4lTDwDAIPoYO3o8c/kA/aPlS2nggMHsOGrXDqpXtz479/v/xbO5exDgcMxBNpVrtdkoLCyU3nxjhCb4WO41MjLy161bt2IWLw0EcAz0JNy+fRutfddlUPKJBAVGgIwKJan0CXnGUssDKAmAvKKaiHsAbulQBGX1603B8X+6ECApOYkiIjbRu++8x+KxUAOWZilXrjx9MGkCTfpgslMRxC125uzpTPXDCQCxx2v9BzGgN0VsoHZt2jMyfP/Dd/Rkk6dYfMzRw1StSg12/FjDBkw25mlsH+Dv3bv34Lp1606ZzeZ0u90Oy09AP9/Pzy/14MGDsnssgFBgBLgEZW+vvszabmxa77LMC55TFIAou4DIK+4ZhOVlnIM6WVmUOehVtkRMsbnyKh38s3vPLvr+39+5FONzzz7PVD3Q9zVu1Jj+1fhxNk7PCYDFnIYNfYMKFyrMgF6+4iMa0G8gA3fXnp1MzIl4iEMee7Qha/wdPRZDVStXZ8ddu3UlLEXnzu0D/H379h1es2bN/2EBNLPZjHo/EV09eIIjR47IW7AXUCgwAkARxJd5gaqHL/Mi9gLwzKgGACBEony5GDFv8KZtbHkZpMG+QtgzyP/F1hS4/DMso32XGHY7Acye3XuxXcKQHg3Cbdu3Mo+QmprCjs+dl5XEUyZNY1b79b69dOx4LPMAb74+ghEAah+5DZDA1EIID9WsTQEBgQz02ONHqEqlaizN22+/RVarTXVWD+D/+OOPR1auXHlCkiSAn2a322H5rBooaPBZFewr0dQkYeJQcF4MA/s6CuhuHEA54id2B3mPQJz8EccBeGsfIIv9fhCAfzCXD72e2pQuwP/5559jly9fHmcymc6hu+eY1j1jtVrT4+LiEFfg4R8CCC9u8lFAPvAjzvaJwIvHAB8LY+AbL23WqVMn13w+wD9w4EDc4sWLY9EEQoMPXT1JkhJu3bqVFh8f75OaJy/Y8sARwNMooJYHuFcCtGnThrChk6jkEcE3m80XAL7D8hNMJlNadHR0el4A6es1HjgCiGP8yjEBLQLwhqAnD+CuCsB/4ZUteAMu4wL4hw4dilu0aBEs/yIafGjp22w2DPKk/tXgP7BtAHckcEcAnl4cBlaKPcRhX+UQMEAvW7YsG/5VA1+SpMsY20djD+A7GnywfIxh/aXhgfQAzi6isJqHGOeOCMp5AOUwsAg89wS8AQi9PrZzg4DTYfnxixYtOmoymTCah5G+BIvFAhVvWmxsLPr9fzn4f4kHYH9quss7T1PBSKscExB/K1282m9lm0Bt/F/pAUAEEXxx9k/ZGwABcM369es7wY+JiYkPDw8/SkRXIeQA8Far9Sxa/seOHcOQryHA/1sQQCSFJ/DFbqAWAbj14xsEgMoXrX9MAKHOX7hwYazZbM4E+CaTCd28JEi44+LiMMVrGPANSwAty+fn9XgAJQHEvr/ymFcBIIPSA4jTweK0MAgA64fE+/Dhw3ELFixAgw9z95BwncnJyWHgx8fHA3z5fXcDhQJvA+ipAgqCAO6IoKYFVGr+xCoBdT6sPzo6+nh4ePgxSZKyoeaB5TukXJjPNyT4BeYBPIHurSJIHB3UUv64axOoCUF4FcC7glwQotYW4B4Aff4nnnhC+vHHH+MAvslkugbQ0eBzfKfUrFkzZevWrfKy5wYMeeoBDPh8+XpLINjatWuPLF68+AQRQambhFk9u90O959cq1atZCODn6ceIF9L2vgXv+Fw97B8puEPDg5O8fV9vYJ8XJ89QKNGjWpCGCNJUl0iqiFJUtGCvHED/dd1DPJIknQGjT273X42NDQ0+X4A/548QOPGjcvl5OTUtFgsVbHCG9Y4MBAoBXYreFUb/XuADwl3oUKFkqOjo13fkCmwu/H+j3z2AC1btvTLysqqarVay/n5+QUTkbzY398v5Njt9ss2my3jfgP/njwAMoME2dnZpSRJCrDZbLK2+28W/Pz8bLdu3bpeqlSpi/eL2xch8tkD/M1wfmAf9x8CPLDQ6nuw/wdlUacVcfYuLwAAAABJRU5ErkJggg==');
    
}
img.EventLogIcon {
	height:40px;
    width:40px;
    content: url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAYAAADDPmHLAAARJUlEQVR4Xu2df3RV1ZXHv+e8JCCEX1FahaBYJbz3IjCdgXaWzjjaUay2OrVdTLWtWs17CSiQvKCD06kxIi7QlrxERoG8hFFk1vBj1VYritW2M612OY6lw4+89yLDTCkS0Ck/hBDy654967wQJoG8+/ve3IR7/8t6++y9z96fnHvv+bEvg39d0BFgF3Tv/c7DB+ACh8AHwAfgAo/ABd79YT8CHIjFLmrjp64gRUwC5xcDuAQk8sF4HhHlyfwzxjpBohNgJ8HYEQhxhAV4yygxev+UePz0cGZk2ABA1dU8/WlLMUF8gQEzCWwmExQEZ5daSqCgwwIsxTntJmAXFPr3UMGUJKuuFpb0eqTxkAWAANZc/kCRCPBbQTSXBK7lnI9zI64MOA6B34DjZ8TwRrAmsZcB5IZtu20MKQBk0pMV0Vmc0TeJaB5j/Cq7A2JOn9gLBLYyITZPr2vYPZRgGBIANMVKCgLg9xAQBVBsLknutBKCdnPGEl0jczbOXLnmmDtWzVvxNAAfVkRCgqNSIbqHg48w3033WwqBds7wIiMlHqxb3+y+B/osehKAporon3CIJ8D4Hfq64XEpgR8zUHWwrmGX1zz1FACpymgRiK0A6OteC5Qd/hCwJTeHfW/aD+r32aHPDh2eAGDf0tJxnR1UBRKLwXmOHR3zrA6BThZAbaBr5FPTVq8+Mdh+DjoAqcroN6DQP1p+Xx/sSBq0LwQdDATYg8GaxKsGm9oqPmgA7F10/8TunNy1w3W4N5ClrejMWxB67rkjBtrYJjooAKRjZV8miBcAfNa2nnhQ0e9OdeBQpwAYYfbokfhMLh/QS0FoYQz3heOJt93uhqsA/LK6OueyTw8uJ2Cp2x11217T6S60KQJz8kdAzhlvO9qGueMvwgiePeQELA99dLyabd2quOWvawA0Lym9hITYRGB/7VbnBtPOy0dP4c6C0Wc3XBzsVHC0S2DG6Fx1twRtF5y+XRxvPOqG/64AIF/vSIjX1aZuc8aMxeTvzgcfMdKNfjtuo/5fNqH07rsydk63t8s1R3R3d2FUIICDL66F0noyqw8CrDkX3bcVxdf/t9OOOg5AqjJ6LRT8FBwFap0ZXRTClPkxp/vrmv7n16zBgwsWZOy9//77GD9+PIqKijJ/1y1fhhv+eED1diCA/82B+Or0eOP7TjrtKADJWPQmBvEKwEdpdeJCAqDq8WrcfPQAJuYGNMIiWkHs9lBtw79qxc/s744BkKyM3EZEL+udw/cBGDiFmTUF4GuhusSbZpOs1s4RAOR/PkG8pjf50kEfgOxpykDA8eVQPPFvdkNgOwCZez6Jt/QM+3074wOglVrRygX/0vS6xH9oSRr53VYAkovLpjEm3tN64BvIQR8AHWkTyicsl/958IcN/6NDWpeIbQDI93xFUd4zu0vHB0BXviCEklYuGnGtXZtNbAGgZ4bvo+1WJnl8APQBIKWYwOvTJ0y+3Y6NqbYAkI5FV1qd3rUCwJo1a7GnaY/+CFqUzM3Nw8NLKlFYWJhVU92zq7Fo4UPgnOOtt9/GtKuvxtSpUzPy+l8DVRxl7IlQTX21xa5YPxp2ZmHnDauOWAGgJBLB8eOfWnXBUPvyxYtw/fXXZ22zY8cO/PrX7+Dyyy/Hxx9/jLKyUnn+wDYABEABopuCtQ2/MOT4OcKWRoCeJd2c3Xas6g03AGSc29ra0NraiokTJ55Nvm0jAAC5pwCcZlpZN7AEQCpW+iO71vOtALBhw0vYs8e9W0BeXh4WLnwIl15q7syJLbeA3v9kITaF6hrvNjsKmAYgVRH5Ohj7kVnD57azAoBdPrilx1YAMk+FuD1Uk3jNjP+mAMjs4WsXaTu3cfkAmElfTxsBfISO3FDx88+3GtViCoBURXQVGCqNGlOT9wGwFk0ieipc2/B9o1oMA5DZuq2IJrt37/oAGE1df3kB0cEpNxiqXft7I5qMA2Djg19fR30AjKQtiyzDP4dqEt8xoskQAE2VJZ/nxHcYMaBX1gdAb6Syy8m5AXB+TfGqdUm92gwBkKooecWp41o+AHpTpi4nTx+F44lv6tWmGwB5UFNhTDdZeh3oldMCgIjwX/v2obOjw6hqV+WvuOIK5Ofnq9q0/TWwjzUhhMjLCxTpPX6mG4BULFqPnuPZjlxaADQlk6iqetwR23YqnTNnNh5dqr7r3UkAMn1h7LlQTf1CPf3SBYA8nw+gxcgOHz3GjTwEfvDb32LFipVG1bouXxwOY9myJwZtBOgxLNry8gKTrnq6XnOBRBcA6Vi0nIBaJ6OpNQLs3LkTy55c7qQLtuieNWsWqh5Tfx13fATITA5iQTCeWKvVKU0AZFmWdCwqF3wcrcyhBYCiKHj33XfP7LHX6tbg/T7jmhmYNOmyQR4BpHnaEYo3/JlWJDQByBRrYPidliKrv2sBYFW/l9q7MQLI/gaIwkW1DSm1vmsCkIpFVgDsUacD6APgQISJqkO1DaoPJKoAyOE/VVGy1+w+PyNd8gEwEi3dsslQPKF661YFIF3+wHTigbRucxYEfQAsBE+tKQWuVFsfUAUgWRmpYMTiDrnWT60PgDNR1nobUAcgVvI6A7/VGdf6a/UBcCjKAj8O1SWyFt3KCoCsvZs8duCoW+VX9QCw/c030dLS4lCk7FErdwrPvflmD7wG9rggTxmH44nPZqtemhWAVKx0BkCu1bXTAqCrqwt33f0te7LksJatWzZntoNnu9x6Dey1TwovCj+7bu9A/mQFIBmLlDCwBodjdVb9cAJgy+ZNCASyH/12HQDCveHaxEuGAEjFInUAW+wVAIQQiJaWur7/32j/CwoKUL9ubb9t4OfqcB0AsB+E4/V/ZwiAZCz6SwbcYDQAZuW1RgCpt6OjA6dPe/v7DaNGjYLcNq52uQ0ABG0P1TUM+DCf/RmgPHLIzl2/WmDoAUBLx1D53XUACPtDtYmec2nnXAMCID+z0orWNjcD6gPgXLQFoHwybvLIG6uru3UB0LykNCgEqS4i2O2uD4DdEe2vjwXocwPVFRhwBEhXRL5EjP3cWZf6a/cBcDbanLO/nL6q/h1dI0C6snQeEW1x1iUfAO0qYfZlgIG+Fow3vKIXgAVE9Lx95rU1+SOAdoysSBAoEo43NOoDIBZ5hMCesWLQaFstAOSu4JUrn0bLoUNGVbsqP2VKIR55+GFPzQPIADCgIhhP1OkDoLL0H4jI1Q14WgAMpalgr80EyqQTsaXh2vrz/qkHfAhMxaJyF0mVm/86PgDORpuIVYVr65/UNQJ4EQB5C4jX1uLgQW+vBsqSMIsXLfTcLcAQAGkP3gKc/f9wV7vrM4FGbwFpDz4EupsiZ60NBgDGHgJj0fkErHE2DP48gJvzAMZeA/2JIEfZH5wRwMhEkD8VPOwAMDQV7C8GOZp/eyqFGnTR0GKQV5eDT5w8iZMnBv1jm6qhHzt2LMaMGaMq4/YtwPBysPQ+5bENIfJw6P0PlODUqVMG2XdXXCb/n9Y3emoeQED8vjjeeOVAkci6I6ipPPoLznGjW+HzZwIdjLSpLWEe2xQ6VNYCZEFouRbgpW3hjOGZYE1iwLIlQ2ZbuPz/kNW3Ww55eyq4cHIhrrvuWk89AzBG9wRrGjYaugUkF5dcwwJcFoZw5dK6BbjihEtG3H4INHUwRB4Na/704BECxrsRFx8Ah6IslE+CdesvNXw0TLqTLo9uI47bHHKtn1ofAKeizF4Oxeu/kU27en0AF4pD9TrmA+AQAIzND9XUrzMFQKYwNKHZIdf8EUDz07HWI09cTA2vatxvCoCeCmElzQCfZt0VdQ3+COBIhJtC8cQ1app1FIkqfQqg7zniXh+legCQX+E+dPiw065Y0j950iTMnj3bK6+Bj4fiiWWWAGiuLJslSPynpajoaKwFQHd3d6Y+gNwa5uXLSxNBTCjBYN161Vu45gggbwPJ8shOztkMJwOvBcBQmQmUMfLCrmACfRCON8zRypkmAFJBqiK6CAzPaimz8rsWAHIx6N77vov29nYrZhxvK4+Hb3jxBQ8sBlFZKN4gC3yrXroA2PXoggmB090tnGOklkKzv2sBIPUePXoUx44dM2vClXYTJhSgoGDCID8DiLac7lGXTVu9WnPtXBcAZ0aBtWAocyqKegBwyrbbeh2fCmZYHapJ6KruohsAp4tG+gDYg6H8YEQgl12t9xPzugHIjALl0ZfBcac9rvbX4gNgU1QNfknUEADp8shM4mynTa72U+MDYD2qmf9+UFjr1a+vJUMAyIbJWHQzA/7Wurv+CGD7uQDChlBt4j4juTEMwJ5YydUBwZvAoV4Ky4gXAPwRwGDAzhEXgk4DPFhcV/8HI5oMAyCVpyujTxNhwLpzRoz3lfUBMBu5s+2WheIJw1/VMgXA3kWLxnby00nO2WTLbp9R4ANgIZKE/Z057cWzfviS4S3TpgA4MwrcQYTzas6Y7YYPgNnIyeof/NZgfN12MxpMA5B5LYxFZSGpeWYMn9vGB8BcFAlsYzhef4+51pnSMeav1EMPXSxyO3dxhknmtfS09AEwE0Hxh3ZSZn2+9oXjZlrLNpYAkAqSsehNDHjLrAO97XwAjEVQfiiaCX5DuG7dr4y17C9tGYAzEDzJAPWvJWp46QNgLI2Mse8Ha+qfMtbqfGlbAKB58wLJKeO3ccItZh3yATAQORKvBsdPuZNVVwsDrQYUtQUAqbnn+8KB33DQdDNO+QDojlpTXh67Ts93gfVotA0AaezD2AOf60LgPQ5M1GO8r4wPgI6ICToswL9odLZPTbOtAEhDzbGSLwjg5wDP19GlsyI+AOrREsAJRuzGcG39DiNx1ZK1HQBpMFURuUEQe8PIDiIfALVUiTZGmBusbXxXK6FGf3cEgAwE5dFbBPATvRD4AAycOrnIwzi7IxxPvG00uXrkHQMgA0Es+leAeE3P7cAH4Px0yWE/h7OvDFTnX09y9cg4CkDmmaA8OqebY5vWg6EPwDnpEnSYGP+K3ff8c6FwHID/fzvIeV3tFTGQPwbjvh3BydwResAdsjJtp05hVU0cJfkMKhtCmoRgt9n5tJ8tYK4AII3LreV5p7s3qh03P6EIbDvWhi5vH/6xDF8+5/ibglEIDBR9Eq/mjQjca9d7vpazrgEgHZFFJ9InWqoEURW3YR1Cq3ND6Xc5tx9g7LHpYyetsGOGT2/fXQWg1yn5USqFsMHODSV6O+xFOSbYAQH2HasLO2b6NigASEfl1DEXeA6c32XG8eHSRq7nd1DnIitLulZiMWgA9Dqdqox+VRDWcKDQSkeGXFvCfhDKQnWJNwfT90EHIDMaPPhgPh/R/fcCyhIOPqxfA4RAO+d4pjPQ/oyZPXx2w+IJAM6OBhXzp4IrywXhW8PtIVEe2uCMbxTEHnPj9U4vKJ4CoNfppiVlYSbE4wTMG+ogZBIPbOE5gSemr6pP602MW3KeBKC383sfKb2qW0EMpNwP8FFuBcUeO6INjDcyTnG9BzXtsWtMi6cB6O3KvqWl47o66W4CRQH2p8a66K60rMzBgERO90Wb9JzPd9e7860NCQD6uv1hRSSkyLOJjMnzieHBDuAZ+02yMgwTymYjBzO94PuQA6Bv0NIPR66Ewm4hgbmC4y+0FpxsC7hQPgHPeQcMPyOmbFerw2ebTYcUDWkA+sYkU9OwMjqNBL4IxmYwIWaAsZAgMYWr1W5XCaz80gYgDnDB0izAdgG0u5voveJ4475stXcdypNjaocNANki9EFpaW7+WFHIiE8mIS4WwCUcLF8Qk/MNvXMOHZxRhwC1cuCPjPMjxMTBQ/mFB26sru52LPoeUDzsAfBAjD3tgg+Ap9PjvHM+AM7H2NMWfAA8nR7nnfMBcD7Gnrbwf+UbY+oFLMbWAAAAAElFTkSuQmCC');
    
}
</style>
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/3.0/css/font-awesome.min.css" rel="stylesheet" />
  <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800' rel='stylesheet' type='text/css'>
</head>
<body>

<!--main-container-part-->
<div id="content" class="col-sm-8 col-sm-offset-2" >
<!--breadcrumbs-->
  <div id="content-header">
    <div id="breadcrumb"> <a href="index.html" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a></div>
  </div>
<!--End-breadcrumbs-->

<!--Action boxes-->
  <div class="container-fluid">
    <div class="quick-actions_homepage">
      <ul class="quick-actions">
     
		
		<li class="bg_lg span2"> <a  class="showSingle" target="1"> <i class="icon-inbox"></i>Summary</a> </li>
        <li class="bg_lb" span2> <a  class="showSingle" target="2"> <i class=" icon-list"></i>Events</a> </li>
        <li class="bg_lo" span2> <a class="showSingle" target="3"> <i class="icon-signal"></i>Chart</a> </li>
        <li class="bg_ls" span2> <a class="showSingle" target="4"> <i class=" icon-file"></i>Files</a> </li>
		<li class="bg_lo span2 quick-last"> <a  id="showall"> <i class="icon-th-large"></i> ALL</a> </li>
      </ul>
    </div>
<!--End-Action boxes-->    

<!--Chart-box-->    
<ul class="nav nav-tabs">
  </ul><!-- I dont know why, but this ul stuff needed-->

  <div class="tab-content">
    <div id="menu1" class=" targetDiv">
      <div class="row-fluid">
      <div class="widget-box">
        <div class="widget-title bg_lg"><span class="icon"><i class="icon-inbox"></i></span>
          <h5>Summary</h5>
        </div>
        <div class="widget-content" > 
		$IPv4Statistics

		$IPv6Statistics
		
		$DisconnectionTable

        $DisconnectionsDetails
		
		
        </div>
      </div>
    </div>
    </div>
    <div id="menu2" class="targetDiv">
     <div class="row-fluid">
      <div class="widget-box">
        <div class="widget-title bg_lg"><span class="icon"><i class="icon-list"></i></span>
          <h5>Events</h5>
        </div>
        <div class="widget-content" > 
       
	   
   	    $ErrorEvents

	   </div>
      </div>
    </div>
    </div>
    <div id="menu3" class="targetDiv">
      <div class="row-fluid">
      <div class="widget-box">
        <div class="widget-title bg_lg"><span class="icon"><i class="icon-signal"></i></span>
          <h5>Charts</h5>
        </div>
        <div class="widget-content" > 


		
		
		$ChartDivs
		
		


        </div>
      </div>
    </div>
    </div>
    <div id="menu4" class="targetDiv">
     <div class="row-fluid">
      <div class="widget-box">
        <div class="widget-title bg_lg"><span class="icon"><i class="icon-file"></i></span>
          <h5>Files</h5>
        </div>
        <div class="widget-content" > 
        
        $FileList
		
		
        </div>
      </div>
    </div>
    </div>
    
  </div>
</div>

<!--Footer-part-->

<div class="row-fluid">
  <div id="footer" class="span12"> Powered by: Krisztian Buscsei - krisztian.buscsei@virginmedia.ie</div>
</div>

<!--end-Footer-part-->
<script type="text/javascript">
  /*! jQuery v1.7.2 jquery.com | jquery.org/license */
(function(a,b){function cy(a){return f.isWindow(a)?a:a.nodeType===9?a.defaultView||a.parentWindow:!1}function cu(a){if(!cj[a]){var b=c.body,d=f("<"+a+">").appendTo(b),e=d.css("display");d.remove();if(e==="none"||e===""){ck||(ck=c.createElement("iframe"),ck.frameBorder=ck.width=ck.height=0),b.appendChild(ck);if(!cl||!ck.createElement)cl=(ck.contentWindow||ck.contentDocument).document,cl.write((f.support.boxModel?"<!doctype html>":"")+"<html><body>"),cl.close();d=cl.createElement(a),cl.body.appendChild(d),e=f.css(d,"display"),b.removeChild(ck)}cj[a]=e}return cj[a]}function ct(a,b){var c={};f.each(cp.concat.apply([],cp.slice(0,b)),function(){c[this]=a});return c}function cs(){cq=b}function cr(){setTimeout(cs,0);return cq=f.now()}function ci(){try{return new a.ActiveXObject("Microsoft.XMLHTTP")}catch(b){}}function ch(){try{return new a.XMLHttpRequest}catch(b){}}function cb(a,c){a.dataFilter&&(c=a.dataFilter(c,a.dataType));var d=a.dataTypes,e={},g,h,i=d.length,j,k=d[0],l,m,n,o,p;for(g=1;g<i;g++){if(g===1)for(h in a.converters)typeof h=="string"&&(e[h.toLowerCase()]=a.converters[h]);l=k,k=d[g];if(k==="*")k=l;else if(l!=="*"&&l!==k){m=l+" "+k,n=e[m]||e["* "+k];if(!n){p=b;for(o in e){j=o.split(" ");if(j[0]===l||j[0]==="*"){p=e[j[1]+" "+k];if(p){o=e[o],o===!0?n=p:p===!0&&(n=o);break}}}}!n&&!p&&f.error("No conversion from "+m.replace(" "," to ")),n!==!0&&(c=n?n(c):p(o(c)))}}return c}function ca(a,c,d){var e=a.contents,f=a.dataTypes,g=a.responseFields,h,i,j,k;for(i in g)i in d&&(c[g[i]]=d[i]);while(f[0]==="*")f.shift(),h===b&&(h=a.mimeType||c.getResponseHeader("content-type"));if(h)for(i in e)if(e[i]&&e[i].test(h)){f.unshift(i);break}if(f[0]in d)j=f[0];else{for(i in d){if(!f[0]||a.converters[i+" "+f[0]]){j=i;break}k||(k=i)}j=j||k}if(j){j!==f[0]&&f.unshift(j);return d[j]}}function b_(a,b,c,d){if(f.isArray(b))f.each(b,function(b,e){c||bD.test(a)?d(a,e):b_(a+"["+(typeof e=="object"?b:"")+"]",e,c,d)});else if(!c&&f.type(b)==="object")for(var e in b)b_(a+"["+e+"]",b[e],c,d);else d(a,b)}function b`$(a,c){var d,e,g=f.ajaxSettings.flatOptions||{};for(d in c)c[d]!==b&&((g[d]?a:e||(e={}))[d]=c[d]);e&&f.extend(!0,a,e)}function bZ(a,c,d,e,f,g){f=f||c.dataTypes[0],g=g||{},g[f]=!0;var h=a[f],i=0,j=h?h.length:0,k=a===bS,l;for(;i<j&&(k||!l);i++)l=h[i](c,d,e),typeof l=="string"&&(!k||g[l]?l=b:(c.dataTypes.unshift(l),l=bZ(a,c,d,e,l,g)));(k||!l)&&!g["*"]&&(l=bZ(a,c,d,e,"*",g));return l}function bY(a){return function(b,c){typeof b!="string"&&(c=b,b="*");if(f.isFunction(c)){var d=b.toLowerCase().split(bO),e=0,g=d.length,h,i,j;for(;e<g;e++)h=d[e],j=/^\+/.test(h),j&&(h=h.substr(1)||"*"),i=a[h]=a[h]||[],i[j?"unshift":"push"](c)}}}function bB(a,b,c){var d=b==="width"?a.offsetWidth:a.offsetHeight,e=b==="width"?1:0,g=4;if(d>0){if(c!=="border")for(;e<g;e+=2)c||(d-=parseFloat(f.css(a,"padding"+bx[e]))||0),c==="margin"?d+=parseFloat(f.css(a,c+bx[e]))||0:d-=parseFloat(f.css(a,"border"+bx[e]+"Width"))||0;return d+"px"}d=by(a,b);if(d<0||d==null)d=a.style[b];if(bt.test(d))return d;d=parseFloat(d)||0;if(c)for(;e<g;e+=2)d+=parseFloat(f.css(a,"padding"+bx[e]))||0,c!=="padding"&&(d+=parseFloat(f.css(a,"border"+bx[e]+"Width"))||0),c==="margin"&&(d+=parseFloat(f.css(a,c+bx[e]))||0);return d+"px"}function bo(a){var b=c.createElement("div");bh.appendChild(b),b.innerHTML=a.outerHTML;return b.firstChild}function bn(a){var b=(a.nodeName||"").toLowerCase();b==="input"?bm(a):b!=="script"&&typeof a.getElementsByTagName!="undefined"&&f.grep(a.getElementsByTagName("input"),bm)}function bm(a){if(a.type==="checkbox"||a.type==="radio")a.defaultChecked=a.checked}function bl(a){return typeof a.getElementsByTagName!="undefined"?a.getElementsByTagName("*"):typeof a.querySelectorAll!="undefined"?a.querySelectorAll("*"):[]}function bk(a,b){var c;b.nodeType===1&&(b.clearAttributes&&b.clearAttributes(),b.mergeAttributes&&b.mergeAttributes(a),c=b.nodeName.toLowerCase(),c==="object"?b.outerHTML=a.outerHTML:c!=="input"||a.type!=="checkbox"&&a.type!=="radio"?c==="option"?b.selected=a.defaultSelected:c==="input"||c==="textarea"?b.defaultValue=a.defaultValue:c==="script"&&b.text!==a.text&&(b.text=a.text):(a.checked&&(b.defaultChecked=b.checked=a.checked),b.value!==a.value&&(b.value=a.value)),b.removeAttribute(f.expando),b.removeAttribute("_submit_attached"),b.removeAttribute("_change_attached"))}function bj(a,b){if(b.nodeType===1&&!!f.hasData(a)){var c,d,e,g=f._data(a),h=f._data(b,g),i=g.events;if(i){delete h.handle,h.events={};for(c in i)for(d=0,e=i[c].length;d<e;d++)f.event.add(b,c,i[c][d])}h.data&&(h.data=f.extend({},h.data))}}function bi(a,b){return f.nodeName(a,"table")?a.getElementsByTagName("tbody")[0]||a.appendChild(a.ownerDocument.createElement("tbody")):a}function U(a){var b=V.split("|"),c=a.createDocumentFragment();if(c.createElement)while(b.length)c.createElement(b.pop());return c}function T(a,b,c){b=b||0;if(f.isFunction(b))return f.grep(a,function(a,d){var e=!!b.call(a,d,a);return e===c});if(b.nodeType)return f.grep(a,function(a,d){return a===b===c});if(typeof b=="string"){var d=f.grep(a,function(a){return a.nodeType===1});if(O.test(b))return f.filter(b,d,!c);b=f.filter(b,d)}return f.grep(a,function(a,d){return f.inArray(a,b)>=0===c})}function S(a){return!a||!a.parentNode||a.parentNode.nodeType===11}function K(){return!0}function J(){return!1}function n(a,b,c){var d=b+"defer",e=b+"queue",g=b+"mark",h=f._data(a,d);h&&(c==="queue"||!f._data(a,e))&&(c==="mark"||!f._data(a,g))&&setTimeout(function(){!f._data(a,e)&&!f._data(a,g)&&(f.removeData(a,d,!0),h.fire())},0)}function m(a){for(var b in a){if(b==="data"&&f.isEmptyObject(a[b]))continue;if(b!=="toJSON")return!1}return!0}function l(a,c,d){if(d===b&&a.nodeType===1){var e="data-"+c.replace(k,"-`$1").toLowerCase();d=a.getAttribute(e);if(typeof d=="string"){try{d=d==="true"?!0:d==="false"?!1:d==="null"?null:f.isNumeric(d)?+d:j.test(d)?f.parseJSON(d):d}catch(g){}f.data(a,c,d)}else d=b}return d}function h(a){var b=g[a]={},c,d;a=a.split(/\s+/);for(c=0,d=a.length;c<d;c++)b[a[c]]=!0;return b}var c=a.document,d=a.navigator,e=a.location,f=function(){function J(){if(!e.isReady){try{c.documentElement.doScroll("left")}catch(a){setTimeout(J,1);return}e.ready()}}var e=function(a,b){return new e.fn.init(a,b,h)},f=a.jQuery,g=a.`$,h,i=/^(?:[^#<]*(<[\w\W]+>)[^>]*`$|#([\w\-]*)`$)/,j=/\S/,k=/^\s+/,l=/\s+`$/,m=/^<(\w+)\s*\/?>(?:<\/\1>)?`$/,n=/^[\],:{}\s]*`$/,o=/\\(?:["\\\/bfnrt]|u[0-9a-fA-F]{4})/g,p=/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g,q=/(?:^|:|,)(?:\s*\[)+/g,r=/(webkit)[ \/]([\w.]+)/,s=/(opera)(?:.*version)?[ \/]([\w.]+)/,t=/(msie) ([\w.]+)/,u=/(mozilla)(?:.*? rv:([\w.]+))?/,v=/-([a-z]|[0-9])/ig,w=/^-ms-/,x=function(a,b){return(b+"").toUpperCase()},y=d.userAgent,z,A,B,C=Object.prototype.toString,D=Object.prototype.hasOwnProperty,E=Array.prototype.push,F=Array.prototype.slice,G=String.prototype.trim,H=Array.prototype.indexOf,I={};e.fn=e.prototype={constructor:e,init:function(a,d,f){var g,h,j,k;if(!a)return this;if(a.nodeType){this.context=this[0]=a,this.length=1;return this}if(a==="body"&&!d&&c.body){this.context=c,this[0]=c.body,this.selector=a,this.length=1;return this}if(typeof a=="string"){a.charAt(0)!=="<"||a.charAt(a.length-1)!==">"||a.length<3?g=i.exec(a):g=[null,a,null];if(g&&(g[1]||!d)){if(g[1]){d=d instanceof e?d[0]:d,k=d?d.ownerDocument||d:c,j=m.exec(a),j?e.isPlainObject(d)?(a=[c.createElement(j[1])],e.fn.attr.call(a,d,!0)):a=[k.createElement(j[1])]:(j=e.buildFragment([g[1]],[k]),a=(j.cacheable?e.clone(j.fragment):j.fragment).childNodes);return e.merge(this,a)}h=c.getElementById(g[2]);if(h&&h.parentNode){if(h.id!==g[2])return f.find(a);this.length=1,this[0]=h}this.context=c,this.selector=a;return this}return!d||d.jquery?(d||f).find(a):this.constructor(d).find(a)}if(e.isFunction(a))return f.ready(a);a.selector!==b&&(this.selector=a.selector,this.context=a.context);return e.makeArray(a,this)},selector:"",jquery:"1.7.2",length:0,size:function(){return this.length},toArray:function(){return F.call(this,0)},get:function(a){return a==null?this.toArray():a<0?this[this.length+a]:this[a]},pushStack:function(a,b,c){var d=this.constructor();e.isArray(a)?E.apply(d,a):e.merge(d,a),d.prevObject=this,d.context=this.context,b==="find"?d.selector=this.selector+(this.selector?" ":"")+c:b&&(d.selector=this.selector+"."+b+"("+c+")");return d},each:function(a,b){return e.each(this,a,b)},ready:function(a){e.bindReady(),A.add(a);return this},eq:function(a){a=+a;return a===-1?this.slice(a):this.slice(a,a+1)},first:function(){return this.eq(0)},last:function(){return this.eq(-1)},slice:function(){return this.pushStack(F.apply(this,arguments),"slice",F.call(arguments).join(","))},map:function(a){return this.pushStack(e.map(this,function(b,c){return a.call(b,c,b)}))},end:function(){return this.prevObject||this.constructor(null)},push:E,sort:[].sort,splice:[].splice},e.fn.init.prototype=e.fn,e.extend=e.fn.extend=function(){var a,c,d,f,g,h,i=arguments[0]||{},j=1,k=arguments.length,l=!1;typeof i=="boolean"&&(l=i,i=arguments[1]||{},j=2),typeof i!="object"&&!e.isFunction(i)&&(i={}),k===j&&(i=this,--j);for(;j<k;j++)if((a=arguments[j])!=null)for(c in a){d=i[c],f=a[c];if(i===f)continue;l&&f&&(e.isPlainObject(f)||(g=e.isArray(f)))?(g?(g=!1,h=d&&e.isArray(d)?d:[]):h=d&&e.isPlainObject(d)?d:{},i[c]=e.extend(l,h,f)):f!==b&&(i[c]=f)}return i},e.extend({noConflict:function(b){a.`$===e&&(a.`$=g),b&&a.jQuery===e&&(a.jQuery=f);return e},isReady:!1,readyWait:1,holdReady:function(a){a?e.readyWait++:e.ready(!0)},ready:function(a){if(a===!0&&!--e.readyWait||a!==!0&&!e.isReady){if(!c.body)return setTimeout(e.ready,1);e.isReady=!0;if(a!==!0&&--e.readyWait>0)return;A.fireWith(c,[e]),e.fn.trigger&&e(c).trigger("ready").off("ready")}},bindReady:function(){if(!A){A=e.Callbacks("once memory");if(c.readyState==="complete")return setTimeout(e.ready,1);if(c.addEventListener)c.addEventListener("DOMContentLoaded",B,!1),a.addEventListener("load",e.ready,!1);else if(c.attachEvent){c.attachEvent("onreadystatechange",B),a.attachEvent("onload",e.ready);var b=!1;try{b=a.frameElement==null}catch(d){}c.documentElement.doScroll&&b&&J()}}},isFunction:function(a){return e.type(a)==="function"},isArray:Array.isArray||function(a){return e.type(a)==="array"},isWindow:function(a){return a!=null&&a==a.window},isNumeric:function(a){return!isNaN(parseFloat(a))&&isFinite(a)},type:function(a){return a==null?String(a):I[C.call(a)]||"object"},isPlainObject:function(a){if(!a||e.type(a)!=="object"||a.nodeType||e.isWindow(a))return!1;try{if(a.constructor&&!D.call(a,"constructor")&&!D.call(a.constructor.prototype,"isPrototypeOf"))return!1}catch(c){return!1}var d;for(d in a);return d===b||D.call(a,d)},isEmptyObject:function(a){for(var b in a)return!1;return!0},error:function(a){throw new Error(a)},parseJSON:function(b){if(typeof b!="string"||!b)return null;b=e.trim(b);if(a.JSON&&a.JSON.parse)return a.JSON.parse(b);if(n.test(b.replace(o,"@").replace(p,"]").replace(q,"")))return(new Function("return "+b))();e.error("Invalid JSON: "+b)},parseXML:function(c){if(typeof c!="string"||!c)return null;var d,f;try{a.DOMParser?(f=new DOMParser,d=f.parseFromString(c,"text/xml")):(d=new ActiveXObject("Microsoft.XMLDOM"),d.async="false",d.loadXML(c))}catch(g){d=b}(!d||!d.documentElement||d.getElementsByTagName("parsererror").length)&&e.error("Invalid XML: "+c);return d},noop:function(){},globalEval:function(b){b&&j.test(b)&&(a.execScript||function(b){a.eval.call(a,b)})(b)},camelCase:function(a){return a.replace(w,"ms-").replace(v,x)},nodeName:function(a,b){return a.nodeName&&a.nodeName.toUpperCase()===b.toUpperCase()},each:function(a,c,d){var f,g=0,h=a.length,i=h===b||e.isFunction(a);if(d){if(i){for(f in a)if(c.apply(a[f],d)===!1)break}else for(;g<h;)if(c.apply(a[g++],d)===!1)break}else if(i){for(f in a)if(c.call(a[f],f,a[f])===!1)break}else for(;g<h;)if(c.call(a[g],g,a[g++])===!1)break;return a},trim:G?function(a){return a==null?"":G.call(a)}:function(a){return a==null?"":(a+"").replace(k,"").replace(l,"")},makeArray:function(a,b){var c=b||[];if(a!=null){var d=e.type(a);a.length==null||d==="string"||d==="function"||d==="regexp"||e.isWindow(a)?E.call(c,a):e.merge(c,a)}return c},inArray:function(a,b,c){var d;if(b){if(H)return H.call(b,a,c);d=b.length,c=c?c<0?Math.max(0,d+c):c:0;for(;c<d;c++)if(c in b&&b[c]===a)return c}return-1},merge:function(a,c){var d=a.length,e=0;if(typeof c.length=="number")for(var f=c.length;e<f;e++)a[d++]=c[e];else while(c[e]!==b)a[d++]=c[e++];a.length=d;return a},grep:function(a,b,c){var d=[],e;c=!!c;for(var f=0,g=a.length;f<g;f++)e=!!b(a[f],f),c!==e&&d.push(a[f]);return d},map:function(a,c,d){var f,g,h=[],i=0,j=a.length,k=a instanceof e||j!==b&&typeof j=="number"&&(j>0&&a[0]&&a[j-1]||j===0||e.isArray(a));if(k)for(;i<j;i++)f=c(a[i],i,d),f!=null&&(h[h.length]=f);else for(g in a)f=c(a[g],g,d),f!=null&&(h[h.length]=f);return h.concat.apply([],h)},guid:1,proxy:function(a,c){if(typeof c=="string"){var d=a[c];c=a,a=d}if(!e.isFunction(a))return b;var f=F.call(arguments,2),g=function(){return a.apply(c,f.concat(F.call(arguments)))};g.guid=a.guid=a.guid||g.guid||e.guid++;return g},access:function(a,c,d,f,g,h,i){var j,k=d==null,l=0,m=a.length;if(d&&typeof d=="object"){for(l in d)e.access(a,c,l,d[l],1,h,f);g=1}else if(f!==b){j=i===b&&e.isFunction(f),k&&(j?(j=c,c=function(a,b,c){return j.call(e(a),c)}):(c.call(a,f),c=null));if(c)for(;l<m;l++)c(a[l],d,j?f.call(a[l],l,c(a[l],d)):f,i);g=1}return g?a:k?c.call(a):m?c(a[0],d):h},now:function(){return(new Date).getTime()},uaMatch:function(a){a=a.toLowerCase();var b=r.exec(a)||s.exec(a)||t.exec(a)||a.indexOf("compatible")<0&&u.exec(a)||[];return{browser:b[1]||"",version:b[2]||"0"}},sub:function(){function a(b,c){return new a.fn.init(b,c)}e.extend(!0,a,this),a.superclass=this,a.fn=a.prototype=this(),a.fn.constructor=a,a.sub=this.sub,a.fn.init=function(d,f){f&&f instanceof e&&!(f instanceof a)&&(f=a(f));return e.fn.init.call(this,d,f,b)},a.fn.init.prototype=a.fn;var b=a(c);return a},browser:{}}),e.each("Boolean Number String Function Array Date RegExp Object".split(" "),function(a,b){I["[object "+b+"]"]=b.toLowerCase()}),z=e.uaMatch(y),z.browser&&(e.browser[z.browser]=!0,e.browser.version=z.version),e.browser.webkit&&(e.browser.safari=!0),j.test(" ")&&(k=/^[\s\xA0]+/,l=/[\s\xA0]+`$/),h=e(c),c.addEventListener?B=function(){c.removeEventListener("DOMContentLoaded",B,!1),e.ready()}:c.attachEvent&&(B=function(){c.readyState==="complete"&&(c.detachEvent("onreadystatechange",B),e.ready())});return e}(),g={};f.Callbacks=function(a){a=a?g[a]||h(a):{};var c=[],d=[],e,i,j,k,l,m,n=function(b){var d,e,g,h,i;for(d=0,e=b.length;d<e;d++)g=b[d],h=f.type(g),h==="array"?n(g):h==="function"&&(!a.unique||!p.has(g))&&c.push(g)},o=function(b,f){f=f||[],e=!a.memory||[b,f],i=!0,j=!0,m=k||0,k=0,l=c.length;for(;c&&m<l;m++)if(c[m].apply(b,f)===!1&&a.stopOnFalse){e=!0;break}j=!1,c&&(a.once?e===!0?p.disable():c=[]:d&&d.length&&(e=d.shift(),p.fireWith(e[0],e[1])))},p={add:function(){if(c){var a=c.length;n(arguments),j?l=c.length:e&&e!==!0&&(k=a,o(e[0],e[1]))}return this},remove:function(){if(c){var b=arguments,d=0,e=b.length;for(;d<e;d++)for(var f=0;f<c.length;f++)if(b[d]===c[f]){j&&f<=l&&(l--,f<=m&&m--),c.splice(f--,1);if(a.unique)break}}return this},has:function(a){if(c){var b=0,d=c.length;for(;b<d;b++)if(a===c[b])return!0}return!1},empty:function(){c=[];return this},disable:function(){c=d=e=b;return this},disabled:function(){return!c},lock:function(){d=b,(!e||e===!0)&&p.disable();return this},locked:function(){return!d},fireWith:function(b,c){d&&(j?a.once||d.push([b,c]):(!a.once||!e)&&o(b,c));return this},fire:function(){p.fireWith(this,arguments);return this},fired:function(){return!!i}};return p};var i=[].slice;f.extend({Deferred:function(a){var b=f.Callbacks("once memory"),c=f.Callbacks("once memory"),d=f.Callbacks("memory"),e="pending",g={resolve:b,reject:c,notify:d},h={done:b.add,fail:c.add,progress:d.add,state:function(){return e},isResolved:b.fired,isRejected:c.fired,then:function(a,b,c){i.done(a).fail(b).progress(c);return this},always:function(){i.done.apply(i,arguments).fail.apply(i,arguments);return this},pipe:function(a,b,c){return f.Deferred(function(d){f.each({done:[a,"resolve"],fail:[b,"reject"],progress:[c,"notify"]},function(a,b){var c=b[0],e=b[1],g;f.isFunction(c)?i[a](function(){g=c.apply(this,arguments),g&&f.isFunction(g.promise)?g.promise().then(d.resolve,d.reject,d.notify):d[e+"With"](this===i?d:this,[g])}):i[a](d[e])})}).promise()},promise:function(a){if(a==null)a=h;else for(var b in h)a[b]=h[b];return a}},i=h.promise({}),j;for(j in g)i[j]=g[j].fire,i[j+"With"]=g[j].fireWith;i.done(function(){e="resolved"},c.disable,d.lock).fail(function(){e="rejected"},b.disable,d.lock),a&&a.call(i,i);return i},when:function(a){function m(a){return function(b){e[a]=arguments.length>1?i.call(arguments,0):b,j.notifyWith(k,e)}}function l(a){return function(c){b[a]=arguments.length>1?i.call(arguments,0):c,--g||j.resolveWith(j,b)}}var b=i.call(arguments,0),c=0,d=b.length,e=Array(d),g=d,h=d,j=d<=1&&a&&f.isFunction(a.promise)?a:f.Deferred(),k=j.promise();if(d>1){for(;c<d;c++)b[c]&&b[c].promise&&f.isFunction(b[c].promise)?b[c].promise().then(l(c),j.reject,m(c)):--g;g||j.resolveWith(j,b)}else j!==a&&j.resolveWith(j,d?[a]:[]);return k}}),f.support=function(){var b,d,e,g,h,i,j,k,l,m,n,o,p=c.createElement("div"),q=c.documentElement;p.setAttribute("className","t"),p.innerHTML="   <link/><table></table><a href='/a' style='top:1px;float:left;opacity:.55;'>a</a><input type='checkbox'/>",d=p.getElementsByTagName("*"),e=p.getElementsByTagName("a")[0];if(!d||!d.length||!e)return{};g=c.createElement("select"),h=g.appendChild(c.createElement("option")),i=p.getElementsByTagName("input")[0],b={leadingWhitespace:p.firstChild.nodeType===3,tbody:!p.getElementsByTagName("tbody").length,htmlSerialize:!!p.getElementsByTagName("link").length,style:/top/.test(e.getAttribute("style")),hrefNormalized:e.getAttribute("href")==="/a",opacity:/^0.55/.test(e.style.opacity),cssFloat:!!e.style.cssFloat,checkOn:i.value==="on",optSelected:h.selected,getSetAttribute:p.className!=="t",enctype:!!c.createElement("form").enctype,html5Clone:c.createElement("nav").cloneNode(!0).outerHTML!=="<:nav></:nav>",submitBubbles:!0,changeBubbles:!0,focusinBubbles:!1,deleteExpando:!0,noCloneEvent:!0,inlineBlockNeedsLayout:!1,shrinkWrapBlocks:!1,reliableMarginRight:!0,pixelMargin:!0},f.boxModel=b.boxModel=c.compatMode==="CSS1Compat",i.checked=!0,b.noCloneChecked=i.cloneNode(!0).checked,g.disabled=!0,b.optDisabled=!h.disabled;try{delete p.test}catch(r){b.deleteExpando=!1}!p.addEventListener&&p.attachEvent&&p.fireEvent&&(p.attachEvent("onclick",function(){b.noCloneEvent=!1}),p.cloneNode(!0).fireEvent("onclick")),i=c.createElement("input"),i.value="t",i.setAttribute("type","radio"),b.radioValue=i.value==="t",i.setAttribute("checked","checked"),i.setAttribute("name","t"),p.appendChild(i),j=c.createDocumentFragment(),j.appendChild(p.lastChild),b.checkClone=j.cloneNode(!0).cloneNode(!0).lastChild.checked,b.appendChecked=i.checked,j.removeChild(i),j.appendChild(p);if(p.attachEvent)for(n in{submit:1,change:1,focusin:1})m="on"+n,o=m in p,o||(p.setAttribute(m,"return;"),o=typeof p[m]=="function"),b[n+"Bubbles"]=o;j.removeChild(p),j=g=h=p=i=null,f(function(){var d,e,g,h,i,j,l,m,n,q,r,s,t,u=c.getElementsByTagName("body")[0];!u||(m=1,t="padding:0;margin:0;border:",r="position:absolute;top:0;left:0;width:1px;height:1px;",s=t+"0;visibility:hidden;",n="style='"+r+t+"5px solid #000;",q="<div "+n+"display:block;'><div style='"+t+"0;display:block;overflow:hidden;'></div></div>"+"<table "+n+"' cellpadding='0' cellspacing='0'>"+"<tr><td></td></tr></table>",d=c.createElement("div"),d.style.cssText=s+"width:0;height:0;position:static;top:0;margin-top:"+m+"px",u.insertBefore(d,u.firstChild),p=c.createElement("div"),d.appendChild(p),p.innerHTML="<table><tr><td style='"+t+"0;display:none'></td><td>t</td></tr></table>",k=p.getElementsByTagName("td"),o=k[0].offsetHeight===0,k[0].style.display="",k[1].style.display="none",b.reliableHiddenOffsets=o&&k[0].offsetHeight===0,a.getComputedStyle&&(p.innerHTML="",l=c.createElement("div"),l.style.width="0",l.style.marginRight="0",p.style.width="2px",p.appendChild(l),b.reliableMarginRight=(parseInt((a.getComputedStyle(l,null)||{marginRight:0}).marginRight,10)||0)===0),typeof p.style.zoom!="undefined"&&(p.innerHTML="",p.style.width=p.style.padding="1px",p.style.border=0,p.style.overflow="hidden",p.style.display="inline",p.style.zoom=1,b.inlineBlockNeedsLayout=p.offsetWidth===3,p.style.display="block",p.style.overflow="visible",p.innerHTML="<div style='width:5px;'></div>",b.shrinkWrapBlocks=p.offsetWidth!==3),p.style.cssText=r+s,p.innerHTML=q,e=p.firstChild,g=e.firstChild,i=e.nextSibling.firstChild.firstChild,j={doesNotAddBorder:g.offsetTop!==5,doesAddBorderForTableAndCells:i.offsetTop===5},g.style.position="fixed",g.style.top="20px",j.fixedPosition=g.offsetTop===20||g.offsetTop===15,g.style.position=g.style.top="",e.style.overflow="hidden",e.style.position="relative",j.subtractsBorderForOverflowNotVisible=g.offsetTop===-5,j.doesNotIncludeMarginInBodyOffset=u.offsetTop!==m,a.getComputedStyle&&(p.style.marginTop="1%",b.pixelMargin=(a.getComputedStyle(p,null)||{marginTop:0}).marginTop!=="1%"),typeof d.style.zoom!="undefined"&&(d.style.zoom=1),u.removeChild(d),l=p=d=null,f.extend(b,j))});return b}();var j=/^(?:\{.*\}|\[.*\])`$/,k=/([A-Z])/g;f.extend({cache:{},uuid:0,expando:"jQuery"+(f.fn.jquery+Math.random()).replace(/\D/g,""),noData:{embed:!0,object:"clsid:D27CDB6E-AE6D-11cf-96B8-444553540000",applet:!0},hasData:function(a){a=a.nodeType?f.cache[a[f.expando]]:a[f.expando];return!!a&&!m(a)},data:function(a,c,d,e){if(!!f.acceptData(a)){var g,h,i,j=f.expando,k=typeof c=="string",l=a.nodeType,m=l?f.cache:a,n=l?a[j]:a[j]&&j,o=c==="events";if((!n||!m[n]||!o&&!e&&!m[n].data)&&k&&d===b)return;n||(l?a[j]=n=++f.uuid:n=j),m[n]||(m[n]={},l||(m[n].toJSON=f.noop));if(typeof c=="object"||typeof c=="function")e?m[n]=f.extend(m[n],c):m[n].data=f.extend(m[n].data,c);g=h=m[n],e||(h.data||(h.data={}),h=h.data),d!==b&&(h[f.camelCase(c)]=d);if(o&&!h[c])return g.events;k?(i=h[c],i==null&&(i=h[f.camelCase(c)])):i=h;return i}},removeData:function(a,b,c){if(!!f.acceptData(a)){var d,e,g,h=f.expando,i=a.nodeType,j=i?f.cache:a,k=i?a[h]:h;if(!j[k])return;if(b){d=c?j[k]:j[k].data;if(d){f.isArray(b)||(b in d?b=[b]:(b=f.camelCase(b),b in d?b=[b]:b=b.split(" ")));for(e=0,g=b.length;e<g;e++)delete d[b[e]];if(!(c?m:f.isEmptyObject)(d))return}}if(!c){delete j[k].data;if(!m(j[k]))return}f.support.deleteExpando||!j.setInterval?delete j[k]:j[k]=null,i&&(f.support.deleteExpando?delete a[h]:a.removeAttribute?a.removeAttribute(h):a[h]=null)}},_data:function(a,b,c){return f.data(a,b,c,!0)},acceptData:function(a){if(a.nodeName){var b=f.noData[a.nodeName.toLowerCase()];if(b)return b!==!0&&a.getAttribute("classid")===b}return!0}}),f.fn.extend({data:function(a,c){var d,e,g,h,i,j=this[0],k=0,m=null;if(a===b){if(this.length){m=f.data(j);if(j.nodeType===1&&!f._data(j,"parsedAttrs")){g=j.attributes;for(i=g.length;k<i;k++)h=g[k].name,h.indexOf("data-")===0&&(h=f.camelCase(h.substring(5)),l(j,h,m[h]));f._data(j,"parsedAttrs",!0)}}return m}if(typeof a=="object")return this.each(function(){f.data(this,a)});d=a.split(".",2),d[1]=d[1]?"."+d[1]:"",e=d[1]+"!";return f.access(this,function(c){if(c===b){m=this.triggerHandler("getData"+e,[d[0]]),m===b&&j&&(m=f.data(j,a),m=l(j,a,m));return m===b&&d[1]?this.data(d[0]):m}d[1]=c,this.each(function(){var b=f(this);b.triggerHandler("setData"+e,d),f.data(this,a,c),b.triggerHandler("changeData"+e,d)})},null,c,arguments.length>1,null,!1)},removeData:function(a){return this.each(function(){f.removeData(this,a)})}}),f.extend({_mark:function(a,b){a&&(b=(b||"fx")+"mark",f._data(a,b,(f._data(a,b)||0)+1))},_unmark:function(a,b,c){a!==!0&&(c=b,b=a,a=!1);if(b){c=c||"fx";var d=c+"mark",e=a?0:(f._data(b,d)||1)-1;e?f._data(b,d,e):(f.removeData(b,d,!0),n(b,c,"mark"))}},queue:function(a,b,c){var d;if(a){b=(b||"fx")+"queue",d=f._data(a,b),c&&(!d||f.isArray(c)?d=f._data(a,b,f.makeArray(c)):d.push(c));return d||[]}},dequeue:function(a,b){b=b||"fx";var c=f.queue(a,b),d=c.shift(),e={};d==="inprogress"&&(d=c.shift()),d&&(b==="fx"&&c.unshift("inprogress"),f._data(a,b+".run",e),d.call(a,function(){f.dequeue(a,b)},e)),c.length||(f.removeData(a,b+"queue "+b+".run",!0),n(a,b,"queue"))}}),f.fn.extend({queue:function(a,c){var d=2;typeof a!="string"&&(c=a,a="fx",d--);if(arguments.length<d)return f.queue(this[0],a);return c===b?this:this.each(function(){var b=f.queue(this,a,c);a==="fx"&&b[0]!=="inprogress"&&f.dequeue(this,a)})},dequeue:function(a){return this.each(function(){f.dequeue(this,a)})},delay:function(a,b){a=f.fx?f.fx.speeds[a]||a:a,b=b||"fx";return this.queue(b,function(b,c){var d=setTimeout(b,a);c.stop=function(){clearTimeout(d)}})},clearQueue:function(a){return this.queue(a||"fx",[])},promise:function(a,c){function m(){--h||d.resolveWith(e,[e])}typeof a!="string"&&(c=a,a=b),a=a||"fx";var d=f.Deferred(),e=this,g=e.length,h=1,i=a+"defer",j=a+"queue",k=a+"mark",l;while(g--)if(l=f.data(e[g],i,b,!0)||(f.data(e[g],j,b,!0)||f.data(e[g],k,b,!0))&&f.data(e[g],i,f.Callbacks("once memory"),!0))h++,l.add(m);m();return d.promise(c)}});var o=/[\n\t\r]/g,p=/\s+/,q=/\r/g,r=/^(?:button|input)`$/i,s=/^(?:button|input|object|select|textarea)`$/i,t=/^a(?:rea)?`$/i,u=/^(?:autofocus|autoplay|async|checked|controls|defer|disabled|hidden|loop|multiple|open|readonly|required|scoped|selected)`$/i,v=f.support.getSetAttribute,w,x,y;f.fn.extend({attr:function(a,b){return f.access(this,f.attr,a,b,arguments.length>1)},removeAttr:function(a){return this.each(function(){f.removeAttr(this,a)})},prop:function(a,b){return f.access(this,f.prop,a,b,arguments.length>1)},removeProp:function(a){a=f.propFix[a]||a;return this.each(function(){try{this[a]=b,delete this[a]}catch(c){}})},addClass:function(a){var b,c,d,e,g,h,i;if(f.isFunction(a))return this.each(function(b){f(this).addClass(a.call(this,b,this.className))});if(a&&typeof a=="string"){b=a.split(p);for(c=0,d=this.length;c<d;c++){e=this[c];if(e.nodeType===1)if(!e.className&&b.length===1)e.className=a;else{g=" "+e.className+" ";for(h=0,i=b.length;h<i;h++)~g.indexOf(" "+b[h]+" ")||(g+=b[h]+" ");e.className=f.trim(g)}}}return this},removeClass:function(a){var c,d,e,g,h,i,j;if(f.isFunction(a))return this.each(function(b){f(this).removeClass(a.call(this,b,this.className))});if(a&&typeof a=="string"||a===b){c=(a||"").split(p);for(d=0,e=this.length;d<e;d++){g=this[d];if(g.nodeType===1&&g.className)if(a){h=(" "+g.className+" ").replace(o," ");for(i=0,j=c.length;i<j;i++)h=h.replace(" "+c[i]+" "," ");g.className=f.trim(h)}else g.className=""}}return this},toggleClass:function(a,b){var c=typeof a,d=typeof b=="boolean";if(f.isFunction(a))return this.each(function(c){f(this).toggleClass(a.call(this,c,this.className,b),b)});return this.each(function(){if(c==="string"){var e,g=0,h=f(this),i=b,j=a.split(p);while(e=j[g++])i=d?i:!h.hasClass(e),h[i?"addClass":"removeClass"](e)}else if(c==="undefined"||c==="boolean")this.className&&f._data(this,"__className__",this.className),this.className=this.className||a===!1?"":f._data(this,"__className__")||""})},hasClass:function(a){var b=" "+a+" ",c=0,d=this.length;for(;c<d;c++)if(this[c].nodeType===1&&(" "+this[c].className+" ").replace(o," ").indexOf(b)>-1)return!0;return!1},val:function(a){var c,d,e,g=this[0];{if(!!arguments.length){e=f.isFunction(a);return this.each(function(d){var g=f(this),h;if(this.nodeType===1){e?h=a.call(this,d,g.val()):h=a,h==null?h="":typeof h=="number"?h+="":f.isArray(h)&&(h=f.map(h,function(a){return a==null?"":a+""})),c=f.valHooks[this.type]||f.valHooks[this.nodeName.toLowerCase()];if(!c||!("set"in c)||c.set(this,h,"value")===b)this.value=h}})}if(g){c=f.valHooks[g.type]||f.valHooks[g.nodeName.toLowerCase()];if(c&&"get"in c&&(d=c.get(g,"value"))!==b)return d;d=g.value;return typeof d=="string"?d.replace(q,""):d==null?"":d}}}}),f.extend({valHooks:{option:{get:function(a){var b=a.attributes.value;return!b||b.specified?a.value:a.text}},select:{get:function(a){var b,c,d,e,g=a.selectedIndex,h=[],i=a.options,j=a.type==="select-one";if(g<0)return null;c=j?g:0,d=j?g+1:i.length;for(;c<d;c++){e=i[c];if(e.selected&&(f.support.optDisabled?!e.disabled:e.getAttribute("disabled")===null)&&(!e.parentNode.disabled||!f.nodeName(e.parentNode,"optgroup"))){b=f(e).val();if(j)return b;h.push(b)}}if(j&&!h.length&&i.length)return f(i[g]).val();return h},set:function(a,b){var c=f.makeArray(b);f(a).find("option").each(function(){this.selected=f.inArray(f(this).val(),c)>=0}),c.length||(a.selectedIndex=-1);return c}}},attrFn:{val:!0,css:!0,html:!0,text:!0,data:!0,width:!0,height:!0,offset:!0},attr:function(a,c,d,e){var g,h,i,j=a.nodeType;if(!!a&&j!==3&&j!==8&&j!==2){if(e&&c in f.attrFn)return f(a)[c](d);if(typeof a.getAttribute=="undefined")return f.prop(a,c,d);i=j!==1||!f.isXMLDoc(a),i&&(c=c.toLowerCase(),h=f.attrHooks[c]||(u.test(c)?x:w));if(d!==b){if(d===null){f.removeAttr(a,c);return}if(h&&"set"in h&&i&&(g=h.set(a,d,c))!==b)return g;a.setAttribute(c,""+d);return d}if(h&&"get"in h&&i&&(g=h.get(a,c))!==null)return g;g=a.getAttribute(c);return g===null?b:g}},removeAttr:function(a,b){var c,d,e,g,h,i=0;if(b&&a.nodeType===1){d=b.toLowerCase().split(p),g=d.length;for(;i<g;i++)e=d[i],e&&(c=f.propFix[e]||e,h=u.test(e),h||f.attr(a,e,""),a.removeAttribute(v?e:c),h&&c in a&&(a[c]=!1))}},attrHooks:{type:{set:function(a,b){if(r.test(a.nodeName)&&a.parentNode)f.error("type property can't be changed");else if(!f.support.radioValue&&b==="radio"&&f.nodeName(a,"input")){var c=a.value;a.setAttribute("type",b),c&&(a.value=c);return b}}},value:{get:function(a,b){if(w&&f.nodeName(a,"button"))return w.get(a,b);return b in a?a.value:null},set:function(a,b,c){if(w&&f.nodeName(a,"button"))return w.set(a,b,c);a.value=b}}},propFix:{tabindex:"tabIndex",readonly:"readOnly","for":"htmlFor","class":"className",maxlength:"maxLength",cellspacing:"cellSpacing",cellpadding:"cellPadding",rowspan:"rowSpan",colspan:"colSpan",usemap:"useMap",frameborder:"frameBorder",contenteditable:"contentEditable"},prop:function(a,c,d){var e,g,h,i=a.nodeType;if(!!a&&i!==3&&i!==8&&i!==2){h=i!==1||!f.isXMLDoc(a),h&&(c=f.propFix[c]||c,g=f.propHooks[c]);return d!==b?g&&"set"in g&&(e=g.set(a,d,c))!==b?e:a[c]=d:g&&"get"in g&&(e=g.get(a,c))!==null?e:a[c]}},propHooks:{tabIndex:{get:function(a){var c=a.getAttributeNode("tabindex");return c&&c.specified?parseInt(c.value,10):s.test(a.nodeName)||t.test(a.nodeName)&&a.href?0:b}}}}),f.attrHooks.tabindex=f.propHooks.tabIndex,x={get:function(a,c){var d,e=f.prop(a,c);return e===!0||typeof e!="boolean"&&(d=a.getAttributeNode(c))&&d.nodeValue!==!1?c.toLowerCase():b},set:function(a,b,c){var d;b===!1?f.removeAttr(a,c):(d=f.propFix[c]||c,d in a&&(a[d]=!0),a.setAttribute(c,c.toLowerCase()));return c}},v||(y={name:!0,id:!0,coords:!0},w=f.valHooks.button={get:function(a,c){var d;d=a.getAttributeNode(c);return d&&(y[c]?d.nodeValue!=="":d.specified)?d.nodeValue:b},set:function(a,b,d){var e=a.getAttributeNode(d);e||(e=c.createAttribute(d),a.setAttributeNode(e));return e.nodeValue=b+""}},f.attrHooks.tabindex.set=w.set,f.each(["width","height"],function(a,b){f.attrHooks[b]=f.extend(f.attrHooks[b],{set:function(a,c){if(c===""){a.setAttribute(b,"auto");return c}}})}),f.attrHooks.contenteditable={get:w.get,set:function(a,b,c){b===""&&(b="false"),w.set(a,b,c)}}),f.support.hrefNormalized||f.each(["href","src","width","height"],function(a,c){f.attrHooks[c]=f.extend(f.attrHooks[c],{get:function(a){var d=a.getAttribute(c,2);return d===null?b:d}})}),f.support.style||(f.attrHooks.style={get:function(a){return a.style.cssText.toLowerCase()||b},set:function(a,b){return a.style.cssText=""+b}}),f.support.optSelected||(f.propHooks.selected=f.extend(f.propHooks.selected,{get:function(a){var b=a.parentNode;b&&(b.selectedIndex,b.parentNode&&b.parentNode.selectedIndex);return null}})),f.support.enctype||(f.propFix.enctype="encoding"),f.support.checkOn||f.each(["radio","checkbox"],function(){f.valHooks[this]={get:function(a){return a.getAttribute("value")===null?"on":a.value}}}),f.each(["radio","checkbox"],function(){f.valHooks[this]=f.extend(f.valHooks[this],{set:function(a,b){if(f.isArray(b))return a.checked=f.inArray(f(a).val(),b)>=0}})});var z=/^(?:textarea|input|select)`$/i,A=/^([^\.]*)?(?:\.(.+))?`$/,B=/(?:^|\s)hover(\.\S+)?\b/,C=/^key/,D=/^(?:mouse|contextmenu)|click/,E=/^(?:focusinfocus|focusoutblur)`$/,F=/^(\w*)(?:#([\w\-]+))?(?:\.([\w\-]+))?`$/,G=function(
a){var b=F.exec(a);b&&(b[1]=(b[1]||"").toLowerCase(),b[3]=b[3]&&new RegExp("(?:^|\\s)"+b[3]+"(?:\\s|`$)"));return b},H=function(a,b){var c=a.attributes||{};return(!b[1]||a.nodeName.toLowerCase()===b[1])&&(!b[2]||(c.id||{}).value===b[2])&&(!b[3]||b[3].test((c["class"]||{}).value))},I=function(a){return f.event.special.hover?a:a.replace(B,"mouseenter`$1 mouseleave`$1")};f.event={add:function(a,c,d,e,g){var h,i,j,k,l,m,n,o,p,q,r,s;if(!(a.nodeType===3||a.nodeType===8||!c||!d||!(h=f._data(a)))){d.handler&&(p=d,d=p.handler,g=p.selector),d.guid||(d.guid=f.guid++),j=h.events,j||(h.events=j={}),i=h.handle,i||(h.handle=i=function(a){return typeof f!="undefined"&&(!a||f.event.triggered!==a.type)?f.event.dispatch.apply(i.elem,arguments):b},i.elem=a),c=f.trim(I(c)).split(" ");for(k=0;k<c.length;k++){l=A.exec(c[k])||[],m=l[1],n=(l[2]||"").split(".").sort(),s=f.event.special[m]||{},m=(g?s.delegateType:s.bindType)||m,s=f.event.special[m]||{},o=f.extend({type:m,origType:l[1],data:e,handler:d,guid:d.guid,selector:g,quick:g&&G(g),namespace:n.join(".")},p),r=j[m];if(!r){r=j[m]=[],r.delegateCount=0;if(!s.setup||s.setup.call(a,e,n,i)===!1)a.addEventListener?a.addEventListener(m,i,!1):a.attachEvent&&a.attachEvent("on"+m,i)}s.add&&(s.add.call(a,o),o.handler.guid||(o.handler.guid=d.guid)),g?r.splice(r.delegateCount++,0,o):r.push(o),f.event.global[m]=!0}a=null}},global:{},remove:function(a,b,c,d,e){var g=f.hasData(a)&&f._data(a),h,i,j,k,l,m,n,o,p,q,r,s;if(!!g&&!!(o=g.events)){b=f.trim(I(b||"")).split(" ");for(h=0;h<b.length;h++){i=A.exec(b[h])||[],j=k=i[1],l=i[2];if(!j){for(j in o)f.event.remove(a,j+b[h],c,d,!0);continue}p=f.event.special[j]||{},j=(d?p.delegateType:p.bindType)||j,r=o[j]||[],m=r.length,l=l?new RegExp("(^|\\.)"+l.split(".").sort().join("\\.(?:.*\\.)?")+"(\\.|`$)"):null;for(n=0;n<r.length;n++)s=r[n],(e||k===s.origType)&&(!c||c.guid===s.guid)&&(!l||l.test(s.namespace))&&(!d||d===s.selector||d==="**"&&s.selector)&&(r.splice(n--,1),s.selector&&r.delegateCount--,p.remove&&p.remove.call(a,s));r.length===0&&m!==r.length&&((!p.teardown||p.teardown.call(a,l)===!1)&&f.removeEvent(a,j,g.handle),delete o[j])}f.isEmptyObject(o)&&(q=g.handle,q&&(q.elem=null),f.removeData(a,["events","handle"],!0))}},customEvent:{getData:!0,setData:!0,changeData:!0},trigger:function(c,d,e,g){if(!e||e.nodeType!==3&&e.nodeType!==8){var h=c.type||c,i=[],j,k,l,m,n,o,p,q,r,s;if(E.test(h+f.event.triggered))return;h.indexOf("!")>=0&&(h=h.slice(0,-1),k=!0),h.indexOf(".")>=0&&(i=h.split("."),h=i.shift(),i.sort());if((!e||f.event.customEvent[h])&&!f.event.global[h])return;c=typeof c=="object"?c[f.expando]?c:new f.Event(h,c):new f.Event(h),c.type=h,c.isTrigger=!0,c.exclusive=k,c.namespace=i.join("."),c.namespace_re=c.namespace?new RegExp("(^|\\.)"+i.join("\\.(?:.*\\.)?")+"(\\.|`$)"):null,o=h.indexOf(":")<0?"on"+h:"";if(!e){j=f.cache;for(l in j)j[l].events&&j[l].events[h]&&f.event.trigger(c,d,j[l].handle.elem,!0);return}c.result=b,c.target||(c.target=e),d=d!=null?f.makeArray(d):[],d.unshift(c),p=f.event.special[h]||{};if(p.trigger&&p.trigger.apply(e,d)===!1)return;r=[[e,p.bindType||h]];if(!g&&!p.noBubble&&!f.isWindow(e)){s=p.delegateType||h,m=E.test(s+h)?e:e.parentNode,n=null;for(;m;m=m.parentNode)r.push([m,s]),n=m;n&&n===e.ownerDocument&&r.push([n.defaultView||n.parentWindow||a,s])}for(l=0;l<r.length&&!c.isPropagationStopped();l++)m=r[l][0],c.type=r[l][1],q=(f._data(m,"events")||{})[c.type]&&f._data(m,"handle"),q&&q.apply(m,d),q=o&&m[o],q&&f.acceptData(m)&&q.apply(m,d)===!1&&c.preventDefault();c.type=h,!g&&!c.isDefaultPrevented()&&(!p._default||p._default.apply(e.ownerDocument,d)===!1)&&(h!=="click"||!f.nodeName(e,"a"))&&f.acceptData(e)&&o&&e[h]&&(h!=="focus"&&h!=="blur"||c.target.offsetWidth!==0)&&!f.isWindow(e)&&(n=e[o],n&&(e[o]=null),f.event.triggered=h,e[h](),f.event.triggered=b,n&&(e[o]=n));return c.result}},dispatch:function(c){c=f.event.fix(c||a.event);var d=(f._data(this,"events")||{})[c.type]||[],e=d.delegateCount,g=[].slice.call(arguments,0),h=!c.exclusive&&!c.namespace,i=f.event.special[c.type]||{},j=[],k,l,m,n,o,p,q,r,s,t,u;g[0]=c,c.delegateTarget=this;if(!i.preDispatch||i.preDispatch.call(this,c)!==!1){if(e&&(!c.button||c.type!=="click")){n=f(this),n.context=this.ownerDocument||this;for(m=c.target;m!=this;m=m.parentNode||this)if(m.disabled!==!0){p={},r=[],n[0]=m;for(k=0;k<e;k++)s=d[k],t=s.selector,p[t]===b&&(p[t]=s.quick?H(m,s.quick):n.is(t)),p[t]&&r.push(s);r.length&&j.push({elem:m,matches:r})}}d.length>e&&j.push({elem:this,matches:d.slice(e)});for(k=0;k<j.length&&!c.isPropagationStopped();k++){q=j[k],c.currentTarget=q.elem;for(l=0;l<q.matches.length&&!c.isImmediatePropagationStopped();l++){s=q.matches[l];if(h||!c.namespace&&!s.namespace||c.namespace_re&&c.namespace_re.test(s.namespace))c.data=s.data,c.handleObj=s,o=((f.event.special[s.origType]||{}).handle||s.handler).apply(q.elem,g),o!==b&&(c.result=o,o===!1&&(c.preventDefault(),c.stopPropagation()))}}i.postDispatch&&i.postDispatch.call(this,c);return c.result}},props:"attrChange attrName relatedNode srcElement altKey bubbles cancelable ctrlKey currentTarget eventPhase metaKey relatedTarget shiftKey target timeStamp view which".split(" "),fixHooks:{},keyHooks:{props:"char charCode key keyCode".split(" "),filter:function(a,b){a.which==null&&(a.which=b.charCode!=null?b.charCode:b.keyCode);return a}},mouseHooks:{props:"button buttons clientX clientY fromElement offsetX offsetY pageX pageY screenX screenY toElement".split(" "),filter:function(a,d){var e,f,g,h=d.button,i=d.fromElement;a.pageX==null&&d.clientX!=null&&(e=a.target.ownerDocument||c,f=e.documentElement,g=e.body,a.pageX=d.clientX+(f&&f.scrollLeft||g&&g.scrollLeft||0)-(f&&f.clientLeft||g&&g.clientLeft||0),a.pageY=d.clientY+(f&&f.scrollTop||g&&g.scrollTop||0)-(f&&f.clientTop||g&&g.clientTop||0)),!a.relatedTarget&&i&&(a.relatedTarget=i===a.target?d.toElement:i),!a.which&&h!==b&&(a.which=h&1?1:h&2?3:h&4?2:0);return a}},fix:function(a){if(a[f.expando])return a;var d,e,g=a,h=f.event.fixHooks[a.type]||{},i=h.props?this.props.concat(h.props):this.props;a=f.Event(g);for(d=i.length;d;)e=i[--d],a[e]=g[e];a.target||(a.target=g.srcElement||c),a.target.nodeType===3&&(a.target=a.target.parentNode),a.metaKey===b&&(a.metaKey=a.ctrlKey);return h.filter?h.filter(a,g):a},special:{ready:{setup:f.bindReady},load:{noBubble:!0},focus:{delegateType:"focusin"},blur:{delegateType:"focusout"},beforeunload:{setup:function(a,b,c){f.isWindow(this)&&(this.onbeforeunload=c)},teardown:function(a,b){this.onbeforeunload===b&&(this.onbeforeunload=null)}}},simulate:function(a,b,c,d){var e=f.extend(new f.Event,c,{type:a,isSimulated:!0,originalEvent:{}});d?f.event.trigger(e,null,b):f.event.dispatch.call(b,e),e.isDefaultPrevented()&&c.preventDefault()}},f.event.handle=f.event.dispatch,f.removeEvent=c.removeEventListener?function(a,b,c){a.removeEventListener&&a.removeEventListener(b,c,!1)}:function(a,b,c){a.detachEvent&&a.detachEvent("on"+b,c)},f.Event=function(a,b){if(!(this instanceof f.Event))return new f.Event(a,b);a&&a.type?(this.originalEvent=a,this.type=a.type,this.isDefaultPrevented=a.defaultPrevented||a.returnValue===!1||a.getPreventDefault&&a.getPreventDefault()?K:J):this.type=a,b&&f.extend(this,b),this.timeStamp=a&&a.timeStamp||f.now(),this[f.expando]=!0},f.Event.prototype={preventDefault:function(){this.isDefaultPrevented=K;var a=this.originalEvent;!a||(a.preventDefault?a.preventDefault():a.returnValue=!1)},stopPropagation:function(){this.isPropagationStopped=K;var a=this.originalEvent;!a||(a.stopPropagation&&a.stopPropagation(),a.cancelBubble=!0)},stopImmediatePropagation:function(){this.isImmediatePropagationStopped=K,this.stopPropagation()},isDefaultPrevented:J,isPropagationStopped:J,isImmediatePropagationStopped:J},f.each({mouseenter:"mouseover",mouseleave:"mouseout"},function(a,b){f.event.special[a]={delegateType:b,bindType:b,handle:function(a){var c=this,d=a.relatedTarget,e=a.handleObj,g=e.selector,h;if(!d||d!==c&&!f.contains(c,d))a.type=e.origType,h=e.handler.apply(this,arguments),a.type=b;return h}}}),f.support.submitBubbles||(f.event.special.submit={setup:function(){if(f.nodeName(this,"form"))return!1;f.event.add(this,"click._submit keypress._submit",function(a){var c=a.target,d=f.nodeName(c,"input")||f.nodeName(c,"button")?c.form:b;d&&!d._submit_attached&&(f.event.add(d,"submit._submit",function(a){a._submit_bubble=!0}),d._submit_attached=!0)})},postDispatch:function(a){a._submit_bubble&&(delete a._submit_bubble,this.parentNode&&!a.isTrigger&&f.event.simulate("submit",this.parentNode,a,!0))},teardown:function(){if(f.nodeName(this,"form"))return!1;f.event.remove(this,"._submit")}}),f.support.changeBubbles||(f.event.special.change={setup:function(){if(z.test(this.nodeName)){if(this.type==="checkbox"||this.type==="radio")f.event.add(this,"propertychange._change",function(a){a.originalEvent.propertyName==="checked"&&(this._just_changed=!0)}),f.event.add(this,"click._change",function(a){this._just_changed&&!a.isTrigger&&(this._just_changed=!1,f.event.simulate("change",this,a,!0))});return!1}f.event.add(this,"beforeactivate._change",function(a){var b=a.target;z.test(b.nodeName)&&!b._change_attached&&(f.event.add(b,"change._change",function(a){this.parentNode&&!a.isSimulated&&!a.isTrigger&&f.event.simulate("change",this.parentNode,a,!0)}),b._change_attached=!0)})},handle:function(a){var b=a.target;if(this!==b||a.isSimulated||a.isTrigger||b.type!=="radio"&&b.type!=="checkbox")return a.handleObj.handler.apply(this,arguments)},teardown:function(){f.event.remove(this,"._change");return z.test(this.nodeName)}}),f.support.focusinBubbles||f.each({focus:"focusin",blur:"focusout"},function(a,b){var d=0,e=function(a){f.event.simulate(b,a.target,f.event.fix(a),!0)};f.event.special[b]={setup:function(){d++===0&&c.addEventListener(a,e,!0)},teardown:function(){--d===0&&c.removeEventListener(a,e,!0)}}}),f.fn.extend({on:function(a,c,d,e,g){var h,i;if(typeof a=="object"){typeof c!="string"&&(d=d||c,c=b);for(i in a)this.on(i,c,d,a[i],g);return this}d==null&&e==null?(e=c,d=c=b):e==null&&(typeof c=="string"?(e=d,d=b):(e=d,d=c,c=b));if(e===!1)e=J;else if(!e)return this;g===1&&(h=e,e=function(a){f().off(a);return h.apply(this,arguments)},e.guid=h.guid||(h.guid=f.guid++));return this.each(function(){f.event.add(this,a,e,d,c)})},one:function(a,b,c,d){return this.on(a,b,c,d,1)},off:function(a,c,d){if(a&&a.preventDefault&&a.handleObj){var e=a.handleObj;f(a.delegateTarget).off(e.namespace?e.origType+"."+e.namespace:e.origType,e.selector,e.handler);return this}if(typeof a=="object"){for(var g in a)this.off(g,c,a[g]);return this}if(c===!1||typeof c=="function")d=c,c=b;d===!1&&(d=J);return this.each(function(){f.event.remove(this,a,d,c)})},bind:function(a,b,c){return this.on(a,null,b,c)},unbind:function(a,b){return this.off(a,null,b)},live:function(a,b,c){f(this.context).on(a,this.selector,b,c);return this},die:function(a,b){f(this.context).off(a,this.selector||"**",b);return this},delegate:function(a,b,c,d){return this.on(b,a,c,d)},undelegate:function(a,b,c){return arguments.length==1?this.off(a,"**"):this.off(b,a,c)},trigger:function(a,b){return this.each(function(){f.event.trigger(a,b,this)})},triggerHandler:function(a,b){if(this[0])return f.event.trigger(a,b,this[0],!0)},toggle:function(a){var b=arguments,c=a.guid||f.guid++,d=0,e=function(c){var e=(f._data(this,"lastToggle"+a.guid)||0)%d;f._data(this,"lastToggle"+a.guid,e+1),c.preventDefault();return b[e].apply(this,arguments)||!1};e.guid=c;while(d<b.length)b[d++].guid=c;return this.click(e)},hover:function(a,b){return this.mouseenter(a).mouseleave(b||a)}}),f.each("blur focus focusin focusout load resize scroll unload click dblclick mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave change select submit keydown keypress keyup error contextmenu".split(" "),function(a,b){f.fn[b]=function(a,c){c==null&&(c=a,a=null);return arguments.length>0?this.on(b,null,a,c):this.trigger(b)},f.attrFn&&(f.attrFn[b]=!0),C.test(b)&&(f.event.fixHooks[b]=f.event.keyHooks),D.test(b)&&(f.event.fixHooks[b]=f.event.mouseHooks)}),function(){function x(a,b,c,e,f,g){for(var h=0,i=e.length;h<i;h++){var j=e[h];if(j){var k=!1;j=j[a];while(j){if(j[d]===c){k=e[j.sizset];break}if(j.nodeType===1){g||(j[d]=c,j.sizset=h);if(typeof b!="string"){if(j===b){k=!0;break}}else if(m.filter(b,[j]).length>0){k=j;break}}j=j[a]}e[h]=k}}}function w(a,b,c,e,f,g){for(var h=0,i=e.length;h<i;h++){var j=e[h];if(j){var k=!1;j=j[a];while(j){if(j[d]===c){k=e[j.sizset];break}j.nodeType===1&&!g&&(j[d]=c,j.sizset=h);if(j.nodeName.toLowerCase()===b){k=j;break}j=j[a]}e[h]=k}}}var a=/((?:\((?:\([^()]+\)|[^()]+)+\)|\[(?:\[[^\[\]]*\]|['"][^'"]*['"]|[^\[\]'"]+)+\]|\\.|[^ >+~,(\[\\]+)+|[>+~])(\s*,\s*)?((?:.|\r|\n)*)/g,d="sizcache"+(Math.random()+"").replace(".",""),e=0,g=Object.prototype.toString,h=!1,i=!0,j=/\\/g,k=/\r\n/g,l=/\W/;[0,0].sort(function(){i=!1;return 0});var m=function(b,d,e,f){e=e||[],d=d||c;var h=d;if(d.nodeType!==1&&d.nodeType!==9)return[];if(!b||typeof b!="string")return e;var i,j,k,l,n,q,r,t,u=!0,v=m.isXML(d),w=[],x=b;do{a.exec(""),i=a.exec(x);if(i){x=i[3],w.push(i[1]);if(i[2]){l=i[3];break}}}while(i);if(w.length>1&&p.exec(b))if(w.length===2&&o.relative[w[0]])j=y(w[0]+w[1],d,f);else{j=o.relative[w[0]]?[d]:m(w.shift(),d);while(w.length)b=w.shift(),o.relative[b]&&(b+=w.shift()),j=y(b,j,f)}else{!f&&w.length>1&&d.nodeType===9&&!v&&o.match.ID.test(w[0])&&!o.match.ID.test(w[w.length-1])&&(n=m.find(w.shift(),d,v),d=n.expr?m.filter(n.expr,n.set)[0]:n.set[0]);if(d){n=f?{expr:w.pop(),set:s(f)}:m.find(w.pop(),w.length===1&&(w[0]==="~"||w[0]==="+")&&d.parentNode?d.parentNode:d,v),j=n.expr?m.filter(n.expr,n.set):n.set,w.length>0?k=s(j):u=!1;while(w.length)q=w.pop(),r=q,o.relative[q]?r=w.pop():q="",r==null&&(r=d),o.relative[q](k,r,v)}else k=w=[]}k||(k=j),k||m.error(q||b);if(g.call(k)==="[object Array]")if(!u)e.push.apply(e,k);else if(d&&d.nodeType===1)for(t=0;k[t]!=null;t++)k[t]&&(k[t]===!0||k[t].nodeType===1&&m.contains(d,k[t]))&&e.push(j[t]);else for(t=0;k[t]!=null;t++)k[t]&&k[t].nodeType===1&&e.push(j[t]);else s(k,e);l&&(m(l,h,e,f),m.uniqueSort(e));return e};m.uniqueSort=function(a){if(u){h=i,a.sort(u);if(h)for(var b=1;b<a.length;b++)a[b]===a[b-1]&&a.splice(b--,1)}return a},m.matches=function(a,b){return m(a,null,null,b)},m.matchesSelector=function(a,b){return m(b,null,null,[a]).length>0},m.find=function(a,b,c){var d,e,f,g,h,i;if(!a)return[];for(e=0,f=o.order.length;e<f;e++){h=o.order[e];if(g=o.leftMatch[h].exec(a)){i=g[1],g.splice(1,1);if(i.substr(i.length-1)!=="\\"){g[1]=(g[1]||"").replace(j,""),d=o.find[h](g,b,c);if(d!=null){a=a.replace(o.match[h],"");break}}}}d||(d=typeof b.getElementsByTagName!="undefined"?b.getElementsByTagName("*"):[]);return{set:d,expr:a}},m.filter=function(a,c,d,e){var f,g,h,i,j,k,l,n,p,q=a,r=[],s=c,t=c&&c[0]&&m.isXML(c[0]);while(a&&c.length){for(h in o.filter)if((f=o.leftMatch[h].exec(a))!=null&&f[2]){k=o.filter[h],l=f[1],g=!1,f.splice(1,1);if(l.substr(l.length-1)==="\\")continue;s===r&&(r=[]);if(o.preFilter[h]){f=o.preFilter[h](f,s,d,r,e,t);if(!f)g=i=!0;else if(f===!0)continue}if(f)for(n=0;(j=s[n])!=null;n++)j&&(i=k(j,f,n,s),p=e^i,d&&i!=null?p?g=!0:s[n]=!1:p&&(r.push(j),g=!0));if(i!==b){d||(s=r),a=a.replace(o.match[h],"");if(!g)return[];break}}if(a===q)if(g==null)m.error(a);else break;q=a}return s},m.error=function(a){throw new Error("Syntax error, unrecognized expression: "+a)};var n=m.getText=function(a){var b,c,d=a.nodeType,e="";if(d){if(d===1||d===9||d===11){if(typeof a.textContent=="string")return a.textContent;if(typeof a.innerText=="string")return a.innerText.replace(k,"");for(a=a.firstChild;a;a=a.nextSibling)e+=n(a)}else if(d===3||d===4)return a.nodeValue}else for(b=0;c=a[b];b++)c.nodeType!==8&&(e+=n(c));return e},o=m.selectors={order:["ID","NAME","TAG"],match:{ID:/#((?:[\w\u00c0-\uFFFF\-]|\\.)+)/,CLASS:/\.((?:[\w\u00c0-\uFFFF\-]|\\.)+)/,NAME:/\[name=['"]*((?:[\w\u00c0-\uFFFF\-]|\\.)+)['"]*\]/,ATTR:/\[\s*((?:[\w\u00c0-\uFFFF\-]|\\.)+)\s*(?:(\S?=)\s*(?:(['"])(.*?)\3|(#?(?:[\w\u00c0-\uFFFF\-]|\\.)*)|)|)\s*\]/,TAG:/^((?:[\w\u00c0-\uFFFF\*\-]|\\.)+)/,CHILD:/:(only|nth|last|first)-child(?:\(\s*(even|odd|(?:[+\-]?\d+|(?:[+\-]?\d*)?n\s*(?:[+\-]\s*\d+)?))\s*\))?/,POS:/:(nth|eq|gt|lt|first|last|even|odd)(?:\((\d*)\))?(?=[^\-]|`$)/,PSEUDO:/:((?:[\w\u00c0-\uFFFF\-]|\\.)+)(?:\((['"]?)((?:\([^\)]+\)|[^\(\)]*)+)\2\))?/},leftMatch:{},attrMap:{"class":"className","for":"htmlFor"},attrHandle:{href:function(a){return a.getAttribute("href")},type:function(a){return a.getAttribute("type")}},relative:{"+":function(a,b){var c=typeof b=="string",d=c&&!l.test(b),e=c&&!d;d&&(b=b.toLowerCase());for(var f=0,g=a.length,h;f<g;f++)if(h=a[f]){while((h=h.previousSibling)&&h.nodeType!==1);a[f]=e||h&&h.nodeName.toLowerCase()===b?h||!1:h===b}e&&m.filter(b,a,!0)},">":function(a,b){var c,d=typeof b=="string",e=0,f=a.length;if(d&&!l.test(b)){b=b.toLowerCase();for(;e<f;e++){c=a[e];if(c){var g=c.parentNode;a[e]=g.nodeName.toLowerCase()===b?g:!1}}}else{for(;e<f;e++)c=a[e],c&&(a[e]=d?c.parentNode:c.parentNode===b);d&&m.filter(b,a,!0)}},"":function(a,b,c){var d,f=e++,g=x;typeof b=="string"&&!l.test(b)&&(b=b.toLowerCase(),d=b,g=w),g("parentNode",b,f,a,d,c)},"~":function(a,b,c){var d,f=e++,g=x;typeof b=="string"&&!l.test(b)&&(b=b.toLowerCase(),d=b,g=w),g("previousSibling",b,f,a,d,c)}},find:{ID:function(a,b,c){if(typeof b.getElementById!="undefined"&&!c){var d=b.getElementById(a[1]);return d&&d.parentNode?[d]:[]}},NAME:function(a,b){if(typeof b.getElementsByName!="undefined"){var c=[],d=b.getElementsByName(a[1]);for(var e=0,f=d.length;e<f;e++)d[e].getAttribute("name")===a[1]&&c.push(d[e]);return c.length===0?null:c}},TAG:function(a,b){if(typeof b.getElementsByTagName!="undefined")return b.getElementsByTagName(a[1])}},preFilter:{CLASS:function(a,b,c,d,e,f){a=" "+a[1].replace(j,"")+" ";if(f)return a;for(var g=0,h;(h=b[g])!=null;g++)h&&(e^(h.className&&(" "+h.className+" ").replace(/[\t\n\r]/g," ").indexOf(a)>=0)?c||d.push(h):c&&(b[g]=!1));return!1},ID:function(a){return a[1].replace(j,"")},TAG:function(a,b){return a[1].replace(j,"").toLowerCase()},CHILD:function(a){if(a[1]==="nth"){a[2]||m.error(a[0]),a[2]=a[2].replace(/^\+|\s*/g,"");var b=/(-?)(\d*)(?:n([+\-]?\d*))?/.exec(a[2]==="even"&&"2n"||a[2]==="odd"&&"2n+1"||!/\D/.test(a[2])&&"0n+"+a[2]||a[2]);a[2]=b[1]+(b[2]||1)-0,a[3]=b[3]-0}else a[2]&&m.error(a[0]);a[0]=e++;return a},ATTR:function(a,b,c,d,e,f){var g=a[1]=a[1].replace(j,"");!f&&o.attrMap[g]&&(a[1]=o.attrMap[g]),a[4]=(a[4]||a[5]||"").replace(j,""),a[2]==="~="&&(a[4]=" "+a[4]+" ");return a},PSEUDO:function(b,c,d,e,f){if(b[1]==="not")if((a.exec(b[3])||"").length>1||/^\w/.test(b[3]))b[3]=m(b[3],null,null,c);else{var g=m.filter(b[3],c,d,!0^f);d||e.push.apply(e,g);return!1}else if(o.match.POS.test(b[0])||o.match.CHILD.test(b[0]))return!0;return b},POS:function(a){a.unshift(!0);return a}},filters:{enabled:function(a){return a.disabled===!1&&a.type!=="hidden"},disabled:function(a){return a.disabled===!0},checked:function(a){return a.checked===!0},selected:function(a){a.parentNode&&a.parentNode.selectedIndex;return a.selected===!0},parent:function(a){return!!a.firstChild},empty:function(a){return!a.firstChild},has:function(a,b,c){return!!m(c[3],a).length},header:function(a){return/h\d/i.test(a.nodeName)},text:function(a){var b=a.getAttribute("type"),c=a.type;return a.nodeName.toLowerCase()==="input"&&"text"===c&&(b===c||b===null)},radio:function(a){return a.nodeName.toLowerCase()==="input"&&"radio"===a.type},checkbox:function(a){return a.nodeName.toLowerCase()==="input"&&"checkbox"===a.type},file:function(a){return a.nodeName.toLowerCase()==="input"&&"file"===a.type},password:function(a){return a.nodeName.toLowerCase()==="input"&&"password"===a.type},submit:function(a){var b=a.nodeName.toLowerCase();return(b==="input"||b==="button")&&"submit"===a.type},image:function(a){return a.nodeName.toLowerCase()==="input"&&"image"===a.type},reset:function(a){var b=a.nodeName.toLowerCase();return(b==="input"||b==="button")&&"reset"===a.type},button:function(a){var b=a.nodeName.toLowerCase();return b==="input"&&"button"===a.type||b==="button"},input:function(a){return/input|select|textarea|button/i.test(a.nodeName)},focus:function(a){return a===a.ownerDocument.activeElement}},setFilters:{first:function(a,b){return b===0},last:function(a,b,c,d){return b===d.length-1},even:function(a,b){return b%2===0},odd:function(a,b){return b%2===1},lt:function(a,b,c){return b<c[3]-0},gt:function(a,b,c){return b>c[3]-0},nth:function(a,b,c){return c[3]-0===b},eq:function(a,b,c){return c[3]-0===b}},filter:{PSEUDO:function(a,b,c,d){var e=b[1],f=o.filters[e];if(f)return f(a,c,b,d);if(e==="contains")return(a.textContent||a.innerText||n([a])||"").indexOf(b[3])>=0;if(e==="not"){var g=b[3];for(var h=0,i=g.length;h<i;h++)if(g[h]===a)return!1;return!0}m.error(e)},CHILD:function(a,b){var c,e,f,g,h,i,j,k=b[1],l=a;switch(k){case"only":case"first":while(l=l.previousSibling)if(l.nodeType===1)return!1;if(k==="first")return!0;l=a;case"last":while(l=l.nextSibling)if(l.nodeType===1)return!1;return!0;case"nth":c=b[2],e=b[3];if(c===1&&e===0)return!0;f=b[0],g=a.parentNode;if(g&&(g[d]!==f||!a.nodeIndex)){i=0;for(l=g.firstChild;l;l=l.nextSibling)l.nodeType===1&&(l.nodeIndex=++i);g[d]=f}j=a.nodeIndex-e;return c===0?j===0:j%c===0&&j/c>=0}},ID:function(a,b){return a.nodeType===1&&a.getAttribute("id")===b},TAG:function(a,b){return b==="*"&&a.nodeType===1||!!a.nodeName&&a.nodeName.toLowerCase()===b},CLASS:function(a,b){return(" "+(a.className||a.getAttribute("class"))+" ").indexOf(b)>-1},ATTR:function(a,b){var c=b[1],d=m.attr?m.attr(a,c):o.attrHandle[c]?o.attrHandle[c](a):a[c]!=null?a[c]:a.getAttribute(c),e=d+"",f=b[2],g=b[4];return d==null?f==="!=":!f&&m.attr?d!=null:f==="="?e===g:f==="*="?e.indexOf(g)>=0:f==="~="?(" "+e+" ").indexOf(g)>=0:g?f==="!="?e!==g:f==="^="?e.indexOf(g)===0:f==="`$="?e.substr(e.length-g.length)===g:f==="|="?e===g||e.substr(0,g.length+1)===g+"-":!1:e&&d!==!1},POS:function(a,b,c,d){var e=b[2],f=o.setFilters[e];if(f)return f(a,c,b,d)}}},p=o.match.POS,q=function(a,b){return"\\"+(b-0+1)};for(var r in o.match)o.match[r]=new RegExp(o.match[r].source+/(?![^\[]*\])(?![^\(]*\))/.source),o.leftMatch[r]=new RegExp(/(^(?:.|\r|\n)*?)/.source+o.match[r].source.replace(/\\(\d+)/g,q));o.match.globalPOS=p;var s=function(a,b){a=Array.prototype.slice.call(a,0);if(b){b.push.apply(b,a);return b}return a};try{Array.prototype.slice.call(c.documentElement.childNodes,0)[0].nodeType}catch(t){s=function(a,b){var c=0,d=b||[];if(g.call(a)==="[object Array]")Array.prototype.push.apply(d,a);else if(typeof a.length=="number")for(var e=a.length;c<e;c++)d.push(a[c]);else for(;a[c];c++)d.push(a[c]);return d}}var u,v;c.documentElement.compareDocumentPosition?u=function(a,b){if(a===b){h=!0;return 0}if(!a.compareDocumentPosition||!b.compareDocumentPosition)return a.compareDocumentPosition?-1:1;return a.compareDocumentPosition(b)&4?-1:1}:(u=function(a,b){if(a===b){h=!0;return 0}if(a.sourceIndex&&b.sourceIndex)return a.sourceIndex-b.sourceIndex;var c,d,e=[],f=[],g=a.parentNode,i=b.parentNode,j=g;if(g===i)return v(a,b);if(!g)return-1;if(!i)return 1;while(j)e.unshift(j),j=j.parentNode;j=i;while(j)f.unshift(j),j=j.parentNode;c=e.length,d=f.length;for(var k=0;k<c&&k<d;k++)if(e[k]!==f[k])return v(e[k],f[k]);return k===c?v(a,f[k],-1):v(e[k],b,1)},v=function(a,b,c){if(a===b)return c;var d=a.nextSibling;while(d){if(d===b)return-1;d=d.nextSibling}return 1}),function(){var a=c.createElement("div"),d="script"+(new Date).getTime(),e=c.documentElement;a.innerHTML="<a name='"+d+"'/>",e.insertBefore(a,e.firstChild),c.getElementById(d)&&(o.find.ID=function(a,c,d){if(typeof c.getElementById!="undefined"&&!d){var e=c.getElementById(a[1]);return e?e.id===a[1]||typeof e.getAttributeNode!="undefined"&&e.getAttributeNode("id").nodeValue===a[1]?[e]:b:[]}},o.filter.ID=function(a,b){var c=typeof a.getAttributeNode!="undefined"&&a.getAttributeNode("id");return a.nodeType===1&&c&&c.nodeValue===b}),e.removeChild(a),e=a=null}(),function(){var a=c.createElement("div");a.appendChild(c.createComment("")),a.getElementsByTagName("*").length>0&&(o.find.TAG=function(a,b){var c=b.getElementsByTagName(a[1]);if(a[1]==="*"){var d=[];for(var e=0;c[e];e++)c[e].nodeType===1&&d.push(c[e]);c=d}return c}),a.innerHTML="<a href='#'></a>",a.firstChild&&typeof a.firstChild.getAttribute!="undefined"&&a.firstChild.getAttribute("href")!=="#"&&(o.attrHandle.href=function(a){return a.getAttribute("href",2)}),a=null}(),c.querySelectorAll&&function(){var a=m,b=c.createElement("div"),d="__sizzle__";b.innerHTML="<p class='TEST'></p>";if(!b.querySelectorAll||b.querySelectorAll(".TEST").length!==0){m=function(b,e,f,g){e=e||c;if(!g&&!m.isXML(e)){var h=/^(\w+`$)|^\.([\w\-]+`$)|^#([\w\-]+`$)/.exec(b);if(h&&(e.nodeType===1||e.nodeType===9)){if(h[1])return s(e.getElementsByTagName(b),f);if(h[2]&&o.find.CLASS&&e.getElementsByClassName)return s(e.getElementsByClassName(h[2]),f)}if(e.nodeType===9){if(b==="body"&&e.body)return s([e.body],f);if(h&&h[3]){var i=e.getElementById(h[3]);if(!i||!i.parentNode)return s([],f);if(i.id===h[3])return s([i],f)}try{return s(e.querySelectorAll(b),f)}catch(j){}}else if(e.nodeType===1&&e.nodeName.toLowerCase()!=="object"){var k=e,l=e.getAttribute("id"),n=l||d,p=e.parentNode,q=/^\s*[+~]/.test(b);l?n=n.replace(/'/g,"\\`$&"):e.setAttribute("id",n),q&&p&&(e=e.parentNode);try{if(!q||p)return s(e.querySelectorAll("[id='"+n+"'] "+b),f)}catch(r){}finally{l||k.removeAttribute("id")}}}return a(b,e,f,g)};for(var e in a)m[e]=a[e];b=null}}(),function(){var a=c.documentElement,b=a.matchesSelector||a.mozMatchesSelector||a.webkitMatchesSelector||a.msMatchesSelector;if(b){var d=!b.call(c.createElement("div"),"div"),e=!1;try{b.call(c.documentElement,"[test!='']:sizzle")}catch(f){e=!0}m.matchesSelector=function(a,c){c=c.replace(/\=\s*([^'"\]]*)\s*\]/g,"='`$1']");if(!m.isXML(a))try{if(e||!o.match.PSEUDO.test(c)&&!/!=/.test(c)){var f=b.call(a,c);if(f||!d||a.document&&a.document.nodeType!==11)return f}}catch(g){}return m(c,null,null,[a]).length>0}}}(),function(){var a=c.createElement("div");a.innerHTML="<div class='test e'></div><div class='test'></div>";if(!!a.getElementsByClassName&&a.getElementsByClassName("e").length!==0){a.lastChild.className="e";if(a.getElementsByClassName("e").length===1)return;o.order.splice(1,0,"CLASS"),o.find.CLASS=function(a,b,c){if(typeof b.getElementsByClassName!="undefined"&&!c)return b.getElementsByClassName(a[1])},a=null}}(),c.documentElement.contains?m.contains=function(a,b){return a!==b&&(a.contains?a.contains(b):!0)}:c.documentElement.compareDocumentPosition?m.contains=function(a,b){return!!(a.compareDocumentPosition(b)&16)}:m.contains=function(){return!1},m.isXML=function(a){var b=(a?a.ownerDocument||a:0).documentElement;return b?b.nodeName!=="HTML":!1};var y=function(a,b,c){var d,e=[],f="",g=b.nodeType?[b]:b;while(d=o.match.PSEUDO.exec(a))f+=d[0],a=a.replace(o.match.PSEUDO,"");a=o.relative[a]?a+"*":a;for(var h=0,i=g.length;h<i;h++)m(a,g[h],e,c);return m.filter(f,e)};m.attr=f.attr,m.selectors.attrMap={},f.find=m,f.expr=m.selectors,f.expr[":"]=f.expr.filters,f.unique=m.uniqueSort,f.text=m.getText,f.isXMLDoc=m.isXML,f.contains=m.contains}();var L=/Until`$/,M=/^(?:parents|prevUntil|prevAll)/,N=/,/,O=/^.[^:#\[\.,]*`$/,P=Array.prototype.slice,Q=f.expr.match.globalPOS,R={children:!0,contents:!0,next:!0,prev:!0};f.fn.extend({find:function(a){var b=this,c,d;if(typeof a!="string")return f(a).filter(function(){for(c=0,d=b.length;c<d;c++)if(f.contains(b[c],this))return!0});var e=this.pushStack("","find",a),g,h,i;for(c=0,d=this.length;c<d;c++){g=e.length,f.find(a,this[c],e);if(c>0)for(h=g;h<e.length;h++)for(i=0;i<g;i++)if(e[i]===e[h]){e.splice(h--,1);break}}return e},has:function(a){var b=f(a);return this.filter(function(){for(var a=0,c=b.length;a<c;a++)if(f.contains(this,b[a]))return!0})},not:function(a){return this.pushStack(T(this,a,!1),"not",a)},filter:function(a){return this.pushStack(T(this,a,!0),"filter",a)},is:function(a){return!!a&&(typeof a=="string"?Q.test(a)?f(a,this.context).index(this[0])>=0:f.filter(a,this).length>0:this.filter(a).length>0)},closest:function(a,b){var c=[],d,e,g=this[0];if(f.isArray(a)){var h=1;while(g&&g.ownerDocument&&g!==b){for(d=0;d<a.length;d++)f(g).is(a[d])&&c.push({selector:a[d],elem:g,level:h});g=g.parentNode,h++}return c}var i=Q.test(a)||typeof a!="string"?f(a,b||this.context):0;for(d=0,e=this.length;d<e;d++){g=this[d];while(g){if(i?i.index(g)>-1:f.find.matchesSelector(g,a)){c.push(g);break}g=g.parentNode;if(!g||!g.ownerDocument||g===b||g.nodeType===11)break}}c=c.length>1?f.unique(c):c;return this.pushStack(c,"closest",a)},index:function(a){if(!a)return this[0]&&this[0].parentNode?this.prevAll().length:-1;if(typeof a=="string")return f.inArray(this[0],f(a));return f.inArray(a.jquery?a[0]:a,this)},add:function(a,b){var c=typeof a=="string"?f(a,b):f.makeArray(a&&a.nodeType?[a]:a),d=f.merge(this.get(),c);return this.pushStack(S(c[0])||S(d[0])?d:f.unique(d))},andSelf:function(){return this.add(this.prevObject)}}),f.each({parent:function(a){var b=a.parentNode;return b&&b.nodeType!==11?b:null},parents:function(a){return f.dir(a,"parentNode")},parentsUntil:function(a,b,c){return f.dir(a,"parentNode",c)},next:function(a){return f.nth(a,2,"nextSibling")},prev:function(a){return f.nth(a,2,"previousSibling")},nextAll:function(a){return f.dir(a,"nextSibling")},prevAll:function(a){return f.dir(a,"previousSibling")},nextUntil:function(a,b,c){return f.dir(a,"nextSibling",c)},prevUntil:function(a,b,c){return f.dir(a,"previousSibling",c)},siblings:function(a){return f.sibling((a.parentNode||{}).firstChild,a)},children:function(a){return f.sibling(a.firstChild)},contents:function(a){return f.nodeName(a,"iframe")?a.contentDocument||a.contentWindow.document:f.makeArray(a.childNodes)}},function(a,b){f.fn[a]=function(c,d){var e=f.map(this,b,c);L.test(a)||(d=c),d&&typeof d=="string"&&(e=f.filter(d,e)),e=this.length>1&&!R[a]?f.unique(e):e,(this.length>1||N.test(d))&&M.test(a)&&(e=e.reverse());return this.pushStack(e,a,P.call(arguments).join(","))}}),f.extend({filter:function(a,b,c){c&&(a=":not("+a+")");return b.length===1?f.find.matchesSelector(b[0],a)?[b[0]]:[]:f.find.matches(a,b)},dir:function(a,c,d){var e=[],g=a[c];while(g&&g.nodeType!==9&&(d===b||g.nodeType!==1||!f(g).is(d)))g.nodeType===1&&e.push(g),g=g[c];return e},nth:function(a,b,c,d){b=b||1;var e=0;for(;a;a=a[c])if(a.nodeType===1&&++e===b)break;return a},sibling:function(a,b){var c=[];for(;a;a=a.nextSibling)a.nodeType===1&&a!==b&&c.push(a);return c}});var V="abbr|article|aside|audio|bdi|canvas|data|datalist|details|figcaption|figure|footer|header|hgroup|mark|meter|nav|output|progress|section|summary|time|video",W=/ jQuery\d+="(?:\d+|null)"/g,X=/^\s+/,Y=/<(?!area|br|col|embed|hr|img|input|link|meta|param)(([\w:]+)[^>]*)\/>/ig,Z=/<([\w:]+)/,`$=/<tbody/i,_=/<|&#?\w+;/,ba=/<(?:script|style)/i,bb=/<(?:script|object|embed|option|style)/i,bc=new RegExp("<(?:"+V+")[\\s/>]","i"),bd=/checked\s*(?:[^=]|=\s*.checked.)/i,be=/\/(java|ecma)script/i,bf=/^\s*<!(?:\[CDATA\[|\-\-)/,bg={option:[1,"<select multiple='multiple'>","</select>"],legend:[1,"<fieldset>","</fieldset>"],thead:[1,"<table>","</table>"],tr:[2,"<table><tbody>","</tbody></table>"],td:[3,"<table><tbody><tr>","</tr></tbody></table>"],col:[2,"<table><tbody></tbody><colgroup>","</colgroup></table>"],area:[1,"<map>","</map>"],_default:[0,"",""]},bh=U(c);bg.optgroup=bg.option,bg.tbody=bg.tfoot=bg.colgroup=bg.caption=bg.thead,bg.th=bg.td,f.support.htmlSerialize||(bg._default=[1,"div<div>","</div>"]),f.fn.extend({text:function(a){return f.access(this,function(a){return a===b?f.text(this):this.empty().append((this[0]&&this[0].ownerDocument||c).createTextNode(a))},null,a,arguments.length)},wrapAll:function(a){if(f.isFunction(a))return this.each(function(b){f(this).wrapAll(a.call(this,b))});if(this[0]){var b=f(a,this[0].ownerDocument).eq(0).clone(!0);this[0].parentNode&&b.insertBefore(this[0]),b.map(function(){var a=this;while(a.firstChild&&a.firstChild.nodeType===1)a=a.firstChild;return a}).append(this)}return this},wrapInner:function(a){if(f.isFunction(a))return this.each(function(b){f(this).wrapInner(a.call(this,b))});return this.each(function(){var b=f(this),c=b.contents();c.length?c.wrapAll(a):b.append(a)})},wrap:function(a){var b=f.isFunction(a);return this.each(function(c){f(this).wrapAll(b?a.call(this,c):a)})},unwrap:function(){return this.parent().each(function(){f.nodeName(this,"body")||f(this).replaceWith(this.childNodes)}).end()},append:function(){return this.domManip(arguments,!0,function(a){this.nodeType===1&&this.appendChild(a)})},prepend:function(){return this.domManip(arguments,!0,function(a){this.nodeType===1&&this.insertBefore(a,this.firstChild)})},before:function(){if(this[0]&&this[0].parentNode)return this.domManip(arguments,!1,function(a){this.parentNode.insertBefore(a,this)});if(arguments.length){var a=f
.clean(arguments);a.push.apply(a,this.toArray());return this.pushStack(a,"before",arguments)}},after:function(){if(this[0]&&this[0].parentNode)return this.domManip(arguments,!1,function(a){this.parentNode.insertBefore(a,this.nextSibling)});if(arguments.length){var a=this.pushStack(this,"after",arguments);a.push.apply(a,f.clean(arguments));return a}},remove:function(a,b){for(var c=0,d;(d=this[c])!=null;c++)if(!a||f.filter(a,[d]).length)!b&&d.nodeType===1&&(f.cleanData(d.getElementsByTagName("*")),f.cleanData([d])),d.parentNode&&d.parentNode.removeChild(d);return this},empty:function(){for(var a=0,b;(b=this[a])!=null;a++){b.nodeType===1&&f.cleanData(b.getElementsByTagName("*"));while(b.firstChild)b.removeChild(b.firstChild)}return this},clone:function(a,b){a=a==null?!1:a,b=b==null?a:b;return this.map(function(){return f.clone(this,a,b)})},html:function(a){return f.access(this,function(a){var c=this[0]||{},d=0,e=this.length;if(a===b)return c.nodeType===1?c.innerHTML.replace(W,""):null;if(typeof a=="string"&&!ba.test(a)&&(f.support.leadingWhitespace||!X.test(a))&&!bg[(Z.exec(a)||["",""])[1].toLowerCase()]){a=a.replace(Y,"<`$1></`$2>");try{for(;d<e;d++)c=this[d]||{},c.nodeType===1&&(f.cleanData(c.getElementsByTagName("*")),c.innerHTML=a);c=0}catch(g){}}c&&this.empty().append(a)},null,a,arguments.length)},replaceWith:function(a){if(this[0]&&this[0].parentNode){if(f.isFunction(a))return this.each(function(b){var c=f(this),d=c.html();c.replaceWith(a.call(this,b,d))});typeof a!="string"&&(a=f(a).detach());return this.each(function(){var b=this.nextSibling,c=this.parentNode;f(this).remove(),b?f(b).before(a):f(c).append(a)})}return this.length?this.pushStack(f(f.isFunction(a)?a():a),"replaceWith",a):this},detach:function(a){return this.remove(a,!0)},domManip:function(a,c,d){var e,g,h,i,j=a[0],k=[];if(!f.support.checkClone&&arguments.length===3&&typeof j=="string"&&bd.test(j))return this.each(function(){f(this).domManip(a,c,d,!0)});if(f.isFunction(j))return this.each(function(e){var g=f(this);a[0]=j.call(this,e,c?g.html():b),g.domManip(a,c,d)});if(this[0]){i=j&&j.parentNode,f.support.parentNode&&i&&i.nodeType===11&&i.childNodes.length===this.length?e={fragment:i}:e=f.buildFragment(a,this,k),h=e.fragment,h.childNodes.length===1?g=h=h.firstChild:g=h.firstChild;if(g){c=c&&f.nodeName(g,"tr");for(var l=0,m=this.length,n=m-1;l<m;l++)d.call(c?bi(this[l],g):this[l],e.cacheable||m>1&&l<n?f.clone(h,!0,!0):h)}k.length&&f.each(k,function(a,b){b.src?f.ajax({type:"GET",global:!1,url:b.src,async:!1,dataType:"script"}):f.globalEval((b.text||b.textContent||b.innerHTML||"").replace(bf,"/*`$0*/")),b.parentNode&&b.parentNode.removeChild(b)})}return this}}),f.buildFragment=function(a,b,d){var e,g,h,i,j=a[0];b&&b[0]&&(i=b[0].ownerDocument||b[0]),i.createDocumentFragment||(i=c),a.length===1&&typeof j=="string"&&j.length<512&&i===c&&j.charAt(0)==="<"&&!bb.test(j)&&(f.support.checkClone||!bd.test(j))&&(f.support.html5Clone||!bc.test(j))&&(g=!0,h=f.fragments[j],h&&h!==1&&(e=h)),e||(e=i.createDocumentFragment(),f.clean(a,i,e,d)),g&&(f.fragments[j]=h?e:1);return{fragment:e,cacheable:g}},f.fragments={},f.each({appendTo:"append",prependTo:"prepend",insertBefore:"before",insertAfter:"after",replaceAll:"replaceWith"},function(a,b){f.fn[a]=function(c){var d=[],e=f(c),g=this.length===1&&this[0].parentNode;if(g&&g.nodeType===11&&g.childNodes.length===1&&e.length===1){e[b](this[0]);return this}for(var h=0,i=e.length;h<i;h++){var j=(h>0?this.clone(!0):this).get();f(e[h])[b](j),d=d.concat(j)}return this.pushStack(d,a,e.selector)}}),f.extend({clone:function(a,b,c){var d,e,g,h=f.support.html5Clone||f.isXMLDoc(a)||!bc.test("<"+a.nodeName+">")?a.cloneNode(!0):bo(a);if((!f.support.noCloneEvent||!f.support.noCloneChecked)&&(a.nodeType===1||a.nodeType===11)&&!f.isXMLDoc(a)){bk(a,h),d=bl(a),e=bl(h);for(g=0;d[g];++g)e[g]&&bk(d[g],e[g])}if(b){bj(a,h);if(c){d=bl(a),e=bl(h);for(g=0;d[g];++g)bj(d[g],e[g])}}d=e=null;return h},clean:function(a,b,d,e){var g,h,i,j=[];b=b||c,typeof b.createElement=="undefined"&&(b=b.ownerDocument||b[0]&&b[0].ownerDocument||c);for(var k=0,l;(l=a[k])!=null;k++){typeof l=="number"&&(l+="");if(!l)continue;if(typeof l=="string")if(!_.test(l))l=b.createTextNode(l);else{l=l.replace(Y,"<`$1></`$2>");var m=(Z.exec(l)||["",""])[1].toLowerCase(),n=bg[m]||bg._default,o=n[0],p=b.createElement("div"),q=bh.childNodes,r;b===c?bh.appendChild(p):U(b).appendChild(p),p.innerHTML=n[1]+l+n[2];while(o--)p=p.lastChild;if(!f.support.tbody){var s=`$.test(l),t=m==="table"&&!s?p.firstChild&&p.firstChild.childNodes:n[1]==="<table>"&&!s?p.childNodes:[];for(i=t.length-1;i>=0;--i)f.nodeName(t[i],"tbody")&&!t[i].childNodes.length&&t[i].parentNode.removeChild(t[i])}!f.support.leadingWhitespace&&X.test(l)&&p.insertBefore(b.createTextNode(X.exec(l)[0]),p.firstChild),l=p.childNodes,p&&(p.parentNode.removeChild(p),q.length>0&&(r=q[q.length-1],r&&r.parentNode&&r.parentNode.removeChild(r)))}var u;if(!f.support.appendChecked)if(l[0]&&typeof (u=l.length)=="number")for(i=0;i<u;i++)bn(l[i]);else bn(l);l.nodeType?j.push(l):j=f.merge(j,l)}if(d){g=function(a){return!a.type||be.test(a.type)};for(k=0;j[k];k++){h=j[k];if(e&&f.nodeName(h,"script")&&(!h.type||be.test(h.type)))e.push(h.parentNode?h.parentNode.removeChild(h):h);else{if(h.nodeType===1){var v=f.grep(h.getElementsByTagName("script"),g);j.splice.apply(j,[k+1,0].concat(v))}d.appendChild(h)}}}return j},cleanData:function(a){var b,c,d=f.cache,e=f.event.special,g=f.support.deleteExpando;for(var h=0,i;(i=a[h])!=null;h++){if(i.nodeName&&f.noData[i.nodeName.toLowerCase()])continue;c=i[f.expando];if(c){b=d[c];if(b&&b.events){for(var j in b.events)e[j]?f.event.remove(i,j):f.removeEvent(i,j,b.handle);b.handle&&(b.handle.elem=null)}g?delete i[f.expando]:i.removeAttribute&&i.removeAttribute(f.expando),delete d[c]}}}});var bp=/alpha\([^)]*\)/i,bq=/opacity=([^)]*)/,br=/([A-Z]|^ms)/g,bs=/^[\-+]?(?:\d*\.)?\d+`$/i,bt=/^-?(?:\d*\.)?\d+(?!px)[^\d\s]+`$/i,bu=/^([\-+])=([\-+.\de]+)/,bv=/^margin/,bw={position:"absolute",visibility:"hidden",display:"block"},bx=["Top","Right","Bottom","Left"],by,bz,bA;f.fn.css=function(a,c){return f.access(this,function(a,c,d){return d!==b?f.style(a,c,d):f.css(a,c)},a,c,arguments.length>1)},f.extend({cssHooks:{opacity:{get:function(a,b){if(b){var c=by(a,"opacity");return c===""?"1":c}return a.style.opacity}}},cssNumber:{fillOpacity:!0,fontWeight:!0,lineHeight:!0,opacity:!0,orphans:!0,widows:!0,zIndex:!0,zoom:!0},cssProps:{"float":f.support.cssFloat?"cssFloat":"styleFloat"},style:function(a,c,d,e){if(!!a&&a.nodeType!==3&&a.nodeType!==8&&!!a.style){var g,h,i=f.camelCase(c),j=a.style,k=f.cssHooks[i];c=f.cssProps[i]||i;if(d===b){if(k&&"get"in k&&(g=k.get(a,!1,e))!==b)return g;return j[c]}h=typeof d,h==="string"&&(g=bu.exec(d))&&(d=+(g[1]+1)*+g[2]+parseFloat(f.css(a,c)),h="number");if(d==null||h==="number"&&isNaN(d))return;h==="number"&&!f.cssNumber[i]&&(d+="px");if(!k||!("set"in k)||(d=k.set(a,d))!==b)try{j[c]=d}catch(l){}}},css:function(a,c,d){var e,g;c=f.camelCase(c),g=f.cssHooks[c],c=f.cssProps[c]||c,c==="cssFloat"&&(c="float");if(g&&"get"in g&&(e=g.get(a,!0,d))!==b)return e;if(by)return by(a,c)},swap:function(a,b,c){var d={},e,f;for(f in b)d[f]=a.style[f],a.style[f]=b[f];e=c.call(a);for(f in b)a.style[f]=d[f];return e}}),f.curCSS=f.css,c.defaultView&&c.defaultView.getComputedStyle&&(bz=function(a,b){var c,d,e,g,h=a.style;b=b.replace(br,"-`$1").toLowerCase(),(d=a.ownerDocument.defaultView)&&(e=d.getComputedStyle(a,null))&&(c=e.getPropertyValue(b),c===""&&!f.contains(a.ownerDocument.documentElement,a)&&(c=f.style(a,b))),!f.support.pixelMargin&&e&&bv.test(b)&&bt.test(c)&&(g=h.width,h.width=c,c=e.width,h.width=g);return c}),c.documentElement.currentStyle&&(bA=function(a,b){var c,d,e,f=a.currentStyle&&a.currentStyle[b],g=a.style;f==null&&g&&(e=g[b])&&(f=e),bt.test(f)&&(c=g.left,d=a.runtimeStyle&&a.runtimeStyle.left,d&&(a.runtimeStyle.left=a.currentStyle.left),g.left=b==="fontSize"?"1em":f,f=g.pixelLeft+"px",g.left=c,d&&(a.runtimeStyle.left=d));return f===""?"auto":f}),by=bz||bA,f.each(["height","width"],function(a,b){f.cssHooks[b]={get:function(a,c,d){if(c)return a.offsetWidth!==0?bB(a,b,d):f.swap(a,bw,function(){return bB(a,b,d)})},set:function(a,b){return bs.test(b)?b+"px":b}}}),f.support.opacity||(f.cssHooks.opacity={get:function(a,b){return bq.test((b&&a.currentStyle?a.currentStyle.filter:a.style.filter)||"")?parseFloat(RegExp.`$1)/100+"":b?"1":""},set:function(a,b){var c=a.style,d=a.currentStyle,e=f.isNumeric(b)?"alpha(opacity="+b*100+")":"",g=d&&d.filter||c.filter||"";c.zoom=1;if(b>=1&&f.trim(g.replace(bp,""))===""){c.removeAttribute("filter");if(d&&!d.filter)return}c.filter=bp.test(g)?g.replace(bp,e):g+" "+e}}),f(function(){f.support.reliableMarginRight||(f.cssHooks.marginRight={get:function(a,b){return f.swap(a,{display:"inline-block"},function(){return b?by(a,"margin-right"):a.style.marginRight})}})}),f.expr&&f.expr.filters&&(f.expr.filters.hidden=function(a){var b=a.offsetWidth,c=a.offsetHeight;return b===0&&c===0||!f.support.reliableHiddenOffsets&&(a.style&&a.style.display||f.css(a,"display"))==="none"},f.expr.filters.visible=function(a){return!f.expr.filters.hidden(a)}),f.each({margin:"",padding:"",border:"Width"},function(a,b){f.cssHooks[a+b]={expand:function(c){var d,e=typeof c=="string"?c.split(" "):[c],f={};for(d=0;d<4;d++)f[a+bx[d]+b]=e[d]||e[d-2]||e[0];return f}}});var bC=/%20/g,bD=/\[\]`$/,bE=/\r?\n/g,bF=/#.*`$/,bG=/^(.*?):[ \t]*([^\r\n]*)\r?`$/mg,bH=/^(?:color|date|datetime|datetime-local|email|hidden|month|number|password|range|search|tel|text|time|url|week)`$/i,bI=/^(?:about|app|app\-storage|.+\-extension|file|res|widget):`$/,bJ=/^(?:GET|HEAD)`$/,bK=/^\/\//,bL=/\?/,bM=/<script\b[^<]*(?:(?!<\/script>)<[^<]*)*<\/script>/gi,bN=/^(?:select|textarea)/i,bO=/\s+/,bP=/([?&])_=[^&]*/,bQ=/^([\w\+\.\-]+:)(?:\/\/([^\/?#:]*)(?::(\d+))?)?/,bR=f.fn.load,bS={},bT={},bU,bV,bW=["*/"]+["*"];try{bU=e.href}catch(bX){bU=c.createElement("a"),bU.href="",bU=bU.href}bV=bQ.exec(bU.toLowerCase())||[],f.fn.extend({load:function(a,c,d){if(typeof a!="string"&&bR)return bR.apply(this,arguments);if(!this.length)return this;var e=a.indexOf(" ");if(e>=0){var g=a.slice(e,a.length);a=a.slice(0,e)}var h="GET";c&&(f.isFunction(c)?(d=c,c=b):typeof c=="object"&&(c=f.param(c,f.ajaxSettings.traditional),h="POST"));var i=this;f.ajax({url:a,type:h,dataType:"html",data:c,complete:function(a,b,c){c=a.responseText,a.isResolved()&&(a.done(function(a){c=a}),i.html(g?f("<div>").append(c.replace(bM,"")).find(g):c)),d&&i.each(d,[c,b,a])}});return this},serialize:function(){return f.param(this.serializeArray())},serializeArray:function(){return this.map(function(){return this.elements?f.makeArray(this.elements):this}).filter(function(){return this.name&&!this.disabled&&(this.checked||bN.test(this.nodeName)||bH.test(this.type))}).map(function(a,b){var c=f(this).val();return c==null?null:f.isArray(c)?f.map(c,function(a,c){return{name:b.name,value:a.replace(bE,"\r\n")}}):{name:b.name,value:c.replace(bE,"\r\n")}}).get()}}),f.each("ajaxStart ajaxStop ajaxComplete ajaxError ajaxSuccess ajaxSend".split(" "),function(a,b){f.fn[b]=function(a){return this.on(b,a)}}),f.each(["get","post"],function(a,c){f[c]=function(a,d,e,g){f.isFunction(d)&&(g=g||e,e=d,d=b);return f.ajax({type:c,url:a,data:d,success:e,dataType:g})}}),f.extend({getScript:function(a,c){return f.get(a,b,c,"script")},getJSON:function(a,b,c){return f.get(a,b,c,"json")},ajaxSetup:function(a,b){b?b`$(a,f.ajaxSettings):(b=a,a=f.ajaxSettings),b`$(a,b);return a},ajaxSettings:{url:bU,isLocal:bI.test(bV[1]),global:!0,type:"GET",contentType:"application/x-www-form-urlencoded; charset=UTF-8",processData:!0,async:!0,accepts:{xml:"application/xml, text/xml",html:"text/html",text:"text/plain",json:"application/json, text/javascript","*":bW},contents:{xml:/xml/,html:/html/,json:/json/},responseFields:{xml:"responseXML",text:"responseText"},converters:{"* text":a.String,"text html":!0,"text json":f.parseJSON,"text xml":f.parseXML},flatOptions:{context:!0,url:!0}},ajaxPrefilter:bY(bS),ajaxTransport:bY(bT),ajax:function(a,c){function w(a,c,l,m){if(s!==2){s=2,q&&clearTimeout(q),p=b,n=m||"",v.readyState=a>0?4:0;var o,r,u,w=c,x=l?ca(d,v,l):b,y,z;if(a>=200&&a<300||a===304){if(d.ifModified){if(y=v.getResponseHeader("Last-Modified"))f.lastModified[k]=y;if(z=v.getResponseHeader("Etag"))f.etag[k]=z}if(a===304)w="notmodified",o=!0;else try{r=cb(d,x),w="success",o=!0}catch(A){w="parsererror",u=A}}else{u=w;if(!w||a)w="error",a<0&&(a=0)}v.status=a,v.statusText=""+(c||w),o?h.resolveWith(e,[r,w,v]):h.rejectWith(e,[v,w,u]),v.statusCode(j),j=b,t&&g.trigger("ajax"+(o?"Success":"Error"),[v,d,o?r:u]),i.fireWith(e,[v,w]),t&&(g.trigger("ajaxComplete",[v,d]),--f.active||f.event.trigger("ajaxStop"))}}typeof a=="object"&&(c=a,a=b),c=c||{};var d=f.ajaxSetup({},c),e=d.context||d,g=e!==d&&(e.nodeType||e instanceof f)?f(e):f.event,h=f.Deferred(),i=f.Callbacks("once memory"),j=d.statusCode||{},k,l={},m={},n,o,p,q,r,s=0,t,u,v={readyState:0,setRequestHeader:function(a,b){if(!s){var c=a.toLowerCase();a=m[c]=m[c]||a,l[a]=b}return this},getAllResponseHeaders:function(){return s===2?n:null},getResponseHeader:function(a){var c;if(s===2){if(!o){o={};while(c=bG.exec(n))o[c[1].toLowerCase()]=c[2]}c=o[a.toLowerCase()]}return c===b?null:c},overrideMimeType:function(a){s||(d.mimeType=a);return this},abort:function(a){a=a||"abort",p&&p.abort(a),w(0,a);return this}};h.promise(v),v.success=v.done,v.error=v.fail,v.complete=i.add,v.statusCode=function(a){if(a){var b;if(s<2)for(b in a)j[b]=[j[b],a[b]];else b=a[v.status],v.then(b,b)}return this},d.url=((a||d.url)+"").replace(bF,"").replace(bK,bV[1]+"//"),d.dataTypes=f.trim(d.dataType||"*").toLowerCase().split(bO),d.crossDomain==null&&(r=bQ.exec(d.url.toLowerCase()),d.crossDomain=!(!r||r[1]==bV[1]&&r[2]==bV[2]&&(r[3]||(r[1]==="http:"?80:443))==(bV[3]||(bV[1]==="http:"?80:443)))),d.data&&d.processData&&typeof d.data!="string"&&(d.data=f.param(d.data,d.traditional)),bZ(bS,d,c,v);if(s===2)return!1;t=d.global,d.type=d.type.toUpperCase(),d.hasContent=!bJ.test(d.type),t&&f.active++===0&&f.event.trigger("ajaxStart");if(!d.hasContent){d.data&&(d.url+=(bL.test(d.url)?"&":"?")+d.data,delete d.data),k=d.url;if(d.cache===!1){var x=f.now(),y=d.url.replace(bP,"`$1_="+x);d.url=y+(y===d.url?(bL.test(d.url)?"&":"?")+"_="+x:"")}}(d.data&&d.hasContent&&d.contentType!==!1||c.contentType)&&v.setRequestHeader("Content-Type",d.contentType),d.ifModified&&(k=k||d.url,f.lastModified[k]&&v.setRequestHeader("If-Modified-Since",f.lastModified[k]),f.etag[k]&&v.setRequestHeader("If-None-Match",f.etag[k])),v.setRequestHeader("Accept",d.dataTypes[0]&&d.accepts[d.dataTypes[0]]?d.accepts[d.dataTypes[0]]+(d.dataTypes[0]!=="*"?", "+bW+"; q=0.01":""):d.accepts["*"]);for(u in d.headers)v.setRequestHeader(u,d.headers[u]);if(d.beforeSend&&(d.beforeSend.call(e,v,d)===!1||s===2)){v.abort();return!1}for(u in{success:1,error:1,complete:1})v[u](d[u]);p=bZ(bT,d,c,v);if(!p)w(-1,"No Transport");else{v.readyState=1,t&&g.trigger("ajaxSend",[v,d]),d.async&&d.timeout>0&&(q=setTimeout(function(){v.abort("timeout")},d.timeout));try{s=1,p.send(l,w)}catch(z){if(s<2)w(-1,z);else throw z}}return v},param:function(a,c){var d=[],e=function(a,b){b=f.isFunction(b)?b():b,d[d.length]=encodeURIComponent(a)+"="+encodeURIComponent(b)};c===b&&(c=f.ajaxSettings.traditional);if(f.isArray(a)||a.jquery&&!f.isPlainObject(a))f.each(a,function(){e(this.name,this.value)});else for(var g in a)b_(g,a[g],c,e);return d.join("&").replace(bC,"+")}}),f.extend({active:0,lastModified:{},etag:{}});var cc=f.now(),cd=/(\=)\?(&|`$)|\?\?/i;f.ajaxSetup({jsonp:"callback",jsonpCallback:function(){return f.expando+"_"+cc++}}),f.ajaxPrefilter("json jsonp",function(b,c,d){var e=typeof b.data=="string"&&/^application\/x\-www\-form\-urlencoded/.test(b.contentType);if(b.dataTypes[0]==="jsonp"||b.jsonp!==!1&&(cd.test(b.url)||e&&cd.test(b.data))){var g,h=b.jsonpCallback=f.isFunction(b.jsonpCallback)?b.jsonpCallback():b.jsonpCallback,i=a[h],j=b.url,k=b.data,l="`$1"+h+"`$2";b.jsonp!==!1&&(j=j.replace(cd,l),b.url===j&&(e&&(k=k.replace(cd,l)),b.data===k&&(j+=(/\?/.test(j)?"&":"?")+b.jsonp+"="+h))),b.url=j,b.data=k,a[h]=function(a){g=[a]},d.always(function(){a[h]=i,g&&f.isFunction(i)&&a[h](g[0])}),b.converters["script json"]=function(){g||f.error(h+" was not called");return g[0]},b.dataTypes[0]="json";return"script"}}),f.ajaxSetup({accepts:{script:"text/javascript, application/javascript, application/ecmascript, application/x-ecmascript"},contents:{script:/javascript|ecmascript/},converters:{"text script":function(a){f.globalEval(a);return a}}}),f.ajaxPrefilter("script",function(a){a.cache===b&&(a.cache=!1),a.crossDomain&&(a.type="GET",a.global=!1)}),f.ajaxTransport("script",function(a){if(a.crossDomain){var d,e=c.head||c.getElementsByTagName("head")[0]||c.documentElement;return{send:function(f,g){d=c.createElement("script"),d.async="async",a.scriptCharset&&(d.charset=a.scriptCharset),d.src=a.url,d.onload=d.onreadystatechange=function(a,c){if(c||!d.readyState||/loaded|complete/.test(d.readyState))d.onload=d.onreadystatechange=null,e&&d.parentNode&&e.removeChild(d),d=b,c||g(200,"success")},e.insertBefore(d,e.firstChild)},abort:function(){d&&d.onload(0,1)}}}});var ce=a.ActiveXObject?function(){for(var a in cg)cg[a](0,1)}:!1,cf=0,cg;f.ajaxSettings.xhr=a.ActiveXObject?function(){return!this.isLocal&&ch()||ci()}:ch,function(a){f.extend(f.support,{ajax:!!a,cors:!!a&&"withCredentials"in a})}(f.ajaxSettings.xhr()),f.support.ajax&&f.ajaxTransport(function(c){if(!c.crossDomain||f.support.cors){var d;return{send:function(e,g){var h=c.xhr(),i,j;c.username?h.open(c.type,c.url,c.async,c.username,c.password):h.open(c.type,c.url,c.async);if(c.xhrFields)for(j in c.xhrFields)h[j]=c.xhrFields[j];c.mimeType&&h.overrideMimeType&&h.overrideMimeType(c.mimeType),!c.crossDomain&&!e["X-Requested-With"]&&(e["X-Requested-With"]="XMLHttpRequest");try{for(j in e)h.setRequestHeader(j,e[j])}catch(k){}h.send(c.hasContent&&c.data||null),d=function(a,e){var j,k,l,m,n;try{if(d&&(e||h.readyState===4)){d=b,i&&(h.onreadystatechange=f.noop,ce&&delete cg[i]);if(e)h.readyState!==4&&h.abort();else{j=h.status,l=h.getAllResponseHeaders(),m={},n=h.responseXML,n&&n.documentElement&&(m.xml=n);try{m.text=h.responseText}catch(a){}try{k=h.statusText}catch(o){k=""}!j&&c.isLocal&&!c.crossDomain?j=m.text?200:404:j===1223&&(j=204)}}}catch(p){e||g(-1,p)}m&&g(j,k,m,l)},!c.async||h.readyState===4?d():(i=++cf,ce&&(cg||(cg={},f(a).unload(ce)),cg[i]=d),h.onreadystatechange=d)},abort:function(){d&&d(0,1)}}}});var cj={},ck,cl,cm=/^(?:toggle|show|hide)`$/,cn=/^([+\-]=)?([\d+.\-]+)([a-z%]*)`$/i,co,cp=[["height","marginTop","marginBottom","paddingTop","paddingBottom"],["width","marginLeft","marginRight","paddingLeft","paddingRight"],["opacity"]],cq;f.fn.extend({show:function(a,b,c){var d,e;if(a||a===0)return this.animate(ct("show",3),a,b,c);for(var g=0,h=this.length;g<h;g++)d=this[g],d.style&&(e=d.style.display,!f._data(d,"olddisplay")&&e==="none"&&(e=d.style.display=""),(e===""&&f.css(d,"display")==="none"||!f.contains(d.ownerDocument.documentElement,d))&&f._data(d,"olddisplay",cu(d.nodeName)));for(g=0;g<h;g++){d=this[g];if(d.style){e=d.style.display;if(e===""||e==="none")d.style.display=f._data(d,"olddisplay")||""}}return this},hide:function(a,b,c){if(a||a===0)return this.animate(ct("hide",3),a,b,c);var d,e,g=0,h=this.length;for(;g<h;g++)d=this[g],d.style&&(e=f.css(d,"display"),e!=="none"&&!f._data(d,"olddisplay")&&f._data(d,"olddisplay",e));for(g=0;g<h;g++)this[g].style&&(this[g].style.display="none");return this},_toggle:f.fn.toggle,toggle:function(a,b,c){var d=typeof a=="boolean";f.isFunction(a)&&f.isFunction(b)?this._toggle.apply(this,arguments):a==null||d?this.each(function(){var b=d?a:f(this).is(":hidden");f(this)[b?"show":"hide"]()}):this.animate(ct("toggle",3),a,b,c);return this},fadeTo:function(a,b,c,d){return this.filter(":hidden").css("opacity",0).show().end().animate({opacity:b},a,c,d)},animate:function(a,b,c,d){function g(){e.queue===!1&&f._mark(this);var b=f.extend({},e),c=this.nodeType===1,d=c&&f(this).is(":hidden"),g,h,i,j,k,l,m,n,o,p,q;b.animatedProperties={};for(i in a){g=f.camelCase(i),i!==g&&(a[g]=a[i],delete a[i]);if((k=f.cssHooks[g])&&"expand"in k){l=k.expand(a[g]),delete a[g];for(i in l)i in a||(a[i]=l[i])}}for(g in a){h=a[g],f.isArray(h)?(b.animatedProperties[g]=h[1],h=a[g]=h[0]):b.animatedProperties[g]=b.specialEasing&&b.specialEasing[g]||b.easing||"swing";if(h==="hide"&&d||h==="show"&&!d)return b.complete.call(this);c&&(g==="height"||g==="width")&&(b.overflow=[this.style.overflow,this.style.overflowX,this.style.overflowY],f.css(this,"display")==="inline"&&f.css(this,"float")==="none"&&(!f.support.inlineBlockNeedsLayout||cu(this.nodeName)==="inline"?this.style.display="inline-block":this.style.zoom=1))}b.overflow!=null&&(this.style.overflow="hidden");for(i in a)j=new f.fx(this,b,i),h=a[i],cm.test(h)?(q=f._data(this,"toggle"+i)||(h==="toggle"?d?"show":"hide":0),q?(f._data(this,"toggle"+i,q==="show"?"hide":"show"),j[q]()):j[h]()):(m=cn.exec(h),n=j.cur(),m?(o=parseFloat(m[2]),p=m[3]||(f.cssNumber[i]?"":"px"),p!=="px"&&(f.style(this,i,(o||1)+p),n=(o||1)/j.cur()*n,f.style(this,i,n+p)),m[1]&&(o=(m[1]==="-="?-1:1)*o+n),j.custom(n,o,p)):j.custom(n,h,""));return!0}var e=f.speed(b,c,d);if(f.isEmptyObject(a))return this.each(e.complete,[!1]);a=f.extend({},a);return e.queue===!1?this.each(g):this.queue(e.queue,g)},stop:function(a,c,d){typeof a!="string"&&(d=c,c=a,a=b),c&&a!==!1&&this.queue(a||"fx",[]);return this.each(function(){function h(a,b,c){var e=b[c];f.removeData(a,c,!0),e.stop(d)}var b,c=!1,e=f.timers,g=f._data(this);d||f._unmark(!0,this);if(a==null)for(b in g)g[b]&&g[b].stop&&b.indexOf(".run")===b.length-4&&h(this,g,b);else g[b=a+".run"]&&g[b].stop&&h(this,g,b);for(b=e.length;b--;)e[b].elem===this&&(a==null||e[b].queue===a)&&(d?e[b](!0):e[b].saveState(),c=!0,e.splice(b,1));(!d||!c)&&f.dequeue(this,a)})}}),f.each({slideDown:ct("show",1),slideUp:ct("hide",1),slideToggle:ct("toggle",1),fadeIn:{opacity:"show"},fadeOut:{opacity:"hide"},fadeToggle:{opacity:"toggle"}},function(a,b){f.fn[a]=function(a,c,d){return this.animate(b,a,c,d)}}),f.extend({speed:function(a,b,c){var d=a&&typeof a=="object"?f.extend({},a):{complete:c||!c&&b||f.isFunction(a)&&a,duration:a,easing:c&&b||b&&!f.isFunction(b)&&b};d.duration=f.fx.off?0:typeof d.duration=="number"?d.duration:d.duration in f.fx.speeds?f.fx.speeds[d.duration]:f.fx.speeds._default;if(d.queue==null||d.queue===!0)d.queue="fx";d.old=d.complete,d.complete=function(a){f.isFunction(d.old)&&d.old.call(this),d.queue?f.dequeue(this,d.queue):a!==!1&&f._unmark(this)};return d},easing:{linear:function(a){return a},swing:function(a){return-Math.cos(a*Math.PI)/2+.5}},timers:[],fx:function(a,b,c){this.options=b,this.elem=a,this.prop=c,b.orig=b.orig||{}}}),f.fx.prototype={update:function(){this.options.step&&this.options.step.call(this.elem,this.now,this),(f.fx.step[this.prop]||f.fx.step._default)(this)},cur:function(){if(this.elem[this.prop]!=null&&(!this.elem.style||this.elem.style[this.prop]==null))return this.elem[this.prop];var a,b=f.css(this.elem,this.prop);return isNaN(a=parseFloat(b))?!b||b==="auto"?0:b:a},custom:function(a,c,d){function h(a){return e.step(a)}var e=this,g=f.fx;this.startTime=cq||cr(),this.end=c,this.now=this.start=a,this.pos=this.state=0,this.unit=d||this.unit||(f.cssNumber[this.prop]?"":"px"),h.queue=this.options.queue,h.elem=this.elem,h.saveState=function(){f._data(e.elem,"fxshow"+e.prop)===b&&(e.options.hide?f._data(e.elem,"fxshow"+e.prop,e.start):e.options.show&&f._data(e.elem,"fxshow"+e.prop,e.end))},h()&&f.timers.push(h)&&!co&&(co=setInterval(g.tick,g.interval))},show:function(){var a=f._data(this.elem,"fxshow"+this.prop);this.options.orig[this.prop]=a||f.style(this.elem,this.prop),this.options.show=!0,a!==b?this.custom(this.cur(),a):this.custom(this.prop==="width"||this.prop==="height"?1:0,this.cur()),f(this.elem).show()},hide:function(){this.options.orig[this.prop]=f._data(this.elem,"fxshow"+this.prop)||f.style(this.elem,this.prop),this.options.hide=!0,this.custom(this.cur(),0)},step:function(a){var b,c,d,e=cq||cr(),g=!0,h=this.elem,i=this.options;if(a||e>=i.duration+this.startTime){this.now=this.end,this.pos=this.state=1,this.update(),i.animatedProperties[this.prop]=!0;for(b in i.animatedProperties)i.animatedProperties[b]!==!0&&(g=!1);if(g){i.overflow!=null&&!f.support.shrinkWrapBlocks&&f.each(["","X","Y"],function(a,b){h.style["overflow"+b]=i.overflow[a]}),i.hide&&f(h).hide();if(i.hide||i.show)for(b in i.animatedProperties)f.style(h,b,i.orig[b]),f.removeData(h,"fxshow"+b,!0),f.removeData(h,"toggle"+b,!0);d=i.complete,d&&(i.complete=!1,d.call(h))}return!1}i.duration==Infinity?this.now=e:(c=e-this.startTime,this.state=c/i.duration,this.pos=f.easing[i.animatedProperties[this.prop]](this.state,c,0,1,i.duration),this.now=this.start+(this.end-this.start)*this.pos),this.update();return!0}},f.extend(f.fx,{tick:function(){var a,b=f.timers,c=0;for(;c<b.length;c++)a=b[c],!a()&&b[c]===a&&b.splice(c--,1);b.length||f.fx.stop()},interval:13,stop:function(){clearInterval(co),co=null},speeds:{slow:600,fast:200,_default:400},step:{opacity:function(a){f.style(a.elem,"opacity",a.now)},_default:function(a){a.elem.style&&a.elem.style[a.prop]!=null?a.elem.style[a.prop]=a.now+a.unit:a.elem[a.prop]=a.now}}}),f.each(cp.concat.apply([],cp),function(a,b){b.indexOf("margin")&&(f.fx.step[b]=function(a){f.style(a.elem,b,Math.max(0,a.now)+a.unit)})}),f.expr&&f.expr.filters&&(f.expr.filters.animated=function(a){return f.grep(f.timers,function(b){return a===b.elem}).length});var cv,cw=/^t(?:able|d|h)`$/i,cx=/^(?:body|html)`$/i;"getBoundingClientRect"in c.documentElement?cv=function(a,b,c,d){try{d=a.getBoundingClientRect()}catch(e){}if(!d||!f.contains(c,a))return d?{top:d.top,left:d.left}:{top:0,left:0};var g=b.body,h=cy(b),i=c.clientTop||g.clientTop||0,j=c.clientLeft||g.clientLeft||0,k=h.pageYOffset||f.support.boxModel&&c.scrollTop||g.scrollTop,l=h.pageXOffset||f.support.boxModel&&c.scrollLeft||g.scrollLeft,m=d.top+k-i,n=d.left+l-j;return{top:m,left:n}}:cv=function(a,b,c){var d,e=a.offsetParent,g=a,h=b.body,i=b.defaultView,j=i?i.getComputedStyle(a,null):a.currentStyle,k=a.offsetTop,l=a.offsetLeft;while((a=a.parentNode)&&a!==h&&a!==c){if(f.support.fixedPosition&&j.position==="fixed")break;d=i?i.getComputedStyle(a,null):a.currentStyle,k-=a.scrollTop,l-=a.scrollLeft,a===e&&(k+=a.offsetTop,l+=a.offsetLeft,f.support.doesNotAddBorder&&(!f.support.doesAddBorderForTableAndCells||!cw.test(a.nodeName))&&(k+=parseFloat(d.borderTopWidth)||0,l+=parseFloat(d.borderLeftWidth)||0),g=e,e=a.offsetParent),f.support.subtractsBorderForOverflowNotVisible&&d.overflow!=="visible"&&(k+=parseFloat(d.borderTopWidth)||0,l+=parseFloat(d.borderLeftWidth)||0),j=d}if(j.position==="relative"||j.position==="static")k+=h.offsetTop,l+=h.offsetLeft;f.support.fixedPosition&&j.position==="fixed"&&(k+=Math.max(c.scrollTop,h.scrollTop),l+=Math.max(c.scrollLeft,h.scrollLeft));return{top:k,left:l}},f.fn.offset=function(a){if(arguments.length)return a===b?this:this.each(function(b){f.offset.setOffset(this,a,b)});var c=this[0],d=c&&c.ownerDocument;if(!d)return null;if(c===d.body)return f.offset.bodyOffset(c);return cv(c,d,d.documentElement)},f.offset={bodyOffset:function(a){var b=a.offsetTop,c=a.offsetLeft;f.support.doesNotIncludeMarginInBodyOffset&&(b+=parseFloat(f.css(a,"marginTop"))||0,c+=parseFloat(f.css(a,"marginLeft"))||0);return{top:b,left:c}},setOffset:function(a,b,c){var d=f.css(a,"position");d==="static"&&(a.style.position="relative");var e=f(a),g=e.offset(),h=f.css(a,"top"),i=f.css(a,"left"),j=(d==="absolute"||d==="fixed")&&f.inArray("auto",[h,i])>-1,k={},l={},m,n;j?(l=e.position(),m=l.top,n=l.left):(m=parseFloat(h)||0,n=parseFloat(i)||0),f.isFunction(b)&&(b=b.call(a,c,g)),b.top!=null&&(k.top=b.top-g.top+m),b.left!=null&&(k.left=b.left-g.left+n),"using"in b?b.using.call(a,k):e.css(k)}},f.fn.extend({position:function(){if(!this[0])return null;var a=this[0],b=this.offsetParent(),c=this.offset(),d=cx.test(b[0].nodeName)?{top:0,left:0}:b.offset();c.top-=parseFloat(f.css(a,"marginTop"))||0,c.left-=parseFloat(f.css(a,"marginLeft"))||0,d.top+=parseFloat(f.css(b[0],"borderTopWidth"))||0,d.left+=parseFloat(f.css(b[0],"borderLeftWidth"))||0;return{top:c.top-d.top,left:c.left-d.left}},offsetParent:function(){return this.map(function(){var a=this.offsetParent||c.body;while(a&&!cx.test(a.nodeName)&&f.css(a,"position")==="static")a=a.offsetParent;return a})}}),f.each({scrollLeft:"pageXOffset",scrollTop:"pageYOffset"},function(a,c){var d=/Y/.test(c);f.fn[a]=function(e){return f.access(this,function(a,e,g){var h=cy(a);if(g===b)return h?c in h?h[c]:f.support.boxModel&&h.document.documentElement[e]||h.document.body[e]:a[e];h?h.scrollTo(d?f(h).scrollLeft():g,d?g:f(h).scrollTop()):a[e]=g},a,e,arguments.length,null)}}),f.each({Height:"height",Width:"width"},function(a,c){var d="client"+a,e="scroll"+a,g="offset"+a;f.fn["inner"+a]=function(){var a=this[0];return a?a.style?parseFloat(f.css(a,c,"padding")):this[c]():null},f.fn["outer"+a]=function(a){var b=this[0];return b?b.style?parseFloat(f.css(b,c,a?"margin":"border")):this[c]():null},f.fn[c]=function(a){return f.access(this,function(a,c,h){var i,j,k,l;if(f.isWindow(a)){i=a.document,j=i.documentElement[d];return f.support.boxModel&&j||i.body&&i.body[d]||j}if(a.nodeType===9){i=a.documentElement;if(i[d]>=i[e])return i[d];return Math.max(a.body[e],i[e],a.body[g],i[g])}if(h===b){k=f.css(a,c),l=parseFloat(k);return f.isNumeric(l)?l:k}f(a).css(c,h)},c,a,arguments.length,null)}}),a.jQuery=a.`$=f,typeof define=="function"&&define.amd&&define.amd.jQuery&&define("jquery",[],function(){return f})})(window);
</script> 
<script type="text/javascript">
  /*!
* Bootstrap.js by @fat & @mdo
* Copyright 2012 Twitter, Inc.
* http://www.apache.org/licenses/LICENSE-2.0.txt
*/
!function(e){"use strict";e(function(){e.support.transition=function(){var e=function(){var e=document.createElement("bootstrap"),t={WebkitTransition:"webkitTransitionEnd",MozTransition:"transitionend",OTransition:"oTransitionEnd otransitionend",transition:"transitionend"},n;for(n in t)if(e.style[n]!==undefined)return t[n]}();return e&&{end:e}}()})}(window.jQuery),!function(e){"use strict";var t='[data-dismiss="alert"]',n=function(n){e(n).on("click",t,this.close)};n.prototype.close=function(t){function s(){i.trigger("closed").remove()}var n=e(this),r=n.attr("data-target"),i;r||(r=n.attr("href"),r=r&&r.replace(/.*(?=#[^\s]*`$)/,"")),i=e(r),t&&t.preventDefault(),i.length||(i=n.hasClass("alert")?n:n.parent()),i.trigger(t=e.Event("close"));if(t.isDefaultPrevented())return;i.removeClass("in"),e.support.transition&&i.hasClass("fade")?i.on(e.support.transition.end,s):s()},e.fn.alert=function(t){return this.each(function(){var r=e(this),i=r.data("alert");i||r.data("alert",i=new n(this)),typeof t=="string"&&i[t].call(r)})},e.fn.alert.Constructor=n,e(document).on("click.alert.data-api",t,n.prototype.close)}(window.jQuery),!function(e){"use strict";var t=function(t,n){this.`$element=e(t),this.options=e.extend({},e.fn.button.defaults,n)};t.prototype.setState=function(e){var t="disabled",n=this.`$element,r=n.data(),i=n.is("input")?"val":"html";e+="Text",r.resetText||n.data("resetText",n[i]()),n[i](r[e]||this.options[e]),setTimeout(function(){e=="loadingText"?n.addClass(t).attr(t,t):n.removeClass(t).removeAttr(t)},0)},t.prototype.toggle=function(){var e=this.`$element.closest('[data-toggle="buttons-radio"]');e&&e.find(".active").removeClass("active"),this.`$element.toggleClass("active")},e.fn.button=function(n){return this.each(function(){var r=e(this),i=r.data("button"),s=typeof n=="object"&&n;i||r.data("button",i=new t(this,s)),n=="toggle"?i.toggle():n&&i.setState(n)})},e.fn.button.defaults={loadingText:"loading..."},e.fn.button.Constructor=t,e(document).on("click.button.data-api","[data-toggle^=button]",function(t){var n=e(t.target);n.hasClass("btn")||(n=n.closest(".btn")),n.button("toggle")})}(window.jQuery),!function(e){"use strict";var t=function(t,n){this.`$element=e(t),this.options=n,this.options.slide&&this.slide(this.options.slide),this.options.pause=="hover"&&this.`$element.on("mouseenter",e.proxy(this.pause,this)).on("mouseleave",e.proxy(this.cycle,this))};t.prototype={cycle:function(t){return t||(this.paused=!1),this.options.interval&&!this.paused&&(this.interval=setInterval(e.proxy(this.next,this),this.options.interval)),this},to:function(t){var n=this.`$element.find(".item.active"),r=n.parent().children(),i=r.index(n),s=this;if(t>r.length-1||t<0)return;return this.sliding?this.`$element.one("slid",function(){s.to(t)}):i==t?this.pause().cycle():this.slide(t>i?"next":"prev",e(r[t]))},pause:function(t){return t||(this.paused=!0),this.`$element.find(".next, .prev").length&&e.support.transition.end&&(this.`$element.trigger(e.support.transition.end),this.cycle()),clearInterval(this.interval),this.interval=null,this},next:function(){if(this.sliding)return;return this.slide("next")},prev:function(){if(this.sliding)return;return this.slide("prev")},slide:function(t,n){var r=this.`$element.find(".item.active"),i=n||r[t](),s=this.interval,o=t=="next"?"left":"right",u=t=="next"?"first":"last",a=this,f;this.sliding=!0,s&&this.pause(),i=i.length?i:this.`$element.find(".item")[u](),f=e.Event("slide",{relatedTarget:i[0]});if(i.hasClass("active"))return;if(e.support.transition&&this.`$element.hasClass("slide")){this.`$element.trigger(f);if(f.isDefaultPrevented())return;i.addClass(t),i[0].offsetWidth,r.addClass(o),i.addClass(o),this.`$element.one(e.support.transition.end,function(){i.removeClass([t,o].join(" ")).addClass("active"),r.removeClass(["active",o].join(" ")),a.sliding=!1,setTimeout(function(){a.`$element.trigger("slid")},0)})}else{this.`$element.trigger(f);if(f.isDefaultPrevented())return;r.removeClass("active"),i.addClass("active"),this.sliding=!1,this.`$element.trigger("slid")}return s&&this.cycle(),this}},e.fn.carousel=function(n){return this.each(function(){var r=e(this),i=r.data("carousel"),s=e.extend({},e.fn.carousel.defaults,typeof n=="object"&&n),o=typeof n=="string"?n:s.slide;i||r.data("carousel",i=new t(this,s)),typeof n=="number"?i.to(n):o?i[o]():s.interval&&i.cycle()})},e.fn.carousel.defaults={interval:5e3,pause:"hover"},e.fn.carousel.Constructor=t,e(document).on("click.carousel.data-api","[data-slide]",function(t){var n=e(this),r,i=e(n.attr("data-target")||(r=n.attr("href"))&&r.replace(/.*(?=#[^\s]+`$)/,"")),s=e.extend({},i.data(),n.data());i.carousel(s),t.preventDefault()})}(window.jQuery),!function(e){"use strict";var t=function(t,n){this.`$element=e(t),this.options=e.extend({},e.fn.collapse.defaults,n),this.options.parent&&(this.`$parent=e(this.options.parent)),this.options.toggle&&this.toggle()};t.prototype={constructor:t,dimension:function(){var e=this.`$element.hasClass("width");return e?"width":"height"},show:function(){var t,n,r,i;if(this.transitioning)return;t=this.dimension(),n=e.camelCase(["scroll",t].join("-")),r=this.`$parent&&this.`$parent.find("> .accordion-group > .in");if(r&&r.length){i=r.data("collapse");if(i&&i.transitioning)return;r.collapse("hide"),i||r.data("collapse",null)}this.`$element[t](0),this.transition("addClass",e.Event("show"),"shown"),e.support.transition&&this.`$element[t](this.`$element[0][n])},hide:function(){var t;if(this.transitioning)return;t=this.dimension(),this.reset(this.`$element[t]()),this.transition("removeClass",e.Event("hide"),"hidden"),this.`$element[t](0)},reset:function(e){var t=this.dimension();return this.`$element.removeClass("collapse")[t](e||"auto")[0].offsetWidth,this.`$element[e!==null?"addClass":"removeClass"]("collapse"),this},transition:function(t,n,r){var i=this,s=function(){n.type=="show"&&i.reset(),i.transitioning=0,i.`$element.trigger(r)};this.`$element.trigger(n);if(n.isDefaultPrevented())return;this.transitioning=1,this.`$element[t]("in"),e.support.transition&&this.`$element.hasClass("collapse")?this.`$element.one(e.support.transition.end,s):s()},toggle:function(){this[this.`$element.hasClass("in")?"hide":"show"]()}},e.fn.collapse=function(n){return this.each(function(){var r=e(this),i=r.data("collapse"),s=typeof n=="object"&&n;i||r.data("collapse",i=new t(this,s)),typeof n=="string"&&i[n]()})},e.fn.collapse.defaults={toggle:!0},e.fn.collapse.Constructor=t,e(document).on("click.collapse.data-api","[data-toggle=collapse]",function(t){var n=e(this),r,i=n.attr("data-target")||t.preventDefault()||(r=n.attr("href"))&&r.replace(/.*(?=#[^\s]+`$)/,""),s=e(i).data("collapse")?"toggle":n.data();n[e(i).hasClass("in")?"addClass":"removeClass"]("collapsed"),e(i).collapse(s)})}(window.jQuery),!function(e){"use strict";function r(){e(t).each(function(){i(e(this)).removeClass("open")})}function i(t){var n=t.attr("data-target"),r;return n||(n=t.attr("href"),n=n&&/#/.test(n)&&n.replace(/.*(?=#[^\s]*`$)/,"")),r=e(n),r.length||(r=t.parent()),r}var t="[data-toggle=dropdown]",n=function(t){var n=e(t).on("click.dropdown.data-api",this.toggle);e("html").on("click.dropdown.data-api",function(){n.parent().removeClass("open")})};n.prototype={constructor:n,toggle:function(t){var n=e(this),s,o;if(n.is(".disabled, :disabled"))return;return s=i(n),o=s.hasClass("open"),r(),o||(s.toggleClass("open"),n.focus()),!1},keydown:function(t){var n,r,s,o,u,a;if(!/(38|40|27)/.test(t.keyCode))return;n=e(this),t.preventDefault(),t.stopPropagation();if(n.is(".disabled, :disabled"))return;o=i(n),u=o.hasClass("open");if(!u||u&&t.keyCode==27)return n.click();r=e("[role=menu] li:not(.divider) a",o);if(!r.length)return;a=r.index(r.filter(":focus")),t.keyCode==38&&a>0&&a--,t.keyCode==40&&a<r.length-1&&a++,~a||(a=0),r.eq(a).focus()}},e.fn.dropdown=function(t){return this.each(function(){var r=e(this),i=r.data("dropdown");i||r.data("dropdown",i=new n(this)),typeof t=="string"&&i[t].call(r)})},e.fn.dropdown.Constructor=n,e(document).on("click.dropdown.data-api touchstart.dropdown.data-api",r).on("click.dropdown touchstart.dropdown.data-api",".dropdown form",function(e){e.stopPropagation()}).on("click.dropdown.data-api touchstart.dropdown.data-api",t,n.prototype.toggle).on("keydown.dropdown.data-api touchstart.dropdown.data-api",t+", [role=menu]",n.prototype.keydown)}(window.jQuery),!function(e){"use strict";var t=function(t,n){this.options=n,this.`$element=e(t).delegate('[data-dismiss="modal"]',"click.dismiss.modal",e.proxy(this.hide,this)),this.options.remote&&this.`$element.find(".modal-body").load(this.options.remote)};t.prototype={constructor:t,toggle:function(){return this[this.isShown?"hide":"show"]()},show:function(){var t=this,n=e.Event("show");this.`$element.trigger(n);if(this.isShown||n.isDefaultPrevented())return;this.isShown=!0,this.escape(),this.backdrop(function(){var n=e.support.transition&&t.`$element.hasClass("fade");t.`$element.parent().length||t.`$element.appendTo(document.body),t.`$element.show(),n&&t.`$element[0].offsetWidth,t.`$element.addClass("in").attr("aria-hidden",!1),t.enforceFocus(),n?t.`$element.one(e.support.transition.end,function(){t.`$element.focus().trigger("shown")}):t.`$element.focus().trigger("shown")})},hide:function(t){t&&t.preventDefault();var n=this;t=e.Event("hide"),this.`$element.trigger(t);if(!this.isShown||t.isDefaultPrevented())return;this.isShown=!1,this.escape(),e(document).off("focusin.modal"),this.`$element.removeClass("in").attr("aria-hidden",!0),e.support.transition&&this.`$element.hasClass("fade")?this.hideWithTransition():this.hideModal()},enforceFocus:function(){var t=this;e(document).on("focusin.modal",function(e){t.`$element[0]!==e.target&&!t.`$element.has(e.target).length&&t.`$element.focus()})},escape:function(){var e=this;this.isShown&&this.options.keyboard?this.`$element.on("keyup.dismiss.modal",function(t){t.which==27&&e.hide()}):this.isShown||this.`$element.off("keyup.dismiss.modal")},hideWithTransition:function(){var t=this,n=setTimeout(function(){t.`$element.off(e.support.transition.end),t.hideModal()},500);this.`$element.one(e.support.transition.end,function(){clearTimeout(n),t.hideModal()})},hideModal:function(e){this.`$element.hide().trigger("hidden"),this.backdrop()},removeBackdrop:function(){this.`$backdrop.remove(),this.`$backdrop=null},backdrop:function(t){var n=this,r=this.`$element.hasClass("fade")?"fade":"";if(this.isShown&&this.options.backdrop){var i=e.support.transition&&r;this.`$backdrop=e('<div class="modal-backdrop '+r+'" />').appendTo(document.body),this.`$backdrop.click(this.options.backdrop=="static"?e.proxy(this.`$element[0].focus,this.`$element[0]):e.proxy(this.hide,this)),i&&this.`$backdrop[0].offsetWidth,this.`$backdrop.addClass("in"),i?this.`$backdrop.one(e.support.transition.end,t):t()}else!this.isShown&&this.`$backdrop?(this.`$backdrop.removeClass("in"),e.support.transition&&this.`$element.hasClass("fade")?this.`$backdrop.one(e.support.transition.end,e.proxy(this.removeBackdrop,this)):this.removeBackdrop()):t&&t()}},e.fn.modal=function(n){return this.each(function(){var r=e(this),i=r.data("modal"),s=e.extend({},e.fn.modal.defaults,r.data(),typeof n=="object"&&n);i||r.data("modal",i=new t(this,s)),typeof n=="string"?i[n]():s.show&&i.show()})},e.fn.modal.defaults={backdrop:!0,keyboard:!0,show:!0},e.fn.modal.Constructor=t,e(document).on("click.modal.data-api",'[data-toggle="modal"]',function(t){var n=e(this),r=n.attr("href"),i=e(n.attr("data-target")||r&&r.replace(/.*(?=#[^\s]+`$)/,"")),s=i.data("modal")?"toggle":e.extend({remote:!/#/.test(r)&&r},i.data(),n.data());t.preventDefault(),i.modal(s).one("hide",function(){n.focus()})})}(window.jQuery),!function(e){"use strict";var t=function(e,t){this.init("tooltip",e,t)};t.prototype={constructor:t,init:function(t,n,r){var i,s;this.type=t,this.`$element=e(n),this.options=this.getOptions(r),this.enabled=!0,this.options.trigger=="click"?this.`$element.on("click."+this.type,this.options.selector,e.proxy(this.toggle,this)):this.options.trigger!="manual"&&(i=this.options.trigger=="hover"?"mouseenter":"focus",s=this.options.trigger=="hover"?"mouseleave":"blur",this.`$element.on(i+"."+this.type,this.options.selector,e.proxy(this.enter,this)),this.`$element.on(s+"."+this.type,this.options.selector,e.proxy(this.leave,this))),this.options.selector?this._options=e.extend({},this.options,{trigger:"manual",selector:""}):this.fixTitle()},getOptions:function(t){return t=e.extend({},e.fn[this.type].defaults,t,this.`$element.data()),t.delay&&typeof t.delay=="number"&&(t.delay={show:t.delay,hide:t.delay}),t},enter:function(t){var n=e(t.currentTarget)[this.type](this._options).data(this.type);if(!n.options.delay||!n.options.delay.show)return n.show();clearTimeout(this.timeout),n.hoverState="in",this.timeout=setTimeout(function(){n.hoverState=="in"&&n.show()},n.options.delay.show)},leave:function(t){var n=e(t.currentTarget)[this.type](this._options).data(this.type);this.timeout&&clearTimeout(this.timeout);if(!n.options.delay||!n.options.delay.hide)return n.hide();n.hoverState="out",this.timeout=setTimeout(function(){n.hoverState=="out"&&n.hide()},n.options.delay.hide)},show:function(){var e,t,n,r,i,s,o;if(this.hasContent()&&this.enabled){e=this.tip(),this.setContent(),this.options.animation&&e.addClass("fade"),s=typeof this.options.placement=="function"?this.options.placement.call(this,e[0],this.`$element[0]):this.options.placement,t=/in/.test(s),e.detach().css({top:0,left:0,display:"block"}).insertAfter(this.`$element),n=this.getPosition(t),r=e[0].offsetWidth,i=e[0].offsetHeight;switch(t?s.split(" ")[1]:s){case"bottom":o={top:n.top+n.height,left:n.left+n.width/2-r/2};break;case"top":o={top:n.top-i,left:n.left+n.width/2-r/2};break;case"left":o={top:n.top+n.height/2-i/2,left:n.left-r};break;case"right":o={top:n.top+n.height/2-i/2,left:n.left+n.width}}e.offset(o).addClass(s).addClass("in")}},setContent:function(){var e=this.tip(),t=this.getTitle();e.find(".tooltip-inner")[this.options.html?"html":"text"](t),e.removeClass("fade in top bottom left right")},hide:function(){function r(){var t=setTimeout(function(){n.off(e.support.transition.end).detach()},500);n.one(e.support.transition.end,function(){clearTimeout(t),n.detach()})}var t=this,n=this.tip();return n.removeClass("in"),e.support.transition&&this.`$tip.hasClass("fade")?r():n.detach(),this},fixTitle:function(){var e=this.`$element;(e.attr("title")||typeof e.attr("data-original-title")!="string")&&e.attr("data-original-title",e.attr("title")||"").removeAttr("title")},hasContent:function(){return this.getTitle()},getPosition:function(t){return e.extend({},t?{top:0,left:0}:this.`$element.offset(),{width:this.`$element[0].offsetWidth,height:this.`$element[0].offsetHeight})},getTitle:function(){var e,t=this.`$element,n=this.options;return e=t.attr("data-original-title")||(typeof n.title=="function"?n.title.call(t[0]):n.title),e},tip:function(){return this.`$tip=this.`$tip||e(this.options.template)},validate:function(){this.`$element[0].parentNode||(this.hide(),this.`$element=null,this.options=null)},enable:function(){this.enabled=!0},disable:function(){this.enabled=!1},toggleEnabled:function(){this.enabled=!this.enabled},toggle:function(t){var n=e(t.currentTarget)[this.type](this._options).data(this.type);n[n.tip().hasClass("in")?"hide":"show"]()},destroy:function(){this.hide().`$element.off("."+this.type).removeData(this.type)}},e.fn.tooltip=function(n){return this.each(function(){var r=e(this),i=r.data("tooltip"),s=typeof n=="object"&&n;i||r.data("tooltip",i=new t(this,s)),typeof n=="string"&&i[n]()})},e.fn.tooltip.Constructor=t,e.fn.tooltip.defaults={animation:!0,placement:"top",selector:!1,template:'<div class="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner"></div></div>',trigger:"hover",title:"",delay:0,html:!1}}(window.jQuery),!function(e){"use strict";var t=function(e,t){this.init("popover",e,t)};t.prototype=e.extend({},e.fn.tooltip.Constructor.prototype,{constructor:t,setContent:function(){var e=this.tip(),t=this.getTitle(),n=this.getContent();e.find(".popover-title")[this.options.html?"html":"text"](t),e.find(".popover-content > *")[this.options.html?"html":"text"](n),e.removeClass("fade top bottom left right in")},hasContent:function(){return this.getTitle()||this.getContent()},getContent:function(){var e,t=this.`$element,n=this.options;return e=t.attr("data-content")||(typeof n.content=="function"?n.content.call(t[0]):n.content),e},tip:function(){return this.`$tip||(this.`$tip=e(this.options.template)),this.`$tip},destroy:function(){this.hide().`$element.off("."+this.type).removeData(this.type)}}),e.fn.popover=function(n){return this.each(function(){var r=e(this),i=r.data("popover"),s=typeof n=="object"&&n;i||r.data("popover",i=new t(this,s)),typeof n=="string"&&i[n]()})},e.fn.popover.Constructor=t,e.fn.popover.defaults=e.extend({},e.fn.tooltip.defaults,{placement:"right",trigger:"click",content:"",template:'<div class="popover"><div class="arrow"></div><div class="popover-inner"><h3 class="popover-title"></h3><div class="popover-content"><p></p></div></div></div>'})}(window.jQuery),!function(e){"use strict";function t(t,n){var r=e.proxy(this.process,this),i=e(t).is("body")?e(window):e(t),s;this.options=e.extend({},e.fn.scrollspy.defaults,n),this.`$scrollElement=i.on("scroll.scroll-spy.data-api",r),this.selector=(this.options.target||(s=e(t).attr("href"))&&s.replace(/.*(?=#[^\s]+`$)/,"")||"")+" .nav li > a",this.`$body=e("body"),this.refresh(),this.process()}t.prototype={constructor:t,refresh:function(){var t=this,n;this.offsets=e([]),this.targets=e([]),n=this.`$body.find(this.selector).map(function(){var t=e(this),n=t.data("target")||t.attr("href"),r=/^#\w/.test(n)&&e(n);return r&&r.length&&[[r.position().top,n]]||null}).sort(function(e,t){return e[0]-t[0]}).each(function(){t.offsets.push(this[0]),t.targets.push(this[1])})},process:function(){var e=this.`$scrollElement.scrollTop()+this.options.offset,t=this.`$scrollElement[0].scrollHeight||this.`$body[0].scrollHeight,n=t-this.`$scrollElement.height(),r=this.offsets,i=this.targets,s=this.activeTarget,o;if(e>=n)return s!=(o=i.last()[0])&&this.activate(o);for(o=r.length;o--;)s!=i[o]&&e>=r[o]&&(!r[o+1]||e<=r[o+1])&&this.activate(i[o])},activate:function(t){var n,r;this.activeTarget=t,e(this.selector).parent(".active").removeClass("active"),r=this.selector+'[data-target="'+t+'"],'+this.selector+'[href="'+t+'"]',n=e(r).parent("li").addClass("active"),n.parent(".dropdown-menu").length&&(n=n.closest("li.dropdown").addClass("active")),n.trigger("activate")}},e.fn.scrollspy=function(n){return this.each(function(){var r=e(this),i=r.data("scrollspy"),s=typeof n=="object"&&n;i||r.data("scrollspy",i=new t(this,s)),typeof n=="string"&&i[n]()})},e.fn.scrollspy.Constructor=t,e.fn.scrollspy.defaults={offset:10},e(window).on("load",function(){e('[data-spy="scroll"]').each(function(){var t=e(this);t.scrollspy(t.data())})})}(window.jQuery),!function(e){"use strict";var t=function(t){this.element=e(t)};t.prototype={constructor:t,show:function(){var t=this.element,n=t.closest("ul:not(.dropdown-menu)"),r=t.attr("data-target"),i,s,o;r||(r=t.attr("href"),r=r&&r.replace(/.*(?=#[^\s]*`$)/,""));if(t.parent("li").hasClass("active"))return;i=n.find(".active:last a")[0],o=e.Event("show",{relatedTarget:i}),t.trigger(o);if(o.isDefaultPrevented())return;s=e(r),this.activate(t.parent("li"),n),this.activate(s,s.parent(),function(){t.trigger({type:"shown",relatedTarget:i})})},activate:function(t,n,r){function o(){i.removeClass("active").find("> .dropdown-menu > .active").removeClass("active"),t.addClass("active"),s?(t[0].offsetWidth,t.addClass("in")):t.removeClass("fade"),t.parent(".dropdown-menu")&&t.closest("li.dropdown").addClass("active"),r&&r()}var i=n.find("> .active"),s=r&&e.support.transition&&i.hasClass("fade");s?i.one(e.support.transition.end,o):o(),i.removeClass("in")}},e.fn.tab=function(n){return this.each(function(){var r=e(this),i=r.data("tab");i||r.data("tab",i=new t(this)),typeof n=="string"&&i[n]()})},e.fn.tab.Constructor=t,e(document).on("click.tab.data-api",'[data-toggle="tab"], [data-toggle="pill"]',function(t){t.preventDefault(),e(this).tab("show")})}(window.jQuery),!function(e){"use strict";var t=function(t,n){this.`$element=e(t),this.options=e.extend({},e.fn.typeahead.defaults,n),this.matcher=this.options.matcher||this.matcher,this.sorter=this.options.sorter||this.sorter,this.highlighter=this.options.highlighter||this.highlighter,this.updater=this.options.updater||this.updater,this.`$menu=e(this.options.menu).appendTo("body"),this.source=this.options.source,this.shown=!1,this.listen()};t.prototype={constructor:t,select:function(){var e=this.`$menu.find(".active").attr("data-value");return this.`$element.val(this.updater(e)).change(),this.hide()},updater:function(e){return e},show:function(){var t=e.extend({},this.`$element.offset(),{height:this.`$element[0].offsetHeight});return this.`$menu.css({top:t.top+t.height,left:t.left}),this.`$menu.show(),this.shown=!0,this},hide:function(){return this.`$menu.hide(),this.shown=!1,this},lookup:function(t){var n;return this.query=this.`$element.val(),!this.query||this.query.length<this.options.minLength?this.shown?this.hide():this:(n=e.isFunction(this.source)?this.source(this.query,e.proxy(this.process,this)):this.source,n?this.process(n):this)},process:function(t){var n=this;return t=e.grep(t,function(e){return n.matcher(e)}),t=this.sorter(t),t.length?this.render(t.slice(0,this.options.items)).show():this.shown?this.hide():this},matcher:function(e){return~e.toLowerCase().indexOf(this.query.toLowerCase())},sorter:function(e){var t=[],n=[],r=[],i;while(i=e.shift())i.toLowerCase().indexOf(this.query.toLowerCase())?~i.indexOf(this.query)?n.push(i):r.push(i):t.push(i);return t.concat(n,r)},highlighter:function(e){var t=this.query.replace(/[\-\[\]{}()*+?.,\\\^`$|#\s]/g,"\\`$&");return e.replace(new RegExp("("+t+")","ig"),function(e,t){return"<strong>"+t+"</strong>"})},render:function(t){var n=this;return t=e(t).map(function(t,r){return t=e(n.options.item).attr("data-value",r),t.find("a").html(n.highlighter(r)),t[0]}),t.first().addClass("active"),this.`$menu.html(t),this},next:function(t){var n=this.`$menu.find(".active").removeClass("active"),r=n.next();r.length||(r=e(this.`$menu.find("li")[0])),r.addClass("active")},prev:function(e){var t=this.`$menu.find(".active").removeClass("active"),n=t.prev();n.length||(n=this.`$menu.find("li").last()),n.addClass("active")},listen:function(){this.`$element.on("blur",e.proxy(this.blur,this)).on("keypress",e.proxy(this.keypress,this)).on("keyup",e.proxy(this.keyup,this)),this.eventSupported("keydown")&&this.`$element.on("keydown",e.proxy(this.keydown,this)),this.`$menu.on("click",e.proxy(this.click,this)).on("mouseenter","li",e.proxy(this.mouseenter,this))},eventSupported:function(e){var t=e in this.`$element;return t||(this.`$element.setAttribute(e,"return;"),t=typeof this.`$element[e]=="function"),t},move:function(e){if(!this.shown)return;switch(e.keyCode){case 9:case 13:case 27:e.preventDefault();break;case 38:e.preventDefault(),this.prev();break;case 40:e.preventDefault(),this.next()}e.stopPropagation()},keydown:function(t){this.suppressKeyPressRepeat=!~e.inArray(t.keyCode,[40,38,9,13,27]),this.move(t)},keypress:function(e){if(this.suppressKeyPressRepeat)return;this.move(e)},keyup:function(e){switch(e.keyCode){case 40:case 38:case 16:case 17:case 18:break;case 9:case 13:if(!this.shown)return;this.select();break;case 27:if(!this.shown)return;this.hide();break;default:this.lookup()}e.stopPropagation(),e.preventDefault()},blur:function(e){var t=this;setTimeout(function(){t.hide()},150)},click:function(e){e.stopPropagation(),e.preventDefault(),this.select()},mouseenter:function(t){this.`$menu.find(".active").removeClass("active"),e(t.currentTarget).addClass("active")}},e.fn.typeahead=function(n){return this.each(function(){var r=e(this),i=r.data("typeahead"),s=typeof n=="object"&&n;i||r.data("typeahead",i=new t(this,s)),typeof n=="string"&&i[n]()})},e.fn.typeahead.defaults={source:[],items:8,menu:'<ul class="typeahead dropdown-menu"></ul>',item:'<li><a href="#"></a></li>',minLength:1},e.fn.typeahead.Constructor=t,e(document).on("focus.typeahead.data-api",'[data-provide="typeahead"]',function(t){var n=e(this);if(n.data("typeahead"))return;t.preventDefault(),n.typeahead(n.data())})}(window.jQuery),!function(e){"use strict";var t=function(t,n){this.options=e.extend({},e.fn.affix.defaults,n),this.`$window=e(window).on("scroll.affix.data-api",e.proxy(this.checkPosition,this)).on("click.affix.data-api",e.proxy(function(){setTimeout(e.proxy(this.checkPosition,this),1)},this)),this.`$element=e(t),this.checkPosition()};t.prototype.checkPosition=function(){if(!this.`$element.is(":visible"))return;var t=e(document).height(),n=this.`$window.scrollTop(),r=this.`$element.offset(),i=this.options.offset,s=i.bottom,o=i.top,u="affix affix-top affix-bottom",a;typeof i!="object"&&(s=o=i),typeof o=="function"&&(o=i.top()),typeof s=="function"&&(s=i.bottom()),a=this.unpin!=null&&n+this.unpin<=r.top?!1:s!=null&&r.top+this.`$element.height()>=t-s?"bottom":o!=null&&n<=o?"top":!1;if(this.affixed===a)return;this.affixed=a,this.unpin=a=="bottom"?r.top-n:null,this.`$element.removeClass(u).addClass("affix"+(a?"-"+a:""))},e.fn.affix=function(n){return this.each(function(){var r=e(this),i=r.data("affix"),s=typeof n=="object"&&n;i||r.data("affix",i=new t(this,s)),typeof n=="string"&&i[n]()})},e.fn.affix.Constructor=t,e.fn.affix.defaults={offset:0},e(window).on("load",function(){e('[data-spy="affix"]').each(function(){var t=e(this),n=t.data();n.offset=n.offset||{},n.offsetBottom&&(n.offset.bottom=n.offsetBottom),n.offsetTop&&(n.offset.top=n.offsetTop),t.affix(n)})})}(window.jQuery);
</script>
  
  <script type="text/javascript" src="http://www.google.com/jsapi"></script>
    <script type="text/javascript" src="http://almende.github.io/chap-links-library/js/graph/graph.js"></script>
	
	
    <!-- <link rel="stylesheet" type="text/css" href="http://almende.github.io/chap-links-library/js/graph/graph.css"> -->
	<style type="text/css">
	div.graph-frame{border:1px solid #d3d3d3}div.graph-axis,div.graph-axis-grid{border-width:1px}div.graph-axis{border-color:#bfbfbf}div.graph-axis-grid-minor{border-color:#999;filter:alpha(opacity=25);opacity:.25}div.graph-axis-grid-major,div.graph-axis-grid-vertical{border-color:#000;filter:alpha(opacity=25);opacity:.25}div.graph-axis-button-menu{left:10px;top:10px}div.graph-axis-button{color:#fff;background-color:#d3d3d3;width:16px;height:16px;text-align:center;font-size:13px;border-radius:2px;margin-right:5px;margin-bottom:5px;cursor:pointer;line-height:16px}div.graph-axis-button:hover{background-color:gray}div.graph-background-area{position:absolute;left:0;top:0;width:100%;height:0}div.graph-axis-text{padding:3px;color:#4D4D4D}div.graph-axis-text-vertical{padding-right:10px}div.graph-legend{border:1px solid #bfbfbf}div.graph-legend-item{color:#4D4D4D;margin:5px}div.graph-tooltip-dot{position:absolute;width:0;height:0;margin-left:-4px;margin-top:-4px;border:4px solid #4d4d4d;border-radius:4px}div.graph-tooltip-label{position:absolute;background-color:#fff;background-color:rgba(255,255,255,.8);border:1px solid #bfbfbf;border-radius:1px;box-shadow:0 0 15px rgba(128,128,128,.2);padding:5px;color:#4d4d4d}div.graph-tooltip-label table{width:200px}div.graph-tooltip-label td{vertical-align:top}
	</style>
    <!--[if IE]><script src="../excanvas.js"></script><![endif]-->

    <script type="text/javascript">
		
      google.load("visualization", "1");

      // Set callback to run when API is loaded
      google.setOnLoadCallback(drawVisualization);
		
      // Called when the Visualization API is loaded.
      function drawVisualization() {

    
        $DataRows
		
		
		// specify options
        var options = {
          "width":  "100%",
          "height": "500px",
		  "visible": "true"
		  
        };

       

        // Draw our graph with the created data and options
		//`$('#menu3').toggle(function () {
		`$('#menu3').show(function () {
        $GrapInitializer
        
		
		});
		
      }

   </script>
 
<script>
function StarterFunction()
{
	
	
    jQuery('#menu3').hide();
        
}
jQuery(function(){ 
		
		
		jQuery('.targetDiv').hide();
		jQuery('#menu1').show();
		
		
         jQuery('#showall').click(function(){
               jQuery('.targetDiv').show();
        });
        jQuery('.showSingle').click(function(){
              jQuery('.targetDiv').hide();
              jQuery('#menu'+`$(this).attr('target')).show();
        });
		setTimeout(StarterFunction, 3000);
		
		
		
		
		
});
</script>
</body>
</html>

"@
    $var  | Set-Content $OutputFile
}

function Generate-HtmlPing()
{
    param([PingStatistic]$PingStatistics)
    $var=@"
    	<div class="widget-box">
        <div class="widget-title bg_ly" >
          <h5>Ping statistics to $($PingStatistics.TargetAddress)</h5>
        </div>
		<div class="widget-content" > 
		<tt>
		Ping statistics for $($PingStatistics.TargetAddress):<br />
		Packets: Sent = $($PingStatistics.SentPackages), Received = $($PingStatistics.ReceivedPackages), Lost = $($PingStatistics.LostPackages) ($($PingStatistics.LostPercentage)% loss),<br />
		Approximate round trip times in milli-seconds:<br />
		Minimum = $($PingStatistics.MinResponseTime)ms, Maximum = $($PingStatistics.MaxResponeTime)ms, Average = $($PingStatistics.avarageResponseTime)ms<br />
		</tt>
		</div>
		</div>

"@

return $var
}

function Generate-HTMLDisconnectionTable()
{
        param([PingStatistic]$PingStatistics)
        
        $DiscoTable=""
        $DiscoCounter=0
        if($PingStatistics.ListOfDisconnections.Count -gt 0)
        {
            foreach($nextDisco in $PingStatistics.ListOfDisconnections)
            {
                $DiscoTable+="  <tr><td>$DiscoCounter</td><td>$($nextDisco.StartTime)</td><td>$($nextDisco.LastTime)</td><td class=""center""> $($nextDisco.Count)</td></tr>"
                $DiscoCounter++
                
                $var=@"
   	<div class="widget-box">
        <div class="widget-title bg_ly" >
          <h5>Disconnections:</h5>
        </div>
		<div class="widget-content" > 
		<tt>
		<table class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th>Counter</th>
                  <th>Connection lost</th>
                  <th>Connection resumed</th>
                  <th>Missed pings</th>
                </tr>
              </thead>
              <tbody>
                
                $DiscoTable

              </tbody>
            </table>
		</tt>
		</div>
		</div>

"@

            }
        }
        else
        {
            $var=@"

               	<div class="widget-box">
        <div class="widget-title bg_ly" >
          <h5>Disconnections:</h5>
        </div>
		<div class="widget-content" > 
		<tt>
		    No disconnection occur during test run.
		</tt>
		</div>
		</div>
"@
        }




return $var
}

function Generate-HTMLDetailedDisconnectionList()
{
    param([PingStatistic]$PingStatistics, $filteredInterfaceLogs, $filteredEventLogs)
    $DiscoPingLog=""
    $DiscoCounter=0

    foreach($nextDisco in $PingStatistics.ListOfDisconnections)
    {
        $DiscoPingLog+=@" 
            		<div class="widget-box">
        <div class="widget-title bg_ly" >
          <h5>Disconnection Nr: $DiscoCounter - $($nextDisco.StartTime) - $($nextDisco.LastTime)</h5>
        </div>
		<div class="widget-content" > 
		<hr /><center>IPv4 ping log:</center><hr />
		<tt>
		<pre>
$($nextDisco.LogMessage)
        </pre>
		</tt>
		<hr /><center>Interface log:</center><hr />
		<tt>
        <pre>    
        
$(
if($filteredInterfaceLogs[$DiscoCounter].InterfaceLog -eq $null)
{
    #$filteredInterfaceLogs.InterfaceLog
    #"$((($filteredInterfaceLogs.InterfaceLog).split("`n")).Trim())<br />" 
    ($filteredInterfaceLogs.InterfaceLog).split("`n")  | &{ process{"$($_.Trim())<br \>"}}
}
else
{
    #$filteredInterfaceLogs[$DiscoCounter].InterfaceLog
    #"$((($filteredInterfaceLogs[$DiscoCounter].InterfaceLog).split("`n")).Trim())<br />"
    ($filteredInterfaceLogs[$DiscoCounter].InterfaceLog).split("`n")| &{process{"$($_.Trim())<br \>"}}
}

)   
      
        </pre>
		</tt>
		<hr /><center>Event log:</center><hr />
		<tt>
<pre>
$(

if($filteredEventLogs[$DiscoCounter] -eq "<")
{
    #$filteredEventLogs
    #"$((($filteredEventLogs).split("`n")).Trim())<br />" 
    ($filteredEventLogs).split("`n")|&{process{"$($_.Trim())<br \>"}} 
}
else
{
    #$filteredEventLogs[$DiscoCounter]
    #"$((($filteredEventLogs[$DiscoCounter]).split("`n")).Trim())<br />"
    ($filteredEventLogs[$DiscoCounter]).split("`n")| &{process{"$($_.Trim())<br \>"}}
}

)
</pre></tt>
		</div>
		</div>
		            
"@
    $DiscoCounter++
    }
    return $DiscoPingLog
}

function Generate-HTMLErrorEventList()
{
    param($fullFilteredErrorEventLogs)
    $FullErrorLog=""
     $FullWlanAutoConfigError= $fullFilteredEventLogs.WlanAutoConfig | Where-Object {$_.EntryType -eq "Information"}
    if($FullWlanAutoConfigError.Count -gt 0)
    {
        

        foreach($nextWlanAutoConfigError in $FullWlanAutoConfigError)
        {
            $createTime=get-date $nextWlanAutoConfigError.TimeGenerated -Format "dd/MM/yyyy hh:mm:ss"
            $headerName=$nextWlanAutoConfigError.Message.Split(".")[0]
            $ID=$nextWlanAutoConfigError.InstanceId
            $message=$($nextWlanAutoConfigError | Format-List | Out-String )
        	$FullErrorLog+=@"
        <div class="widget-box">
        <div class="widget-title bg_ly" >
          <h5>$createTime - $ID - WLan Autoconfig - $headerName</h5>
        </div>
		<div class="widget-content" > 
		<tt>
		<pre>
$message
		</pre>
		</tt>
		</div>
		</div>
"@
        }






    }
    else
    {
                	$FullErrorLog+=@"
        <div class="widget-box">
        <div class="widget-title bg_ly" >
          <h5>No error recorded in WLan Autoconfig </h5>
        </div>
		<div class="widget-content" > 
		<tt>
		<pre>
Errors = 0
		</pre>
		</tt>
		</div>
		</div>
"@
        
    }


    $FullWlanOperationalError=$fullFilteredEventLogs.WlanOperational | Where-Object {$_.LevelDisplayName -eq "Error"}
      if($FullWlanOperationalError.Count -gt 0)
    {
        

        foreach($nextWlanOperationalError in $FullWlanOperationalError)
        {
            $createTime=get-date $nextWlanOperationalError.TimeCreated -Format "dd/MM/yyyy hh:mm:ss"
            $headerName=$nextWlanOperationalError.Message.Split("`n")[0]
            $ID=$nextWlanOperationalError.Id
            $message=$($nextWlanOperationalError | Format-List | Out-String)
        	$FullErrorLog+=@"
        <div class="widget-box">
        <div class="widget-title bg_ly" >
          <h5>$createTime - $ID - WLan Operational - $headerName</h5>
        </div>
		<div class="widget-content" > 
		<tt>
		<pre>
$($message | Out-String)
		</pre>
		</tt>
		</div>
		</div>
"@
        }






    }
    else
    {
                	$FullErrorLog+=@"
        <div class="widget-box">
        <div class="widget-title bg_ly" >
          <h5>No error recorded in WLan Operational </h5>
        </div>
		<div class="widget-content" > 
		<tt>
		<pre>
Errors = 0
		</pre>
		</tt>
		</div>
		</div>
"@
        
    }

    $FullWiredOperationalError=$fullFilteredEventLogs.WiredOperational | Where-Object {$_.LevelDisplayName -eq "Error"}
      if($FullWiredOperationalError.Count -gt 0)
    {
        

        foreach($nextWiredOperationalError in $FullWiredOperationalError)
        {
            $createTime=get-date $nextWiredOperationalError.TimeCreated -Format "dd/MM/yyyy hh:mm:ss"
            $headerName=$nextWiredOperationalError.Message.Split("`n")[0]
            $ID=$nextWiredOperationalError.Id
            $message=$($nextWiredOperationalError | Format-List | Out-String )
        	$FullErrorLog+=@"
        <div class="widget-box">
        <div class="widget-title bg_ly" >
          <h5>$createTime - $ID - Wired Operational - $headerName</h5>
        </div>
		<div class="widget-content" > 
		<tt>
		<pre>
$message
		</pre>
		</tt>
		</div>
		</div>
"@
        }






    }
    else
    {
                	$FullErrorLog+=@"
        <div class="widget-box">
        <div class="widget-title bg_ly" >
          <h5>No error recorded in Wired Operational </h5>
        </div>
		<div class="widget-content" > 
		<tt>
		<pre>
Errors = 0
		</pre>
		</tt>
		</div>
		</div>
"@
        
    }   

    $FullDHCPv6ClientError=$fullFilteredEventLogs.DHCPv6Client | Where-Object {$_.LevelDisplayName -eq "Error"}
    if($FullDHCPv6ClientError.Count -gt 0)
    {
        

        foreach($nextDHCP6Error in $FullDHCPv6ClientError)
        {
            $createTime=get-date $nextDHCP6Error.TimeCreated -Format "dd/MM/yyyy hh:mm:ss"
            $headerName=$nextDHCP6Error.Message.Split(".")[0]
            $ID=$nextDHCP6Error.Id
            $message=$($nextDHCP6Error | Format-List | Out-String)
        	$FullErrorLog+=@"
        <div class="widget-box">
        <div class="widget-title bg_ly" >
          <h5>$createTime - $ID - DHCP V6 - $headerName</h5>
        </div>
		<div class="widget-content" > 
		<tt>
		<pre>
$message
		</pre>
		</tt>
		</div>
		</div>
"@
        }






    }
    else
    {
                	$FullErrorLog+=@"
        <div class="widget-box">
        <div class="widget-title bg_ly" >
          <h5>No error recorded in DHCP V6 </h5>
        </div>
		<div class="widget-content" > 
		<tt>
		<pre>
Errors = 0
		</pre>
		</tt>
		</div>
		</div>
"@
        
    }   

    $FullDHCPClientError=$fullFilteredEventLogs.DHCPClient | Where-Object {$_.LevelDisplayName -eq "Error"}
    if($FullDHCPClientError.Count -gt 0)
    {
        

        foreach($nextDHCP4Error in $FullDHCPClientError)
        {
            $createTime=get-date $nextDHCP4Error.TimeCreated -Format "dd/MM/yyyy hh:mm:ss"
            $headerName=$nextDHCP4Error.Message.Split(".")[0]
            $ID=$nextDHCP4Error.Id
            $message=$($nextDHCP4Error | Format-List| Out-String)
        	$FullErrorLog+=@"
        <div class="widget-box">
        <div class="widget-title bg_ly" >
          <h5>$createTime - $ID - DHCP V4 - $headerName</h5>
        </div>
		<div class="widget-content" > 
		<tt>
		<pre>
$message
		</pre>
		</tt>
		</div>
		</div>
"@
        }






    }
    else
    {
                	$FullErrorLog+=@"
        <div class="widget-box">
        <div class="widget-title bg_ly" >
          <h5>No error recorded in DHCP V4 </h5>
        </div>
		<div class="widget-content" > 
		<tt>
		<pre>
Errors = 0
		</pre>
		</tt>
		</div>
		</div>
"@
        
    }   


    $FullDNSClientError=$fullFilteredEventLogs.DNSClient | Where-Object {$_.EntryType -eq "Error"}
    if($FullDNSClientError.Count -gt 0)
    {
        

        foreach($nextDNSError in $FullDNSClientError)
        {
            $createTime=get-date $nextDNSError.TimeGenerated -Format "dd/MM/yyyy hh:mm:ss"
            $headerName=$nextDNSError.Message.Split(".")[0]
            $ID=$nextDNSError.InstanceId
            $message=$($nextDNSError | Format-List | Out-String)
        	$FullErrorLog+=@"
        <div class="widget-box">
        <div class="widget-title bg_ly" >
          <h5>$createTime - $ID - DHCP V4 - $headerName</h5>
        </div>
		<div class="widget-content" > 
		<tt>
		<pre>
$message
		</pre>
		</tt>
		</div>
		</div>
"@
        }






    }
    else
    {
                	$FullErrorLog+=@"
        <div class="widget-box">
        <div class="widget-title bg_ly" >
          <h5>No error recorded in DNS </h5>
        </div>
		<div class="widget-content" > 
		<tt>
		<pre>
Errors = 0
		</pre>
		</tt>
		</div>
		</div>
"@
        
    }   

    return $FullErrorLog


}

function Generate-HTMLChartTarget()
{
    param($FileList, $StartCount)
    $var=""
    if($StartCount -eq $null)
    {
        $LoopCounter=0
    }
    else
    {
        $LoopCounter=$StartCount
    }
    foreach($nextFile in $FileList)
    {
    if($nextFile -like "*Iperf*")
    {
        $Title="Iperf speed statistic of"
    }
    else
    {
        $Title="Ping statistic of"
    }
$var+=@"
<div class="widget-box">
    <div class="widget-title bg_ly" >
        <h5>$Title $nextFile</h5>
    </div>
    <div class="widget-content" > 


        <div class="row-fluid">
            <div id="mygraph$LoopCounter" class="span9"></div>
        </div>

    </div>
</div>

"@
        $LoopCounter++
    }
    return $var
}

function Convert-V4PingLog2ChartData
{
    param($V4PingLogFile, $AvarageRespondTime, $ChartCounter, $dumpdir)
    
    $var=Get-Content "$dumpdir$V4PingLogFile"
    $varArray=$var.split("`n")
    $varArray=$varArray | Select-Object -Skip 2

    $pattern = "^([0-9]{2})\.([0-9]{2})\.([0-9]{4})-([0-9]{2}):([0-9]{2}):([0-9]{2})> Reply from [0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}: bytes=32 time[=<]([0-9]{1,4})ms TTL=[0-9]{1,3}$";
    $pattern2="^([0-9]{2})\.([0-9]{2})\.([0-9]{4})-([0-9]{2}):([0-9]{2}):([0-9]{2})> (Request timed out.|General failure.|Reply from [0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}: Destination host unreachable.)$"
    [int]$counter=0
    $ConvertedData=foreach($nextLine in $varArray)
    {
        if($nextLine -like "* time[=<]*")    
        {
           $nextLine -replace $pattern, "data$ChartCounter.addRow([new Date(`$3, `$2, `$1, `$4, `$5, `$6,  00), `$7, $AvarageRespondTime]); "
        }
        else
        {
            $nextLine -replace $pattern2, "data$ChartCounter.addRow([new Date(`$3, `$2, `$1, `$4, `$5, `$6,  00), 0, $AvarageRespondTime]); "
        }
        if($counter -eq 1000)
        {
            "`n"
            $counter=0
        }
        $counter++
    }
    return $ConvertedData
}


function Convert-V6PingLog2ChartData
{
    param($V6PingLogFile, $AvarageRespondTime, $ChartCounter, $dumpdir)
    
    $var=Get-Content "$dumpdir$V6PingLogFile"
    $varArray=$var.split("`n")
    $varArray=$varArray | Select-Object -Skip 2
    
    $pattern = "^([0-9]{2})\.([0-9]{2})\.([0-9]{4})-([0-9]{2}):([0-9]{2}):([0-9]{2})> Reply from .*: time[=<]([0-9]{1,4})ms\s{0,3}$";
    $pattern2="^([0-9]{2})\.([0-9]{2})\.([0-9]{4})-([0-9]{2}):([0-9]{2}):([0-9]{2})> (Request timed out.|General failure.|Reply from .*: Destination host unreachable.)$"
    [int]$counter=0

    $ConvertedData=foreach($nextLine in $varArray)
    {
    
        if($nextLine -like "* time[=<]*")    
        {
            $nextLine -replace $pattern, "data$ChartCounter.addRow([new Date(`$3, `$2, `$1, `$4, `$5, `$6,  00), `$7, $AvarageRespondTime]); "
        }
        else
        {
            $nextLine -replace $pattern2, "data$ChartCounter.addRow([new Date(`$3, `$2, `$1, `$4, `$5, `$6,  00), 0, $AvarageRespondTime]); "
        }
        if($counter -eq 1000)
        {
            "`n"
            $counter=0
        }
        $counter++
    }
    return $ConvertedData
}

function Convert-IperfLog2ChartData()
{
    param($IperfLogFile, $ChartCounter, $dumpdir, $TimePrefix)
    $var=Get-Content "$dumpdir$IperfLogFile"
    $varArray=$var.split("`n")
    $varArray=$varArray | Select-Object -Skip 3
 
$template2=@"
[{Counter*:  5}]   {Time:7}.00-{Time2:8}.00   sec  {Data:28.0 MBytes}   {Speed:235} Mbits/sec
[{Counter*:  5}]   {Time:7}.00-{Time2:8}.00   sec  {Data:27.2 MBytes}   {Speed:228} Mbits/sec 
[{Counter*:  5}]   {Time:7}.00-{Time2:8}.00   sec   {Data:144 KBytes}  {Speed:1.18} Mbits/sec  18  
[{Counter*:  5}]   {Time:7}.00-{Time2:8}.00   sec   {Data:128 KBytes}  {Speed:1.05} Mbits/sec  0.269 ms  0/16 (0%) 
"@
$CurrentTime=[datetime]::ParseExact($TimePrefix,'ddMMyyyy-HHmmss',$null)


$varArray | ConvertFrom-String -TemplateContent $template2 | &{process{
$temp=$($CurrentTime.AddSeconds($($_.Time2)) | get-date -Format "yyyy, MM, dd, HH, mm, ss")
"data$ChartCounter.addRow([new Date( $temp, 00), $($_.Speed)]); "}}

#$CurrentTime.AddHours(-$($_.Time)) | get-date -Format "yyyy, MM, dd, hh, mm, ss"
#data1.addRow([new Date(2017, 02, 05, 13, 10, 04,  00), 14, 12]);
   
    #$pattern = "^([0-9]{2})\.([0-9]{2})\.([0-9]{4})-([0-9]{2}):([0-9]{2}):([0-9]{2})> Reply from .*: time[=<]([0-9]{1,4})ms\s{0,3}$";
    #$pattern2="^([0-9]{2})\.([0-9]{2})\.([0-9]{4})-([0-9]{2}):([0-9]{2}):([0-9]{2})> (Request timed out.|General failure.|Reply from .*: Destination host unreachable.)$"
    #[int]$counter=0

    #$ConvertedData=foreach($nextLine in $varArray)
    #{
    
     #   if($nextLine -like "* time[=<]*")    
     #   {
     #       $nextLine -replace $pattern, "data$ChartCounter.addRow([new Date(`$3, `$2, `$1, `$4, `$5, `$6,  00), `$7, $AvarageRespondTime]); "
     #   }
      #  else
      #  {
      #      $nextLine -replace $pattern2, "data$ChartCounter.addRow([new Date(`$3, `$2, `$1, `$4, `$5, `$6,  00), 0, $AvarageRespondTime]); "
      #  }
      #  if($counter -eq 1000)
      #  {
      #      "`n"
      #      $counter=0
      #  }
       # $counter++
    #}
    #return $ConvertedData
}


function Generate-HTMLChartData()
{
    param($FileList, $AvarageResponseTime, $dumpdir)
    $var=""
    $LoopCounter=0
    foreach($nextFile in $FileList)
    {
        if($nextFile -like "*v6*")
        {
            $DataRow=Convert-V6PingLog2ChartData -V6PingLogFile $nextFile -AvarageRespondTime $AvarageResponseTime -ChartCounter $LoopCounter -dumpdir $dumpdir
        }
        else
        {
            $DataRow=Convert-V4PingLog2ChartData -V4PingLogFile $nextFile -AvarageRespondTime $AvarageResponseTime -ChartCounter $LoopCounter -dumpdir $dumpdir
        }
$var+=@"
var data$LoopCounter = new google.visualization.DataTable();
data$LoopCounter.addColumn('date', 'time');
data$LoopCounter.addColumn('number', 'Ping result');
data$LoopCounter.addColumn('number', 'Avarage');
$DataRow
var graph$LoopCounter = new links.Graph(document.getElementById('mygraph$LoopCounter'));

"@
        $LoopCounter++
    }
    return $var
}

function Generate-HTMLChartInitiator()
{
    param($FileList,$CounterStart)
    $var=""
        if($CounterStart -eq $null)
    {
        $LoopCounter=0
    }
    else
    {
        $LoopCounter=$CounterStart
    }
    
    foreach($nextFile in $FileList)
    {
$var+=@"
graph$LoopCounter.draw(data$LoopCounter, options);
"@
        $LoopCounter++
    }
    return $var
}


function Generate-HTMLIperfChartData()
{
    param($FileList, $dumpdir, $CounterStart, $TimePrefix)
    $var=""
    $LoopCounter=$CounterStart
    foreach($nextFile in $FileList)
    {
        $DataRow=Convert-IperfLog2ChartData -IperfLogFile $nextFile -ChartCounter $LoopCounter -dumpdir $dumpdir -TimePrefix $TimePrefix
  

$var+=@"
var data$LoopCounter = new google.visualization.DataTable();
data$LoopCounter.addColumn('date', 'time');
data$LoopCounter.addColumn('number', 'mbps');
$DataRow
var graph$LoopCounter = new links.Graph(document.getElementById('mygraph$LoopCounter'));

"@
        $LoopCounter++
    }
    return $var
}



function Generate-HTMLFileList()
{
    param($filelist)
    $HtmlList=""
    foreach($nextFile in $filelist)
    {
        $IconType=""
        if($nextFile.Extension -eq ".evtx")
        {
            $IconType="EventLogIcon"
        }
        elseif($nextFile.Extension -eq ".html")
        {
            $IconType="HtmlIcon"
        }
        elseif($nextFile.Extension -eq ".zip")
        {
            $IconType="WireSharkIcon"
        }
        elseif($nextFile.Extension -eq ".csv")
        {
            $IconType="CsvIcon"
        }
        else
        {
            $IconType="LogIcon"
        }
        
        $HtmlList+=@"
<li>
	<a href="$($nextFile.Name)">
	<div class="row-fluid " style="border: 1px solid #CDCDCD; padding: 10px; width: 95%; ">
	<span ><img class="$IconType" /></span>
	<span style="margin-left: 10px;">$($nextFile.Name)</span>
	</div>
	</a>
</li>
"@

    
    }
    $FullListDiv=@"
<div class="row-fluid">
	<ul style="list-style: none;">
		$HtmlList
	</ul>
</div>
"@

    return $FullListDiv

}

function Generate-ChannelTestSummary()
{
    param($filelist)
    foreach($nextFile in $filelist)
    {
        
    }
}


#-----------------------------------------------------------------------------HTML Report helper functions------------------------------------------------

#endregion
#region General informations
#-----------------------------------------------------------------------------General Informations--------------------------------------------------------
$FullHelp=@"
This scipt written to automate stability and load testes over wifi and lan connections.


    Script has 4 main functionallity:
        -PingStability
            -To start: Start-PingStability
            -To get help: get-help Start-PingStability


    Simple Commands:
        - Help-ATS_General   : Display this help message.
        

Note:
    - Iperf3, Wireshark and 7-zip must be installed.
    - Script must run as elevated to Adminstrator.
    - Script must run in PS console. ISE use debug mode and can cause issues.
    - Iperf3 must be used on both client and server side.

System requirement:
    - Window 10 
    - PowerShell 5.1 or newer

"@
$CurrentVersion=@"
Ver:    1.0 - 27/11/2016
    - Implement PingStability main function
    - Implement Version tracker
    - Implement Help
    - Implement Additional required software check
    - Implement general setting save, to speed up script load
    - Implement HTML report generator
    - Implement ping line chart for HTML report    
"@
$AllVersions=@"
Ver:    1.0 - 27/11/2016
    - Implement PingStability main function
    - Implement Version tracker
    - Implement Help
    - Implement Additional required software check
    - Implement general setting save, to speed up script load    
    - Implement HTML report generator
    - Implement ping line chart for HTML report
"@

#-----------------------------------------------------------------------------General Informations--------------------------------------------------------
#endregion
#region Classes
class NetworkState
{
	[string]$InterfaceIP
	[string]$State
	[string]$ESSID
	[string]$BSSID
	[string]$Authentication
	[string]$ReceiveRate
	[string]$TransmitRate
	[string]$Signal
	[string]$CurrentChannel
    [string]$LogHeader
    [string]$LogLine
    [string]$LogCsvHeader
    [string]$logCsvLine

	[void]CheckCurrentStatus($AdapterName)
    {
        #Get-NetAdapter for list of available interfaces
        #Get-NetAdapterHardwareInfo this one could be better, it show only phisical devices
        #Get-NetAdapterStatistics -name Wi-Fi  get information about transfered bytes
        #$bela | Format-Table -Property * -HideTableHeaders  - insert no row in table
        #$bela2 | Format-Table -Property * -insert header in emty file
        $this.InterfaceIP=""
	    $this.State=""
	    $this.ESSID=""
	    $this.BSSID=""
	    $this.Authentication=""
	    $this.ReceiveRate=""
	    $this.TransmitRate=""
	    $this.Signal=""
	    $this.CurrentChannel=""
        $this.LogHeader=""
        $this.LogLine=""
        $this.LogCsvHeader=""
        $this.logCsvLine=""

        #$Adapter=Get-NetAdapter | Where-Object {$_.PhysicalMediaType -like "*802.11"} 
        #if(-not ($Adapter.Status -eq "Up"))
        #{
        #    $Adapter=Get-NetAdapter |Where-Object {$_.Name -like "*Ethernet*" -and $_.PhysicalMediaType -eq "802.3"} 
        #}
        $ipconfig=$null
        
        $ipconfig=Get-NetIPAddress | Where-Object {$_.interfacealias -like $($AdapterName) -and $_.AddressFamily -eq "IPv4"}
        $Adapter=Get-NetAdapter | Where-Object {$_.Name -eq $AdapterName}

        $this.InterfaceIP=($ipconfig.IPv4Address).ToString()
        if($Adapter.PhysicalMediaType -like "*802.11")
        {
            $Interface=""
            $Interface=netsh wlan show interface
        
            $this.State=$Interface[7].Split(":")[1].Trim()
            if( "$($this.State)" -eq "connected" )
            {
                $this.ESSID=$Interface[8].Split(":")[1].Trim()
                $this.BSSID=$Interface[9].Substring($Interface[9].IndexOf(" : ")+3).Trim()
                $this.Authentication=$Interface[12].Split(":")[1].Trim()
                $this.ReceiveRate=$Interface[16].Split(":")[1].Trim()
                $this.TransmitRate=$Interface[17].Split(":")[1].Trim()
                $this.Signal=$Interface[18].Split(":")[1].Trim()
                $this.CurrentChannel=$Interface[15].Split(":")[1].Trim()
            }
        }
        
        $this.LogHeader="{0,-16} {1,-15} {2,-20} {3,-18} {4,-15} {5,-7} {6,-7} {7,-7} {8,-7}" -f "InterfaceIP", "State", "ESSID", "BSSID", "Authentication", "RxRate","TxRate", "Signal", "Channel" 
        $this.LogHeader+="`n"+"{0} {1} {2} {3} {4} {5} {6} {7} {8}" -f "".PadLeft(16,"-"), "".PadLeft(15,"-"), "".PadLeft(20,"-"), "".PadLeft(18,"-"), "".PadLeft(15,"-"), "".PadLeft(7,"-"), "".PadLeft(7,"-"), "".PadLeft(7,"-"), "".PadLeft(7,"-")
        $this.LogLine="{0,-16} {1,-15} {2,-20} {3,-18} {4,-15} {5,-7} {6,-7} {7,-7} {8,-7}" -f $this.InterfaceIP, $this.State, $this.ESSID, $this.BSSID, $this.Authentication, $this.ReceiveRate, $this.TransmitRate, $this.Signal, $this.CurrentChannel
        $this.LogCsvHeader="{0};{1};{2};{3};{4};{5};{6};{7};{8}" -f "InterfaceIP", "State", "ESSID", "BSSID", "Authentication", "RxRate","TxRate", "Signal", "Channel" 
        $this.logCsvLine="{0};{1};{2};{3};{4};{5};{6};{7};{8}" -f $this.InterfaceIP, $this.State, $this.ESSID, $this.BSSID, $this.Authentication, $this.ReceiveRate, $this.TransmitRate, $this.Signal, $this.CurrentChannel
    }
}

Class ATSSettings
{
    [string]FindApplication([string]$FileName, [string]$ErrorMessage)
    {
            $FullName=""
            $FileItem=Get-ChildItem -Path 'C:\Program Files' -Recurse | Where-Object {$_.Name -eq $FileName}
            $FullName=$FileItem.DirectoryName+"\"
            if ($FullName -eq "\" )
            {
                $FileItem=Get-ChildItem -Path 'C:\Program Files (x86)' -Recurse | Where-Object {$_.Name -eq $FileName}
                $FuokllName=$FileItem.DirectoryName+"\"
                if($FullName -eq "\" )
                {
                    Write-host $ErrorMessage -ForegroundColor Yellow
                    $FullName=""
                }
            }
            return $FullName
       
    }

    ATSSettings()
    {
        $ScriptLocation = split-path -Parent $PSCommandPath
        $this.WorkPath =$ScriptLocation
        
        #Check if script was already run before
        if(Test-Path "$ScriptLocation\Settings.xml")
        {
            #load saved settings to a temporary variable
            $temp = Import-Clixml "$ScriptLocation\Settings.xml"
            
            #copy values one-by-one to new variable
            $this.dumpcap =$temp.dumpcap
            $this.iperf =$temp.iperf
            $this.Archiver7z =$temp.Archiver7z
            #$this.Curl=$temp.Curl
            
            #Double check loaded references
                   #if($this.Curl -ne "")
                    #{
                    #    if(!(Test-Path $this.Curl))
                    #    {
                    #        $this.Curl=$this.FindApplication("curl.exe", "curl.exe can't be found.`nPlease download and copy to c:\program files\curl.`n`nhttp://www.paehl.com/open_source/downloads/curl_X64_ssl.7z`n")       
                    #    }
                    #}
                    #else
                    #{
                    #    $this.Curl=$this.FindApplication("curl.exe", "curl.exe can't be found.`nPlease download and copy to c:\program files\curl.`n`nhttp://www.paehl.com/open_source/downloads/curl_X64_ssl.7z`n")       
                    #}      

                    if($this.dumpcap -ne "")
                    {
                        if(!(Test-Path $this.dumpcap))
                        {
                            $this.dumpcap=$this.FindApplication("dumpcap.exe", "Dumpcamp.exe can't be found.`nPlease install Wireshark.")       
                        }
                    }
                    else
                    {
                        $this.dumpcap=$this.FindApplication("dumpcap.exe", "Dumpcamp.exe can't be found.`nPlease install Wireshark.")       
                    }      

                    if($this.iperf -ne "")
                    {
                        if(!(Test-Path $this.iperf))
                        {
                            $this.iperf=$this.FindApplication("iperf3.exe", "Iperf3.exe can't be found in Program Files folder.`nPlease install or copy Iperf3 in Program Files or subfolder of Program files.")       
                        }  
                    }
                    else
                    {
                        $this.iperf=$this.FindApplication("iperf3.exe", "Iperf3.exe can't be found in Program Files folder.`nPlease install or copy Iperf3 in Program Files or subfolder of Program files.")              
                    }

                    if($this.Archiver7z -ne "")
                    {
                        if(!(Test-Path $this.Archiver7z))
                        {
                            $this.Archiver7z=$this.FindApplication("7z.exe", "7z.exe can't be found in Program Files folder.`nPlease install 7z from 7-zip.org")       
                        }  
                    }
                    else
                    {
                        $this.Archiver7z=$this.FindApplication("7z.exe", "7z.exe can't be found in Program Files folder.`nPlease install 7z from 7-zip.org")       
                    }
        }
        else
        {
            #if it was not runngin before, than find required applicatioins.

            #find dumpcap.exe
            $this.dumpcap=$this.FindApplication("dumpcap.exe", "Dumpcamp.exe can't be found.`nPlease install Wireshark.")       
            

            #find iperf.exe
            $this.iperf=$this.FindApplication("iperf3.exe", "Iperf3.exe can't be found in Program Files folder.`nPlease install or copy Iperf3 in Program Files or subfolder of Program files.")       

            #find 7z.exe
            $this.Archiver7z=$this.FindApplication("7z.exe", "7z.exe can't be found in Program Files folder.`nPlease install 7z from 7-zip.org")       

            #find Curl
            #$this.Curl=$this.FindApplication("curl.exe", "curl.exe can't be found.`nPlease download and copy to c:\program files\curl.`n`nhttp://www.paehl.com/open_source/downloads/curl_X64_ssl.7z`n")       
        }

        #Save new settings
        $this | Export-Clixml "$ScriptLocation\Settings.xml"        
        
    }
    [string]$dumpcap
    [string]$iperf
    [string]$Archiver7z
    [string]$WorkPath
    #[string]$Curl

}
Class PingStatistic
{
    [void]ExtractStatisticFromFile([string]$FileName)
    {
    
    $this.SentPackages=0;
    $this.ReceivedPackages=0;
    $this.LostPackages=0;
    $this.MinResponseTime=3000;
    $this.MaxResponeTime=0;
    $this.sumForAvg=0;
    $this.ListOfDisconnections=@()
    $PreviousPackageLost=$false
    $lostPackageCounter=0
    $firstLostPackageTimeStamp=""
    $LastPackageLostTimeStamp=""
    $CurrentLostConnectionLog=""
       
        $lineCount=$(get-content($FileName) | Measure-Object).Count
        foreach($nextLine in Get-Content($FileName) |select -skip 2)
        {
            if($nextLine -like  "*time=*")
            {
                $this.TargetAddress=$($nextLine.Split(" "))[3].trimend(":")
                break
            }
        }             
        
        foreach($nextLine in Get-Content($FileName) |select -skip 2)
        {
            #----------------------------------------------------------------------------------------------------------Statistics---------------------------------------------------------------------------------------
            
           $this.SentPackages++
           if($nextLine -like "*time=*")
           {
            $this.ReceivedPackages++
            #$CurrentResponeTime=$nextLine.Substring($nextline.indexof("time=")+5,$nextline.IndexOf("ms TTL")-($nextline.indexof("time=")+5))
            $CurrentResponeTime=$nextLine.Substring($nextline.indexof("time=")+5,$nextline.IndexOf("ms")-($nextline.indexof("time=")+5))
            if($this.MinResponseTime -gt $CurrentResponeTime)
            {
                $this.MinResponseTime=$CurrentResponeTime            
            }
            if($this.MaxResponeTime -lt $CurrentResponeTime)
            {
                $this.MaxResponeTime=$CurrentResponeTime
            }
            $this.sumForAvg+=$CurrentResponeTime
            
           }
           else
           {
           $this.LostPackages++
           
           }
           #$tempOutput= "Sent:{0} Recieved:{1} Lost:{2} Min:{3} Max:{4} Sum{5}" -f $this.SentPackages, $this.ReceivedPackages, $this.LostPackages, $this.MinResponseTime, $this.MaxResponeTime, $this.sumForAvg
           $Percenatege=[int]($this.SentPackages / $lineCount *100)
           Write-Progress -Activity 'Processing ping log' -PercentComplete $Percenatege
           #Sleep 0.2
           #-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
           #-------------------------------------------------------------------------------------------------------------ConnectionLostList----------------------------------------------------------------------------
           
           if($nextLine -like "*time=*")
           {
                if($PreviousPackageLost -and $lostPackageCounter -gt 10)
                {
                    #save to list: start TimeStamp, Stop timestamp, Counter                    
                    #$this.ListOfDisconnections[$this.ListOfDisconnections.Count]=@{"StartTime"="$firstLostPackageTimeStamp"; "LastTime"="$LastPackageLostTimeStamp"; "Count"="$lostPackageCounter"; "LogMessage"="$CurrentLostConnectionLog"}
                    #$this.ListOfDisconnections[$this.ListOfDisconnections.Count]=@([pscustomobject]@{StartTime="$firstLostPackageTimeStamp"; LastTime="$LastPackageLostTimeStamp"; Count="$lostPackageCounter"; LogMessage="$CurrentLostConnectionLog"})
                    $this.ListOfDisconnections+=@([pscustomobject]@{StartTime="$firstLostPackageTimeStamp"; LastTime="$LastPackageLostTimeStamp"; Count="$lostPackageCounter"; LogMessage="$CurrentLostConnectionLog"})
                    
                }
                $CurrentLostConnectionLog=""
                $PreviousPackageLost=$false
                $lostPackageCounter=0
           }
           else
           {
                if(-not $PreviousPackageLost)
                {
                    $firstLostPackageTimeStamp=($nextLine.split(">"))[0]
                }
                $LastPackageLostTimeStamp=($nextLine.split(">"))[0]
                $lostPackageCounter++
                $PreviousPackageLost=$true
                $CurrentLostConnectionLog+="`n$nextLine"
           }
           #-------------------------------------------------------------------------------------------------------------ConnectionLostList----------------------------------------------------------------------------

        }
        if($PreviousPackageLost -and $lostPackageCounter -gt 10)
        {
            $this.ListOfDisconnections+=@([pscustomobject]@{StartTime="$firstLostPackageTimeStamp"; LastTime="$LastPackageLostTimeStamp"; Count="$lostPackageCounter"; LogMessage="$CurrentLostConnectionLog"})
        }
                
                
        Write-Progress -Activity 'Processing ping log' -PercentComplete 100
        $this.avarageResponseTime=$this.sumForAvg/$this.ReceivedPackages
        $this.LostPercentage=[int]($this.LostPackages / $lineCount *100)
        $this.Summary="Ping statistics for {0}:`nPackets: Sent = {1}, Received = {2}, Lost = {3} ({4}% loss),`nApproximate round trip times in milli-seconds:`nMinimum = {5}ms, Maximum = {6}ms, Average = {7}ms" -f $this.TargetAddress, $this.SentPackages, $this.ReceivedPackages, $this.LostPackages, $this.LostPercentage, $this.MinResponseTime, $this.MaxResponeTime, $this.avarageResponseTime
     
    }
    [string]$TargetAddress=""
    [int]$SentPackages=0;
    [int]$ReceivedPackages=0;
    [int]$LostPackages=0;
    [int]$LostPercentage=0;
    [int]$MinResponseTime=3000;
    [int]$MaxResponeTime=0;
    [int]$avarageResponseTime=0;
    [long]$sumForAvg=0;
    [string]$Summary="";
    [System.Object]$ListOfDisconnections=@{}
    
}
#endregion
#region BG functions
  $BGZipper={
        param($ZipperLocation, $File, $TargetArchive)
        Write-Verbose "BGZipper - next file to add to archive:$File"
        #"BGZipper - next file to add to archive:$File" >> "$TargetArchive.error.log"
        if(-not ($File -eq ""))
        {
            Write-Verbose "BGZipper -after if - next file to add to archive:$File"
            $cmd=$ZipperLocation+"7z.exe"
        
            & $cmd a "$TargetArchive.zip"  "$File"
            Remove-Item $File
        }
    }
    
    $BGPing={
        param($TargetAddress, $DumpDir, $PreFix, $TimeStamp, $ScriptLocation)
        Import-Module -name $ScriptLocation
        Clear-Host
        ping $TargetAddress -t | DateEcho | Tee-Object -FilePath "$DumpDir$PreFix.$TimeStamp.PingOutput.log"
    }
    $BGPingA={
        param($TargetAddress, $OutPutFile, $ScriptLocation, $IPv6=$false)
        Import-Module -name $ScriptLocation
        Clear-Host
        if($IPv6)
        {
            ping -6 $TargetAddress -t | DateEcho | Tee-Object -FilePath $OutPutFile
        }
        else
        {
            ping $TargetAddress -t | DateEcho | Tee-Object -FilePath $OutPutFile
        }
    }
    $BGPing6={
        param($TargetAddress, $DumpDir, $PreFix, $TimeStamp, $ScriptLocation)
        Import-Module -name $ScriptLocation
        Clear-Host
        ping -6 $TargetAddress -t | DateEcho | Tee-Object -FilePath "$DumpDir$PreFix.$TimeStamp.Ping6Output.log"
    }
    $BGCurl={
        param($TargetUrl, $CurlLocation, $SpeedLimit)
        Set-Location $CurlLocation
        do {
        cmd.exe /c ".\curl.exe $TargetUrl -o NUL --retry 0 --limit-rate $SpeedLimit --speed-limit 3000 --speed-time 2 --connect-timeout 10"
        }while($true)
        
    }
    $BGIperf={
        param($Location, $Server, $Port, $SpeedLimit, $UDPSwitch, $ModeSwitch, $OutPutFile, $ScriptLocation)
        Import-Module -name $ScriptLocation
        Clear-Host
        Set-Location $Location 
	   #cmd.exe /c ".\iperf3.exe -c $Server -p $Port -i 1 -b $SpeedLimit.0m -w 2.0m -f m -t 0 $UDPSwitch$ModeSwitch" 
        cmd.exe /c .\iperf3.exe -c $Server -p $Port -i 1 -b "$SpeedLimit.0m" -w 2.0m -f m -t 0 $UDPSwitch$ModeSwitch  --logfile $OutPutFile
    }
    $BGPackageCapture={
        param($dumpcap, $outputPath, $ZipperLocation, $ScriptLocation, $AdapterName)
        Import-Module -name $ScriptLocation
        Clear-Host
        Set-Location $dumpcap
        "asdfasdfa " >>$outputPath.$timeStamp.pcap.error
        do{
            $timeStamp= get-date -Format "yyyyMMddHHmmss"
            try{
                CMD /c .\dumpcap.exe -i $AdapterName -w "$outputPath.$timeStamp.pcap" -p -a duration:3600
                sleep 3
            }
            finally
            {
                $tmpvar1=Start-Job $BGZipper -ArgumentList $ZipperLocation, "$outputPath.$timeStamp.pcap" , $outputPath 
            }
            
        }while($true)

        
    }
    $BGCollectedLogInfo={
        param($ScriptLocation, $outpath, $refresTime, $WifiEnabled, $TimeStamp,$DumpDir, $PreFix, $AdapterName)
        Import-Module -name $ScriptLocation
        Clear-Host
        [NetworkState]$NetStat=[NetworkState]::new()

        do
        {
            $NetStat.CheckCurrentStatus($AdapterName)
            $Netstat.LogLine | DateEcho  | Tee-Object -FilePath "$DumpDir$PreFix.$TimeStamp.NetworkStatus.log" -Append
            $Netstat.logCsvLine | DateEcho  | Add-Content "$DumpDir$PreFix.$TimeStamp.NetworkStatus.csv"
            Sleep $refresTime
        }while($true)
        
    }

#endregion
function Test-IperfConnection()
{
    
    param( $jperf3Location, $Jperf3Server, $Jperf3Port, $Jperf3UDP, $Jperf3Mode, $SecondPort)
    $currentLocation=Get-Location
    Set-Location $jperf3Location
    #iperf3 -c $Jperf3Server -p 5003  -i 1 -b 20.0m -w 2.0m -f m -t 0
    #md.exe /c "curl $url -o NUL --retry 0 --limit-rate 150000 --speed-limit 3000 --speed-time 2 --connect-timeout 10"
    if($Jperf3UDP)
    {
        $output=$(cmd.exe /c "iperf3 -c $Jperf3Server -p $Jperf3Port  -i 1 -b 20.0m -w 2.0m -f m -t 2 ")
        if($Jperf3Mode -eq "Dual")
        {
            $Dualoutput=$(cmd.exe /c "iperf3 -c $Jperf3Server -p $SecondPort  -i 1 -b 20.0m -w 2.0m -f m -t 2 ")
        }
        
    }
    else
    {
        $output=$(cmd.exe /c "iperf3 -c $Jperf3Server -p $Jperf3Port  -i 1 -b 20.0m -w 2.0m -f m -t 2 -u")
        if($Jperf3Mode -eq "Dual")
        {
            $Dualoutput=$(cmd.exe /c "iperf3 -c $Jperf3Server -p $SecondPort  -i 1 -b 20.0m -w 2.0m -f m -t 2 -u")
        }
    }

    Set-Location $currentLocation
    if($output -like "*Connected to $Jperf3Server*")
    {
        if($Jperf3Mode -eq "Dual")
        {
            if($Dualoutput -like "*Connected to $Jperf3Server*")
            {
                return $true
            }
            else
            {
                return $false
            }
        }
        else
        {
            return $true
        }
    }
    else
    {
        return $false
    }
    
}

#region Start-PinstStabiltiy
Function Start-PingStability()
{
<#
.SYNOPSIS
   
.DESCRIPTION
   This funtcion designed to test network stability over wifi or cabled connection.
   Only one active connection must be available. 
    - Network capture will be saved and stored in compressed archive.
    - Ping log will be saved.
    
.PARAMETER <paramName>
   bela bela bela

.EXAMPLE
   <An example of using the script>
#>

    

    [CmdletBinding()]
    param(
        [Parameter(Mandatory=$false, HelpMessage="Please define target IP or address for continuos ping")]
        [string]$TargetAddress="8.8.8.8",
        [Parameter(Mandatory=$false, HelpMessage="Please define target IP or address for continuos IPv6 ping")]
        [string]$Target6Address="heanet.ie",
        [Parameter(Mandatory=$false, HelpMessage="Please define prefix for output log files.")]
        [string]$PreFix="PingStability",
        [Parameter(Mandatory=$true, HelpMessage="Please define target folder to save result.")]
        [string]$DumpDir,
        [Parameter(Mandatory=$false, HelpMessage="This switch enable logging of wifi interface state")]
        [switch]$WifiLog,
        [Parameter(Mandatory=$false, HelpMessage="Turn on for IPv6 ping")]
        [switch]$IPv6=$false,
        [Parameter(Mandatory=$false, HelpMessage="Turn on for Load test option")]
        [switch]$LoadTest=$false,
        [Parameter(Mandatory=$false, HelpMessage="Please define target jperf server")]
        [string]$Jperf3Server="",
        [Parameter(Mandatory=$false, HelpMessage="Please define port for target jperf server")]
        [int]$Jperf3Port=5001,
        [Parameter(Mandatory=$false, HelpMessage="Please define speed limit for test. Conisder carfully as all package will be captured.")]
        [int]$Jperf3SpeedLimit=2,
        [Parameter(Mandatory=$false, HelpMessage="This switch will enable to generate UDP traffic on load test")]
        [switch]$Jperf3UDP=$false,
        [ValidateSet('UpLoad','DownLoad','Dual')]
        [Parameter(Mandatory=$false, HelpMessage="Option to choose between up or down stream traffic or dual traffic.")]
        [string]$Jperf3Mode="DownLoad"
        
    )
    #Confirm loadtest details
      
    [int]$TargetIperf3Server2ndPort=5002
    if($LoadTest)
    {
        if($Jperf3Server -eq "")
        {
            Write-host "Please define IP address of target Iperf3 server:"
            $TargetIperf3Server=Read-Host    
        }
        if($Jperf3Mode -eq "Dual")
        {
            Write-Host "Dual mode require an additional port for second stream. `nPlease specify second port on Iperf3 server:"
            $TargetIperf3Server2ndPort=Read-Host
            
        }
        $isSettingsValid=Test-IperfConnection $MainSettings.iperf $Jperf3Server $Jperf3Port $Jperf3UDP $Jperf3Mode $TargetIperf3Server2ndPort
        
        if($isSettingsValid)
        {
            Write-host "Iperf3 settings are valid"
        }
        else
        {
            Write-host "Iperf3 settings are NOT valid"
            return
        }

        

    }

    $TimeStamp=Get-Date -format "ddMMyyyy-HHmmss"
    
    $PathNotValidated=$true
    do
    {
        if(Test-Path -pathtype Container $DumpDir)
        {
            $PathNotValidated=$false
            if($DumpDir.Substring($DumpDir.Length-1) -ne "\")
            {
                $DumpDir+="\"
            }
        }
        else
        {
            Write-host "$DumpDir is not valid or existing folder, please specify correct path for target to save logs:"
            $DumpDir=Read-Host
        }
    }while($PathNotValidated)

    #Check default network adapter
    $Adapter=Get-NetAdapter | Where-Object {$_.PhysicalMediaType -like "*802.11"} 
    if( -not ($Adapter.Status -eq "Up"))
    {
        $Adapter=Get-NetAdapter |Where-Object {$_.Name -like "*Ethernet*" -and $_.PhysicalMediaType -eq "802.3"} #| format-list -Property Name, PhysicalMediaType
    }
    $AdapterName=$Adapter.Name
    $DefaultGatewayIPv4= (Get-NetIPConfiguration -InterfaceAlias $AdapterName).IPv4DefaultGateway
    $DefaultGatewayIPv6= (Get-NetIPConfiguration -InterfaceAlias $AdapterName).IPv6DefaultGateway


    #start package capture
    $BGJobDumpCap=Start-Job $BGPackageCapture -ArgumentList "$($MainSettings.dumpcap)", "$DumpDir$PreFix.$TimeStamp.PackageCapture", "$($MainSettings.Archiver7z)", $PSCommandPath, $AdapterName
    
    #clear event log
    wevtutil cl Microsoft-Windows-WLAN-AutoConfig/Operational


    #start interface state capture
    $BGJobNetworkStateLog=Start-Job $BGCollectedLogInfo -ArgumentList $PSCommandPath, "$DumpDir$PreFix.$TimeStamp.NetworkState.log", 10, $true, $TimeStamp, $DumpDir, $PreFix, $AdapterName

    #$BGJobPing=Start-Job $BGPing -ArgumentList $TargetAddress, $DumpDir, $PreFix, $TimeStamp, $PSCommandPath
    $BGJobPing=Start-Job $BGPingA -ArgumentList $TargetAddress, "$DumpDir$PreFix.$TimeStamp.PingOutput.log", $PSCommandPath
    $BGJobPingGW=Start-Job $BGPingA -ArgumentList $DefaultGatewayIPv4.NextHop, "$DumpDir$PreFix.$TimeStamp.PingOutput.GWv4.log", $PSCommandPath
    #param($TargetAddress, $OutPutFile, $ScriptLocation, $IPv6=$false)
    
    $BGJobPing6=$null
    #if($IPv6){ $BGJobPing6=Start-Job $BGPing6 -ArgumentList $Target6Address, $DumpDir, $PreFix, $TimeStamp, $PSCommandPath}
    if($IPv6)
    { 
        $BGJobPing6=Start-Job $BGPingA -ArgumentList $Target6Address, "$DumpDir$PreFix.$TimeStamp.IPv6.PingOutput.log", $PSCommandPath, $true
        $BGJobPing6=Start-Job $BGPingA -ArgumentList $DefaultGatewayIPv6.NextHop, "$DumpDir$PreFix.$TimeStamp.IPv6.PingOutput.GWv6.log", $PSCommandPath, $true
    }
    
    #Start Iperf Sessions
    if($LoadTest)
    {
        if($Jperf3UDP -eq $true)
        {
            $UDPSwitch=" -u"
            $UdpExtension=".udp"
        }
        else
        {
            $UDPSwitch=""
            $UdpExtension=".tcp"
        }

        if($Jperf3Mode -eq "DownLoad")
        {
            #meg hivod -r kapcsoloval
            $BGJobIperf=Start-Job $BGIperf -ArgumentList "$($MainSettings.iperf)", $Jperf3Server, $Jperf3Port, $Jperf3SpeedLimit, $UDPSwitch, " -R", "$DumpDir$PreFix.$TimeStamp.Iperf.DS$($UdpExtension).log", $PSCommandPath
            
        }
        elseif($Jperf3Mode -eq "UpLoad")
        {
            #meg hivod siman
            $BGJobIperf=Start-Job $BGIperf -ArgumentList "$($MainSettings.iperf)", $Jperf3Server, $Jperf3Port, $Jperf3SpeedLimit, $UDPSwitch, "", "$DumpDir$PreFix.$TimeStamp.Iperf.US$($UdpExtension).log", $PSCommandPath
        }
        else
        {
            #meg hivod ketszer
            $BGJobIperf=Start-Job $BGIperf -ArgumentList "$($MainSettings.iperf)", $Jperf3Server, $Jperf3Port, $Jperf3SpeedLimit, $UDPSwitch, " -R", "$DumpDir$PreFix.$TimeStamp.Iperf.DS$($UdpExtension).log", $PSCommandPath
            $BGJobIperfDual=Start-Job $BGIperf -ArgumentList "$($MainSettings.iperf)", $Jperf3Server, $TargetIperf3Server2ndPort, $Jperf3SpeedLimit, $UDPSwitch, "", "$DumpDir$PreFix.$TimeStamp.Iperf.US$($UdpExtension).log", $PSCommandPath
        }
        
    }
    

    $PingtestStartTime=Get-Date
    
    try
    {
        do
        {
            $BGJobPing | Receive-Job
            $BGJobNetworkStateLog | Receive-Job
            #$BGJobDumpCap | Receive-Job
            if($IPv6) {$BGJobPing6 | Receive-Job}
            sleep 1
        }while($true)
    }
    finally
    {
        $PingtestStopTime=Get-Date
        $StartTimeStamp= Get-Date -UFormat %s
        
        Write-host "------------------------------------------------------------------"
        Write-host "Script will stop within 15 min, depend on length of logs."
        
        #kill all subshells
        Get-Job | Stop-Job
        Get-Job | Remove-Job
        Write-host "1/9: Package capture stopped successfully."
        
        $LastFileToZip =get-childitem $DumpDir -file *.pcap
        & $BGZipper "$($MainSettings.Archiver7z)" "$($LastFileToZip.FullName)" "$DumpDir$PreFix.$TimeStamp.PackageCapture" >$null 2>&1
        Write-host "2/9: Last pcap file has been added to archive file."
        
        netsh wlan show wlanreport >$null 2>&1
        Copy-Item "C:\ProgramData\Microsoft\Windows\WlanReport\wlan-report-latest.html" "$DumpDir$PreFix.$TimeStamp.WlanEventReport.html"
        Write-host "3/9: Wlan report has been generated."

        
        [PingStatistic]$CurrentPing=[PingStatistic]::new()
        $CurrentPing.ExtractStatisticFromFile("$DumpDir$PreFix.$TimeStamp.PingOutput.log")
        Write-verbose "Ping-Stability - Finally Block - Count of Disconnections:$($CurrentPing.ListOfDisconnections.Count)"       
        if( $IPv6)
        {
            [PingStatistic]$Current6Ping=[PingStatistic]::new()
            $Current6Ping.ExtractStatisticFromFile("$DumpDir$PreFix.$TimeStamp.IPv6.PingOutput.log")
        }
        Write-host "4/9: Ping statistic has been made"

        $filteredInterfaceLogs=Filter-InterfaceLog "$DumpDir$PreFix.$TimeStamp.NetworkStatus.log" $CurrentPing.ListOfDisconnections
        Write-host "5/9: Interface log has been filtered"
        
        
        $filteredEvetLogs=Filter-EventLog $CurrentPing.ListOfDisconnections "2"
        Write-host "6/9: Event logs has been filtered"
        
        $FullTimeStamp=(@{StartTime=$(get-date $PingtestStartTime -Format 'dd.MM.yyyy-HH:mm:ss');LastTime=$(Get-Date $PingtestStopTime -Format 'dd.MM.yyyy-HH:mm:ss')})
        $fullFilteredEventLogs=Filter-EventLog $FullTimeStamp
        Write-host "7/9: All event log has been collected"
        
        Save-EventLogs $PingtestStartTime $PingtestStopTime "$DumpDir$PreFix.$TimeStamp.log"
        Write-host "8/9: Event logs has been saved."
        $PingStabilityAllFileList=Get-ChildItem $DumpDir 
        $PingStabilityAllFileList= $PingStabilityAllFileList | Where-Object {$_.Name -like "*$TimeStamp*"}
        $PingLogs=Get-ChildItem -Path $Dumpdir -Name *$TimeStamp*PingOutput* 
        $ConvertedDiscoEvent=Get-DisconnectionsEvents $filteredEvetLogs

        $Ping4=Generate-HtmlPing -PingStatistics $CurrentPing
        if($IPv6)
        {
            $Ping6=Generate-HtmlPing -PingStatistics $Current6Ping
        }
        else
        {
            $Ping6=""
        }

        
        $discotable=Generate-HTMLDisconnectionTable -PingStatistics $CurrentPing
        $detailedDiscoList=Generate-HTMLDetailedDisconnectionList -PingStatistics $CurrentPing -filteredInterfaceLogs $filteredInterfaceLogs -filteredEventLogs $ConvertedDiscoEvent
        $errors=Generate-HTMLErrorEventList -fullFilteredErrorEventLogs $fullFilteredEventLogs
        
        $ChartTargets=Generate-HTMLChartTarget -FileList $PingLogs
        $DataLines=Generate-HTMLChartData -FileList $PingLogs -AvarageResponseTime $CurrentPing.avarageResponseTime -dumpdir $Dumpdir
        $GrapStarter=Generate-HTMLChartInitiator -FileList $PingLogs
        
        $files=Generate-HTMLFileList -filelist $PingStabilityAllFileList
        $headerTitle="Ping stability report for $PreFix - $TimeStamp"
        if($LoadTest)
        {
            $IperfLogs=Get-ChildItem -Path $Dumpdir -Name *$TimeStamp*Iperf*  
            $IperfTargets=Generate-HTMLChartTarget -FileList $IperfLogs -StartCount $PingLogs.Count
            $IperfDataLines=Generate-HTMLIperfChartData -FileList $IperfLogs -dumpdir $Dumpdir -CounterStart $PingLogs.Count -TimePrefix $TimeStamp
            $IperfGrapStarter=Generate-HTMLChartInitiator -FileList $IperfLogs -CounterStart $PingLogs.Count
            $ChartTargets+=$IperfTargets
            $DataLines+=$IperfDataLines
            $GrapStarter+=$IperfGrapStarter
        
        }

        Generate-HtmlReport -OutputFile "$DumpDir$PreFix.$TimeStamp.HtmlSummary.html" -IPv4Statistics $Ping4 -IPv6Statistics $Ping6 -DisconnectionTable $discotable -DisconnectionsDetails $detailedDiscoList -ErrorEvents $errors -ChartDivs $ChartTargets -FileList $files -DataRows $DataLines -GrapInitializer $GrapStarter -header $headerTitle
        Write-host "9/9: Portable report has been saved."
        
        
        
        
        $ClosingMessage="Script completed after {0:N0} seconds" -f $($(get-date -UFormat %s) - $StartTimeStamp)
        Write-host $ClosingMessage
        #########################################################################################################
        Show-PingSummary $CurrentPing $Current6Ping $IPv6 
        Show-Disconnections $CurrentPing
        Show-PingDisconnections $CurrentPing
        Show-InterfaceDisconnections $filteredInterfaceLogs
        Show-DisconnectionsEvents $filteredEvetLogs
        Show-ErrorEvents $fullFilteredEventLogs
        #########################################################################################################
    }

}
#endregion

#region Channel test
function Start-ChannelTest()
{
    [CmdletBinding()]
    param(
        [Parameter(Mandatory=$false, HelpMessage="Please define duration of iperf testes")]
        [int]$TestDuration=600,
        [Parameter(Mandatory=$false, HelpMessage="Please define the delay time, to make sure wifi network will be available.`n Default 60sec. set atleast 660 for 5G testing ")]
        [int]$Delay=60,
        [Parameter(Mandatory=$false, HelpMessage="Please define number of test iteration")]
        [int]$channelCounter=13,
        [Parameter(Mandatory=$true, HelpMessage="Please define target Iperf3 server")]
        [string]$Iperf3Server="",
        [Parameter(Mandatory=$false, HelpMessage="Please define target port for Iperf3 server")]
        [int]$Port=5001,
        [Parameter(Mandatory=$false, HelpMessage="Please define prefix for output log files.")]
        [string]$PreFix="ChannelLoadTest",
        [Parameter(Mandatory=$true, HelpMessage="Please define target folder to save result.")]
        [string]$DumpDir,
        [Parameter(Mandatory=$false, HelpMessage="This switch will enable to generate UDP traffic on load test")]
        [switch]$Jperf3UDP=$false,
        [Parameter(Mandatory=$false, HelpMessage="Please define speed for UDP test. Default 2m")]
        [string]$UDPSpeedLimit=2,
        [ValidateSet('UpLoad','DownLoad','Dual')]
        [Parameter(Mandatory=$false, HelpMessage="Option to choose between up or down stream traffic or dual traffic.")]
        [string]$Jperf3Mode="DownLoad",
        [Parameter(Mandatory=$false, HelpMessage="This option will print result for TestRail")]
        [switch]$TestRailFormat=$false
        
      
    )
    DynamicParam {
        $ParameterName="ProfileName"
        $RuntimeParameterDictionary = New-Object System.Management.Automation.RuntimeDefinedParameterDictionary
        $AttributeCollection = New-Object System.Collections.ObjectModel.Collection[System.Attribute]
        $ParameterAttribute = New-Object System.Management.Automation.ParameterAttribute
        $ParameterAttribute.Mandatory = $true
        $ParameterAttribute.Position = 1
        $AttributeCollection.Add($ParameterAttribute)
        $arrSet = Get-WifiProfiles
        $ValidateSetAttribute = New-Object System.Management.Automation.ValidateSetAttribute($arrSet)
        $AttributeCollection.Add($ValidateSetAttribute)
        $RuntimeParameter = New-Object System.Management.Automation.RuntimeDefinedParameter($ParameterName, [string], $AttributeCollection)
        $RuntimeParameterDictionary.Add($ParameterName, $RuntimeParameter)

        return $RuntimeParameterDictionary
    }
    begin{
        $ProfileName = $PsBoundParameters[$ParameterName]
     
    }
    process{
            $PathNotValidated=$true
            do
            {
                if(Test-Path -pathtype Container $DumpDir)
                {
                    $PathNotValidated=$false
                    if($DumpDir.Substring($DumpDir.Length-1) -ne "\")
                    {
                        $DumpDir+="\"
                    }
                }
                else
                {
                    Write-host "$DumpDir is not valid or existing folder, please specify correct path for target to save logs:"
                    $DumpDir=Read-Host
                }
            }while($PathNotValidated)

        $WaitingTime=$TestDuration+10
        if($Jperf3UDP -eq $true){$WaitingTime=$WaitingTime*2}
        if($Jperf3Mode -eq "Dual"){$WaitingTime=$WaitingTime*2}
        $WaitingTime=$WaitingTime+$Delay+120
    
        $ts =  [timespan]::fromseconds($WaitingTime*$channelCounter)
        $TotalTime="$($ts.Hours):$($ts.Minutes)"


        Write-Output "----------------------------------------------------------------------------------------------------------------"
        Write-Host "Full loop time is:" -NoNewline
        Write-Host $WaitingTime -ForegroundColor Yellow
        Write-Output "Complete planned test time:$TotalTime"
        Write-Output "Please start polling server with above defined FullLoopTime"
        Write-Output "Press [enter] to start test and start the polling script at the same time"
        Write-Output "Press [h + enter] for more help"
        Write-Output "----------------------------------------------------------------------------------------------------------------"
        $Key=Read-Host
        if($key -eq "h")
        {
            Write-Output "    - The script needs an Iperf3 server connected by cable TO CM."
            Write-Output "    - Use ats_wifitimer.sh on polling server to change channels"
            Write-Output "    - The polling server script and client script does not communicate with each other,"
            Write-Output "      this test based on timing, so make sure start the polling server script and this test at the same time"
            Write-Output ""
            Write-Output "press enter to start test"
            Write-Output "----------------------------------------------------------------------------------------------------------------"
        }
        

        #ping in the background to 8888
        #ping to gateway
        #include to graphs
        #log wifi state during graphs


        #need to check iperf3 server connectivity before test
        #Report:
            #ONly one page, remove tiles on top
            #Sort summary on top
            #include graphs
                #A summary graph for all channels, but separate by type, up down tcp up
            #Store scritp argument list ($myinvocation).Line
                    

        $UDPSpeedLimit="$($UDPSpeedLimit)m"
        $TimeStamp=Get-Date -format "ddMMyyyy-HHmmss"

        for($i=0;$i -lt $channelCounter; $i++)
        {
            $StartTime=get-date
            $LoopCounter="{0:D3}" -f $($i+1)
            
            DateEcho "Couneter:$LoopCounter - New loop started "
            sleep $Delay

            DateEcho "First connection attempt"    
            Connect-WiFi -ProfileName $ProfileName
            sleep 30
            $State= netsh wlan show interface | Where-Object {$_ -like "*State*:*"} | & {process{(($_.split(":"))[1]).trim()}}
            $networkName=netsh wlan show interface | Where-Object {$_ -like "* SSID*:*"} | & {process{(($_.split(":"))[1]).trim()}}
            if($State = "connected")
            {
                if($networkName -ne $ProfileName)
                {
                    DateEcho "Second connection attempt"    
                    Connect-WiFi -ProfileName $ProfileName
                }
            }
            else
            {
                DateEcho "Second connection attempt"    
                Connect-WiFi -ProfileName $ProfileName
            }

            
            sleep 30
            
            $State= netsh wlan show interface | Where-Object {$_ -like "*State*:*"} | & {process{(($_.split(":"))[1]).trim()}}
            $networkName=netsh wlan show interface | Where-Object {$_ -like "* SSID*:*"} | & {process{(($_.split(":"))[1]).trim()}}
            if($State = "connected")
            {
                if($networkName -ne $ProfileName)
                {
                    DateEcho "Test has been interrupted as no connection to CM"
                    return
                }
            }
            else
            {
                DateEcho "Test has been interrupted as no connection to CM"
                return
            }

            #cmd.exe /c "$($MainSettings.iperf)\iperf3.exe" -c $Server -p $Port -i 1 -b "$SpeedLimit.0m" -w 2.0m -f m -t 0 $UDPSwitch$ModeSwitch  --logfile $OutPutFile    
        
            if( $Jperf3Mode -ne "UpLoad"){DateEcho "Loop counter: $LoopCounter - TCP Down test started.";& "$($MainSettings.iperf)\iperf3.exe" -c $Iperf3Server -p $Port -i 1 -w 2.0m -f m -t $TestDuration -R --logfile $DumpDir$PreFix.$TimeStamp.$LoopCounter.Iperf.TCP.Down.log}
            if( $Jperf3Mode -ne "DownLoad"){DateEcho "Loop counter: $LoopCounter - TCP Up test started.";& "$($MainSettings.iperf)\iperf3.exe" -c $Iperf3Server -p $Port -i 1 -w 2.0m -f m -t $TestDuration --logfile $DumpDir$PreFix.$TimeStamp.$LoopCounter.Iperf.TCP.UP.log}
            if($Jperf3UDP -eq $true)
            {
                if( $Jperf3Mode -ne "UpLoad"){DateEcho "Loop counter: $LoopCounter - UDP Down test started.";& "$($MainSettings.iperf)\iperf3.exe" -c $Iperf3Server -p $Port -i 1 -w 2.0m -f m -t $TestDuration -R -u -b $UDPSpeedLimit --logfile $DumpDir$PreFix.$TimeStamp.$LoopCounter.Iperf.UDP.Down.log}
                if( $Jperf3Mode -ne "DownLoad"){DateEcho "Loop counter: $LoopCounter - UDP Up test started.";& "$($MainSettings.iperf)\iperf3.exe" -c $Iperf3Server -p $Port -i 1 -w 2.0m -f m -t $TestDuration -u -b $UDPSpeedLimit --logfile $DumpDir$PreFix.$TimeStamp.$LoopCounter.Iperf.UDP.UP.log}
            
            }
        
            $suspendTime=($StartTime.AddSeconds($WaitingTime)-$(get-date)).TotalMilliseconds
            DateEcho "Loop completed. $suspendTime ms left until next loop"
            sleep -Milliseconds $suspendTime 
        }
    
    #------------------------------------------------------Report------------------------------------------------------------------------------------------
    $iperflogFiles=Get-ChildItem -Path $DumpDir 
    $iperflogFiles =$iperflogFiles | Where-Object {$_.Name -like "*$TimeStamp*"}
    $IperfSummary=Get-IperfSummary $iperflogFiles -NrOfTestLoops $channelCounter $DumpDir
    $IperfSummary | Format-Table
    if($TestRailFormat)
    {
        show-IperfTestrailSummary $IperfSummary
    }
    #make summary
    $ChannelTestSummary=Generate-ChannelTestSummary $iperflogFiles 
    #make function that can print testrail friendly summary.
    #Check udp logs, possibly need to gain server side log in case of udp Down???
    #need to collect multiple values in case of udp test, speed how many frame has been lost
    #Log wifi state in background
    #Log ping to gateway and 8.8.8.8

    #make charts
        #$ChartTargets=Generate-HTMLChartTarget -FileList $PingLogs
        #$DataLines=Generate-HTMLChartData -FileList $PingLogs -AvarageResponseTime $CurrentPing.avarageResponseTime -dumpdir $Dumpdir
        #$GrapStarter=Generate-HTMLChartInitiator -FileList $PingLogs
    #include into html
        
    }
        
}

#endregion
#-----------------------------------------------------------------------------Entry point------------------------------------------------------------------

#Clear-Host
[ATSSettings]$MainSettings=[ATSSettings]::new() 
#write-host "done"
#help-ats
#-----------------------------------------------------------------------------Entry point------------------------------------------------------------------



